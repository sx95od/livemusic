
package com.sx95od.livemusic.API.LastFM.Models.TrackSearch;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Trackmatches implements Serializable
{

    @SerializedName("track")
    @Expose
    private List<Track> track = null;
    private final static long serialVersionUID = -6154775088526935012L;

    public List<Track> getTrack() {
        return track;
    }

    public void setTrack(List<Track> track) {
        this.track = track;
    }

}
