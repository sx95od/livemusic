
package com.sx95od.livemusic.API.Google.Models.TokenErrorModel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenErrorModel implements Serializable {
    @SerializedName("azp")
    @Expose
    private String azp;
    @SerializedName("aud")
    @Expose
    private String aud;
    @SerializedName("sub")
    @Expose
    private String sub;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("exp")
    @Expose
    private String exp;
    @SerializedName("expires_in")
    @Expose
    private String expiresIn;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("email_verified")
    @Expose
    private String emailVerified;
    @SerializedName("access_type")
    @Expose
    private String accessType;
    @SerializedName("error_description")
    @Expose
    private String errorDescription;

    private final static long serialVersionUID = 5652364583622564889L;

    public String getAzp() {
        return azp;
    }

    public void setAzp(String azp) {
        this.azp = azp;
    }

    public String getAud() {
        return aud;
    }

    public void setAud(String aud) {
        this.aud = aud;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

}
