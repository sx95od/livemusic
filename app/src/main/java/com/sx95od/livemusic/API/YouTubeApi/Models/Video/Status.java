
package com.sx95od.livemusic.API.YouTubeApi.Models.Video;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status implements Serializable
{

    @SerializedName("uploadStatus")
    @Expose
    private String uploadStatus;
    @SerializedName("privacyStatus")
    @Expose
    private String privacyStatus;
    @SerializedName("license")
    @Expose
    private String license;
    @SerializedName("embeddable")
    @Expose
    private Boolean embeddable;
    @SerializedName("publicStatsViewable")
    @Expose
    private Boolean publicStatsViewable;
    private final static long serialVersionUID = 6214150378411327972L;

    public String getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(String uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public String getPrivacyStatus() {
        return privacyStatus;
    }

    public void setPrivacyStatus(String privacyStatus) {
        this.privacyStatus = privacyStatus;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Boolean getEmbeddable() {
        return embeddable;
    }

    public void setEmbeddable(Boolean embeddable) {
        this.embeddable = embeddable;
    }

    public Boolean getPublicStatsViewable() {
        return publicStatsViewable;
    }

    public void setPublicStatsViewable(Boolean publicStatsViewable) {
        this.publicStatsViewable = publicStatsViewable;
    }

}
