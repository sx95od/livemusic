
package com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicDetails implements Serializable
{

    @SerializedName("relevantTopicIds")
    @Expose
    private List<String> relevantTopicIds = null;
    @SerializedName("topicCategories")
    @Expose
    private List<String> topicCategories = null;
    private final static long serialVersionUID = 374034022118236882L;

    public List<String> getRelevantTopicIds() {
        return relevantTopicIds;
    }

    public void setRelevantTopicIds(List<String> relevantTopicIds) {
        this.relevantTopicIds = relevantTopicIds;
    }

    public List<String> getTopicCategories() {
        return topicCategories;
    }

    public void setTopicCategories(List<String> topicCategories) {
        this.topicCategories = topicCategories;
    }

}
