
package com.sx95od.livemusic.API.YouTubeApi.Models.MyPlaylists;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentDetails implements Serializable
{

    @SerializedName("itemCount")
    @Expose
    private long itemCount;
    private final static long serialVersionUID = -1753236614048386716L;

    public long getItemCount() {
        return itemCount;
    }

    public void setItemCount(long itemCount) {
        this.itemCount = itemCount;
    }

}
