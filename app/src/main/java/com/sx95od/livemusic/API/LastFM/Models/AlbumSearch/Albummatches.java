
package com.sx95od.livemusic.API.LastFM.Models.AlbumSearch;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Albummatches implements Serializable
{

    @SerializedName("album")
    @Expose
    private List<Album> album = null;
    private final static long serialVersionUID = 3487262852349391810L;

    public List<Album> getAlbum() {
        return album;
    }

    public void setAlbum(List<Album> album) {
        this.album = album;
    }

}
