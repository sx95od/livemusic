
package com.sx95od.livemusic.API.LastFM.Models.ArtistSearch;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Artistmatches implements Serializable
{

    @SerializedName("artist")
    @Expose
    private List<Artist> artist = null;
    private final static long serialVersionUID = 1570479556998461634L;

    public List<Artist> getArtist() {
        return artist;
    }

    public void setArtist(List<Artist> artist) {
        this.artist = artist;
    }

}
