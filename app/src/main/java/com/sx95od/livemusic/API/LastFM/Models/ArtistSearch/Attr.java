
package com.sx95od.livemusic.API.LastFM.Models.ArtistSearch;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attr implements Serializable
{

    @SerializedName("for")
    @Expose
    private String _for;
    private final static long serialVersionUID = -406292073400452863L;

    public String getFor() {
        return _for;
    }

    public void setFor(String _for) {
        this._for = _for;
    }

}
