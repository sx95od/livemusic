
package com.sx95od.livemusic.API.LastFM.Models.ArtistSearch;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArtistSearchModel implements Serializable
{

    @SerializedName("results")
    @Expose
    private Results results;
    private final static long serialVersionUID = 7728808907843115662L;

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

}
