package com.sx95od.livemusic.API.YouTubeApi;

import com.sx95od.livemusic.API.YouTubeApi.Models.Channel.ChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.ChartVideosModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel.MyChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.MyPlaylists.MyPlaylistsModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.Video.YouTubeVideoModel;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by stoly on 31.08.2017.
 */

public interface YouTubeApi {
    public static final String SNIPPET = "snippet";
    public static final String CONTENT_DETAILS = "contentDetails";
    public static final String STATISTICS = "statistics";
    public static final String TOPIC_DETAILS = "topicDetails";
    public static final String BRANDING_SETTINGS = "brandingSettings";
//    public static final String CONTENT_DETAILS = "contentDetails";

    public static final String REGION_UA = "UA";
    public static final String REGION_US = "US";

    public static final String CHART = "mostPopular";

    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_PLAYLIST = "playlist";
    public static final String TYPE_CHANNEL = "channel";

    public static final String CATEGORY_MUSIC = "10";


    @GET("search")
    Observable<SearchByKeywordModel> searchVideo(@Query("q") String q,
                                                 @Query("maxResults") int maxResults,
                                                 @Query("part") String part,
                                                 @Query("key") String apiKey);

    @GET("search")
    Observable<SearchByKeywordModel> searchVideo(@Query("q") String q,
                                                 @Query("maxResults") int maxResults,
                                                 @Query("type") String type,
                                                 @Query("part") String part,
                                                 @Query("key") String apiKey);

    @GET("search")
    Observable<SearchByKeywordModel> searchVideo(@Query("q") String q,
                                                 @Query("maxResults") int maxResults,
                                                 @Query("type") String type,
                                                 @Query("videoCategoryId") String categoryID,
                                                 @Query("part") String part,
                                                 @Query("key") String apiKey);

    @GET("search")
    Observable<SearchByKeywordModel> searchVideo(@Query("q") String q,
                                                 @Query("maxResults") int maxResults,
                                                 @Query("type") String type,
                                                 @Query("videoCategoryId") String categoryID,
                                                 @Query("pageToken") String pageToken,
                                                 @Query("part") String part,
                                                 @Query("key") String apiKey);

    @GET("videos")
    Observable<ChartVideosModel> chartVideos(@Query("regionCode") String regionCode,
                                             @Query("maxResults") int maxResults,
                                             @Query("videoCategoryId") String categoryID,
                                             @Query("pageToken") String pageToken,
                                             @Query("chart") String chart,
                                             @Query("part") String part,
                                             @Query("key") String apiKey);

    @GET("videos")
    Observable<ChartVideosModel> chartVideos(@Query("regionCode") String regionCode,
                                             @Query("maxResults") int maxResults,
                                             @Query("videoCategoryId") String categoryID,
                                             @Query("chart") String chart,
                                             @Query("part") String part,
                                             @Query("key") String apiKey);

    @GET("videos")
    Observable<YouTubeVideoModel> getVideoByID(@Query("id") String id,
                                           @Query("part") String part,
                                           @Query("key") String apiKey);

    @GET("playlists")
    Observable<MyPlaylistsModel> getMyPlaylists(@Header("Authorization") String token,
                                                @Query("maxResults") int maxResults,
                                                @Query("mine") boolean mine,
                                                @Query("part") String part,
                                                @Query("key") String apiKey);

    @GET("channels")
    Observable<MyChannelModel> getMyChannel(@Header("Authorization") String token,
                                            @Query("mine") boolean mine,
                                            @Query("part") String part,
                                            @Query("key") String apiKey);

    @GET("channels")
    Observable<ChannelModel> getChannelByID(@Query("key") String apikey,
                                            @Query("id") String id,
                                            @Query("part") String part);
}
