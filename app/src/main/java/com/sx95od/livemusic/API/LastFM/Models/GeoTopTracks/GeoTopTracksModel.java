
package com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeoTopTracksModel implements Serializable
{

    @SerializedName("tracks")
    @Expose
    private Tracks tracks;
    private final static long serialVersionUID = -5840946431517329826L;

    public Tracks getTracks() {
        return tracks;
    }

    public void setTracks(Tracks tracks) {
        this.tracks = tracks;
    }

}
