
package com.sx95od.livemusic.API.LastFM.Models.AlbumSearch;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AlbumSearchModel implements Serializable
{

    @SerializedName("results")
    @Expose
    private Results results;
    private final static long serialVersionUID = 637263575675966298L;

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

}
