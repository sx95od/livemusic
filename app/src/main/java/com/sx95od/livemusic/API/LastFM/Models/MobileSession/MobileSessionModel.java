
package com.sx95od.livemusic.API.LastFM.Models.MobileSession;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileSessionModel implements Serializable
{

    @SerializedName("session")
    @Expose
    private Session session;
    private final static long serialVersionUID = 84101518928277669L;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

}
