package com.sx95od.livemusic.API.YoutubeHideApi;


import android.util.ArrayMap;

/**
 * Created by stoly on 02.09.2017.
 */

public class YouTubeHideAPIModel {


    ArrayMap<Integer, String> audioMap;


    public YouTubeHideAPIModel(ArrayMap<Integer, String> audioMap){
        this.audioMap = audioMap;
    }

    public ArrayMap<Integer, String> getAudioMap(){
        return audioMap;
    }

    public void setAudioMap(ArrayMap<Integer, String> audioMap) {
        this.audioMap = audioMap;
    }


}
