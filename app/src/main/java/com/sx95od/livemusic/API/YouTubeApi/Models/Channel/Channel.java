
package com.sx95od.livemusic.API.YouTubeApi.Models.Channel;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Channel implements Serializable
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("keywords")
    @Expose
    private String keywords;
    @SerializedName("defaultTab")
    @Expose
    private String defaultTab;
    @SerializedName("showRelatedChannels")
    @Expose
    private boolean showRelatedChannels;
    @SerializedName("showBrowseView")
    @Expose
    private boolean showBrowseView;
    @SerializedName("featuredChannelsTitle")
    @Expose
    private String featuredChannelsTitle;
    @SerializedName("featuredChannelsUrls")
    @Expose
    private List<String> featuredChannelsUrls = null;
    @SerializedName("unsubscribedTrailer")
    @Expose
    private String unsubscribedTrailer;
    @SerializedName("profileColor")
    @Expose
    private String profileColor;
    @SerializedName("country")
    @Expose
    private String country;
    private final static long serialVersionUID = 7255288040982860855L;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDefaultTab() {
        return defaultTab;
    }

    public void setDefaultTab(String defaultTab) {
        this.defaultTab = defaultTab;
    }

    public boolean isShowRelatedChannels() {
        return showRelatedChannels;
    }

    public void setShowRelatedChannels(boolean showRelatedChannels) {
        this.showRelatedChannels = showRelatedChannels;
    }

    public boolean isShowBrowseView() {
        return showBrowseView;
    }

    public void setShowBrowseView(boolean showBrowseView) {
        this.showBrowseView = showBrowseView;
    }

    public String getFeaturedChannelsTitle() {
        return featuredChannelsTitle;
    }

    public void setFeaturedChannelsTitle(String featuredChannelsTitle) {
        this.featuredChannelsTitle = featuredChannelsTitle;
    }

    public List<String> getFeaturedChannelsUrls() {
        return featuredChannelsUrls;
    }

    public void setFeaturedChannelsUrls(List<String> featuredChannelsUrls) {
        this.featuredChannelsUrls = featuredChannelsUrls;
    }

    public String getUnsubscribedTrailer() {
        return unsubscribedTrailer;
    }

    public void setUnsubscribedTrailer(String unsubscribedTrailer) {
        this.unsubscribedTrailer = unsubscribedTrailer;
    }

    public String getProfileColor() {
        return profileColor;
    }

    public void setProfileColor(String profileColor) {
        this.profileColor = profileColor;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
