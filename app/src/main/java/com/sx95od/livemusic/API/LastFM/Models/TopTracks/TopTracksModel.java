
package com.sx95od.livemusic.API.LastFM.Models.TopTracks;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopTracksModel implements Serializable
{

    @SerializedName("tracks")
    @Expose
    private Tracks tracks;
    private final static long serialVersionUID = 6911569568791676795L;

    public Tracks getTracks() {
        return tracks;
    }

    public void setTracks(Tracks tracks) {
        this.tracks = tracks;
    }

}
