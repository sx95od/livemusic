
package com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BrandingSettings implements Serializable
{

    @SerializedName("channel")
    @Expose
    private Channel channel;
    @SerializedName("image")
    @Expose
    private Image image;
    @SerializedName("hints")
    @Expose
    private List<Hint> hints = null;
    private final static long serialVersionUID = -2152074432739349343L;

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public List<Hint> getHints() {
        return hints;
    }

    public void setHints(List<Hint> hints) {
        this.hints = hints;
    }

}
