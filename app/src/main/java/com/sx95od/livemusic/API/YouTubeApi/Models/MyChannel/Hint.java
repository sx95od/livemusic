
package com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hint implements Serializable
{

    @SerializedName("property")
    @Expose
    private String property;
    @SerializedName("value")
    @Expose
    private String value;
    private final static long serialVersionUID = -3050363691304613919L;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
