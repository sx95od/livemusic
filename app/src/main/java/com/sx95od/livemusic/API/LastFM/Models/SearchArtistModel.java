package com.sx95od.livemusic.API.LastFM.Models;

import java.io.Serializable;

/**
 * Created by stoly on 30.08.2017.
 */

public class SearchArtistModel implements Serializable {
    String name;
    String listeners;
    String mbid;
    String url;
    String streamable;
    String smallImg;
    String mediumImg;
    String largeImg;
    String extraLargeImg;

    public SearchArtistModel(String name, String listeners, String mbid, String url, String streamable,
                             String smallImg, String mediumImg, String largeImg, String extraLargeImg){
        this.name = name;
        this.listeners = listeners;
        this.mbid = mbid;
        this.url = url;
        this.streamable = streamable;
        this.smallImg = smallImg;
        this.mediumImg = mediumImg;
        this.largeImg = largeImg;
        this.extraLargeImg = extraLargeImg;
    }

    public String getName(){
        return name;
    }

    public String getListeners(){
        return listeners;
    }

    public String getMbid(){
        return mbid;
    }

    public String getUrl(){
        return url;
    }

    public String getStreamable(){
        return streamable;
    }

    public String getSmallImg(){
        return smallImg;
    }

    public String getMediumImg(){
        return mediumImg;
    }

    public String getLargeImg(){
        return largeImg;
    }

    public String getExtraLargeImg(){
        return extraLargeImg;
    }
}
