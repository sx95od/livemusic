package com.sx95od.livemusic.API.YoutubeHideApi;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by stoly on 30.08.2017.
 */

public interface YoutubeHideAPI {
    @GET("Yt?")
    Call<ResponseBody> retrieve(@Query("id") String id);
}
