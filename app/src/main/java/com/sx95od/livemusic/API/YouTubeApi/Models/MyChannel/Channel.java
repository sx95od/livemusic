
package com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Channel implements Serializable
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("showRelatedChannels")
    @Expose
    private boolean showRelatedChannels;
    @SerializedName("profileColor")
    @Expose
    private String profileColor;
    private final static long serialVersionUID = 5896190019392049082L;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isShowRelatedChannels() {
        return showRelatedChannels;
    }

    public void setShowRelatedChannels(boolean showRelatedChannels) {
        this.showRelatedChannels = showRelatedChannels;
    }

    public String getProfileColor() {
        return profileColor;
    }

    public void setProfileColor(String profileColor) {
        this.profileColor = profileColor;
    }

}
