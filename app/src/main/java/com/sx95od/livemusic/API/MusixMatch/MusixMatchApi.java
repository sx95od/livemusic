package com.sx95od.livemusic.API.MusixMatch;

import com.sx95od.livemusic.Core.Core;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by stoly on 23.08.2017.
 */

public interface MusixMatchApi {
    final int PAGE_SIZE = 100;

    @GET("track.search")
    Call<ResponseBody> getData(@Query("q_artist") String artist, @Query("page_size") int pageSize, @Query("q_track") String track, @Query("apikey") String apikey);
}
