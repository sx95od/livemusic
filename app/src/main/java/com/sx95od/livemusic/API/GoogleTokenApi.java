package com.sx95od.livemusic.API;

import com.sx95od.livemusic.API.Google.Models.TokenErrorModel.TokenErrorModel;

import io.reactivex.Observable;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by stoly on 08.11.2017.
 */

public interface GoogleTokenApi {

    @GET("tokeninfo")
    Call<ResponseBody> getTokenInfo(@Query("access_token") String accessToken);
}
