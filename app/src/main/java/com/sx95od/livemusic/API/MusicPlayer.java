package com.sx95od.livemusic.API;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;

import com.h6ah4i.android.media.IBasicMediaPlayer;
import com.h6ah4i.android.media.IMediaPlayerFactory;
import com.h6ah4i.android.media.opensl.OpenSLMediaPlayer;
import com.h6ah4i.android.media.opensl.OpenSLMediaPlayerContext;
import com.h6ah4i.android.media.opensl.OpenSLMediaPlayerFactory;
import com.h6ah4i.android.media.standard.StandardMediaPlayerFactory;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Events.PlayerEvent;
import com.sx95od.livemusic.Services.AudioService.Listeners.MusicPlayerListener;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by stoly on 16.06.2017.
 */

public class MusicPlayer implements IBasicMediaPlayer.OnCompletionListener, IBasicMediaPlayer.OnErrorListener, IBasicMediaPlayer.OnPreparedListener{
    IMediaPlayerFactory mediaPlayerFactory;
    IBasicMediaPlayer mediaPlayer;
    boolean canPlay = false;
    boolean prepared = false;
    private Timer durationTimer;
    private UpdateDurationTask updateDurationTask;
    private Handler handler = new Handler();
    private MusicPlayerListener musicPlayerListener;

    @Override
    public void onCompletion(IBasicMediaPlayer mp) {
//        EventBus.getDefault().post(new PlayerEvent(PlayerEvent.COMPLETE));
        musicPlayerListener.onComplete();
    }

    @Override
    public boolean onError(IBasicMediaPlayer mp, int what, int extra) {
//        EventBus.getDefault().post(new PlayerEvent(PlayerEvent.ERROR));
        return true;
    }

    @Override
    public void onPrepared(IBasicMediaPlayer mp) {
        prepared = true;
//        EventBus.getDefault().post(new PlayerEvent(PlayerEvent.PREPARED));
        if (canPlay) {
            play();
//            EventBus.getDefault().post(new PlayerEvent(PlayerEvent.PLAY));
        }
    }

    public void setVolume(float volume){
        mediaPlayer.setVolume(volume, volume);
    }

    public MusicPlayer(MusicPlayerListener musicPlayerListener){
        this.musicPlayerListener = musicPlayerListener;
        mediaPlayerFactory = new StandardMediaPlayerFactory(Core.getCoreInstance());
        mediaPlayer = mediaPlayerFactory.createMediaPlayer();
        mediaPlayer.setWakeMode(Core.getCoreInstance(),
                PowerManager.PARTIAL_WAKE_LOCK);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnErrorListener(this);
        durationTimer = new Timer();
        updateDurationTask = new UpdateDurationTask();
    }

    public void play(){
        if (prepared) {
            if (canPlay) {
                mediaPlayer.start();
                durationTimer.cancel();
                updateDurationTask.cancel();
                startTimer();
//                EventBus.getDefault().post(new PlayerEvent(PlayerEvent.PLAY));
            }
        }
    }

    public void reset(){
        mediaPlayer.reset();
    }

    public int getDuration() throws Exception{
        if (prepared) {
            return mediaPlayer.getDuration();
        } else return 0;

    }

    public int getCurrentPosition(){
        if (prepared) {
            return mediaPlayer.getCurrentPosition();
        } else return 0;
    }

    public void setUri(String uri) throws Exception{
        mediaPlayer.setDataSource(Core.getCoreInstance(), Uri.parse(uri));
//        mediaPlayer.setDataSource("http://online-kissfm.tavrmedia.ua/KissFM");
        prepared = false;
        mediaPlayer.prepareAsync();
    }

    public void stop(){
        mediaPlayer.stop();
//        EventBus.getDefault().post(new PlayerEvent(PlayerEvent.PAUSE));
    }

    public void pause(){
        if (prepared) {
            if (durationTimer!=null) {
                durationTimer.cancel();
                updateDurationTask.cancel();
            }
            mediaPlayer.pause();
//            EventBus.getDefault().post(new PlayerEvent(PlayerEvent.PAUSE));
        }
    }

    public void seekTo(int seek){
        if (prepared) {
            mediaPlayer.seekTo(seek);
        }
    }

    public void forceStopTimer(){
        if (durationTimer!=null) {
            durationTimer.cancel();
            updateDurationTask.cancel();
        }
    }

    public void forceStartTimer(){
        if (mediaPlayer.isPlaying()) {
            startTimer();
        }
    }

    public void release(){
        mediaPlayer.release();
    }

    public void setCanPlay(boolean canPlay){
        this.canPlay = canPlay;
    }

    public void startTimer(){
        durationTimer = new Timer();
        updateDurationTask = new UpdateDurationTask();
        durationTimer.schedule(updateDurationTask, 0, 250);
    }


    class UpdateDurationTask extends TimerTask {
        @Override
        public void run() {
            handler.post(() -> {
//                Log.d("HandlerTest", "Tick");
//                EventBus.getDefault().post(new PlayerEvent(PlayerEvent.UPDATE_POSITION_OF_DURATION, mediaPlayer.getCurrentPosition()));
                musicPlayerListener.onUpdateDuration(mediaPlayer.getCurrentPosition());
            });

        }
    }

}
