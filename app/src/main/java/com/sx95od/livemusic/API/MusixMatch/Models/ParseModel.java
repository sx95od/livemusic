package com.sx95od.livemusic.API.MusixMatch.Models;

import android.util.Log;

import com.sx95od.livemusic.Adapters.AdapterModels.MusixMatchModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stoly on 23.08.2017.
 */

public class ParseModel {
    public static ArrayList<MusixMatchModel> getSearchModel(String body){
        ArrayList<MusixMatchModel> objects = new ArrayList<MusixMatchModel>();
        try {
            JSONObject jsonObjectGlobal = new JSONObject(body);
            JSONObject jsonObjectMessage = jsonObjectGlobal.getJSONObject("message");
            JSONObject jsonObjectBody = jsonObjectMessage.getJSONObject("body");
            JSONArray jsonArrayTrackList = jsonObjectBody.getJSONArray("track_list");

            for (int i=0; i<jsonArrayTrackList.length(); i++){
                JSONObject jsonObject = jsonArrayTrackList.getJSONObject(i);
                JSONObject jsonObjectTrack = jsonObject.getJSONObject("track");
                long trackId = jsonObjectTrack.getLong("track_id");
                String trackName = jsonObjectTrack.getString("track_name");
                int hasLyrics = jsonObjectTrack.getInt("has_lyrics");
                long lyricsId = jsonObjectTrack.getLong("lyrics_id");
                long albumId = jsonObjectTrack.getLong("album_id");
                String albumName = jsonObjectTrack.getString("album_name");
                long artistId = jsonObjectTrack.getLong("artist_id");
                String artistName = jsonObjectTrack.getString("artist_name");
                String cover100 = jsonObjectTrack.getString("album_coverart_100x100");
                Log.i("ParseModel", cover100);
                objects.add(new MusixMatchModel(trackId, trackName, hasLyrics,lyricsId,
                        albumId, albumName, artistId, artistName, cover100));
            }
        }catch (Exception e){
            Log.e(ParseModel.class.getClass().getSimpleName(), e.toString());
            return null;
        }finally {
            return objects;
        }

    }
}
