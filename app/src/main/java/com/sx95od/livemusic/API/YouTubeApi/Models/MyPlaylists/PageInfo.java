
package com.sx95od.livemusic.API.YouTubeApi.Models.MyPlaylists;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PageInfo implements Serializable
{

    @SerializedName("totalResults")
    @Expose
    private long totalResults;
    @SerializedName("resultsPerPage")
    @Expose
    private long resultsPerPage;
    private final static long serialVersionUID = 7500921063824686387L;

    public long getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(long totalResults) {
        this.totalResults = totalResults;
    }

    public long getResultsPerPage() {
        return resultsPerPage;
    }

    public void setResultsPerPage(long resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

}
