package com.sx95od.livemusic.API;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.DownloadFormats;
import com.sx95od.livemusic.Events.FragmentEvent;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 03.09.2017.
 */

public class FFMPEGConverter {
    private static final String tag = FFMPEGConverter.class.getSimpleName();

    public static final void opus2mp3(final int pos, final long id, final String fileName, final String uri, final String formatIn, final String formatOut,
                                      String title, String artist, String album, String year, String art){
            String[] cmd = new String[15];
        cmd[0] = "-i";
        cmd[1] = uri+fileName+formatIn;
//        cmd[2] = "-i";
//        cmd[3] = art;
//        cmd[4] = "-c";
//        cmd[5] = "copy";
//        cmd[6] = "-map";
//        cmd[7] = "0";
//        cmd[8] = "-map";
//        cmd[9] = "1";
//        cmd[10] = "-metadata:s:v";
//        cmd[11] = "title=Album Cover";
//        cmd[12] = "-metadata:s:v";
//        cmd[13] = "comment=Cover (Front)";
        cmd[2] = "-acodec";
        cmd[3] = "libmp3lame";
        cmd[4] = "-b:a";
        cmd[5] = "320k";
        cmd[6] = "-metadata";
        cmd[7] = "title="+title;
        cmd[8] = "-metadata";
        cmd[9] = "artist="+artist;
        cmd[10] = "-metadata";
        cmd[11] = "album="+album;
        cmd[12] = "-metadata";
        cmd[13] = "year="+year;
        cmd[14] = uri+fileName+formatOut;
        try {
            FFmpeg fFmpeg = FFmpeg.getInstance(Core.getCoreInstance());
            fFmpeg.execute(cmd, new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.d(tag, "<OnSuccess> "+message);
                    File file = new File(uri+fileName+formatIn);
                    file.delete();
                    file = null;

                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    intent.setData(Uri.fromFile(new File(uri+fileName+formatOut)));
                    Core.getCoreInstance().sendBroadcast(intent);

                    Realm realm = Realm.getInstance(Core.getHideConfigRealm());
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<DownloadFormats> results = realm.where(DownloadFormats.class)
                                    .equalTo("id", id).findAll();
                            results.deleteAllFromRealm();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_DOWNLOADS_REMOVE_ITEM, pos));
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {

                        }
                    });
                }

                @Override
                public void onProgress(String message) {
                    Log.d(tag, "<OnProgress> "+message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e(tag, "<OnFailure> "+message);
                }

                @Override
                public void onStart() {
                    Log.d(tag, "<OnStart>");
                }

                @Override
                public void onFinish() {
                    Log.d(tag, "<OnFinish>");

                }
            });
        }catch (Exception e){
            Log.e(tag, e.toString());
        }
    }
}
