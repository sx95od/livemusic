package com.sx95od.livemusic.API.LastFM.Parsers;

import android.database.Observable;
import android.util.Log;

import com.sx95od.livemusic.API.LastFM.Models.ChartTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.SearchAlbumModel;
import com.sx95od.livemusic.API.LastFM.Models.SearchArtistModel;
import com.sx95od.livemusic.API.LastFM.Models.SearchTrackModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 29.08.2017.
 */

public class LastFMResponseParser {
     static final String tag = "LastFMResponseParser";


    public static final Observable<List<SearchTrackModel>> getTrackList(String data){
        List<SearchTrackModel> objects = new ArrayList<SearchTrackModel>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject jsonObjectResults = jsonObject.getJSONObject("results");
            JSONObject jsonObjectMatches = jsonObjectResults.getJSONObject("trackmatches");
            JSONArray jsonArrayTrack = jsonObjectMatches.getJSONArray("track");
            for (int i=0; i<jsonArrayTrack.length(); i++){
                String name = jsonArrayTrack.getJSONObject(i).getString("name");
                String artist = jsonArrayTrack.getJSONObject(i).getString("artist");
                String url = jsonArrayTrack.getJSONObject(i).getString("url");
                String streamable = jsonArrayTrack.getJSONObject(i).getString("streamable");
                String listeners = jsonArrayTrack.getJSONObject(i).getString("listeners");
                String sImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(0).getString("#text");
                String mImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(1).getString("#text");
                String lImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(2).getString("#text");
                String eImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(3).getString("#text");
                String mbid = jsonArrayTrack.getJSONObject(i).getString("mbid");
                objects.add(new SearchTrackModel(name, artist, url, streamable, Integer.valueOf(listeners), sImg, mImg, lImg, eImg, mbid));
            }
            return (Observable<List<SearchTrackModel>>) objects;
        } catch (Exception e){
            Log.e(LastFMResponseParser.class.getSimpleName(), e.toString());
            return null;
        }
    }

    public static final ArrayList<SearchArtistModel> getArtistList(String data){
        Log.d("getArtistList", data);
        ArrayList<SearchArtistModel> objects = new ArrayList<SearchArtistModel>();
        try{
            JSONObject jsonObject = new JSONObject(data);
            JSONObject jsonObjectResults = jsonObject.getJSONObject("results");
            JSONObject jsonObjectMatches = jsonObjectResults.getJSONObject("artistmatches");
            JSONArray jsonArrayTrack = jsonObjectMatches.getJSONArray("artist");
            for (int i=0; i<jsonArrayTrack.length(); i++) {
                String name = jsonArrayTrack.getJSONObject(i).getString("name");
                String url = jsonArrayTrack.getJSONObject(i).getString("url");
                String streamable = jsonArrayTrack.getJSONObject(i).getString("streamable");
                String listeners = jsonArrayTrack.getJSONObject(i).getString("listeners");
                String sImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(0).getString("#text");
                String mImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(1).getString("#text");
                String lImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(2).getString("#text");
                String eImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(3).getString("#text");
                String mbid = jsonArrayTrack.getJSONObject(i).getString("mbid");
                objects.add(new SearchArtistModel(name, listeners, mbid, url, streamable, sImg, mImg, lImg, eImg));
            }
            return objects;
        }catch (Exception e){
            Log.e(LastFMResponseParser.class.getSimpleName(), e.toString());
            return objects;
        }
    }

    public static final ArrayList<SearchAlbumModel> getAlbumList(String data){
        Log.d("getArtistList", data);
        ArrayList<SearchAlbumModel> objects = new ArrayList<SearchAlbumModel>();
        try{
            JSONObject jsonObject = new JSONObject(data);
            JSONObject jsonObjectResults = jsonObject.getJSONObject("results");
            JSONObject jsonObjectMatches = jsonObjectResults.getJSONObject("albummatches");
            JSONArray jsonArrayTrack = jsonObjectMatches.getJSONArray("album");
            for (int i=0; i<jsonArrayTrack.length(); i++) {
                String name = jsonArrayTrack.getJSONObject(i).getString("name");
                String artist = jsonArrayTrack.getJSONObject(i).getString("artist");
                String url = jsonArrayTrack.getJSONObject(i).getString("url");
                String streamable = jsonArrayTrack.getJSONObject(i).getString("streamable");
//                String listeners = jsonArrayTrack.getJSONObject(i).getString("listeners");
                String sImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(0).getString("#text");
                String mImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(1).getString("#text");
                String lImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(2).getString("#text");
                String eImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(3).getString("#text");
                String mbid = jsonArrayTrack.getJSONObject(i).getString("mbid");
                objects.add(new SearchAlbumModel(name, artist, mbid, url, streamable, sImg, mImg, lImg, eImg));
            }
            return objects;
        }catch (Exception e){
            Log.e(LastFMResponseParser.class.getSimpleName(), e.toString());
            return objects;
        }
    }

    public static final int getResultCount(String data){
        int count = 0;
        try{
            JSONObject jsonObject = new JSONObject(data);
            JSONObject jsonObjectResults = jsonObject.getJSONObject("results");
            Log.d("SUKA", jsonObjectResults.toString());
            count = Integer.valueOf(jsonObjectResults.getString("opensearch:totalResults"));
            return count;
        }catch (Exception e){
            Log.e("SUKA", e.toString());
            return count;
        }
    }

    public static final ArrayList<ChartTopTracksModel> getChartTopTracks(String data){
        ArrayList<ChartTopTracksModel> objects = new ArrayList<ChartTopTracksModel>();
        try{
            JSONObject jsonObject = new JSONObject(data);
            JSONObject jsonObjectTracks = jsonObject.getJSONObject("tracks");
            JSONArray jsonArrayTrack = jsonObjectTracks.getJSONArray("track");
            for (int i=0; i<jsonArrayTrack.length(); i++){
                String name = jsonArrayTrack.getJSONObject(i).getString("name");
                int duration = Integer.valueOf(jsonArrayTrack.getJSONObject(i).getString("duration"));
                String listeners  = jsonArrayTrack.getJSONObject(i).getString("listeners");
                String mbid  = jsonArrayTrack.getJSONObject(i).getString("mbid");
                String url  = jsonArrayTrack.getJSONObject(i).getString("url");
                String streamableText = jsonArrayTrack.getJSONObject(i).getJSONObject("streamable").getString("#text");
                String streamableFulltrack = jsonArrayTrack.getJSONObject(i).getJSONObject("streamable").getString("fulltrack");
                String artist = jsonArrayTrack.getJSONObject(i).getJSONObject("artist").getString("name");
                String artistMbid = jsonArrayTrack.getJSONObject(i).getJSONObject("artist").getString("mbid");
                String artistUrl = jsonArrayTrack.getJSONObject(i).getJSONObject("artist").getString("url");
                String smallImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(0).getString("#text");
                String mediumImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(1).getString("#text");
                String largeImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(2).getString("#text");
                String extraLargeImg = jsonArrayTrack.getJSONObject(i).getJSONArray("image").getJSONObject(3).getString("#text");
                objects.add(new ChartTopTracksModel(name, duration, listeners, mbid, url, streamableText,
                        streamableFulltrack, artist, artistMbid, artistUrl, smallImg, mediumImg,
                        largeImg, extraLargeImg));
            }
            return objects;
        }catch (Exception e){
            Log.e(tag, e.toString());
            return objects;
        }
    }


}
