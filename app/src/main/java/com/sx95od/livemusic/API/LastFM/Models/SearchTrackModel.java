package com.sx95od.livemusic.API.LastFM.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by stoly on 29.08.2017.
 */

public class SearchTrackModel implements Serializable {
    @SerializedName("name")
    @Expose
    private String title;
    private String artist;
    private String url;
    private String streamable;
    private int listeners;
    private String smallImg;
    private String mediumImg;
    private String largeImg;
    private String extraLargeImg;
    private String mbid;


    public SearchTrackModel(String title, String artist, String url, String streamable, int listeners,
                            String smallImg, String mediumImg, String largeImg, String extraLargeImg,
                            String mbid){
        this.artist = artist;
        this.title = title;
        this.extraLargeImg = extraLargeImg;
        this.largeImg = largeImg;
        this.listeners = listeners;
        this.mbid = mbid;
        this.streamable = streamable;
        this.url = url;
        this.mediumImg = mediumImg;
        this.smallImg = smallImg;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public int getListeners(){
        return listeners;
    }

    public String getTitle(){
        return title;
    }

    public String getArtist(){
        return artist;
    }

    public String getUrl(){
        return url;
    }

    public String getStreamable(){
        return streamable;
    }

    public String getSmallImg(){
        return smallImg;
    }

    public String getMediumImg(){
        return mediumImg;
    }

    public String getLargeImg(){
        return largeImg;
    }

    public String getExtraLargeImg(){
        return extraLargeImg;
    }

    public String getMbid(){
        return mbid;
    }
}
