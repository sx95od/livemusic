
package com.sx95od.livemusic.API.LastFM.Models.TrackSearch;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackSearchModel implements Serializable
{

    @SerializedName("results")
    @Expose
    private Results results;
    private final static long serialVersionUID = -5666946655878882516L;

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

}
