package com.sx95od.livemusic.API.LastFM;


import com.sx95od.livemusic.API.LastFM.Models.AlbumSearch.AlbumSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.ArtistSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.GeoTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.MobileSession.MobileSessionModel;
import com.sx95od.livemusic.API.LastFM.Models.TopTracks.TopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.TrackSearchModel;

import java.util.List;


import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by stoly on 29.08.2017.
 */

public interface LastFMApi {

    @GET("?method=track.search")
    Observable<TrackSearchModel> searchTrack(@Query("track") String trackName,
                                             @Query("artist") String artist,
                                             @Query("api_key") String apiKey,
                                             @Query("limit") int limit,
                                             @Query("page") int page,
                                             @Query("format") String format);

    @GET("?method=artist.search")
    Observable<ArtistSearchModel> searchArtist(@Query("artist") String artist,
                                               @Query("api_key") String apiKey,
                                               @Query("limit") int limit,
                                               @Query("page") int page,
                                               @Query("format") String format);

    @GET("?method=album.search")
    Observable<AlbumSearchModel> searchAlbum(@Query("album") String artist,
                                             @Query("api_key") String apiKey,
                                             @Query("limit") int limit,
                                             @Query("page") int page,
                                             @Query("format") String format);

    @GET("?method=chart.getTopTracks")
    Observable<TopTracksModel> getTopTracks(@Query("api_key") String apiKey,
                                            @Query("limit") int limit,
                                            @Query("page") int page,
                                            @Query("format") String format);

    @POST("?method=auth.getMobileSession")
    Observable<MobileSessionModel> getMobileSession(@Query("api_key") String apiKey,
                                                    @Query("api_sig") String apiSig,
                                                    @Query("username") String username,
                                                    @Query("password") String password,
                                                    @Query("format") String format);
    @GET("?method=geo.getTopTracks")
    Observable<GeoTopTracksModel> getGeoTopTracks(@Query("api_key") String apiKey,
                                                  @Query("format") String format,
                                                  @Query("country") String country,
                                                  @Query("limit") int limit,
                                                  @Query("page") int page);

}
