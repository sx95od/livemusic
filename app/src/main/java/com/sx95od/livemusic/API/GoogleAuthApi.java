package com.sx95od.livemusic.API;

import org.json.JSONObject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by stoly on 29.10.2017.
 */

public interface GoogleAuthApi {
    String CODE_AUTHORIZATION = "authorization_code";
    String CODE_REFRESH_TOKEN = "refresh_token";


    @POST("token")
    @FormUrlEncoded
    Observable<ResponseBody> getToken(@Field("client_id") String clientId,
                               @Field("client_secret") String clientSecret,
                               @Field("code") String code,
                               @Field("grant_type") String grantType);


    @POST("token")
    @FormUrlEncoded
    Call<ResponseBody> refreshToken(@Field("client_id") String clientId,
                                    @Field("client_secret") String clientSecret,
                                    @Field("refresh_token") String refreshToken,
                                    @Field("grant_type") String grantType);
}
