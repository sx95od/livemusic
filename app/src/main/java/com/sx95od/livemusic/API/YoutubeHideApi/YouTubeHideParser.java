package com.sx95od.livemusic.API.YoutubeHideApi;

import android.util.ArrayMap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by stoly on 02.09.2017.
 */

public class YouTubeHideParser {
    public static final YouTubeHideAPIModel getVideo(String data){
        YouTubeHideAPIModel model = new YouTubeHideAPIModel(null);
        ArrayMap<Integer, String> arrayMapAudio = new ArrayMap<Integer, String>();
        try{
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArrayFormats = jsonObject.getJSONArray("formats");
            for (int i=0; i<jsonArrayFormats.length(); i++){
                if (jsonArrayFormats.getJSONObject(i).getString("type").equals("audio/webm; codecs=\"opus\"")){
                    arrayMapAudio.put(Integer.valueOf(jsonArrayFormats.getJSONObject(i).getString("audioBitrate")),
                            jsonArrayFormats.getJSONObject(i).getString("url"));
                }
            }
            model.setAudioMap(arrayMapAudio);
            return model;
        }catch (Exception e){
            Log.e(YouTubeHideParser.class.getClass().getSimpleName(), e.toString());
            return model;
        }
    }
}
