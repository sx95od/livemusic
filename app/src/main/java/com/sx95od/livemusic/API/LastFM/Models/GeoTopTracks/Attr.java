
package com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attr implements Serializable
{

    @SerializedName("rank")
    @Expose
    private String rank;
    private final static long serialVersionUID = -6145588686927147876L;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

}
