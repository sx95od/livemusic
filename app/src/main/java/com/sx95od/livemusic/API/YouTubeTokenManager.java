package com.sx95od.livemusic.API;

import android.util.Base64;
import android.util.Log;

import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.YouTubeCredentials;
import com.sx95od.livemusic.Fragments.FragmentFav.Presenter.IPresenter;
import com.sx95od.livemusic.Fragments.FragmentFav.Presenter.Presenter;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by stoly on 01.11.2017.
 */

public class YouTubeTokenManager {
    private static final String tag = "YTTokenManager";
    public static String REFRESH_TOKEN = null;
    public static String ACCESS_TOKEN = null;
    public static boolean call_refresh = false;

    public static void writeTokenToDB(String response){
        Realm realm = Realm.getInstance(Core.getConfigRealm());
        realm.beginTransaction();
//                byte[] dataByte = data.getBytes(StandardCharsets.UTF_8);
//                String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                RealmResults<YouTubeCredentials> resultsAT = realm.where(YouTubeCredentials.class)
                        .equalTo("data_name", "YT_AccessToken").findAll();
                RealmResults<YouTubeCredentials> resultsRT = realm.where(YouTubeCredentials.class)
                        .equalTo("data_name", "YT_RefreshToken").findAll();

                if (resultsAT.size() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String access_token = jsonObject.getString("access_token");

                        byte[] dataByte = access_token.getBytes(StandardCharsets.UTF_8);
                        String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                        resultsAT.get(0).setData(base64);
                    }catch (Exception e){
                        Log.e(tag, e.toString());
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String access_token = jsonObject.getString("access_token");

                        byte[] dataByte = access_token.getBytes(StandardCharsets.UTF_8);
                        String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                        YouTubeCredentials youTubeCredentials = realm.createObject(YouTubeCredentials.class, 0);
                        youTubeCredentials.setData(base64);
                        youTubeCredentials.setData_name("YT_AccessToken");
                    }catch (Exception e){

                    }
                }

                if (resultsRT.size() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String refresh_token = jsonObject.getString("refresh_token");

                        byte[] dataByte = refresh_token.getBytes(StandardCharsets.UTF_8);
                        String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                        resultsRT.get(0).setData(base64);
                    }catch (Exception e){
                        Log.e(tag, e.toString());
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String refresh_token = jsonObject.getString("refresh_token");

                        byte[] dataByte = refresh_token.getBytes(StandardCharsets.UTF_8);
                        String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                        YouTubeCredentials youTubeCredentials = realm.createObject(YouTubeCredentials.class, 1);
                        youTubeCredentials.setData(base64);
                        youTubeCredentials.setData_name("YT_RefreshToken");
                    }catch (Exception e){

                    }
                }
                realm.commitTransaction();
                realm.close();
                realm = null;
    }


    private static void initYouTubeTokenManager(){
        Realm realm = Realm.getInstance(Core.getConfigRealm());
        realm.beginTransaction();
                RealmResults<YouTubeCredentials> resultsAT = realm.where(YouTubeCredentials.class)
                        .equalTo("data_name", "YT_AccessToken").findAll();
                RealmResults<YouTubeCredentials> resultsRT = realm.where(YouTubeCredentials.class)
                        .equalTo("data_name", "YT_RefreshToken").findAll();

                if (resultsAT.size() > 0){
                    byte[] byteAT = Base64.decode(resultsAT.get(0).getData(), Base64.DEFAULT);
                    String aT = new String(byteAT, StandardCharsets.UTF_8);
                    YouTubeTokenManager.ACCESS_TOKEN = aT;
                }

                if (resultsRT.size() > 0){
                    byte[] byteRT = Base64.decode(resultsRT.get(0).getData(), Base64.DEFAULT);
                    String rT = new String(byteRT, StandardCharsets.UTF_8);
                    YouTubeTokenManager.REFRESH_TOKEN = rT;
                }
        realm.commitTransaction();
                realm.close();
                realm = null;
    }


    public static void updateToken(){
//        Core.getCoreInstance().googleAuthApi.refreshToken(BuildConfig.YOUTUBE_WEB_CLIENT_ID
//                ,BuildConfig.YOUTUBE_WEB_CLIENT_SECRET, YouTubeTokenManager.REFRESH_TOKEN, GoogleAuthApi.CODE_REFRESH_TOKEN)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new DisposableObserver<ResponseBody>() {
//                    @Override
//                    public void onNext(ResponseBody responseBody) {
//                        try {
//                            String response = responseBody.string();
//                            writeTokenToDB(response);
//                        }catch (Exception e){
//                            Log.e(tag, ((HttpException) e).message());
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        try {
//                            Log.e(tag, ((HttpException) e).response().errorBody().string() + " error");
//                        }catch (Exception e1){
//
//                        }
//                        ((HttpException) e).printStackTrace();
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
    }

    public static void checkToken(){
        try {
            Response response = Core.googleTokenApi.getTokenInfo(YouTubeTokenManager.ACCESS_TOKEN).execute();

            if (response.code() == 200) return;
            else {
                Response responseRefresh = Core.googleAuthApi.refreshToken(BuildConfig.YOUTUBE_WEB_CLIENT_ID, BuildConfig.YOUTUBE_WEB_CLIENT_SECRET,
                    YouTubeTokenManager.REFRESH_TOKEN, GoogleAuthApi.CODE_REFRESH_TOKEN).execute();

                if (responseRefresh.code() == 200){
                    writeTokenToDB(((ResponseBody) responseRefresh.body()).string());
                    initYouTubeTokenManager();
                    Log.d(tag, ((ResponseBody) responseRefresh.body()).string());
                }

                if (responseRefresh.errorBody()!= null)
                Log.e(tag, responseRefresh.errorBody().string());
            }
        }catch (Exception e){
            Log.e(tag, e.toString());
        }
    }
}
