
package com.sx95od.livemusic.API.YouTubeApi.Models.Channel;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicDetails implements Serializable
{

    @SerializedName("topicIds")
    @Expose
    private List<String> topicIds = null;
    @SerializedName("topicCategories")
    @Expose
    private List<String> topicCategories = null;
    private final static long serialVersionUID = -6706671245173717269L;

    public List<String> getTopicIds() {
        return topicIds;
    }

    public void setTopicIds(List<String> topicIds) {
        this.topicIds = topicIds;
    }

    public List<String> getTopicCategories() {
        return topicCategories;
    }

    public void setTopicCategories(List<String> topicCategories) {
        this.topicCategories = topicCategories;
    }

}
