package com.sx95od.livemusic.API;

import android.util.Log;

import com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel.MyChannelModel;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Fragments.FragmentFav.Presenter.Presenter;

import org.json.JSONObject;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

/**
 * Created by stoly on 06.11.2017.
 */

public class YouTubeRefreshTokenFunction implements Function<Observable<Throwable>, Observable<?>> {
    private final String tag = "YTRefreshFunction";
    boolean alreadyRequested = false;
    boolean alreadyResponsed = false;

    @Override
    public Observable<?> apply(Observable<Throwable> throwableObservable) throws Exception {
        return throwableObservable.flatMap(new Function<Throwable, Observable<?>>() {
            @Override
            public Observable<?> apply(Throwable throwable) throws Exception {
                if (throwable instanceof HttpException && !alreadyRequested) {
                    Log.d(tag, Thread.currentThread().getName());
                    alreadyRequested = true;
                    HttpException httpException = (HttpException) throwable;
                    Log.e(tag, httpException.code()+"");
                    Core.YT_LAST_CODE = httpException.code();
                    if (httpException.code() == 401 ) {
//                        return Core.googleAuthApi.refreshToken(BuildConfig.YOUTUBE_WEB_CLIENT_ID, BuildConfig.YOUTUBE_WEB_CLIENT_SECRET,
//                                YouTubeTokenManager.REFRESH_TOKEN, GoogleAuthApi.CODE_REFRESH_TOKEN).doOnNext(new Consumer<ResponseBody>() {
//                            @Override
//                            public void accept(ResponseBody responseBody) throws Exception {
//                                try{
//                                    String response = responseBody.string();
//
//                                    Log.d("trash", "hey...");
//
//                                    JSONObject jsonObject = new JSONObject(response);
//                                    String access_token = jsonObject.getString("access_token");
//
//                                    YouTubeTokenManager.ACCESS_TOKEN = access_token;
////                                    YouTubeTokenManager.writeTokenToDB(response);
//                                }catch (Exception e){
//                                    Log.e(tag, e.toString());
//                                }
//
//                            }
//                        });
                    }
                }
                return Observable.just(new MyChannelModel());
            }
        });
    }
}
