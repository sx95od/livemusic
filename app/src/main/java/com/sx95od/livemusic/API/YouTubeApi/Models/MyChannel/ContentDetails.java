
package com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentDetails implements Serializable
{

    @SerializedName("relatedPlaylists")
    @Expose
    private RelatedPlaylists relatedPlaylists;
    private final static long serialVersionUID = -5732236315647186702L;

    public RelatedPlaylists getRelatedPlaylists() {
        return relatedPlaylists;
    }

    public void setRelatedPlaylists(RelatedPlaylists relatedPlaylists) {
        this.relatedPlaylists = relatedPlaylists;
    }

}
