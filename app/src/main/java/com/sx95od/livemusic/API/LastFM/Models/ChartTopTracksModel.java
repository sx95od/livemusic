package com.sx95od.livemusic.API.LastFM.Models;

import java.io.Serializable;

/**
 * Created by stoly on 30.08.2017.
 */

public class ChartTopTracksModel implements Serializable{
    String name;
    int duration;
    String listeners;
    String mbid;
    String url;
    String streamableText;
    String streamableFulltrack;
    String artist;
    String artistMbid;
    String artistUrl;
    String smallImg;
    String mediumImg;
    String largeImg;
    String extraLargeImg;

    public ChartTopTracksModel(String name, int duration, String listeners, String mbid, String url,
                               String streamableText, String streamableFulltrack, String artist,
                               String artistMbid, String artistUrl, String smallImg, String mediumImg,
                               String largeImg, String extraLargeImg){
        this.name = name;
        this.duration = duration;
        this.listeners = listeners;
        this.mbid = mbid;
        this.url = url;
        this.streamableText = streamableText;
        this.streamableFulltrack = streamableFulltrack;
        this.artist = artist;
        this.artistMbid = artistMbid;
        this.artistUrl = artistUrl;
        this.smallImg = smallImg;
        this.mediumImg = mediumImg;
        this.largeImg = largeImg;
        this.extraLargeImg = extraLargeImg;
    }

    public String getName(){
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public String getListeners() {
        return listeners;
    }

    public String getMbid() {
        return mbid;
    }

    public String getUrl() {
        return url;
    }

    public String getStreamableText() {
        return streamableText;
    }

    public String getStreamableFulltrack() {
        return streamableFulltrack;
    }

    public String getArtist() {
        return artist;
    }

    public String getArtistMbid() {
        return artistMbid;
    }

    public String getArtistUrl() {
        return artistUrl;
    }

    public String getSmallImg() {
        return smallImg;
    }

    public String getMediumImg() {
        return mediumImg;
    }

    public String getLargeImg() {
        return largeImg;
    }

    public String getExtraLargeImg() {
        return extraLargeImg;
    }
}
