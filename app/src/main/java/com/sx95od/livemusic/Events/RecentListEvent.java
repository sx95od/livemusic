package com.sx95od.livemusic.Events;

/**
 * Created by stoly on 28.08.2017.
 */

public class RecentListEvent {
    public static final int HIDE_BUTTONS = 1;
    public static final int REMOVE_ITEM = 2;
    public static final int ENABLE_PAGING = 3;
    public static final int DISABLE_PAGING = 4;

    int code = 0;
    int pos = -1;

    public RecentListEvent(int code, int pos){
        this.code = code;
        this.pos = pos;
    }

    public RecentListEvent(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }

    public int getPos(){
        return pos;
    }
}
