package com.sx95od.livemusic.Events;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 16.06.2017.
 */

public class PlayerEvent {
    public static final int ERROR = 0;
    public static final int COMPLETE = 1;
    public static final int PREPARED = 2;
    public static final int PAUSE = 3;
    public static final int PLAY = 4;
    public static final int CHANGE_SONG = 5;
    public static final int UPDATE_POSITION_OF_DURATION = 6;
    public static final int SET_SONG_FROM_LIST = 7;
    public static final int SET_SONG_FROM_CURRENT_LIST = 11;
    public static final int ATTACH_PLAYLIST = 8;
    public static final int LIST_POSITION = 9;
    public static final int REQUEST_POSITION = 10;
    public static final int PLAY_NEXT = 12;


    private int code = -1;
    private int position = 0;

    public List<SongModel> tracksArray;

    private String title, artist, album, uri;
    private int id, duration, album_id, artist_id;
    public SongModel song;

    public PlayerEvent(int code) {
        this.code = code;
    }

    public PlayerEvent(int code, int position){
        this.code = code;
        this.position = position;
    }

    public PlayerEvent(int code, SongModel song){
        this.code = code;
        this.song = song;
    }

    public PlayerEvent(int code, int position, List<SongModel> tracksArray){
        this.code = code;
        this.position = position;
        this.tracksArray = tracksArray;
    }


    public PlayerEvent(int code, int id, String title, String artist, String album, String uri, int duration,
                       int album_id, int artist_id){
        this.album = album;
        this.album_id = album_id;
        this.artist = artist;
        this.uri = uri;
        this.duration = duration;
        this.artist_id = artist_id;
        this.title = title;
        this.id = id;
        this.code = code;
    }

    public SongModel getSong(){
        return song;
    }

    public List<SongModel> getTracksArray(){
        return tracksArray;
    }

    public int getPosition(){
        return position;
    }

    public int getCode() {
        return code;
    }

    public int getId(){
        return id;
    }

    public int getDuration(){
        return duration;
    }

    public int getAlbum_id(){
        return album_id;
    }

    public int getArtist_id(){
        return artist_id;
    }

    public String getTitle(){
        return title;
    }

    public String getArtist(){
        return artist;
    }

    public String getAlbum(){
        return album;
    }

    public String getUri(){
        return uri;
    }
}
