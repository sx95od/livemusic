package com.sx95od.livemusic.Events;

/**
 * Created by stoly on 21.07.2017.
 */

public class DBEvent {
    public static final int FAV_EXISTS = 0;
    public static final int FAV_ADDED = 1;
    public static final int FAV_LIKED = 2;
    public static final int FAV_DISLIKED = 3;

    public int code = -1;
    public int idSong = -1;

    public DBEvent(int code){
        this.code = code;
    }

    public DBEvent(int code, int idSong){
        this.code = code;
        this.idSong = idSong;
    }

    public int getCode(){
        return code;
    }

    public int getIdSong(){
        return idSong;
    }
}
