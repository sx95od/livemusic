package com.sx95od.livemusic.Events;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.List;

/**
 * Created by stoly on 25.03.2018.
 */

public class SongListEvent {
    public enum SongListEvents {
        SET_SONG,
        BIND_LIST,
        ADD_SONG
    }

    public int             pos;
    public List<SongModel> songList;
    public SongModel       songModel;
    public SongListEvents  event;


    public SongListEvent(SongListEvents event, int pos) {
        this.pos = pos;
        this.event = event;
    }


    public SongListEvent(SongListEvents event, int pos, List<SongModel> songList) {
        this.pos = pos;
        this.songList = songList;
        this.event = event;
    }


    public SongListEvent(SongListEvents event, SongModel songModel) {
        this.songModel = songModel;
        this.event = event;
    }
}
