package com.sx95od.livemusic.Events;

/**
 * Created by stoly on 21.07.2017.
 */

public class AppEvent {
    public static final int FILE_DELETED = 1;
    public static final int THEME_CHANGED = 2;
    public static final int RESET_ACTIVITY = 3;

    public int code = 0;
    public int id = -1;

    public AppEvent(int code, int id){
        this.code = code;
        this.id = id;
    }

    public AppEvent(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }

    public int getId(){
        return id;
    }
}
