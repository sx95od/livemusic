package com.sx95od.livemusic.Events;

import android.os.Bundle;
import android.util.ArrayMap;
import android.widget.FrameLayout;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.List;

/**
 * Created by stoly on 12.06.2017.
 */

public class FragmentEvent {
    public static final int FRAGMENT_NEW_PLAYLIST = 0;
    public static final int FRAGMENT_SONGSVIEW = 1;
    public static final int FRAGMENT_SHOW_ALL_FAV_SONGS_OPEN = 2;
    public static final int FRAGMENT_VIEW_ALL_SONGS_OPEN = 3;
    public static final int FRAGMENT_VIEW_ALL_SONGS_CLOSE = 4;
    public static final int FRAGMENT_SET_SONG = 5;
    public static final int FRAGMENT_BACK = 6;
    public static final int FRAGMENT_CREATED_PLAYLIST = 7;
    public static final int TUNE_CARD_PLAYLIST = 8;
    public static final int PLAYLIST_ADD_ITEM = 9;
    public static final int PLAYLIST_REMOVE_ITEM = 10;
    public static final int FRAGMENT_EXPAND_BOTTOM_PLAYER_CONTROL = 11;
    public static final int FRAGMENT_PLAYLIST_SONGS_VIEW = 12;
    public static final int TEST = 13;
    public static final int FRAGMENT_MORE = 14;
    public static final int FRAGMENT_DELETED_PLAYLIST = 15;
    public static final int FRAGMENT_SHOW_ALL_PLAYLISTS_OPEN = 16;
    public static final int FRAGMENT_SONG_ADD_TO_PLAYLIST = 17;
    public static final int FRAGMENT_FORCE_LIGHT_STATUS_BAR = 18;
    public static final int PAGING = 19;
    public static final int FRAGMENT_YOUTUBE_SHOW_VIDEO = 20;
    public static final int SET_VIEWPAGER_INTERCEPT_TOUCH = 21;
    public static final int FRAGMENT_DOWNLOADS_SHOW = 22;
    public static final int FRAGMENT_DOWNLOADS_REMOVE_ITEM = 23;
    public static final int FRAGMENT_ALBUMS_VIEW = 24;
    public static final int FRAGMENT_LASTFM_ACC = 25;
    public static final int FRAGMENT_SEARCH_MORE_RESULTS = 26;
    public static final int FRAGMENT_DOWNLOAD = 27;



    private int code = -1;
    private int var = -1;
    private SongModel song;
    private boolean enable;
    private Bundle bundle;
    private ArrayMap<String, Object> paramsMap;
    private List<SongModel> objects;

    public FragmentEvent(int code) {
        this.code = code;
    }

    public FragmentEvent(int code, int pos, List<SongModel> objects){
        this.code = code;
        this.var = pos;
        this.objects = objects;
    }

    public FragmentEvent(int code, Bundle bundle) {
        this.code = code;
        this.bundle = bundle;
    }

    public ArrayMap<String, Object> getParams(){
        return paramsMap;
    }

    public FragmentEvent(int code, ArrayMap<String, Object> arrayMap){
        this.code = code;
        this.paramsMap = arrayMap;
    }

    public FragmentEvent(int code, boolean enable){
        this.code = code;
        this.enable = enable;
    }


    //For playlists work
    public FragmentEvent(int code, int var){
        this.code = code;
        this.var = var;
    }

    public int getMessage() {
        return code;
    }

    public boolean getLightStatusBar(){
        return enable;
    }

    public boolean getEnabled(){
        return enable;
    }

    public Bundle getBundle(){
        return bundle;
    }

    public int getVarInt(){
        return var;
    }

    public List<SongModel> getObjects(){
        return objects;
    }

    public FragmentEvent(int code, SongModel song){
        this.code = code;
        this.song = song;
    }

    public SongModel getSong(){
        return song;
    }
}
