package com.sx95od.livemusic.Events;

/**
 * Created by stoly on 15.07.2017.
 */

public class CoreEvent {
    public static final int LOCAL_SONGS_LOADED = 1;
    public static final int RESET_ACTIVITY = 2;

    private int code = -1;

    public CoreEvent(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }
}
