package com.sx95od.livemusic.Events

/**
 * Created by stoly on 16.03.2018.
 */
class GlobalEvent(inEvent : String) {
    val event = inEvent

    companion object {
        const val UI_RECREATE : String = "UI_RECREATE"
    }
}