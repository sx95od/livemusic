package com.sx95od.livemusic

import com.sx95od.livemusic.Core.Core

/**
 * Created by stoly on 16.01.2018.
 */
class AnimationConstants {
    companion object {
        fun xTrans() = Core.getCoreInstance().resources.getDimension(R.dimen.dp56)
        fun xTransDur() : Long = 420
        fun xFragTransDur() : Long = 320
    }
}