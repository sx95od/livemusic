package com.sx95od.livemusic.Utils;


/**
 * Created by stoly on 23.02.2017.
 */

public class BroadcastConstants {
    public static final String STATE_PLAYING = "StatePlaying";

    public static final String ACTION_PLAY = "action_play";
    public static final String ACTION_PAUSE = "action_pause";
    public static final String ACTION_NEXT = "action_next";
    public static final String ACTION_PREVIOUS = "action_previous";
    public static final String ACTION_CLOSE = "action_close";

    public static final String BROADCAST_TAB = "broadcast_tab";
    public static final String TAB_OPTIONS = "tab_options";
    public static final String TAB_SELECTED = "tab_selected";

    public static final String RECYCLER_VIEW_SCROLL = "recycler_view_scroll";
    public static final String RECYCLER_VIEW_SCROLL_STATE = "recycler_view_scroll_state";
}
