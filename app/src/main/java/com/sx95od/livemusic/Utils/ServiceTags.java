package com.sx95od.livemusic.Utils;

/**
 * Created by stoly on 27.02.2017.
 */

public class ServiceTags {
    public static final String TAG_ACTIVITY = "ActivityLife";
    public static final String TAG_IMAGEPERFORMANCE = "ImagePerformance";

    public static final String PLAYBACK_ERROR = "PLAYBACK ERROR";
    public static final String PLAYBACK_COMPLETION = "PLAYBACK COMPLETION";

    public static final String TAG_MAIN_ACTIVITY_TAB_LAYOUT = "MainActivity TabLayout";
}
