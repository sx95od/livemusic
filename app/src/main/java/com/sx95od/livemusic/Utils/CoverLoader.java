package com.sx95od.livemusic.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.sx95od.livemusic.R;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;

/**
 * Created by stoly on 25.02.2017.
 */

public class CoverLoader extends AsyncTask<String, Void, File> {
    WeakReference<ImageView> imageView;
    WeakReference<ImageView> imageViewBack;
    WeakReference<Context> ctx;



    public CoverLoader(WeakReference<Context> ctx, WeakReference<ImageView> imageView, WeakReference<ImageView> imageViewBack) {
        this.ctx = ctx;
        this.imageView=imageView;
        this.imageViewBack=imageViewBack;
    }

    @Override
    protected File doInBackground(String... params) {
        File file=null;
        MediaMetadataRetriever mData=new MediaMetadataRetriever();
        mData.setDataSource(params[0]);
        if (mData.getEmbeddedPicture()!=null ) {
            File cacheDir = new File(ctx.get().getExternalCacheDir().getAbsolutePath()+"/.covers");
            if (!cacheDir.exists()) {
                cacheDir.mkdir();
            }
            File cacheArt = new File(ctx.get().getExternalCacheDir().getAbsolutePath() + "/.covers/"+params[1]);
            if (!cacheArt.exists()) {
                try {
                    InputStream inputStream = new ByteArrayInputStream(mData.getEmbeddedPicture());
                    Bitmap art = BitmapFactory.decodeStream(inputStream);

                    FileOutputStream out = new FileOutputStream(cacheArt);
                    art.compress(Bitmap.CompressFormat.JPEG, 40, out);
                    file = cacheArt;
                    out.flush();
                    out.close();
                }catch (Exception e){

                }
            } else{
                file = cacheArt;
            }
        }
        mData.release();

        return file;
    }


    @Override
    protected void onPostExecute(final File file) {
        final ImageView imageView = this.imageView.get();
        final ImageView imageViewBack = this.imageViewBack.get();
        imageView.clearAnimation();
        if (imageViewBack!=null) {
            imageViewBack.clearAnimation();
        }
        Log.i("AdapterSong", file+" URI");
        try {
            if (file != null) {
                if (file.length()>0) {


//                    Picasso.with(getApplicationContext()).load(file).placeholder(R.drawable.notemax_grey).into(imageView);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            imageView.post(new Runnable() {
                                @Override
                                public void run() {
                                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
                                    try {
                                        if (bitmap.getWidth() > 0) {
                                            imageView.setImageURI(Uri.parse(file.toString()));
                                        }
                                    } catch (Exception e) {
                                        imageView.startAnimation(imageAnimReverse());
//                                        imageView.setImageResource(R.drawable.notemax_grey);
                                        imageView.startAnimation(imageAnim());
                                    } finally {
                                        imageView.startAnimation(imageAnimReverse());
                                        imageView.startAnimation(imageAnim());
                                    }
                                }
                            });
                        }
                    }).start();


                    if (imageViewBack != null) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                imageViewBack.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                                        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
                                        try {
                                            if (bitmap.getWidth() > 0) {
//                                                GaussianBlur.with(ctx.get().getApplicationContext()).put(bitmap, imageViewBack);
                                            }
                                        } catch (Exception e) {
                                            imageViewBack.startAnimation(imageAnimReverse());
                                            imageViewBack.setImageBitmap(null);
                                        } finally {
                                            imageViewBack.startAnimation(imageAnim());
                                        }
                                    }
                                });
                            }
                        }).start();
                    }
                } else{
                    imageView.startAnimation(imageAnimReverse());
//                    imageView.setImageResource(R.drawable.notemax_grey);
                    imageView.startAnimation(imageAnim());
                    if (imageViewBack!=null) {
                        imageViewBack.startAnimation(imageAnimReverse());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                imageViewBack.setImageBitmap(null);
                            }
                        }, 480);// delay in milliseconds (200)

                    }
                }
            } else {
                imageView.startAnimation(imageAnimReverse());
//                imageView.setImageResource(R.drawable.notemax_grey);
                imageView.startAnimation(imageAnim());
                if (imageViewBack!=null) {
                    imageViewBack.startAnimation(imageAnimReverse());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            imageViewBack.setImageBitmap(null);
                        }
                    }, 480);// delay in milliseconds (200)

                }
            }
        }catch (Exception e){

        }finally {

        }
        super.onPostExecute(file);
    }

    public Animation imageAnim(){
        return AnimationUtils.loadAnimation(ctx.get(), R.anim.image_view);
    }

    public Animation imageAnimReverse(){
        return AnimationUtils.loadAnimation(ctx.get(), R.anim.image_view_reverse);
    }
}