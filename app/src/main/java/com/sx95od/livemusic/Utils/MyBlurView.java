package com.sx95od.livemusic.Utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.sx95od.livemusic.R;

import java.lang.ref.WeakReference;


/**
 * Created by stoly on 05.06.2017.
 */

public class MyBlurView {
    int radius;
    int id;
    WeakReference<Window> window;
    WeakReference<View> view;
    WeakReference<Context> context;




    public MyBlurView(Window window, View view, Context context, int id, int radius){
        this.window = new WeakReference<Window>(window);
        this.view = new WeakReference<View>(view);
        this.radius = radius;
        this.id = id;
        this.context = new WeakReference<Context>(context);
    }

    public void blur(){
        final float radius = 8;



        final View decorView = window.get().getDecorView();
        //Activity's root View. Can also be root View of your layout (preferably)
//        final ViewGroup rootView = (ViewGroup) decorView.findViewById(R.id.activity_main);
        //set background, if your root layout doesn't have one
        final Drawable windowBackground = decorView.getBackground();

    }


}
