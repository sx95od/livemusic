package com.sx95od.livemusic.Utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static android.view.View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION;

/**
 * Created by stoly on 14.08.2017.
 */

public class ThemeEngine {
    public static final String COLOR_ACCENT = "color_accent";
    public static final String DARK_THEME = "dark_theme";

    public static void applyTheme(View view){
        setCardTheme(view);
        setPrimaryTextTheme(view);
        setBackgroundTheme(view);
        setColorAccentTheme(view);
        setButtonColorTheme(view);
        setSecondaryBackgroundTheme(view);
        setSecondaryShapeBackgroundTheme(view);
        setProgressTheme(view);
    }

    private static Context getContext(){
        return Core.getCoreInstance();
    }

    private static void setProgressTheme(View view){
        ArrayList<View> progresses = new ArrayList<View>();
        view.findViewsWithText(progresses, getContext().getString(R.string.theme_progress), FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View progress : progresses) ThemeEngine.setProgressColor(progress);
    }

    private static void setButtonColorTheme(View view){
        ArrayList<View> buttons = new ArrayList<View>();
        view.findViewsWithText(buttons, getContext().getString(R.string.theme_button), FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View button : buttons) ThemeEngine.setButtonColor(button);
    }

    private static void setSecondaryBackgroundTheme(View view){
        ArrayList<View> backgrounds = new ArrayList<View>();
        view.findViewsWithText(backgrounds, getContext().getString(R.string.theme_secondary_background), FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View background : backgrounds) ThemeEngine.setSecondaryBackgroundColor(background);
    }

    private static void setSecondaryShapeBackgroundTheme(View view){
        ArrayList<View> backgrounds = new ArrayList<View>();
        view.findViewsWithText(backgrounds, getContext().getString(R.string.theme_secondary_shape_background), FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View background : backgrounds) ThemeEngine.setSecondaryShapeBackgroundColor(background);
    }

    private static void setColorAccentTheme(View view){
        ArrayList<View> tagViewsColorAccent = new ArrayList<View>();
        view.findViewsWithText(tagViewsColorAccent, getContext().getString(R.string.theme_color_accent), FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View colorAccentView : tagViewsColorAccent) ThemeEngine.setColorAccent(colorAccentView);
    }

    private static void setBackgroundTheme(View view){
        ArrayList<View> tagViewsBackground = new ArrayList<View>();
        view.findViewsWithText(tagViewsBackground, getContext().getString(R.string.theme_background), FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View backgroundView : tagViewsBackground) ThemeEngine.setBackground(backgroundView);
    }

    private static void setCardTheme(View view){
        ArrayList<View> tagCard = new ArrayList<View>();
        view.findViewsWithText(tagCard, getContext().getString(R.string.theme_card), FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View card : tagCard) ThemeEngine.setCardColor((CardView) card);
    }

    private static void setPrimaryTextTheme(View view){
        ArrayList<View> tagPrimaryText = new ArrayList<View>();
        view.findViewsWithText(tagPrimaryText, getContext().getString(R.string.theme_primary_text), FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        for (View primaryText : tagPrimaryText) ThemeEngine.setPrimaryTextColor(primaryText);
    }

    public static ColorStateList getBottomNavViewColorStateList(Context ctx){
        int[][] states = new int[][] {
                new int[] {android.R.attr.state_checked}, // unchecked
                new int[] {-android.R.attr.state_checked}  // pressed
        };

        int[] colors = new int[] {
                PrefManager.getColorAccent(),
                ContextCompat.getColor(ctx, R.color.grey)
        };

        ColorStateList csl = new ColorStateList(states, colors);
        return csl;
    }

    public static void setColorAccent(View view){
        if (view != null) {
            if (view instanceof TextView) {
                ((TextView) view).setTextColor(PrefManager.getColorAccent());
            } else if (view instanceof ImageButton) {
                ((ImageButton) view).setColorFilter(PrefManager.getColorAccent());
            } else if (view instanceof ImageView) {
                ((ImageView) view).setColorFilter(PrefManager.getColorAccent());
            }
        }
    }

    public static void setBackground(View viewGroup){
        if (viewGroup != null) viewGroup.setBackgroundColor(ContextCompat.getColor(Core.getCoreInstance(),
                Core.getCoreInstance().getDarkTheme() == 0 ? R.color.light : R.color.dark));
    }

    public static void setPrimaryTextColor(View view){
        if (view != null) ((TextView) view).setTextColor(ContextCompat.getColor(Core.getCoreInstance(),
                Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark : R.color.light));
        if (view instanceof EditText) ((EditText) view).setHintTextColor(ContextCompat.getColor(Core.getCoreInstance(),
                Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark2 : R.color.light2));
    }

    public static void setButtonColor(View view){
        if (view != null) {
            if (view instanceof  ImageButton) ((ImageButton) view).setColorFilter(ContextCompat.getColor(Core.getCoreInstance(),
                    Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark : R.color.light));
            else if (view instanceof AppCompatImageButton) ((AppCompatImageButton) view).setColorFilter(ContextCompat.getColor(Core.getCoreInstance(),
                    Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark : R.color.light));
            else if (view instanceof ImageView) ((ImageView) view).setColorFilter(ContextCompat.getColor(Core.getCoreInstance(),
                    Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark : R.color.light));
        }
    }

    public static void setCardColor(CardView card){
        if (card != null) card.setCardBackgroundColor(ContextCompat.getColor(Core.getCoreInstance(),
                Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark2 : R.color.white));
    }

    public static void setProgressColor(View view){
//        if (view != null) ((ProgressBar) view).getProgressDrawable().setColorFilter(ContextCompat.getColor(getContext(),
//                Core.getCoreInstance().getDarkTheme() != 0 ? R.color.light : R.color.dark), PorterDuff.Mode.SRC_IN);
    }

    public static void setSecondaryTextColor(View view){
        if (view != null) ((TextView) view).setTextColor(ContextCompat.getColor(Core.getCoreInstance(),
                Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark : R.color.light));
    }

    public static void setSecondaryBackgroundColor(View view){
        if (view != null) view.setBackgroundColor(ContextCompat.getColor(Core.getCoreInstance(),
                Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark2 : R.color.light));
    }

    public static void setSecondaryShapeBackgroundColor(View view){
        if (view != null) ((StateListDrawable) view.getBackground())
                .setColorFilter(ContextCompat.getColor(getContext(),
                        Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark2 : R.color.light),
                        PorterDuff.Mode.SRC_IN);
    }
}
