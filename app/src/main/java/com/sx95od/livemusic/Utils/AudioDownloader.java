package com.sx95od.livemusic.Utils;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.DownloadFormats;
import com.sx95od.livemusic.DataBase.FavSongs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.realm.Realm;

/**
 * Created by stoly on 02.09.2017.
 */

public class AudioDownloader extends AsyncTask<String, String, Void> {
    String uri;
    final String tag = getClass().getSimpleName();
    boolean downloadFail = false;

    public AudioDownloader() {
        super();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }


    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Void doInBackground(final String... params) {
        final String album = "";
        final String artist = params[3];
        final String title = params[2];
        final String year = "";

        int count;
        final String name = params[1].replaceAll("\\s+", "_").replaceAll("\\.", "").replaceAll("/", "-");
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            int lenghtOfFile = connection.getContentLength();
            input = connection.getInputStream();

            File path = new File(Environment.getExternalStorageDirectory()+"/livemusic_downloads/");
            if (!path.exists()){
                path.mkdir();
            }
            path = null;

            OutputStream outputStream = new FileOutputStream(Environment.getExternalStorageDirectory()+"/livemusic_downloads/"+name+".opus");

            Log.d("AudioAsyncTask", lenghtOfFile+"");

            byte data[] = new byte[4096];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                outputStream.write(data, 0, count);
            }

            outputStream.flush();
            outputStream.close();
            input.close();

            uri = Environment.getExternalStorageDirectory()+"/livemusic_downloads/";
            if (Core.getH()) {
                Realm realm = Realm.getInstance(Core.getHideConfigRealm());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        DownloadFormats downloadFormats = realm.createObject(DownloadFormats.class, getNextKey());
                        downloadFormats.setAlbum(album);
                        downloadFormats.setArtist(artist);
                        downloadFormats.setTitle(title);
                        downloadFormats.setFileName(name);
                        downloadFormats.setUri(uri);
                        downloadFormats.setFormat(DownloadFormats.OPUS);
                        downloadFormats.setOutFormat(DownloadFormats.MP3);

                    }
                });
                realm.close();
                realm = null;
            }
//            final String out = Environment.getExternalStorageDirectory()+"/livemusic_downloads/"+name+".mp3".replaceAll("\\s+","_");
//
//            final String[] cmd = new String[11];
//            cmd[0] = "-i";
//            cmd[1] = in;
//            cmd[2] = "-f";
//            cmd[3] = "mp3";
//            cmd[4] = "-b:a";
//            cmd[5] = "320k";
//            cmd[6] = "-metadata";
//            cmd[7] = "title="+params[2]+"";
//            cmd[8] = "-metadata";
//            cmd[9] = "artist="+params[3]+"";
//            cmd[10] = out;
//
//            final FFmpeg ffmpeg = FFmpeg.getInstance(Core.getCoreInstance());
//            try {
//               ffmpeg.execute(cmd, new FFmpegExecuteResponseHandler() {
//                   @Override
//                   public void onSuccess(String message) {
//                       Log.d("AudioAsyncTaskFFMPEG", message);
//                   }
//
//                   @Override
//                   public void onProgress(String message) {
//                        Log.d("AudioAsyncTaskFFMPEG", message);
//                   }
//
//                   @Override
//                   public void onFailure(String message) {
//                       Log.e("AudioAsyncTaskFFMPEG", message);
//                   }
//
//                   @Override
//                   public void onStart() {
//                       Log.d("AudioAsyncTaskFFMPEG", "onStart");
//                   }
//
//                   @Override
//                   public void onFinish() {
//                                 new File(in).delete();
//                                 Log.d("AudioAsyncTaskFFMPEG", "onFinish");
//                                 Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                                 intent.setData(Uri.fromFile(new File(out)));
//                                 Core.getCoreInstance().sendBroadcast(intent);
//                   }
//               });
//            } catch (FFmpegCommandAlreadyRunningException e) {
//                // Handle if FFmpeg is already running
//                Log.e("AudioAsyncTaskFFMPEG", e.toString());
//            }
        } catch (Exception e) {
            downloadFail = true;
            e.printStackTrace();
        }
        return null;
    }

    public int getNextKey() {
        Realm realm = null;
        int nextId = 0;
        try{
            realm = Realm.getInstance(Core.getHideConfigRealm());
            Number maxId = realm.where(DownloadFormats.class).max("id");
            // If there are no rows, currentId is null, so the next id must be 1
            // If currentId is not null, increment it by 1
            nextId = (maxId == null) ? 0 : maxId.intValue() + 1;
        }catch (Exception e){
            realm.close();
            Log.e(tag, e.toString());
            return nextId;
        } finally {
            realm.close();
            return nextId;
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (!downloadFail) {
            Log.d(tag, "Downloading finished!");
            Toast.makeText(Core.getCoreInstance(), "Download finished!", Toast.LENGTH_LONG).show();
        } else {
            Log.e(tag, "Download fail! Try downloading from another video!");
            Toast.makeText(Core.getCoreInstance(), "Download fail! Try downloading from another video!", Toast.LENGTH_LONG).show();
        }
    }
}
