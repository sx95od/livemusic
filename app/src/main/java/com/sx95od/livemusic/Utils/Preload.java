package com.sx95od.livemusic.Utils;

import android.graphics.drawable.Drawable;
import android.util.Log;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by stoly on 07.03.2018.
 */

public class Preload {
    private static String tag = "Preload";
    private static Drawable image_mini_song_placeholder;

    public static void loadPreloaders(){

        Observable.fromCallable(new Callable<Drawable>() {
            @Override
            public Drawable call() throws Exception {
                return Core.withGlide().asDrawable().load(Core.getCoreInstance().getDarkTheme() == 0 ?
                        R.drawable.noimg2light : R.drawable.noimg2dark).submit(140, 140).get();
            }
        }).subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Drawable>() {
            @Override
            public void onNext(Drawable drawable) {
                image_mini_song_placeholder = drawable;
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public static Drawable getImageMiniSongPlaceholder(){
        return image_mini_song_placeholder;
    }
}
