package com.sx95od.livemusic.Utils;

/**
 * Created by stoly on 24.02.2017.
 */

public class Preferences {
    public static final String SHUFFLE_PREF = "shuffle";
    public static final String REPEAT_PREF = "repeat";

    public static final String EXTERNAL_STORAGE = "external_storage";

    //Theme Chooser
    public static final String THEME_CHOOSER = "theme_chooser";
}
