package com.sx95od.livemusic.Utils;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sx95od.livemusic.Core.Core;

/**
 * Created by stoly on 26.08.2017.
 */

public class Utils {
    public static final void hideKeyboard(Context context, View view){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static int getPxInDp(int dp){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                Core.getCoreInstance().getResources().getDisplayMetrics());
    }

    public static float getFloat(int value){
        TypedValue typedValue = new TypedValue();
        Core.getCoreInstance().getResources().getValue(value, typedValue, true);
        return typedValue.getFloat();
    }
}
