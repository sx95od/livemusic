package com.sx95od.livemusic.Utils;

import android.content.Context;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.WeakHashMap;

/**
 * Created by stoly on 13.03.2017.
 */

public class ScanMusic {
    ArrayList<String> arrayList = new ArrayList<String>();

    public ScanMusic(WeakReference<Context> context){

    }

    public ArrayList<String> get(){
        File root = new File("/storage/");
        File[] files = root.listFiles();
        for (File file : files){
            if (file.isDirectory()){
                scanDir(file);
            } else if(file.getAbsolutePath().toString().endsWith(".mp3")){
                arrayList.add(file.getAbsolutePath().toString());
            }
        }
        Log.i("SONGS", arrayList.size()+"");
        return arrayList;
    }

    public void scanDir(File dir){
        Log.i("FILE", dir.toString());
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                    if (file.isDirectory() && !file.getName().equals("Android")) {
                        scanDir(file);
                    } else if (file.getAbsolutePath().toString().endsWith(".mp3")) {
                        arrayList.add(file.getAbsolutePath().toString());
                    }
            }
        }
    }
}
