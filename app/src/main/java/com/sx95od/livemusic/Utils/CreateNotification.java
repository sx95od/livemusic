package com.sx95od.livemusic.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.media.session.MediaSessionManager;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import com.sx95od.livemusic.R;

import java.lang.ref.WeakReference;

/**
 * Created by stoly on 26.02.2017.
 */

public class CreateNotification extends AsyncTask<String, Void, Bitmap>{
    WeakReference<Context> ctx;
    WeakReference<MediaPlayer> player;
    WeakReference<String> title, artist;
    MediaSessionManager mediaSessionManager;
    MediaSession mediaSession;
    MediaSessionCompat mediaSessionCompat;

    public CreateNotification(WeakReference<Context> ctx, WeakReference<MediaPlayer> player) {
        this.ctx = ctx;
        this.player = player;



    }

    @Override
    protected void onPreExecute() {
        mediaSessionManager = (MediaSessionManager) ctx.get().getSystemService(Context.MEDIA_SESSION_SERVICE);
        mediaSessionCompat = new MediaSessionCompat(ctx.get(), "MusicService");
        mediaSessionCompat.setCallback(new MediaSessionCompat.Callback() {
            @Override
            public void onPause() {
                super.onPause();
            }

            @Override
            public void onPlay() {
                super.onPlay();
                mediaSessionCompat.setPlaybackState(new PlaybackStateCompat.Builder()
                        .setState(PlaybackStateCompat.STATE_PLAYING, 0, 0)
                        .setActions(PlaybackStateCompat.ACTION_SKIP_TO_NEXT|PlaybackStateCompat.ACTION_PLAY_PAUSE|PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
                        .build());
                player.get().start();
//                playing=true;
            }
        });
        super.onPreExecute();
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        NotificationCompat.Builder builder = new Builder(ctx.get(), null);
        builder
                .setSmallIcon(R.drawable.circle)
                .setContentTitle(title.get())
                .setContentText(artist.get())
                .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(1,2).setMediaSession(mediaSessionCompat.getSessionToken()))
        .build();

        super.onPostExecute(bitmap);
    }


}
