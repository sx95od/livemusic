package com.sx95od.livemusic.Utils;

import android.content.Context;

import com.sx95od.livemusic.R;

import static com.sx95od.livemusic.Utils.Utils.getPxInDp;

/**
 * Created by stoly on 27.03.2018.
 */

public class SystemUtils {
    private static int status_bar_height = 0;
    private static int regular_action_bar_height = 0;


    public static void setStatusBarHeight(int statusBarHeight){
        status_bar_height = statusBarHeight;
    }


    public static void init(Context context){
        regular_action_bar_height = getPxInDp(context.getResources().getInteger(R.integer.regular_action_bar_height));
    }


    public static int getStatusBarHeight(){
        return status_bar_height;
    }


    public static int getRegularActionBarHeight(){
        return regular_action_bar_height;
    }


    public static int getRegularActionBarPadding(){
        return regular_action_bar_height + status_bar_height;
    }
}
