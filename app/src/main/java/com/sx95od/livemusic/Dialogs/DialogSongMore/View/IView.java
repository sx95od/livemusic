package com.sx95od.livemusic.Dialogs.DialogSongMore.View;

import android.graphics.Bitmap;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;

/**
 * Created by stoly on 22.10.2017.
 */

public interface IView {
    void setLiked(boolean liked);
    void openFirstYouTubeVideo(Item item);
    void youTubeShowError(String errorMessage);
    void showCover(Bitmap bitmap);
}
