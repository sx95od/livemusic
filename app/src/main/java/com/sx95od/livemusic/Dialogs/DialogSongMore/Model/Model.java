package com.sx95od.livemusic.Dialogs.DialogSongMore.Model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.FavSongs;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Utils.MD5;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 22.10.2017.
 */

public class Model implements IModel {

    YouTubeApi youTubeApi = Core.getYoutubeApi();

    @Override
    public Observable<Boolean> getLiked(SongModel songModel) {
       return Observable.defer(new Callable<ObservableSource<? extends Boolean>>() {
           @Override
           public ObservableSource<? extends Boolean> call() throws Exception {
               Realm realm = Realm.getInstance(Core.getConfigRealm());
               RealmResults<FavSongs> results = realm.where(FavSongs.class)
                       .equalTo("_id", songModel.getId()).findAll();
               if (results.size()>0){
                   return Observable.just(true);
               } else return Observable.just(false);
           }
       });
    }

    @Override
    public Observable<SearchByKeywordModel> getSearchByKeyWordYoutube(String q, int maxResults, String type, String categoryID, String part, String apiKey) {
        return youTubeApi.searchVideo(q, maxResults, YouTubeApi.TYPE_VIDEO, YouTubeApi.CATEGORY_MUSIC, part, apiKey);
//        return youTubeApi.searchVideo(q, maxResults, YouTubeApi.TYPE_VIDEO, part, apiKey);
    }

    @Override
    public Observable<Bitmap> getCover(SongModel song) {
        return Observable.fromCallable(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                Bitmap exportBitmap = null;
                Bitmap bitmap = null;
                try {
                    MediaMetadataRetriever mData = new MediaMetadataRetriever();
                    mData.setDataSource(song.getUri());
                    bitmap = Core.withGlide().asBitmap()
                            .centerCrop()
                            .load(mData.getEmbeddedPicture())
                            .submit(192, 192).get();

                    Bitmap newBitmap = Bitmap.createBitmap(192, 192, Bitmap.Config.RGB_565);
                    Canvas canvas = new Canvas(newBitmap);
                    canvas.drawColor(Color.WHITE);
                    canvas.drawBitmap(bitmap, 0, 0, null);
                    mData.release();
                    exportBitmap = newBitmap;
                }catch (Exception e){
                    exportBitmap = Core.withGlide().asBitmap()
                            .centerCrop()
                            .load(Core.getCoreInstance().getDarkTheme() == 1 ? R.drawable.noimg2dark : R.drawable.noimg2light)
                            .submit(192, 192).get();
                }
                return exportBitmap;
            }
        });
    }

    @Override
    public Observable<Boolean> setLiked(SongModel songModel) {
        return Observable.defer(() -> {
            Realm realm = Realm.getInstance(Core.getConfigRealm());

            RealmResults<FavSongs> results = realm.where(FavSongs.class)
                    .equalTo("_id", songModel.getId()).findAll();

            if (results.size() == 0) {
                realm.beginTransaction();
                FavSongs favSongs = realm.createObject(FavSongs.class, generateID());
                favSongs.set_id(songModel.getId());
                favSongs.setAlbum(songModel.getAlbum());
                favSongs.setAlbum_id(songModel.getAlbum_id());
                favSongs.setArtist(songModel.getArtist());
                favSongs.setArtist_id(songModel.getArtist_id());
                favSongs.setDate_modified(songModel.getDate());
                favSongs.setDuration(songModel.getDuration());
                favSongs.setUri(songModel.getUri());
                favSongs.setYear(songModel.getYear());
                favSongs.setTitle(songModel.getTitle());
                favSongs.setMD5(MD5.checkMD5(songModel.getUri()));
                realm.commitTransaction();
                return Observable.just(true);
            } else {
                realm.beginTransaction();
                results.deleteAllFromRealm();
                realm.commitTransaction();
                return Observable.just(false);
            }
        });
    }

    private int generateID(){
        Realm realm = Realm.getInstance(Core.getConfigRealm());
        Number currentIdNum = realm.where(FavSongs.class).max("id");
        int nextId;
        if(currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }
        return nextId;
    }
}
