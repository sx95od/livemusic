package com.sx95od.livemusic.Dialogs;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.sx95od.livemusic.Acts.MyActivity;
import com.sx95od.livemusic.AnimationConstants;
import com.sx95od.livemusic.R2;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by stoly on 25.03.2018.
 */

public class MyDialog extends DialogFragment {
    AnimatorSet animatorSet = new AnimatorSet();

    @OnClick(R2.id.root)
    public void onRootClick(){
        getActivity().onBackPressed();
    }

    public void backStack(){
        animateView(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        animateView(false);

        ButterKnife.bind(this, view);
    }

    private void animateView(boolean destroy){
        animatorSet = new AnimatorSet();

        float alphaStart = destroy ? 1f : 0f;
        float alphaEnd   = destroy ? 0f : 1f;
        float scaleStart  = destroy ? 1f : 0.95f;
        float scaleEnd = destroy ? 0.95f : 1f;

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (destroy) ((MyActivity) getActivity()).dialogBackStack();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        ObjectAnimator objectAnimatorAlpha = ObjectAnimator.ofFloat(getView(), View.ALPHA, alphaStart, alphaEnd);
        ObjectAnimator objectAnimatorScaleX = ObjectAnimator.ofFloat(getView(), View.SCALE_X, scaleStart, scaleEnd);
        ObjectAnimator objectAnimatorScaleY = ObjectAnimator.ofFloat(getView(), View.SCALE_Y, scaleStart, scaleEnd);

        animatorSet.setDuration(AnimationConstants.Companion.xFragTransDur());
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(objectAnimatorAlpha, objectAnimatorScaleX, objectAnimatorScaleY);
        animatorSet.start();
    }
}
