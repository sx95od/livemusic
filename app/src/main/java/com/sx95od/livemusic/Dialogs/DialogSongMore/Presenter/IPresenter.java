package com.sx95od.livemusic.Dialogs.DialogSongMore.Presenter;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

/**
 * Created by stoly on 22.10.2017.
 */

public interface IPresenter {
    void getLiked(SongModel songModel);
    void setLiked(SongModel songModel);
    void onSearchVideosOnYouTube(SongModel track);
    void onDestroy();
    void getCover(SongModel songModel);
}
