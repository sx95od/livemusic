package com.sx95od.livemusic.Dialogs.DialogSongMore.Presenter;

import android.graphics.Bitmap;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Dialogs.DialogSongMore.Model.IModel;
import com.sx95od.livemusic.Dialogs.DialogSongMore.Model.Model;
import com.sx95od.livemusic.Dialogs.DialogSongMore.View.IView;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by stoly on 22.10.2017.
 */

public class Presenter implements IPresenter{
    private IView view;
    private IModel model = new Model();

    private CompositeDisposable disposable = new CompositeDisposable();

    public Presenter(IView view) {
        this.view = view;
    }


    @Override
    public void getLiked(SongModel songModel) {
        disposable.add(model.getLiked(songModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean aBoolean) {
                        view.setLiked(aBoolean);
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void setLiked(SongModel songModel) {
        disposable.add(model.setLiked(songModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean aBoolean) {
                         view.setLiked(aBoolean);
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onSearchVideosOnYouTube(SongModel track) {
        disposable.add(model.getSearchByKeyWordYoutube(track.getTitle()+" "+track.getArtist(), 1, YouTubeApi.TYPE_VIDEO,
                YouTubeApi.CATEGORY_MUSIC, YouTubeApi.SNIPPET, BuildConfig.YOUTUBE_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<SearchByKeywordModel>() {
                    @Override
                    public void onNext(SearchByKeywordModel searchByKeywordModel) {
                        if (searchByKeywordModel.getPageInfo().getTotalResults()>0) {
                            view.openFirstYouTubeVideo(searchByKeywordModel.getItems().get(0));
                        } else{
                            view.youTubeShowError(Core.getCoreInstance()
                                    .getString(R.string.nothing_found));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.youTubeShowError(Core.getCoreInstance()
                                .getString(R.string.connection_failed));
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void getCover(SongModel songModel) {
        disposable.add(model.getCover(songModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Bitmap>() {
                    @Override
                    public void onNext(Bitmap bitmap) {
                        view.showCover(bitmap);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showCover(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }


}
