package com.sx95od.livemusic.Dialogs.DialogSongMore;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions;
import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.Acts.MyActivity;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Dialogs.DialogSongMore.Presenter.Presenter;
import com.sx95od.livemusic.Dialogs.DialogSongMore.View.IView;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.FavSongs;
import com.sx95od.livemusic.Dialogs.MyDialog;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.SongListEvent;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.FragmentYouTubeShowVideo;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.Utils.MD5;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 20.07.2017.
 */

public class BottomSheetDialogSongFragment extends MyDialog implements IView {
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.artist) TextView artist;
    @BindView(R2.id.album) TextView album;
    @BindView(R2.id.cv_top)
    CardView _topCard;

    @BindView(R2.id.cv_content)
    CardView _contentCard;

    @BindView(R2.id.addtoplaylist_textview) TextView addtoplaylistText;
    @BindView(R2.id.next_textview) TextView nextText;
    @BindView(R2.id.share_textview) TextView shareText;
    @BindView(R2.id.cover) ImageView cover;
    @BindView(R2.id.like) ImageView like;
    @BindView(R2.id.share) ImageView share;
    @BindView(R2.id.delete) ImageView delete;
    @BindView(R2.id.add_to_playlist) ImageView addToPlaylist;
    @BindView(R2.id.next) ImageView next;
    @BindView(R2.id.fav_parent) FlexboxLayout parentFav;
    @BindView(R2.id.share_parent) FlexboxLayout parentShare;
    @BindView(R2.id.search_on_youtube_parent) FlexboxLayout _parentSearchYoutube;
    @BindView(R2.id.add_toplaylist_parent) FlexboxLayout parentAddToPlaylist;
    @BindView(R2.id.delete_parent) FlexboxLayout parentDelete;
    @BindView(R2.id.next_parent) FlexboxLayout parentNext;
    @BindView(R2.id.fav_textview) TextView favText;
    @BindView(R2.id.root) CoordinatorLayout root;
    @BindView(R2.id.top) LinearLayout top;

    ProgressDialog progressDialog;

    boolean liked = false;
    Thread thread;
    Unbinder unbinder;

    SongModel songModel;

    Presenter presenter;

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        presenter.onDestroy();
        super.onDestroyView();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.dialog_song_more, container, false);
        presenter = new Presenter(this);
        unbinder = ButterKnife.bind(this, contentView);

        readInfo(songModel);

        presenter.getLiked(songModel);

        return contentView;
    }


    public void bindSongModel(SongModel songModel){
        this.songModel = songModel;
    }


    @OnClick(R2.id.share_parent)
    public void onShareClick(){
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///"+songModel.getUri()));
        shareIntent.setType("audio/mp3");
        startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));
    }


    @OnClick(R2.id.next_parent)
    public void onNextClick(){
        EventBus.getDefault().post(new SongListEvent(SongListEvent.SongListEvents.ADD_SONG, songModel));
        Toast.makeText(getContext(), getString(R.string.song_was_added_to_the_queue), Toast.LENGTH_LONG).show();
    }

    public void readInfo(final SongModel song){
        presenter.getCover(song);

        title.setText(song.getTitle());
        artist.setText(song.getArtist());
        album.setText(song.getAlbum());

        parentFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int newId = getNextKey();
                try{
                    final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
                    if (liked) {
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmResults<FavSongs> results = realm.where(FavSongs.class).equalTo("_id", song.getId()).findAll();
                                results.deleteAllFromRealm();
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                like.setImageResource(R.drawable.unlike_dark);
                                favText.setText(getString(R.string.add_to_fav));
                                liked = false;
                                realm.close();
                            }
                        }, new Realm.Transaction.OnError() {
                            @Override
                            public void onError(Throwable error) {
                                realm.close();
                            }
                        });
                    } else {
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm bgRealm) {
                                FavSongs favSongs = bgRealm.createObject(FavSongs.class, newId);
                                favSongs.set_id(song.getId());
                                favSongs.setAlbum(song.getAlbum());
                                favSongs.setAlbum_id(song.getAlbum_id());
                                favSongs.setArtist(song.getArtist());
                                favSongs.setArtist_id(song.getArtist_id());
                                favSongs.setDate_modified(song.getDate());
                                favSongs.setDuration(song.getDuration());
                                favSongs.setTitle(song.getTitle());
                                favSongs.setUri(song.getUri());
                                favSongs.setYear(song.getYear());
                                favSongs.setMD5(MD5.checkMD5(song.getUri()));
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {

                                like.setImageResource(R.drawable.like_dark);
                                favText.setText(getString(R.string.remove_from_favs));
                                liked = true;
                                realm.close();
                            }
                        }, new Realm.Transaction.OnError() {
                            @Override
                            public void onError(Throwable error) {
                                // Transaction failed and was automatically canceled.
                                realm.close();
                                Log.i("RealmError", error.toString());
                                realm.close();

                            }
                        });
                    }

                } catch (Exception e){
                }
            }
        });
    }

    public int getNextKey() {
        Realm realm = null;
        int nextId = 0;
        try{
            realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
            Number maxId = realm.where(FavSongs.class).max("id");
            // If there are no rows, currentId is null, so the next id must be 1
            // If currentId is not null, increment it by 1
            nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
        }catch (Exception e){
            realm.close();
            return nextId;
        } finally {
            realm.close();
            return nextId;
        }
    }

    @OnClick({R2.id.search_on_youtube_parent}) void onClick(View view){
        switch (view.getId()){
            case R.id.search_on_youtube_parent:
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setTitle(getString(R.string.search));
                progressDialog.setMessage(getString(R.string.searching_track_on_youtube));
                progressDialog.show();
                presenter.onSearchVideosOnYouTube((SongModel) getArguments().getSerializable("adapter_song"));
                break;
        }
    }

    @Override
    public void setLiked(boolean liked) {
        if (liked){
            like.setImageResource(R.drawable.like_dark);
            favText.setText(getString(R.string.remove_from_favs));
        } else{
            like.setImageResource(R.drawable.unlike_dark);
            favText.setText(getString(R.string.add_to_fav));
        }
    }

    @Override
    public void openFirstYouTubeVideo(Item item) {
        progressDialog.dismiss();
        progressDialog = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable("youtube_item", item);
        ((MyActivity) getActivity()).hidePlayerWindow();
//        dismiss();
//        ((MyActivity) getActivity()).onRootBlurClick();
        FragmentYouTubeShowVideo fragmentYouTubeShowVideo = new FragmentYouTubeShowVideo();
        fragmentYouTubeShowVideo.setArguments(bundle);
        ((MyActivity) getActivity()).addFragment(fragmentYouTubeShowVideo);
    }

    @Override
    public void youTubeShowError(String errorMessage) {
        progressDialog.dismiss();
        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showCover(Bitmap bitmap) {
        Core.withGlide()
                .asBitmap()
                .load(bitmap)
                .transition(BitmapTransitionOptions.withCrossFade(300))
                .into(cover);
    }
}
