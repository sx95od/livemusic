package com.sx95od.livemusic.Dialogs.DialogSongMore.Model;

import android.graphics.Bitmap;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import io.reactivex.Observable;

/**
 * Created by stoly on 22.10.2017.
 */

public interface IModel {
    Observable<Boolean> getLiked(SongModel songModel);
    Observable<Boolean> setLiked(SongModel songModel);
    Observable<SearchByKeywordModel> getSearchByKeyWordYoutube(String q, int maxResults, String type, String categoryID, String part, String apiKey);
    Observable<Bitmap>  getCover(SongModel song);
}
