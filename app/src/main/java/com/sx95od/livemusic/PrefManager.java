package com.sx95od.livemusic;

import android.support.v4.content.ContextCompat;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Utils.SystemUtils;

import static com.sx95od.livemusic.Utils.Utils.getPxInDp;

/**
 * Created by stoly on 01.01.2018.
 */

public class PrefManager {
    public static String COLOR_ACCENT                 = "com.sx95od.livemusic.prefmanager.color_accent";
    public static String SONGS_LIST_INDEX             = "com.sx95od.livemusic.prefmanager.songs_list_index";
    public static String UI_FRAGMENT_CONTAINER_RADIUS = "com.sx95od.livemusic.prefmanager.ui_fragment_container_radius";


    public static void setColorAccent(int color){
        Core.getPref().edit().putInt(COLOR_ACCENT, color).commit();
    }

    public static int getColorAccent() {
        return Core.getPref().getInt(COLOR_ACCENT, ContextCompat.getColor(Core.getCoreInstance(), R.color.pink));
    }


    public static boolean getSongsListIndex() {
        return Core.getPref().getBoolean(SONGS_LIST_INDEX, true);
    }


    public static float getFragmentContainerRadius() {
        return Core.getPref().getFloat(UI_FRAGMENT_CONTAINER_RADIUS, 0f);
    }


    public static int getTopRegularActionBarHeight(){
        return SystemUtils.getStatusBarHeight() + SystemUtils.getRegularActionBarHeight();
    }

    public static int getBottomNavigationBarHeight(){
        int dpHeight = Core.getCoreInstance().getResources().getInteger(R.integer.bottom_navigation_lite);
        return getPxInDp(dpHeight);
    }

    public static int getBottomListPadding() {
        int bottomNavigationBarHeight = getPxInDp(Core.getCoreInstance().getResources().getInteger(R.integer.bottom_navigation_lite));
        return getPxInDp(60) + bottomNavigationBarHeight;
    }
}
