package com.sx95od.livemusic.Adapters.AdapterModels;

import java.io.Serializable;

/**
 * Created by stoly on 26.02.2017.
 */

public class GenreModel implements Serializable {
    int id;
    String genre;

    public GenreModel(int id, String genre){
        this.id = id;
        this.genre = genre;
    }

    public int getId(){
        return id;
    }

    public String getGenre(){
        return genre;
    }
}
