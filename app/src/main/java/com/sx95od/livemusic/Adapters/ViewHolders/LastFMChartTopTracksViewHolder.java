package com.sx95od.livemusic.Adapters.ViewHolders;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.API.LastFM.Models.ChartTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 30.08.2017.
 */

public class LastFMChartTopTracksViewHolder extends BaseViewHolder{
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.artist) TextView artist;
    @BindView(R2.id.lastfm_chart_top_tracks_adapter_listeners_textview) TextView listeners;
    @BindView(R2.id.cover) ImageView cover;
    @BindView(R2.id.fl_root)
    FrameLayout _root;

    Listeners.OnLastFMTopChartTracksItemClick onItemClickListener;
    Track track;


    public LastFMChartTopTracksViewHolder(View itemView, Listeners.OnLastFMTopChartTracksItemClick onItemClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.onItemClickListener = onItemClickListener;
        _root.getLayoutParams().width = Core.getWidthScreen() - Core.getCoreInstance().getResources().getDimensionPixelSize(R.dimen.dp12)*2;

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) onItemClickListener.onItemClick(track);
            }
        });
    }

    public void bindData(Track track) {
        this.track = track;

        title.setText(track.getName());
        artist.setText(track.getArtist().getName());
        listeners.setText(track.getListeners());
        GlideApp.with(Core.getCoreInstance())
                .load(track.getImage().get(track.getImage().size()-1).getText())
                .placeholder(R.drawable.lastfm_logo)
                .error(R.drawable.lastfm_logo)
                .override(512)
                .into(cover);
    }


    public ImageView getCover(){
        return cover;
    }
}
