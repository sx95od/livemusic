package com.sx95od.livemusic.Adapters.FolderAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Utils.CircleImageView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by stoly on 04.03.2017.
 */

public class FolderAdapter extends RecyclerView.Adapter<FolderAdapter.ViewHolder>{
    ArrayList<FolderFields> objects;
    Context ctx;


    public FolderAdapter(Context ctx, ArrayList<FolderFields> objects) {
        this.ctx = ctx;
        this.objects = objects;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView countFiles, path, title;

        public ViewHolder(View itemView) {
            super(itemView);

            countFiles = (TextView) itemView.findViewById(R.id.count_files);
            path = (TextView) itemView.findViewById(R.id.path);
            title = (TextView) itemView.findViewById(R.id.title);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.folder, parent, false);
        FolderAdapter.ViewHolder vh = new FolderAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(objects.get(position).getTitle());
        holder.path.setText(objects.get(position).getPath());
        holder.countFiles.setText(String.valueOf(objects.get(position).getCount())+" "+ctx.getString(R.string.files_count));

        File filePng = new File(objects.get(position).getPath()+"/icon.png");
        File fileJpg = new File(objects.get(position).getPath()+"/icon.jpg");

//        if (fileJpg.exists()){
//            Picasso.with(ctx).load(fileJpg).resize(150, 150).placeholder(R.drawable.folder_mini).into(holder.icon);
//        } else if (filePng.exists()){
//            Picasso.with(ctx).load(filePng).resize(150, 150).placeholder(R.drawable.folder_mini).into(holder.icon);
//        } else{
//            holder.icon.setImageResource(R.drawable.folder_mini);
//        }

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }


}
