package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.FragmentSearchMoreResults;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 23.09.2017.
 */

public class SearchYouTubeSearchVideoHeader extends RecyclerView.ViewHolder {
    private final String tag = "YTVideoHeader";
    @BindView(R2.id.matches) TextView _matches;
    @BindView(R2.id.textview_more_results) TextView _moreResults;

    Listeners.OnSearchAdapterListener onSearchAdapterListener;

    String query;


    public SearchYouTubeSearchVideoHeader(View itemView, Listeners.OnSearchAdapterListener onSearchAdapterListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.onSearchAdapterListener = onSearchAdapterListener;
        _moreResults.setOnClickListener(view -> {
            Log.d(tag, query);
            if (this.onSearchAdapterListener != null) {
                ArrayMap<String, Object> paramsMap = new ArrayMap<String, Object>();
                paramsMap.put(FragmentSearchMoreResults.TYPE_RESULTS, FragmentSearchMoreResults.YOUTUBE_VIDEO);
                paramsMap.put(FragmentSearchMoreResults.QUERY, query);

                onSearchAdapterListener.onYouTubeSeeAllClick(paramsMap);
//                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_SEARCH_MORE_RESULTS, paramsMap));
            }
        });
    }

    public void setQuery(String query){
        Log.d(tag, query);
        this.query = query;
    }

    public void setMatches(int count){
        _matches.setText(Core.getCoreInstance().getResources().getQuantityString(R.plurals.matches, count, count));
    }
}
