package com.sx95od.livemusic.Adapters.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.ViewHolders.AlbumSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.DividerViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.InternalAlbumViewHolder;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.R;

import java.util.List;

/**
 * Created by stoly on 10.08.2017.
 */

public class AlbumAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    List<AlbumModel> albumObjects;

    OnAlbumItemClickListener onAlbumItemClickListener;

    public AlbumAdapter(List<AlbumModel> albumObjects, OnAlbumItemClickListener onAlbumItemClickListener) {
        this.onAlbumItemClickListener = onAlbumItemClickListener;
        this.albumObjects = albumObjects;
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof InternalAlbumViewHolder)
            GlideApp.with(Core.getCoreInstance()).clear(((InternalAlbumViewHolder) holder).getCover());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        RecyclerView.ViewHolder vh = null;

        if (viewType == 0) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_internal_album_v2, parent, false);
            vh = new InternalAlbumViewHolder(v, onAlbumItemClickListener);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider_list_margined, parent, false);
            vh = new DividerViewHolder(v, parent.getContext().getResources().getDimensionPixelSize(R.dimen.dp122), 0);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof InternalAlbumViewHolder) ((InternalAlbumViewHolder) holder).bindData(albumObjects.get(position/2));
    }

    public interface OnAlbumItemClickListener{
        public void onClickListener(AlbumModel object);
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2 == 0 ? 0 : 1;
    }

    @Override
    public int getItemCount() {
        return albumObjects.size()*2-1;
    }
}