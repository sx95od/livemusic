package com.sx95od.livemusic.Adapters.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.Adapters.ViewHolders.ViewHolderMenu;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 20.11.2017.
 */

public class AdapterMenu extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Menu menu;

    public AdapterMenu(Menu menu) {
        this.menu = menu;
        Log.d("mymenu", ""+menu.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_menu, parent, false);
        ViewHolderMenu vh = new ViewHolderMenu(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolderMenu) holder).bindItem(menu.getItem(position));
    }

    @Override
    public int getItemCount() {
        return menu.size();
    }
}
