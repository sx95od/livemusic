package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by stoly on 25.08.2017.
 */

public class DividerViewHolder extends RecyclerView.ViewHolder{
    public DividerViewHolder(View itemView, int leftMargin, int rightMargin) {
        super(itemView);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) itemView.getLayoutParams();
        layoutParams.setMargins(leftMargin, 0, rightMargin, 0);
    }

    public DividerViewHolder(View itemView){
        super(itemView);
    }


}
