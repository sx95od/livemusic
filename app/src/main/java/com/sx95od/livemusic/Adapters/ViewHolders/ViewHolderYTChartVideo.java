package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.target.Target;
import com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.Item;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 11.11.2017.
 */

public class ViewHolderYTChartVideo extends BaseViewHolder {
    Listeners.OnYTChartVideoListener ytChartVideoListener;
    Item item;

    @BindView(R2.id.tv_title)
    TextView _title;

    @BindView(R2.id.tv_channel)
    TextView _channel;

    @BindView(R2.id.iv_thumb)
    ImageView _thumb;

    @BindView(R2.id.fl_root)
    FrameLayout _root;


    public ViewHolderYTChartVideo(View itemView, Listeners.OnYTChartVideoListener ytChartVideoListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.ytChartVideoListener = ytChartVideoListener;
        _root.getLayoutParams().width = Core.getWidthScreen() - Core.getCoreInstance().getResources().getDimensionPixelSize(R.dimen.dp12)*2;

        itemView.setOnClickListener(view -> {
            if (ytChartVideoListener != null){
                ytChartVideoListener.onVideoClickListener(item);
            }
        });
    }

    public void bindData(Item item){
        this.item = item;
        _channel.setText(item.getSnippet().getChannelTitle());
        _title.setText(item.getSnippet().getTitle());

        GlideApp.with(Core.getCoreInstance())
                .load(item.getSnippet().getThumbnails().getMaxres() == null ?
        item.getSnippet().getThumbnails().getHigh().getUrl() : item.getSnippet().getThumbnails().getMaxres().getUrl())
                .placeholder(R.drawable.youtube_noimg)
                .error(R.drawable.youtube_noimg)
                .override(640)
                .into(_thumb);
    }

    public void reBindData(){
        _channel.setText(item.getSnippet().getChannelTitle());
        _title.setText(item.getSnippet().getTitle());

        GlideApp.with(Core.getCoreInstance())
                .load(item.getSnippet().getThumbnails().getMaxres() == null ?
                        item.getSnippet().getThumbnails().getHigh().getUrl() : item.getSnippet().getThumbnails().getMaxres().getUrl())
                .placeholder(R.drawable.youtube_noimg)
                .error(R.drawable.youtube_noimg)
                .override(512)
                .into(_thumb);
    }

    public ImageView getCover(){
        return _thumb;
    }
}
