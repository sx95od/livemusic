package com.sx95od.livemusic.Adapters.HomeSongAdapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.sx95od.livemusic.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by stoly on 22.02.2017.
 */

public class HomeSongAdapter extends RecyclerView.Adapter<HomeSongAdapter.ViewHolder>{
    ArrayList<HomeSongFields> objects;
    Context ctx;
    int pos=-1;
    int flag=0;
    int posHolder;
    HomeSongAdapter.ViewHolder holder;
    Handler handler = new Handler();

    int cacheSize = 8 * 1024 * 1024; // 8MiB
    LruCache<String, Bitmap> bitmapCache = new LruCache<String, Bitmap>(cacheSize) {
        protected int sizeOf(String key, Bitmap value) {
            return value.getByteCount();
        }
    };



    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView art;
        public TextView title;
        public LoadArt loadArt;
//        public TextView artist;
//        public TextView duration;
//        public ImageView state;
//        public CoordinatorLayout item;

        public ViewHolder(View v, final Context ctx, final Handler handler) {
            super(v);
            art = (ImageView) v.findViewById(R.id.art);
            title = (TextView) v.findViewById(R.id.title);
            loadArt = new LoadArt(art);
        }

        public ImageView getArt(){
            return art;
        }
    }

    public HomeSongAdapter(ArrayList<HomeSongFields> objects, Context ctx, int flag){
        this.objects = objects;
        this.ctx = ctx;
    }



    @Override
    public HomeSongAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_song, parent, false);
        HomeSongAdapter.ViewHolder vh = new HomeSongAdapter.ViewHolder(null, ctx, handler);
        return vh;
    }


    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            bitmapCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return bitmapCache.get(key);
    }

    @Override
    public long getItemId(int position) {
        return objects.get(position).hashCode();
    }

    @Override
    public void onBindViewHolder(final HomeSongAdapter.ViewHolder holder, final int position) {
        holder.title.setText(objects.get(position).getTitle());

        if(holder.loadArt != null) {
            holder.loadArt.cancel(true);
            holder.loadArt = (LoadArt) new LoadArt(holder.art).execute(objects.get(position).getUri());
        }

//        new LoadArt(new WeakReference<ImageView>(holder.art)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, objects.get(position).getUri());

    }


    @Override
    public void onViewRecycled(HomeSongAdapter.ViewHolder holder) {
        holder.getArt().setImageResource(0);
        super.onViewRecycled(holder);
    }

    public void setCurrPlaying(int pos){
        this.pos = pos;
    }

    public int getCurrPlaying(){
        return pos;
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }



    private class LoadArt extends AsyncTask<String, Void, Bitmap>{

        WeakReference<ImageView> imageView;

        public LoadArt(ImageView imageView) {
            this.imageView=new WeakReference<ImageView>(imageView);
        }

        @Override
        protected void onPreExecute() {
            ImageView imageView = this.imageView.get();
//            imageView.setImageResource(R.drawable.note_mini);
            super.onPreExecute();
        }


        @Override
        protected Bitmap doInBackground(String... params) {
            if (getBitmapFromMemCache(params[0])!=null){
                return getBitmapFromMemCache(params[0]);
            } else {
                Bitmap bitmap = null;
                try {
                    MediaMetadataRetriever mData = new MediaMetadataRetriever();
                    mData.setDataSource(params[0]);
//                    bitmap = Glide.with(ctx).load(mData.getEmbeddedPicture()).asBitmap().into(240, 240).get();
                    bitmap = decodeSampledBitmapFromResource(mData.getEmbeddedPicture(), 192, 192);
                    addBitmapToMemoryCache(params[0], bitmap);
                } catch (Exception e) {

                }
                return bitmap;
            }
        }

        public Animation imageAnim(){
            return AnimationUtils.loadAnimation(ctx, R.anim.image_view);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(!isCancelled()) {
                ImageView imageView = this.imageView.get();

                try {
                    if (bitmap != null) {
                        Log.i("AlbumAdapter", "+");
                        imageView.setImageBitmap(bitmap);
                    } else {
                        Log.i("AlbumAdapter", "-");
//                        imageView.setImageResource(R.drawable.note_mini);
                    }
                } catch (Exception e) {

                }
            }
            super.onPostExecute(bitmap);
        }

        public Bitmap decodeSampledBitmapFromResource(byte[] array,
                                                      int reqWidth, int reqHeight) {

            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(array, 0, array.length, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(array, 0, array.length, options);
        }

        public int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }
    }
}