package com.sx95od.livemusic.Adapters.ViewHolders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.annotation.WorkerThread;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.PlayerEvent;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.ThemeViews.STextView;
import com.sx95od.livemusic.Utils.Preload;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 24.08.2017.
 */

public class SongViewHolder extends RecyclerView.ViewHolder{
    @BindView(R2.id.art) ImageView art;
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.artist) TextView artist;
    @BindView(R2.id.duration) TextView duration;
    @BindView(R2.id.state) ImageView state;
    @BindView(R2.id.playing) ImageView playing;
    @BindView(R2.id.add) ImageButton add;
    @BindView(R2.id.cover_block) CardView coverBlock;
    @BindView(R2.id.tv_song_index) STextView _index;

    private SongAdapter.OnItemClickListener onItemClickListener = null;
    private SongAdapter.OnItemMoreClickListener onItemMoreClickListener = null;
    private SongAdapter.OnShuffleClickListener onShuffleClickListener = null;
    Listeners.OnLocalSongItemClickListener onSongClickListener = null;

    private int playingPos = -1;

    private List<SongModel> objects;


    public int position;
    public int subFlag;
    public SongModel songField;

    public LoadArt loadArt;
    public Thread thread;

    /**args values
     * args[0] - COUNT_BUTTONS
     * args[1] - FLAG**/
    public SongViewHolder(View v,
                          final SongAdapter.OnItemClickListener onItemClickListener,
                          final SongAdapter.OnItemMoreClickListener onItemMoreClickListener,
                          final Listeners.OnLocalSongItemClickListener onSongClickListener,
                          final int... args) {
        super(v);
        ButterKnife.bind(this, v);
        Context ctx = v.getContext();
        this.onItemMoreClickListener = onItemMoreClickListener;
        this.onItemClickListener = onItemClickListener;
        this.onSongClickListener = onSongClickListener;


//        v.setOnLongClickListener(view -> {
//            try {
//                String[] cmd = {"-i", getSongField().getUri(), "-metadata", "title=Проверка тайтл", "-metadata", "artist=Проверка Артист", "-c", "copy", getSongField().getUri()};
//                FFmpeg.getInstance(Core.getCoreInstance()).execute(cmd, new FFmpegExecuteResponseHandler() {
//                    @Override
//                    public void onStart() {
//                        Log.d("testFFMPEG", "OnStart");
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        Log.d("testFFMPEG", "OnStart");
//                    }
//
//                    @Override
//                    public void onSuccess(String s) {
//                        Log.d("testFFMPEG success", s);
//                    }
//
//                    @Override
//                    public void onProgress(String s) {
//                        Log.d("testFFMPEG progress", s);
//                    }
//
//                    @Override
//                    public void onFailure(String s) {
//                        Log.d("testFFMPEG fail", s);
//                    }
//                });
//            } catch (FFmpegCommandAlreadyRunningException e) {
//                e.printStackTrace();
//            }
//            return false;
//        });

        playing.setColorFilter(PrefManager.getColorAccent());

        if (((Core) ctx.getApplicationContext()).getDarkTheme() == R.style.AppThemeDark){
            title.setTextColor(ContextCompat.getColor(ctx, R.color.text_white));
            artist.setTextColor(ContextCompat.getColor(ctx, R.color.text_light));
            duration.setTextColor(ContextCompat.getColor(ctx, R.color.text_light));
            add.setColorFilter(ContextCompat.getColor(ctx, R.color.light));
            v.setBackgroundResource(R.color.dark);
        }

        v.setOnClickListener(v1 -> {
            if (onItemClickListener!=null){
                onItemClickListener.onItemClickListener(getSongField(), getSubFlag(), getPos()-args[0]);
                Log.d("SongVH", "<OnClick> was called interface of item click listener");
            }
            if (onSongClickListener != null){
                onSongClickListener.onSongItemClick(getPos());
            }
            if (subFlag != SongAdapter.FLAG_ADDING_TO_PLAYLIST){
                if (subFlag == SongAdapter.FLAG_CURRENT_LIST){
                    EventBus.getDefault().post(new PlayerEvent(PlayerEvent.SET_SONG_FROM_CURRENT_LIST, getPos()));
                }else {
                    Log.d("SongVH", "<OnClick> was selected item from songs list");
                    EventBus.getDefault().post(new PlayerEvent(PlayerEvent.SET_SONG_FROM_LIST, getPos(), getObjects()));
                    EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_EXPAND_BOTTOM_PLAYER_CONTROL));
                }
            }
        });

        loadArt = new LoadArt(art);

        switch (args[1]){
            case SongAdapter.FLAG_SET_OFF_MORE:
                getAdd().setVisibility(View.GONE);
                getPlaying().setVisibility(View.GONE);
                getDuration().setVisibility(View.GONE);
                break;
            case SongAdapter.FLAG_ADDING_TO_PLAYLIST:
                getAdd().setVisibility(View.VISIBLE);
                getPlaying().setVisibility(View.GONE);
                getDuration().setVisibility(View.GONE);
                getAdd().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean selected = false;
                        if (songField.getSelected()){
                            selected = false;
                            getAdd().setImageResource(R.drawable.add);
                            EventBus.getDefault().post(new FragmentEvent(FragmentEvent.PLAYLIST_REMOVE_ITEM, songField));
                        } else{
                            selected = true;
                            getAdd().setImageResource(R.drawable.check);
                            EventBus.getDefault().post(new FragmentEvent(FragmentEvent.PLAYLIST_ADD_ITEM, songField));
                        }
                        songField.changeSelected(selected);
//                        notifyItemChanged(getPos()*2+1);
                        Log.d("SongVH", "added "+getPos()+" position");
                    }
                });
                break;
            case SongAdapter.FLAG_CURRENT_LIST:
                getAdd().setVisibility(View.GONE);
                coverBlock.setVisibility(View.GONE);
                getPlaying().setVisibility(View.GONE);
                getDuration().setVisibility(View.VISIBLE);
                break;
            case SongAdapter.FLAG_ALBUM_SONGS:
                getAdd().setVisibility(View.GONE);
                getPlaying().setVisibility(View.GONE);
                coverBlock.setVisibility(View.GONE);
                v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        if (onItemMoreClickListener != null) {
//                            Log.d("SA.getMoreListener", "size "+objects.size()+", pos "+getPos());
                            onItemMoreClickListener.onItemOptionClickListener(songField);
                        }
                        return true;
                    }
                });
                break;
            default:
                getAdd().setVisibility(View.GONE);
                getPlaying().setVisibility(View.GONE);
                getDuration().setVisibility(View.GONE);

                v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        if (onSongClickListener != null) {
                            onSongClickListener.onSongItemLongClick(position);
                        }
                        return true;
                    }
                });
                break;
        }
    }

    public void bindObjects(final List<SongModel> objects){
        this.objects = objects;
    }

    private List<SongModel> getObjects(){
        return objects;
    }

    public void bindData(final SongModel data, int position, int flag, int playingPos){
        Log.d("SongVH", "<Binding data> flag is "+flag);

        if (PrefManager.getSongsListIndex()) _index.setText(String.valueOf(position+1));

        songField = data;
        String title = data.getTitle();
        String artist = data.getArtist();
        try {
            title = new String(title.getBytes(), StandardCharsets.UTF_8);
            artist = new String(artist.getBytes(), StandardCharsets.UTF_8);
        } catch (Exception e) {

        }
        long c = System.currentTimeMillis();
        setSongField(data);
        setPos(position);
        setSubFlag(flag);
        if (data.getSelected()) {
            getAdd().setImageResource(R.drawable.check);
        } else {
            getAdd().setImageResource(R.drawable.add);
        }


        if (getDuration().getVisibility() == View.VISIBLE) {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    final String duration = setTime(data.getDuration());
                    getDuration().post(new Runnable() {
                        @Override
                        public void run() {
                            getDuration().setText(duration);
                        }
                    });
                }
            });
            thread.setPriority(Thread.NORM_PRIORITY);
            thread.start();
        }
        if (flag == SongAdapter.FLAG_CURRENT_LIST) {
            if (playingPos == position) {
                getPlaying().setVisibility(View.VISIBLE);
                _index.setVisibility(View.GONE);
            } else {
                getPlaying().setVisibility(View.GONE);
                _index.setVisibility(View.VISIBLE);
            }
        } else {
            if (getLoadArt() != null) {
                getLoadArt().cancel(true);
                newLoadArt(getArt(), data.getUri());
            }
//            GlideApp.with(Core.getCoreInstance()).load(data.getU)
        }

        getTitle().setText(title);
        getArtist().setText(artist);

        c = System.currentTimeMillis() - c;
        Log.d(getClass().getSimpleName() + ".onBindView", "pos " + position + " loaded for " + c + " ms");
    }



    @WorkerThread
    private String setTime(int millSec){
        String dur;
        int sec = millSec/1000;
        if (sec%60>9){
            dur = String.valueOf(Math.round(sec/60)+":"+sec%60);
        } else{
            dur = String.valueOf(Math.round(sec/60)+":"+"0"+sec%60);
        }

        final String duration = dur;
        return duration;
    }

    public void newLoadArt(ImageView art, String uri){
        loadArt = (SongViewHolder.LoadArt) new SongViewHolder.LoadArt(art).execute(uri);
    }

    public void setPlayingPos(int playingPos){
        this.playingPos = playingPos;
    }

    public void setSongField(SongModel songField){
        this.songField = songField;
    }

    public SongModel getSongField(){
        return songField;
    }

    public void setSubFlag(int subFlag){
        this.subFlag = subFlag;
    }

    public int getSubFlag(){
        return subFlag;
    }

    public LoadArt getLoadArt(){
        return loadArt;
    }

    public void setPos(int position){
        this.position = position;
    }

    public int getPos(){
        return position;
    }

    public ImageView getArt(){
        return art;
    }

    public CardView getCoverBlock(){
        return coverBlock;
    }

    public ImageView getPlaying(){
        return playing;
    }

    public ImageButton getAdd(){
        return add;
    }

    public TextView getDuration(){
        return duration;
    }

    public TextView getTitle(){
        return title;
    }

    public TextView getArtist(){
        return artist;
    }

    public Thread getThread(){
        return thread;
    }


    public class LoadArt extends AsyncTask<String, Void, Bitmap> {

        WeakReference<ImageView> imageView;

        public LoadArt(ImageView imageView) {
            this.imageView=new WeakReference<ImageView>(imageView);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ImageView imageView = this.imageView.get();
            GlideApp.with(Core.getCoreInstance()).load(Core.getCoreInstance().getDarkTheme() == 0 ? R.drawable.noimg2light :
                    R.drawable.noimg2dark)
                    .override(130)
                    .centerCrop()
                    .into(imageView);
//            GlideApp.with(Core.getCoreInstance())
//                    .load(R.drawable.noimg2)
//                    .override(128)
//                    .skipMemoryCache(true)
//                    .into(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap img = null;
            try {
                MediaMetadataRetriever mData = new MediaMetadataRetriever();
                mData.setDataSource(params[0]);
                img = decodeSampledBitmapFromResource(mData.getEmbeddedPicture(), 130, 130);
                mData.release();
            } catch (Exception e) {
                Log.i("Error", e.toString());
            }
            return img;

        }

        @Override
        protected void onPostExecute(Bitmap img) {
            if(!isCancelled()) {
                try {
                    ImageView art = this.imageView.get();
                    if (img != null) {
//                        GlideApp.with(Core.getCoreInstance())
//                                .load(img)
//                                .skipMemoryCache(true)
//                                .into(art);
                        Core.withGlide().asBitmap()
                                .load(img)
                                .placeholder(Preload.getImageMiniSongPlaceholder())
                                .centerCrop()
                                .transition(BitmapTransitionOptions.withCrossFade(320))
                                .into(art);
                    }
                }catch (Exception e){

                }
            }
            super.onPostExecute(img);
        }

        public Bitmap decodeSampledBitmapFromResource(byte[] array,
                                                      int reqWidth, int reqHeight) {

            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(array, 0, array.length, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(array, 0, array.length, options);
        }

        public int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }
    }
}
