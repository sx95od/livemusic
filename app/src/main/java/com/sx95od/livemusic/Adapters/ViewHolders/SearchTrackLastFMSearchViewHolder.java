package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sx95od.livemusic.API.LastFM.Models.SearchTrackModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.Track;
import com.sx95od.livemusic.Adapters.Adapters.SearchAdapter;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 29.08.2017.
 */

public class SearchTrackLastFMSearchViewHolder extends RecyclerView.ViewHolder {
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.artist) TextView artist;
    @BindView(R2.id.art) ImageView cover;
    @BindView(R2.id.listeners) TextView listeners;
    SearchAdapter.OnItemClickListener onItemClickListener;
    Track object;

    public SearchTrackLastFMSearchViewHolder(View itemView, final SearchAdapter.OnItemClickListener onItemClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.onItemClickListener = onItemClickListener;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener!=null){
                    onItemClickListener.onItemClickLister(object);
                }
            }
        });
    }

    public void bindData(Track object){
        this.object = object;
        GlideApp.with(Core.getCoreInstance())
                .load(object.getImage().get(object.getImage().size()-1).getText())
                .placeholder(R.drawable.noimg)
                .error(R.drawable.noimg)
                .skipMemoryCache(true)
                .into(cover);
        title.setText(object.getName());
        artist.setText(object.getArtist());
        listeners.setText(String.valueOf(object.getListeners()));
    }

    public ImageView getCover(){
        return cover;
    }
}
