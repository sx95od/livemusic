package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.Track;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 01.10.2017.
 */

public class ViewHolderLastFMGeoTopChartTracks extends BaseViewHolder {
    @BindView(R2.id.cover) ImageView _cover;
//    @BindView(R2.id.tv_lastfm_geo_chart_top_tracks_adapter_listeners) TextView _listeners;
    @BindView(R2.id.tv_title) TextView _title;
    @BindView(R2.id.tv_artist) TextView _artist;
    @BindView(R2.id.fl_root) FrameLayout _root;

    Track track;


    public ViewHolderLastFMGeoTopChartTracks(View itemView, Listeners.OnLastFMGeoTopChartTracksItemClick onLastFMGeoTopChartTracksItemClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
//        _root.getLayoutParams().width = Core.getWidthScreen() - Core.getCoreInstance().getResources().getDimensionPixelSize(R.dimen.dp18)*2;
        _root.getLayoutParams().width = Core.getWidthScreen()/2-itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dp12);
        _cover.getLayoutParams().height = _root.getLayoutParams().width-itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dp12);
        Log.d("visota", _root.getLayoutParams().width+"");
//        _root.getLayoutParams().height = _root.getLayoutParams().height/2;

        itemView.setOnClickListener(view -> {
            if (onLastFMGeoTopChartTracksItemClick != null){
                onLastFMGeoTopChartTracksItemClick.onItemClick(track);
            }
        });
    }

    public ImageView getCover(){
        return _cover;
    }


    public void bindData(Track track){
        this.track = track;
        _title.setText(track.getName());
        _artist.setText(track.getArtist().getName());
//        _listeners.setText(track.getListeners());
        GlideApp.with(Core.getCoreInstance())
                .load(track.getImage().get(track.getImage().size()-1).getText())
                .placeholder(R.drawable.lastfm_logo)
                .error(R.drawable.lastfm_logo)
                .override(392)
                .into(_cover);
    }
}
