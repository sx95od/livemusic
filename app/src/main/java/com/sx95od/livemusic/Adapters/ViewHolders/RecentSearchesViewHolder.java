package com.sx95od.livemusic.Adapters.ViewHolders;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.method.Touch;
import android.util.Log;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.Adapters.Adapters.RecentSearchesAdapter;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.SearchHistory;
import com.sx95od.livemusic.Events.AppEvent;
import com.sx95od.livemusic.Events.RecentListEvent;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.EventBusException;
import org.greenrobot.eventbus.Subscribe;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 27.08.2017.
 */

public class RecentSearchesViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener{
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.delete_flex) FlexboxLayout deleteFlex;
    @BindView(R2.id.frame_root) FrameLayout frameRoot;
    @BindView(R2.id.delete) ImageButton delete;

    private final int speedAnimation = 150;
    private final float multiplier = 1.25f;

    public final String tag = "RecentViewHolder";
    RecentSearchesAdapter.OnItemClickListener onItemClickListener;
    boolean moving = false;
    boolean showed = false;

    float startPoint = 0;
    float sensitive = 0;
    float defaultTranslation = 0;


    int pos = -1;
    String query;

    public RecentSearchesViewHolder(final View itemView, final RecentSearchesAdapter.OnItemClickListener onItemClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
//        EventBus.getDefault().register(this);

        sensitive = (float) Core.getCoreInstance().getResources().getDimension(R.dimen.slide_delete);
        defaultTranslation = (float) Core.getCoreInstance().getResources().getDimension(R.dimen.recent_searches_delete_translation);

        Log.d(tag, "start translation is " + deleteFlex.getTranslationX());


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFromRecent(query, getPos());
                EventBus.getDefault().post(new RecentListEvent(RecentListEvent.HIDE_BUTTONS, -1));
            }
        });


        title.setTextColor(PrefManager.getColorAccent());
        this.onItemClickListener = onItemClickListener;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RecentSearchesViewHolder.this.onItemClickListener != null) {
                    RecentSearchesViewHolder.this.onItemClickListener.OnItemClickListener(title.getText().toString());
                }
            }
        });

//        itemView.setOnTouchListener(this);
    }

    @Subscribe
    public void onEvent(RecentListEvent event){
        switch (event.getCode()){
            case RecentListEvent.HIDE_BUTTONS:
                if (event.getPos()!=getPos()){
                    deleteFlex.animate().translationX(defaultTranslation).setDuration(speedAnimation)
                            .withEndAction(() -> deleteFlex.setVisibility(View.GONE)).start();
                }
                break;
        }
    }

    public int getPos(){
        return pos;
    }

    private void deleteFromRecent(final String text, final int pos){
        final Realm realm = Realm.getInstance(Core.getConfigRealm());
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<SearchHistory> results = realm.where(SearchHistory.class)
                        .equalTo("query", text).findAll();
                results.deleteAllFromRealm();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new RecentListEvent(RecentListEvent.REMOVE_ITEM, pos));
                realm.close();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                realm.close();
            }
        });
    }


    public void bindData(String query, int pos){
        title.setText(query);
        this.query = new String(query);
        this.pos = pos;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                EventBus.getDefault().post(new RecentListEvent(RecentListEvent.HIDE_BUTTONS, getPos()));
                Log.d(tag, "<OnTouch> ACTION_DOWN; Position is "+pos+"; TranslationX is "+deleteFlex.getTranslationX());
                deleteFlex.setVisibility(View.VISIBLE);
                startPoint = (float) event.getX();
                moving = false;
                if (deleteFlex.getTranslationX()==0) showed = true;
                return false;
            case MotionEvent.ACTION_MOVE:
                Log.d(tag, "<OnTouch> ACTION_MOVE; Start point is "+startPoint+"; get x is "+event.getX());
                moving = true;
                if (event.getX() <= startPoint && !showed){
                    Log.d(tag, "<OnTouch> ACTION_MOVE; Delete button is not visible");
                    if (defaultTranslation - (startPoint - event.getX())/multiplier>0) {
                        deleteFlex.setTranslationX(defaultTranslation - (startPoint - event.getX())/multiplier);
                    } else{
                        deleteFlex.setTranslationX(0);
                    }
                } else if (event.getX() >= startPoint && showed){
                    Log.d(tag, "<OnTouch> ACTION_MOVE; Delete button is visible");
                    if (defaultTranslation - (startPoint - event.getX())/multiplier>0) {
                        deleteFlex.setTranslationX((event.getX() - startPoint)/multiplier);
                    } else{
                        deleteFlex.setTranslationX(0);
                    }
                }
                return true;
            default:
                frameRoot.invalidate();
                startPoint = 0;
                showed = false;
                if (deleteFlex.getTranslationX()<defaultTranslation/2){
                    deleteFlex.animate().translationX(0).setDuration(speedAnimation).start();
                } else{
                    deleteFlex.animate().translationX(defaultTranslation).setDuration(speedAnimation).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            deleteFlex.setVisibility(View.GONE);
                        }
                    }).start();
                }
                if (moving) return true; else return false;
        }
    }

    public void unregisterEventBus() {
        try {
            EventBus.getDefault().unregister(this);
        }catch (EventBusException e){

        }
    }
}
