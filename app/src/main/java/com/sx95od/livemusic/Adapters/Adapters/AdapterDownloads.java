package com.sx95od.livemusic.Adapters.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.Adapters.AdapterModels.ModelDownloads;
import com.sx95od.livemusic.Adapters.ViewHolders.ViewHolderDownloads;
import com.sx95od.livemusic.R;

import java.util.List;

/**
 * Created by stoly on 03.09.2017.
 */

public class AdapterDownloads extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<ModelDownloads> objects;


    public AdapterDownloads(List<ModelDownloads> objects) {
        this.objects = objects;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_downloads, parent, false);
        vh = new ViewHolderDownloads(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolderDownloads) holder).bindData(objects.get(position), position);
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }
}
