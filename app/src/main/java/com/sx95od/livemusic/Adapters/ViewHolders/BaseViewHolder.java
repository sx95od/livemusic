package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sx95od.livemusic.Utils.ThemeEngine;

/**
 * Created by stoly on 04.01.2018.
 */

public class BaseViewHolder extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
    }
}
