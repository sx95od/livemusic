package com.sx95od.livemusic.Adapters.Adapters;

import android.content.Context;
import android.support.annotation.UiThread;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Adapters.ViewHolders.DividerViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SongViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.ViewHolderAlbumTop;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.PlayerEvent;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 22.02.2017.
 */

public class SongAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //flags for tune item view elements
    public static final int FLAG_SET_OFF_MORE = 1;
    public static final int FLAG_ADDING_TO_PLAYLIST = 2;
    public static final int FLAG_VISIBLE_COUNT = 3;
    public static final int FLAG_CURRENT_LIST = 4;
    public static final int FLAG_SEARCH_LIST = 5;
    public static final int FLAG_ALBUM_SONGS = 6;

    //types for item format
    private static final int TYPE_NORMAL_ITEM = 0;
    private static final int TYPE_BUTTON_ITEM = 1;
    private static final int TYPE_DIVIDER = 2;
    private static final int TYPE_ALBUM_SONGS = 3;

    //count of buttons in top items of recyclerview
    private int COUNT_BUTTONS = 1;


    private OnItemClickListener onItemClickListener = null;
    private OnItemMoreClickListener onItemMoreClickListener = null;
    private OnShuffleClickListener onShuffleClickListener = null;
    private Listeners.OnLocalSongItemClickListener onSongClickListener = null;

    private AlbumModel albumTop;


    List<SongModel> objects;

    int pos=-1;
    int firsPos = 0;
    int lastPos = 0;
    int flag = 0;
    int count = 0;
    boolean liked = false;

    public void setOnItemClickListener(Listeners.OnLocalSongItemClickListener onItemClickListener){
        this.onSongClickListener = onItemClickListener;
    }


    @Deprecated
    public void setOnItemMoreClickListener(OnItemMoreClickListener onItemMoreClickListener){
        this.onItemMoreClickListener = onItemMoreClickListener;
    }


    public void setOnShuffleClickListener(OnShuffleClickListener onShuffleClickListener){
        this.onShuffleClickListener = onShuffleClickListener;
    }


    public void setAlbumTop(AlbumModel albumTop){
        this.albumTop = albumTop;
    }


    public String getFirstLetter(int pos){
        return objects.get(pos).getTitle().substring(0, 1);
    }


    public class ViewHolderTopButtons extends RecyclerView.ViewHolder{
        @BindView(R2.id.icon) ImageView icon;
        @BindView(R2.id.text) TextView text;

        public ViewHolderTopButtons(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            text.setTextColor(PrefManager.getColorAccent());
            icon.setColorFilter(PrefManager.getColorAccent());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onShuffleClickListener != null){
                        onShuffleClickListener.onShuffleClickListener();
                    }
                }
            });
        }
    }


    public void changeTrackList(List<SongModel> newTrackList){
        this.objects = null;
        this.objects = newTrackList;
        notifyDataSetChanged();
    }

    public SongAdapter(List<SongModel> objects, int flag, OnItemMoreClickListener onItemMoreClickListener,
                       Listeners.OnLocalSongItemClickListener onSongClickListener){
        WeakReference<List<SongModel>> listWeakReference = new WeakReference<List<SongModel>>(objects);
        this.objects = listWeakReference.get();
        this.flag = flag;
        this.onItemMoreClickListener = onItemMoreClickListener;
        this.onSongClickListener = onSongClickListener;
        if (flag == FLAG_CURRENT_LIST){
            COUNT_BUTTONS = 0;
        }
    }

    public SongAdapter(int flag){
        this.flag = flag;
    }

    public SongAdapter(List<SongModel> objects, int flag, OnItemMoreClickListener onItemMoreClickListener){
        WeakReference<List<SongModel>> listWeakReference = new WeakReference<List<SongModel>>(objects);
        this.objects = listWeakReference.get();
        this.flag = flag;
        this.onItemMoreClickListener = onItemMoreClickListener;
        if (flag == FLAG_CURRENT_LIST){
            COUNT_BUTTONS = 0;
        }
    }

    public SongAdapter(List<SongModel> objects, int flag, int count, OnItemMoreClickListener onItemMoreClickListener){
        WeakReference<List<SongModel>> listWeakReference = new WeakReference<List<SongModel>>(objects);
        this.objects = listWeakReference.get();
        this.flag = flag;
        this.count = count;
        this.onItemMoreClickListener = onItemMoreClickListener;
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        if (viewType == TYPE_NORMAL_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_song, parent, false);
            vh = new SongViewHolder(v, onItemClickListener, onItemMoreClickListener, onSongClickListener, COUNT_BUTTONS, flag);
        } else if (viewType==TYPE_BUTTON_ITEM){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_songs_top_buttons, parent, false);
             vh = new ViewHolderTopButtons(v);
        } else if (viewType == TYPE_DIVIDER){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider_list_margined, parent, false);
            vh = new DividerViewHolder(v, parent.getContext().getResources().getDimensionPixelSize(R.dimen.dp78), 0);
        } else if (viewType == TYPE_ALBUM_SONGS){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_song_album_top, parent, false);
            vh = new ViewHolderAlbumTop(v, onSongClickListener);
        }
        return vh;
    }

    @Override
    public int getItemViewType(int position) {
//        int type = TYPE_NORMAL_ITEM;
//        if (position < COUNT_BUTTONS && count==0 && (flag!=FLAG_CURRENT_LIST && flag!=FLAG_ADDING_TO_PLAYLIST)) {
//            type = TYPE_BUTTON_ITEM;
//        } else if (position % 2 == 0 && count==0 && flag!=FLAG_CURRENT_LIST){
//            type = TYPE_DIVIDER;
//        } else if (position % 2 !=0 && count!=0){
//            type = TYPE_DIVIDER;
//        }
        if (flag == FLAG_ALBUM_SONGS){
            if (position == 0) return TYPE_ALBUM_SONGS;
            else return TYPE_NORMAL_ITEM;
        } else
        return TYPE_NORMAL_ITEM;
    }


    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holderGlobal) {
        super.onViewRecycled(holderGlobal);
        try {
            SongViewHolder holder = (SongViewHolder) holderGlobal;
            if (flag != FLAG_CURRENT_LIST) {
                GlideApp.with(Core.getCoreInstance()).clear(holder.getArt());
                holder.getArt().setImageBitmap(null);
            }
        }catch (Exception e){

        }
    }


    @UiThread
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holderGlobal, int positionGlobal) {
        if (holderGlobal instanceof SongViewHolder){
            final SongViewHolder holder = (SongViewHolder) holderGlobal;
            int position = positionGlobal;
            if (flag == FLAG_ALBUM_SONGS){
                position -= 1;
            }

            SongModel songModel = objects.get(position);
            holder.bindData(songModel, position, flag, pos);
            holder.bindObjects(objects);
        } else if (holderGlobal instanceof ViewHolderAlbumTop){
            final ViewHolderAlbumTop holder = (ViewHolderAlbumTop) holderGlobal;
            holder.bindAlbum(albumTop);
        }
    }


    public List<SongModel> getObjects(){
        return objects;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setCurrPlaying(int pos){
        this.pos = pos;
    }

    public interface OnItemClickListener{
        public abstract void onItemClickListener(SongModel songField, int flag, int position);

    }

    public interface OnItemMoreClickListener{
        public abstract void onItemOptionClickListener(SongModel songField);
    }

    public interface OnShuffleClickListener{
        public abstract void onShuffleClickListener();
    }

    public int getCurrPlaying(){
        return pos;
    }

    @Override
    public int getItemCount() {
        int offset = 0;
        if (flag == FLAG_ALBUM_SONGS) offset = 1;
        return  objects == null ? 0 : objects.size()+offset;
    }




}