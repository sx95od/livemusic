package com.sx95od.livemusic.Adapters.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track;
import com.sx95od.livemusic.Adapters.ViewHolders.LastFMChartTopTracksViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.ViewHolderLoader;
import com.sx95od.livemusic.Core.Core;

import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 30.08.2017.
 */

public class LastFMChartTopTracksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Track> objects;
    Listeners.OnLastFMTopChartTracksItemClick onItemClickListener;
    private Context context;

    final int VIEWTYPE_ITEM = 0;
    final int VIEWTYPE_LOADER = 1;

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder instanceof LastFMChartTopTracksViewHolder) {
            Glide.with(Core.getCoreInstance()).clear(((LastFMChartTopTracksViewHolder) holder).getCover());
            ((LastFMChartTopTracksViewHolder) holder).getCover().setImageBitmap(null);
        }
        super.onViewRecycled(holder);
    }

    public LastFMChartTopTracksAdapter(Listeners.OnLastFMTopChartTracksItemClick onItemClickListener,
                                       Context context) {
        this.onItemClickListener = onItemClickListener;
        this.context = context;
        setHasStableIds(true);
    }

    public List<Track> getObjects(){
        if (objects!=null)
        return objects;
        else return null;
    }



    public void addItems(List<Track> objects){
        if (this.objects == null){
            this.objects = new ArrayList<Track>();
        }
        this.objects.addAll(objects);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        RecyclerView.ViewHolder vh = null;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                v = LayoutInflater.from(context).inflate(R.layout.adapter_lastfm_chart_top_tracks, parent, false);
                vh = new LastFMChartTopTracksViewHolder(v, onItemClickListener);
                return vh;
            default:
                v = LayoutInflater.from(context).inflate(R.layout.adapter_loader, parent, false);
                vh = new ViewHolderLoader(v, Core.getCoreInstance().getResources().getDimensionPixelSize(R.dimen.dp312), 0);
                return vh;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == getItemCount()-1 ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((LastFMChartTopTracksViewHolder) holder).bindData(objects.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position < getItemCount() ? position : -1;
    }

    @Override
    public int getItemCount() {
        return objects == null ? 0 : objects.size()+1;
    }
}
