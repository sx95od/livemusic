package com.sx95od.livemusic.Adapters.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.sx95od.livemusic.API.LastFM.Models.AlbumSearch.Album;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.Artist;
import com.sx95od.livemusic.API.LastFM.Models.SearchAlbumModel;
import com.sx95od.livemusic.API.LastFM.Models.SearchArtistModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.Track;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.AdapterModels.ArtistModel;
import com.sx95od.livemusic.Adapters.AdapterModels.GenreModel;
import com.sx95od.livemusic.Adapters.AdapterModels.MusixMatchModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Adapters.ViewHolders.AlbumHeaderSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.AlbumSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.DividerViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchAlbumLastFMHeaderSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchAlbumLastFMSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchArtistLastFMHeaderSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchArtistLastFMSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchTrackLastFMHeaderSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchTrackLastFMSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchYouTubeSearchVideoViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchYouTubeSearchVideoHeader;
import com.sx95od.livemusic.Adapters.ViewHolders.SongHeaderSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SongViewHolder;
import com.sx95od.livemusic.Core.AppPreferences;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 23.08.2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private final String tag = this.getClass().getSimpleName();

    public static final int TYPE_HEADER_SONG = 0;
    public static final int TYPE_HEADER_ALBUM = 1;
    public static final int TYPE_HEADER_ARTIST = 2;
    public static final int TYPE_HEADER_GENRE = 3;
    public static final int TYPE_HEADER_LASTFM_SONG = 4;
    public static final int TYPE_HEADER_LASTFM_ARTIST = 11;
    public static final int TYPE_HEADER_LASTFM_ALBUM = 12;
    public static final int TYPE_HEADER_YOUTUBE_VIDEO = 15;

    public static final int TYPE_SONG = 5;
    public static final int TYPE_ALBUM = 6;
    public static final int TYPE_ARTIST = 7;
    public static final int TYPE_GENRE = 8;
    public static final int TYPE_LASTFM_SONG = 9;
    public static final int TYPE_LASTFM_ARTIST = 13;
    public static final int TYPE_LASTFM_ALBUM= 14;
    public static final int TYPE_YOUTUBE_VIDEO= 16;

    public static final int TYPE_DIVIDER = 10;

    private static final int SONG_CODE = 2;
//    private static final int MUSIXMATCH_CODE = 4;
    private static final int ALBUM_CODE = 4;
    private static final int ARTIST_CODE = 8;
    private static final int GENRE_CODE = 16;
    private int CODE = 0;

    ArrayList<MusixMatchModel> musixMatchObjects;
    List<SongModel> songObjects;
    List<AlbumModel> albumObjects;
    List<ArtistModel> artistObjects;
    List<GenreModel> genreObjects;
    List<Track> lastFMtracksObjects;
    List<Artist> lastFMartistObjects;
    List<Album> lastFMalbumObjects;
    List<Item> youtubeVideoObjects;

    public OnItemClickListener onItemClickListener;

    public Listeners.OnYouTubeVideoItemClickListener onYouTubeVideoItemClickListener;
    public Listeners.OnSearchAdapterListener onSearchAdapterListener;

    int startPosSong = -1;
    int startPosAlbum = -1;
    int startPosArtist = -1;
    int startPosGenre = -1;
    int startPosLastfmTrack = -1;
    int startPosLastfmArtist = -1;
    int startPosLastfmAlbum = -1;
    int startPosYouTubeVideo = -1;
    int countObjects = 0;
    int[] resultCount;

    int songCount, albumCount, artistCount, genreCount, lastFMtracksCount, lastFMartistCount, lastFMalbumCount, youTubeVideoCount = 0;

    Context context;

    String query;

    public interface OnItemClickListener{
        public abstract void onItemClickLister(Track object);
        public abstract void onItemClickLister(SearchArtistModel object);
        public abstract void onItemClickLister(SearchAlbumModel object);
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof SearchTrackLastFMSearchViewHolder){
            Glide.with(Core.getCoreInstance()).clear(((SearchTrackLastFMSearchViewHolder) holder).getCover());
        } else if (holder instanceof SearchAlbumLastFMSearchViewHolder) {
            Glide.with(Core.getCoreInstance()).clear(((SearchAlbumLastFMSearchViewHolder) holder).getCover());
        } else if (holder instanceof SearchArtistLastFMSearchViewHolder) {
            Glide.with(Core.getCoreInstance()).clear(((SearchArtistLastFMSearchViewHolder) holder).getCover());
        }
    }

    public void addTrackLastFM(List<Track> lastFMtracksObjects, int count){
        this.lastFMtracksObjects = lastFMtracksObjects;
        resultCount[0] = count;
        calculateItems();
        notifyItemRangeChanged(startPosLastfmTrack, getItemCount());
    }

    public void addYouTubeVideo(List<Item> youtubeVideoObjects, int count){
        this.youtubeVideoObjects = youtubeVideoObjects;
        resultCount[3] = count;
        calculateItems();
        notifyItemRangeChanged(startPosYouTubeVideo, getItemCount());

    }

    public void addArtistLastFM(List<Artist> lastFMartistObjects, int count){
        this.lastFMartistObjects = lastFMartistObjects;
        resultCount[1] = count;
        calculateItems();
        notifyItemRangeChanged(startPosLastfmArtist, getItemCount());
    }

    public void addAlbumLastFM(List<Album> lastFMalbumObjects, int count){
        this.lastFMalbumObjects = lastFMalbumObjects;
        resultCount[2] = count;
        calculateItems();
        notifyItemRangeChanged(startPosLastfmAlbum, getItemCount());
    }

    public void addInternalSongs(List<SongModel> songObjects){
        this.songObjects = songObjects;
        calculateItems();
        notifyItemRangeChanged(startPosSong, getItemCount());
    }

    public void addInternalAlbums(List<AlbumModel> albumObjects){
        this.albumObjects = albumObjects;
        calculateItems();
        notifyItemRangeChanged(startPosAlbum, getItemCount());
    }

    private void calculateItems(){
        final int VISIBLE_ITEMS = Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS,
                AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS_DEFAULT);

        songCount = songObjects == null ? 0 : songObjects.size()>VISIBLE_ITEMS ? VISIBLE_ITEMS : songObjects.size();
        startPosSong = songCount > 0 ? 0 : -1;
        albumCount = albumObjects == null ? 0 : albumObjects.size()>VISIBLE_ITEMS ? VISIBLE_ITEMS : albumObjects.size();
        startPosAlbum = albumCount > 0 ? songCount * 2 : -1;
        artistCount = artistObjects == null ? 0 : artistObjects.size()>VISIBLE_ITEMS ? VISIBLE_ITEMS : artistObjects.size();
        startPosArtist = artistCount > 0 ? (songCount+albumCount)*2 : -1;
        genreCount = genreObjects == null ? 0 : genreObjects.size()>VISIBLE_ITEMS ? VISIBLE_ITEMS : genreObjects.size();
        startPosGenre = genreCount > 0 ? (songCount+albumCount+artistCount)*2 : -1;

        lastFMtracksCount = lastFMtracksObjects == null ? 0 : lastFMtracksObjects.size()>VISIBLE_ITEMS ? VISIBLE_ITEMS : lastFMtracksObjects.size();
        startPosLastfmTrack = lastFMtracksCount > 0 ? (songCount+albumCount+artistCount+genreCount)*2 : -1;
        lastFMartistCount = lastFMartistObjects == null ? 0 : lastFMartistObjects.size()>VISIBLE_ITEMS ? VISIBLE_ITEMS : lastFMartistObjects.size();
        startPosLastfmArtist = lastFMartistCount > 0 ? (songCount+albumCount+artistCount+genreCount+lastFMtracksCount)*2 : -1;
        lastFMalbumCount = lastFMalbumObjects == null ? 0 : lastFMalbumObjects.size()>VISIBLE_ITEMS ? VISIBLE_ITEMS : lastFMalbumObjects.size();
        startPosLastfmAlbum = lastFMalbumCount > 0 ? (songCount+albumCount+artistCount+genreCount+lastFMtracksCount+lastFMartistCount)*2 : -1;
        youTubeVideoCount = youtubeVideoObjects == null ? 0 : youtubeVideoObjects.size() > VISIBLE_ITEMS ? VISIBLE_ITEMS : youtubeVideoObjects.size();
        startPosYouTubeVideo = youTubeVideoCount > 0 ? (songCount+albumCount+artistCount+genreCount+lastFMtracksCount+lastFMartistCount+lastFMalbumCount)*2 : -1;

        countObjects = songCount+albumCount+artistCount+genreCount+lastFMtracksCount+lastFMartistCount+lastFMalbumCount+youTubeVideoCount;
    }

    public SearchAdapter(Listeners.OnYouTubeVideoItemClickListener onYouTubeVideoItemClickListener,
                         OnItemClickListener onItemClickListener,
                         Listeners.OnSearchAdapterListener onSearchAdapterListener,
                         String query, int[] resultCount){
        Log.d(tag, query);
        this.query = query;
        setHasStableIds(true);
        this.onItemClickListener = onItemClickListener;
        this.onSearchAdapterListener = onSearchAdapterListener;
        this.onYouTubeVideoItemClickListener = onYouTubeVideoItemClickListener;
        this.resultCount = resultCount;
        calculateItems();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("SearchAdapter", "<OnCreateViewHolder> ViewType is "+viewType);
        RecyclerView.ViewHolder vh = null;
        View v = null;
        switch (viewType){
            case TYPE_ALBUM:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_album, parent, false);
                vh = new AlbumSearchViewHolder(v);
                break;
            case TYPE_ARTIST:

                break;
            case TYPE_GENRE:

                break;
            case TYPE_SONG:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_song, parent, false);
                vh = new SongViewHolder(v, null, null, null,0, SongAdapter.FLAG_SET_OFF_MORE);
                break;
            case TYPE_HEADER_ALBUM:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_album_header, parent, false);
                vh = new AlbumHeaderSearchViewHolder(v);
                break;
            case TYPE_HEADER_ARTIST:

                break;
            case TYPE_HEADER_GENRE:

                break;
            case TYPE_HEADER_SONG:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_song_header, parent, false);
                vh = new SongHeaderSearchViewHolder(v);
                break;
            case TYPE_DIVIDER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider_list_margined, parent, false);
                vh = new DividerViewHolder(v);
                break;
            case TYPE_LASTFM_SONG:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_lastfm_track, parent, false);
                vh = new SearchTrackLastFMSearchViewHolder(v, onItemClickListener);
                break;
            case TYPE_HEADER_LASTFM_SONG:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_lastfm_track_header, parent, false);
                vh = new SearchTrackLastFMHeaderSearchViewHolder(v);
                break;
            case TYPE_HEADER_LASTFM_ARTIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_lastfm_artist_header, parent, false);
                vh = new SearchArtistLastFMHeaderSearchViewHolder(v);
                break;
            case TYPE_LASTFM_ARTIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_lastfm_artist, parent, false);
                vh = new SearchArtistLastFMSearchViewHolder(v);
                break;
            case TYPE_HEADER_LASTFM_ALBUM:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_lastfm_album_header, parent, false);
                vh = new SearchAlbumLastFMHeaderSearchViewHolder(v);
                break;
            case TYPE_LASTFM_ALBUM:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_lastfm_album, parent, false);
                vh = new SearchAlbumLastFMSearchViewHolder(v);
                break;
            case TYPE_HEADER_YOUTUBE_VIDEO:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_youtube_video_header, parent, false);
                vh = new SearchYouTubeSearchVideoHeader(v, onSearchAdapterListener);
                ((SearchYouTubeSearchVideoHeader) vh).setQuery(query);
                ((SearchYouTubeSearchVideoHeader) vh).setMatches(resultCount[3]);
                break;
            case TYPE_YOUTUBE_VIDEO:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_youtube_video, parent, false);
                vh = new SearchYouTubeSearchVideoViewHolder(v, onYouTubeVideoItemClickListener);
                break;
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderGlobal, int position) {
        if (holderGlobal instanceof SongViewHolder){
            SongViewHolder holder = (SongViewHolder) holderGlobal;
            holder.bindData(songObjects.get(position/2), position/2, 0, -1);
            holder.bindObjects(songObjects);
        } else if (holderGlobal instanceof SongHeaderSearchViewHolder){
            SongHeaderSearchViewHolder holder = (SongHeaderSearchViewHolder) holderGlobal;
            holder.setMatches(songObjects.size());
        } else if (holderGlobal instanceof SearchTrackLastFMSearchViewHolder){
            SearchTrackLastFMSearchViewHolder holder = (SearchTrackLastFMSearchViewHolder) holderGlobal;
            holder.bindData(lastFMtracksObjects.get((position - startPosLastfmTrack)/2));
        } else if (holderGlobal instanceof SearchTrackLastFMHeaderSearchViewHolder){
            SearchTrackLastFMHeaderSearchViewHolder holder = (SearchTrackLastFMHeaderSearchViewHolder) holderGlobal;
            holder.setMatches(resultCount[0]);
        } else if (holderGlobal instanceof SearchArtistLastFMSearchViewHolder){
            SearchArtistLastFMSearchViewHolder holder = (SearchArtistLastFMSearchViewHolder) holderGlobal;
            holder.bindData(lastFMartistObjects.get((position - startPosLastfmArtist)/2));
        } else if (holderGlobal instanceof SearchArtistLastFMHeaderSearchViewHolder){
            SearchArtistLastFMHeaderSearchViewHolder holder = (SearchArtistLastFMHeaderSearchViewHolder) holderGlobal;
            holder.setMatches(resultCount[1]);
        } else if (holderGlobal instanceof SearchAlbumLastFMSearchViewHolder){
            SearchAlbumLastFMSearchViewHolder holder = (SearchAlbumLastFMSearchViewHolder) holderGlobal;
            holder.bindData(lastFMalbumObjects.get((position - startPosLastfmAlbum)/2));
        } else if (holderGlobal instanceof SearchAlbumLastFMHeaderSearchViewHolder){
            SearchAlbumLastFMHeaderSearchViewHolder holder = (SearchAlbumLastFMHeaderSearchViewHolder) holderGlobal;
            holder.setMatches(resultCount[2]);
        } else if (holderGlobal instanceof AlbumHeaderSearchViewHolder){
            AlbumHeaderSearchViewHolder holder = (AlbumHeaderSearchViewHolder) holderGlobal;
            holder.setMatches(albumObjects.size());
        } else if (holderGlobal instanceof AlbumSearchViewHolder){
            AlbumSearchViewHolder holder = (AlbumSearchViewHolder) holderGlobal;
            holder.bindData(albumObjects.get((position - startPosAlbum) / 2));
        } else if (holderGlobal instanceof SearchYouTubeSearchVideoViewHolder){
            SearchYouTubeSearchVideoViewHolder holder  = (SearchYouTubeSearchVideoViewHolder) holderGlobal;
            holder.bindData(youtubeVideoObjects.get((position - startPosYouTubeVideo) / 2));
        }
    }

    @Override
    public int getItemCount() {
        return countObjects*2; //need to change!
    }

    @Override
    public int getItemViewType(int position) {
        Log.d("SearchAdapter", "<getItemViewType> position = "+position);
       if (startPosSong != -1 && position < songCount*2){
           if (position == startPosSong){
               return TYPE_HEADER_SONG;
           } else{
               return position % 2 == 0 ? TYPE_DIVIDER : TYPE_SONG;
           }
       } else if (startPosAlbum != -1 && position - startPosAlbum < albumCount*2){
           if (position == startPosAlbum){
               return TYPE_HEADER_ALBUM;
           } else{
               return position % 2 == 0 ? TYPE_DIVIDER : TYPE_ALBUM;
           }
       } else if (startPosArtist != -1 && position - startPosArtist < artistCount*2){
           if (position == startPosArtist){
               return TYPE_HEADER_ARTIST;
           } else{
               return position % 2 == 0 ? TYPE_DIVIDER : TYPE_ARTIST;
           }
       } else if (startPosGenre != -1 && position - startPosGenre < genreCount*2){
           if (position == startPosGenre){
               return TYPE_HEADER_GENRE;
           } else{
               return position % 2 == 0 ? TYPE_DIVIDER : TYPE_GENRE;
           }
       } else if (startPosLastfmTrack != -1 && position - startPosLastfmTrack < lastFMtracksCount*2){
           if (position == startPosLastfmTrack){
               return TYPE_HEADER_LASTFM_SONG;
           } else{
               return position % 2 == 0 ? TYPE_DIVIDER : TYPE_LASTFM_SONG;
           }
       } else if (startPosLastfmArtist != -1 && position - startPosLastfmArtist < lastFMartistCount*2){
           if (position == startPosLastfmArtist){
               return TYPE_HEADER_LASTFM_ARTIST;
           } else{
               return position % 2 == 0 ? TYPE_DIVIDER : TYPE_LASTFM_ARTIST;
           }
       }  else if (startPosLastfmAlbum != -1 && position - startPosLastfmAlbum < lastFMalbumCount*2){
           if (position == startPosLastfmAlbum){
               return TYPE_HEADER_LASTFM_ALBUM;
           } else{
               return position % 2 == 0 ? TYPE_DIVIDER : TYPE_LASTFM_ALBUM;
           }
       }  else if (startPosYouTubeVideo != -1 && position - startPosYouTubeVideo < youTubeVideoCount*2){
           if (position == startPosYouTubeVideo){
               return TYPE_HEADER_YOUTUBE_VIDEO;
           } else {
               return position % 2 == 0 ? TYPE_DIVIDER : TYPE_YOUTUBE_VIDEO;
           }
       } else return -1;
    }




    @Override
    public long getItemId(int position) {
//        Log.d("SearchAdapter", "<getItemId> position = " + position);
//            if (startPosSong != -1 && position < songCount * 2) {
//                if (position == startPosSong) {
//                    return -1;
//                } else {
//                    return position % 2 == 0 ? -1 : songObjects.get(position / 2).hashCode();
//                }
//            } else if (startPosAlbum != -1 && position - startPosAlbum < albumCount * 2) {
//                if (position == startPosAlbum) {
//                    return -1;
//                } else {
//                    return position % 2 == 0 ? -1 : albumObjects.get((position - startPosAlbum) / 2).hashCode();
//                }
//            } else if (startPosArtist != -1 && position - startPosArtist < artistCount * 2) {
//                if (position == startPosArtist) {
//                    return -1;
//                } else {
//                    return position % 2 == 0 ? -1 : artistObjects.get((position - startPosArtist) / 2).hashCode();
//                }
//            } else if (startPosGenre != -1 && position - startPosGenre < genreCount * 2) {
//                if (position == startPosGenre) {
//                    return -1;
//                } else {
//                    return position % 2 == 0 ? -1 : genreObjects.get((position - startPosGenre) / 2).hashCode();
//                }
//            } else if (startPosLastfmTrack != -1 && position - startPosLastfmTrack < lastFMtracksCount * 2) {
//                if (position == startPosLastfmTrack) {
//                    return -1;
//                } else {
//                    return position % 2 == 0 ? -1 : lastFMtracksObjects.get((position - startPosLastfmTrack) / 2).hashCode();
//                }
//            } else if (startPosLastfmArtist != -1 && position - startPosLastfmArtist < lastFMartistCount * 2) {
//                if (position == startPosLastfmArtist) {
//                    return -1;
//                } else {
//                    return position % 2 == 0 ? -1 : lastFMartistObjects.get((position - startPosLastfmArtist) / 2).hashCode();
//                }
//            } else if (startPosLastfmAlbum != -1 && position - startPosLastfmAlbum < lastFMalbumCount * 2) {
//                if (position == startPosLastfmAlbum) {
//                    return -1;
//                } else {
//                    return position % 2 == 0 ? -1 : lastFMalbumObjects.get((position - startPosLastfmAlbum) / 2).hashCode();
//                }
//            } else return -1;
        return position;
    }
}
