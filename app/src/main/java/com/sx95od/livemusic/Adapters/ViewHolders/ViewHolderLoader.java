package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 05.10.2017.
 */

public class ViewHolderLoader extends RecyclerView.ViewHolder {
    @BindView(R2.id.fr_loader) FrameLayout _loader;

    public ViewHolderLoader(View itemView, int height, int width) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        if (height!=0) _loader.getLayoutParams().height = height;
        if (width!=0) _loader.getLayoutParams().width = width;
    }
}
