package com.sx95od.livemusic.Adapters.HomeSongAdapter;

import java.io.Serializable;

/**
 * Created by stoly on 22.02.2017.
 */

public class HomeSongFields implements Serializable {
    int id;
    String uri;
    String art;
    String title;
    String artist;
    String album;
    int artist_id;
    int album_id;
    int duration;
    String year;
    int date;
    boolean selected;

    public HomeSongFields(int id, String uri, String art, String title, String artist, String album, int album_id, int artist_id, int duration, String year, int date, boolean selected){
        this.id = id;
        this.uri = uri;
        this.art = art;
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.album_id = album_id;
        this.artist_id = artist_id;
        this.duration = duration;
        this.year = year;
        this.date = date;
        this.selected = selected;
    }

    public int getId(){
        return id;
    }

    public String getUri(){
        return uri;
    }

    public String getArt(){
        return art;
    }

    public String getTitle(){
        return title;
    }

    public String getArtist(){
        return artist;
    }

    public String getAlbum(){
        return album;
    }

    public int getArtist_id(){
        return artist_id;
    }

    public int getAlbum_id(){
        return album_id;
    }

    public int getDuration(){
        return duration;
    }

    public String getYear(){
        return year;
    }

    public boolean getSelected(){
        return selected;
    }

    public void changeSelected(boolean s){
        this.selected = s;
    }

    public int getDate(){
        return date;
    }

}
