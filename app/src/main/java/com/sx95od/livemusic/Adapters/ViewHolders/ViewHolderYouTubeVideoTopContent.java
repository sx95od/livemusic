package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.API.YouTubeApi.Models.Channel.ChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.Video.YouTubeVideoModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 08.10.2017.
 */

public class ViewHolderYouTubeVideoTopContent extends RecyclerView.ViewHolder {
    ChannelModel channelModel;
    YouTubeVideoModel videoModel;
    Listeners.OnYouTubeVideoTopContentClick onContentClick;

    @BindView(R2.id.textview_title) TextView _title;
//    @BindView(R2.id.textview_like_count) TextView _likeCount;
//    @BindView(R2.id.textview_dislike_count) TextView _dislikeCount;
//    @BindView(R2.id.textview_views_count) TextView _viewsCount;
    @BindView(R2.id.textview_channel_title) TextView _channelTitle;
//    @BindView(R2.id.textview_channel_subscribers) TextView _channelSubs;
//    @BindView(R2.id.fb_download) FlexboxLayout _fbDownload;


//    @BindView(R2.id.imagebutton_download) ImageButton _downloadButton;



    @BindView(R2.id.imageview_cover) ImageView _coverChannel;

    public ViewHolderYouTubeVideoTopContent(View itemView, Listeners.OnYouTubeVideoTopContentClick onContentClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.onContentClick = onContentClick;

//        _downloadButton.setOnClickListener(view -> {
//            if (onContentClick != null){
//                onContentClick.downloadClick(videoModel.getItems().get(0).getId());
//            }
//        });

//        if (!Core.getH()) _fbDownload.setVisibility(View.GONE);
    }

    public void bindData(YouTubeVideoModel videoModel1, ChannelModel channelModel1){
        this.videoModel = videoModel1;
        this.channelModel = channelModel1;

        _title.setText(videoModel.getItems().get(0).getSnippet().getTitle());
//        setCount(_likeCount, videoModel.getItems().get(0).getStatistics().getLikeCount());
//        setCount(_dislikeCount, videoModel.getItems().get(0).getStatistics().getDislikeCount());
//        setCount(_viewsCount, videoModel.getItems().get(0).getStatistics().getViewCount());

        _channelTitle.setText(channelModel.getItems().get(0).getSnippet().getTitle());
//        _channelSubs.setText(Core.getCoreInstance().getResources().getQuantityString(R.plurals.subscribers_count,
//                Integer.valueOf(channelModel.getItems().get(0).getStatistics().getSubscriberCount()),
//                        Integer.valueOf(channelModel.getItems().get(0).getStatistics().getSubscriberCount())));

        GlideApp.with(Core.getCoreInstance())
                .load(channelModel.getItems().get(0).getSnippet().getThumbnails().getHigh().getUrl())
                .override(228)
                .into(_coverChannel);
    }

    private void setCount(TextView textView, String count){
        if (textView == null){
            return;
        } else if ((Integer.valueOf(count)<1000000) &&
                (Integer.valueOf(count)>1000)){
            textView.setText(count.substring(0, count.length()-3)+"K");
        } else if (Integer.valueOf(count)>1000000){
            textView.setText(count.substring(0, count.length()-6)+"KK");
        } else{
            textView.setText(count);
        }
    }
}
