package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.Adapters.Adapters.SearchAdapter;
import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 23.09.2017.
 */

public class SearchYouTubeSearchVideoViewHolder extends RecyclerView.ViewHolder {
    @BindView(R2.id.imageview_cover) ImageView cover;
    @BindView(R2.id.textview_title) TextView title;
    @BindView(R2.id.textview_channel) TextView channel;
    @BindView(R2.id.textview_date) TextView date;

    Listeners.OnYouTubeVideoItemClickListener onItemClickListener;

    Item item;

    public SearchYouTubeSearchVideoViewHolder(View itemView, Listeners.OnYouTubeVideoItemClickListener onItemClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.onItemClickListener = onItemClickListener;

        itemView.setOnClickListener(view -> {
            if (onItemClickListener != null){
                onItemClickListener.onVideoClickListener(item);
            }
        });
    }

    public void bindData(Item item){
        this.item = item;
        GlideApp.with(Core.getCoreInstance())
                .load(item.getSnippet().getThumbnails().getHigh().getUrl())
                .skipMemoryCache(true)
                .into(cover);
        title.setText(item.getSnippet().getTitle());
        channel.setText(item.getSnippet().getChannelTitle());
    }
}
