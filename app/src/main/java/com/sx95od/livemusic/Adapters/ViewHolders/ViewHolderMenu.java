package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 20.11.2017.
 */

public class ViewHolderMenu extends RecyclerView.ViewHolder {
    @BindView(R2.id.iv_icon)
    ImageView _icon;

    @BindView(R2.id.tv_item)
    TextView _item;

    public ViewHolderMenu(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindItem(MenuItem menuItem){
        _icon.setImageDrawable(menuItem.getIcon());
        _item.setText(menuItem.getTitle());
    }
}
