package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.ThemeViews.SFlexboxLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by stoly on 28.01.2018.
 */

public class ViewHolderAlbumTop extends RecyclerView.ViewHolder {
    @BindView(R2.id.tv_title)
    TextView _title;

    @BindView(R2.id.tv_artist)
    TextView _artist;

    @BindView(R2.id.iv_cover)
    ImageView _cover;

    @BindView(R2.id.iv_back)
    ImageView _back;

    Listeners.OnLocalSongItemClickListener onLocalSongItemClickListener;


    public ViewHolderAlbumTop(View itemView,
                              Listeners.OnLocalSongItemClickListener onLocalSongItemClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        this.onLocalSongItemClickListener = onLocalSongItemClickListener;
    }

    @OnClick(R2.id.fbl_play_album)
    public void playAlbumClick(){
        if (onLocalSongItemClickListener != null){
            onLocalSongItemClickListener.onAlbumPlayClick();
        }
    }


    public void bindAlbum(AlbumModel albumModel){
        _title.setText(albumModel.getAlbum());
        _artist.setText(albumModel.getArtist());

        loadImage(_cover, albumModel.getAlbumArt(), 700);
        loadImage(_back, albumModel.getAlbumArt(), 340);
    }


    private void loadImage(ImageView view, String path, int override){
        Core.withGlide().load(path)
                .override(override)
                .transition(DrawableTransitionOptions.withCrossFade(750))
                .into(view);
    }
}
