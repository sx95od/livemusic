package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.Artist;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.ArtistSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.SearchArtistModel;
import com.sx95od.livemusic.API.LastFM.Models.SearchTrackModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 29.08.2017.
 */

public class SearchArtistLastFMSearchViewHolder extends RecyclerView.ViewHolder {
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.art) ImageView cover;
    @BindView(R2.id.listeners) TextView listeners;

    public SearchArtistLastFMSearchViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindData(Artist object){
        GlideApp.with(Core.getCoreInstance())
                .load(object.getImage().get(object.getImage().size()-1).getText())
                .placeholder(R.drawable.noimg)
                .error(R.drawable.noimg)
                .skipMemoryCache(true)
                .into(cover);
        title.setText(object.getName());
        listeners.setText(String.valueOf(object.getListeners()));
    }

    public ImageView getCover(){
        return cover;
    }
}
