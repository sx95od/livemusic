package com.sx95od.livemusic.Adapters.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.Adapters.ViewHolders.AlbumSearchViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.DividerViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SearchYouTubeSearchVideoViewHolder;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 24.09.2017.
 */

public class YouTubeVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int TYPE_VIDEO = 0;
    private final int TYPE_DIVIDER = 1;

    private List<Item> videoList;

    Listeners.OnYouTubeVideoItemClickListener onYouTubeVideoItemClickListener;


    public YouTubeVideoAdapter(Listeners.OnYouTubeVideoItemClickListener onYouTubeVideoItemClickListener) {
        this.onYouTubeVideoItemClickListener = onYouTubeVideoItemClickListener;
        videoList = new ArrayList<Item>();
    }


    public void addObjects(List<Item> videoList){
        this.videoList.addAll(this.videoList.size(), videoList);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        View v = null;
        switch (viewType){
            case TYPE_VIDEO:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_youtube_video, parent, false);
                vh = new SearchYouTubeSearchVideoViewHolder(v, onYouTubeVideoItemClickListener);
                break;
            case TYPE_DIVIDER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider_list_margined, parent, false);
                vh = new DividerViewHolder(v, Core.getCoreInstance().getResources().getDimensionPixelSize(R.dimen.dp174),
                        0);
                break;
        }
        return vh;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SearchYouTubeSearchVideoViewHolder){
            SearchYouTubeSearchVideoViewHolder holderVideo = (SearchYouTubeSearchVideoViewHolder) holder;
            holderVideo.bindData(videoList.get(position / 2));
        }
    }


    @Override
    public long getItemId(int position) {
        return position % 2 == 0 ? -1 : position;
    }


    @Override
    public int getItemViewType(int position) {
        return position % 2 != 0 ? TYPE_DIVIDER : TYPE_VIDEO;
    }


    @Override
    public int getItemCount() {
        return videoList.size()*2-1;
    }
}
