package com.sx95od.livemusic.Adapters.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.Track;
import com.sx95od.livemusic.Adapters.ViewHolders.LastFMChartTopTracksViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.ViewHolderLastFMGeoTopChartTracks;
import com.sx95od.livemusic.Adapters.ViewHolders.ViewHolderLoader;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 01.10.2017.
 */

public class LastFMGeoChartTopTracksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Track> objects;
    private ViewHolderLoader loader;
    private Context context;

    final int VIEWTYPE_ITEM = 0;
    final int VIEWTYPE_LOADER = 1;

    Listeners.OnLastFMGeoTopChartTracksItemClick onLastFMGeoTopChartTracksItemClick;

    public LastFMGeoChartTopTracksAdapter(Listeners.OnLastFMGeoTopChartTracksItemClick onLastFMGeoTopChartTracksItemClick,
                                          Context context) {
        this.onLastFMGeoTopChartTracksItemClick = onLastFMGeoTopChartTracksItemClick;
        this.context = context;
        setHasStableIds(true);
    }


    public void setAdapter(List<Track> objects){
        this.objects = objects;
    }

    public void addItems(List<Track> objects){
        if (this.objects == null){
            this.objects = new ArrayList<Track>();
        }
        this.objects.addAll(objects);
    }

    public List<Track> getObjects(){
        return objects;
    }

    @Override
    public int getItemViewType(int position) {
        return position == getItemCount()-1 ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        RecyclerView.ViewHolder vh = null;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                v = LayoutInflater.from(context).inflate(R.layout.adapter_lastfm_geo_chart_top_tracks, parent, false);
                vh = new ViewHolderLastFMGeoTopChartTracks(v, onLastFMGeoTopChartTracksItemClick);
                return vh;
            default:
                v = LayoutInflater.from(context).inflate(R.layout.adapter_loader, parent, false);
                vh = new ViewHolderLoader(v, Core.getCoreInstance().getResources().getDimensionPixelSize(R.dimen.dp312), 0);
                return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderLastFMGeoTopChartTracks) {
            ((ViewHolderLastFMGeoTopChartTracks) holder).bindData(objects.get(position));
        }
    }

    @Override
    public long getItemId(int position) {
        return position < getItemCount() ? position : -1;
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder instanceof ViewHolderLastFMGeoTopChartTracks) {
            Core.withGlide().clear(((ViewHolderLastFMGeoTopChartTracks) holder).getCover());
            ((ViewHolderLastFMGeoTopChartTracks) holder).getCover().setImageBitmap(null);
        }
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return objects == null ? 0 : objects.size()+1;
    }
}
