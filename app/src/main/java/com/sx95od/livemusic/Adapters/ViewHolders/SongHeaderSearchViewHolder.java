package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 25.08.2017.
 */

public class SongHeaderSearchViewHolder extends RecyclerView.ViewHolder {
    @BindView(R2.id.matches) TextView matches;

    public SongHeaderSearchViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setMatches(int count){
        matches.setText(matches.getContext().getResources().getQuantityString(R.plurals.matches, count, count));
    }
}
