package com.sx95od.livemusic.Adapters.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 14.08.2017.
 */

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ViewHolder> {
    ArrayList<Integer> colors;
    Context context;
    private OnItemClickListener onItemClickListener = null;


    public ColorAdapter(Context context, ArrayList<Integer> colors){
        this.colors = colors;
        this.context = context;
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.color) ImageView color;
        int pos;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClickListener(colors.get(getPos()));
                    }
                }
            });
        }

        public ImageView getColor(){
            return color;
        }

        public int getPos(){
            return pos;
        }

        public void setPos(int pos){
            this.pos = pos;
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_color, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setPos(position);
        holder.getColor().setColorFilter(ContextCompat.getColor(context, colors.get(position)));
    }


    @Override
    public int getItemCount() {
        return colors.size();
    }

    public interface OnItemClickListener{
        public abstract void onItemClickListener(int color);

    }


}
