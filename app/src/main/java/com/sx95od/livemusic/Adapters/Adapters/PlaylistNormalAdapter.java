package com.sx95od.livemusic.Adapters.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sx95od.livemusic.Adapters.AdapterModels.PlaylistModel;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.R;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by stoly on 07.08.2017.
 */

public class PlaylistNormalAdapter extends RecyclerView.Adapter<PlaylistNormalAdapter.ViewHolder> {
    public static final int ADD_TO_PLAYLIST = 1;
    int code = 0;
    private ArrayList<PlaylistModel> objects;
    private OnItemMoreClickListener onItemMoreClickListener = null;
    private Context ctx;

    public PlaylistNormalAdapter(ArrayList<PlaylistModel> objects, Context ctx){
        WeakReference<ArrayList<PlaylistModel>> arrayListWeakReference =
                new WeakReference<ArrayList<PlaylistModel>>(objects);
        this.objects = arrayListWeakReference.get();
        WeakReference<Context> contextWeakReference =
                new WeakReference<Context>(ctx);
        this.ctx = contextWeakReference.get();
    }

    public PlaylistNormalAdapter(ArrayList<PlaylistModel> objects, Context ctx, int code){
        WeakReference<ArrayList<PlaylistModel>> arrayListWeakReference =
                new WeakReference<ArrayList<PlaylistModel>>(objects);
        this.objects = arrayListWeakReference.get();
        WeakReference<Context> contextWeakReference =
                new WeakReference<Context>(ctx);
        this.ctx = contextWeakReference.get();
        this.code = code;
    }

    public void setOnItemMoreClickListener(OnItemMoreClickListener onItemMoreClickListener){
        this.onItemMoreClickListener = onItemMoreClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView cover;
        TextView im, title, about, count;
        ImageButton more;
        int pos;

        public ViewHolder(View itemView) {
            super(itemView);
            im = (TextView) itemView.findViewById(R.id.im);
            title = (TextView) itemView.findViewById(R.id.title);
            about = (TextView) itemView.findViewById(R.id.about);
            count = (TextView) itemView.findViewById(R.id.count);
            cover = (ImageView) itemView.findViewById(R.id.cover);
            more = (ImageButton) itemView.findViewById(R.id.more);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (code==0) {
                        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_PLAYLIST_SONGS_VIEW, objects.get(getPos()).getId()));
                    } else{
                        if (onItemMoreClickListener != null) {
                            Log.d("SA.getMoreListener", "size "+objects.size()+", pos "+getPos());
                            onItemMoreClickListener.onItemOptionClickListener(objects.get(getPos()));
                        }
                    }
                }
            });

            getMore().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemMoreClickListener != null) {
                        Log.d("SA.getMoreListener", "size "+objects.size()+", pos "+getPos());
                        onItemMoreClickListener.onItemOptionClickListener(objects.get(getPos()));
                    }
                }
            });

            if (code==ADD_TO_PLAYLIST){
                getMore().setVisibility(View.GONE);
            }
        }

        public TextView getIm(){
            return im;
        }

        public TextView getTitle(){
            return title;
        }

        public TextView getAbout(){
            return about;
        }

        public TextView getCount(){
            return count;
        }

        public void setPos(int pos){
            this.pos = pos;
        }

        public int getPos(){
            return pos;
        }

        public ImageButton getMore(){
            return more;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_playlist_normal, parent, false);
        PlaylistNormalAdapter.ViewHolder vh = new PlaylistNormalAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.setPos(position);
        String im = "";
        for (String s : objects.get(position).getName().replaceAll("(\\w)\\w*","$1").split("\\W+")){
            im += s.toUpperCase();
            if (im.length()>=2) break;
        }
        holder.getIm().setText(im);
        holder.getAbout().setText(objects.get(position).getAbout());
        holder.getTitle().setText(objects.get(position).getName());
        holder.getCount().setText(ctx.getResources().getQuantityString(R.plurals.tracks_count, objects.get(position).getCount(),
                objects.get(position).getCount()));

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    public interface OnItemMoreClickListener{
        public abstract void onItemOptionClickListener(PlaylistModel playlistField);
    }


}
