package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sx95od.livemusic.Adapters.AdapterModels.MusixMatchModel;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 23.08.2017.
 */

public class MusixMatchSearchViewHolder extends RecyclerView.ViewHolder {
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.artist) TextView artist;
    @BindView(R2.id.cover) ImageView cover;
    int pos = 0;


    public MusixMatchSearchViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);


    }

    public void bindData(MusixMatchModel object, int position){
        setPos(position);
        setArtist(object.getArtistName());
        setTitle(object.getTrackName());
        Glide.with(getCover().getContext()).load(object.getAlbumCoverArt100()).into(getCover());
    }

    public void setPos(int pos){
        this.pos = pos;
    }

    public void setTitle(String title){
        this.title.setText(title);
    }

    public void setArtist(String artist){
        this.artist.setText(artist);
    }

    public ImageView getCover(){
        return cover;
    }
}
