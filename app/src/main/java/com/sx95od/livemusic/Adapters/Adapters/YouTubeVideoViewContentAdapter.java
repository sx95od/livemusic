package com.sx95od.livemusic.Adapters.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.API.YouTubeApi.Models.Channel.ChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.Video.YouTubeVideoModel;
import com.sx95od.livemusic.Adapters.ViewHolders.ViewHolderYouTubeVideoTopContent;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;

import java.util.List;

/**
 * Created by stoly on 08.10.2017.
 */

public class YouTubeVideoViewContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int TYPE_TOP_CONTENT = 1;
    private final int TYPE_VIDEO = 2;
    private final int TYPE_COMMENT = 3;

    private List<String> videoObjects;
    private List<String> commentObjects;

    private YouTubeVideoModel videoModel;
    private ChannelModel channelModel;

    Listeners.OnYouTubeVideoTopContentClick onContentClick;

    int countItems = 0;


    public YouTubeVideoViewContentAdapter(Listeners.OnYouTubeVideoTopContentClick onContentClick) {
        this.onContentClick = onContentClick;
    }


    public void addTopContent(YouTubeVideoModel videoModel, ChannelModel channelModel){
        this.videoModel = videoModel;
        this.channelModel = channelModel;
        countItems++;
        notifyItemRangeInserted(0, getItemCount());
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return TYPE_TOP_CONTENT;
        } else{
            return -1;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = null;
        RecyclerView.ViewHolder vh = null;
        switch (i){
            case TYPE_TOP_CONTENT:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_youtube_video_top_content2, viewGroup,  false);
                vh = new ViewHolderYouTubeVideoTopContent(view, onContentClick);
                return vh;
            default:
                return null;
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolderYouTubeVideoTopContent){
            ViewHolderYouTubeVideoTopContent h = (ViewHolderYouTubeVideoTopContent) viewHolder;
            h.bindData(videoModel, channelModel);
        }
    }

    @Override
    public int getItemCount() {
        return countItems;
    }
}
