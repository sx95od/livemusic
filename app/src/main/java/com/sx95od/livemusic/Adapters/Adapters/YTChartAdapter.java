package com.sx95od.livemusic.Adapters.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.Item;
import com.sx95od.livemusic.Adapters.ViewHolders.ViewHolderYTChartVideo;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 11.11.2017.
 */

public class YTChartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String tag = "YTChartAdapter";

    Context context;

    List<Item> objects = new ArrayList<Item>();

    Listeners.OnYTChartVideoListener ytChartVideoListener;

    public YTChartAdapter(Listeners.OnYTChartVideoListener ytChartVideoListener, Context context) {
        this.ytChartVideoListener = ytChartVideoListener;
        this.context = context;
        setHasStableIds(true);
    }

    public void addItems(List<Item> objects){
        this.objects.addAll(objects);
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        GlideApp.with(context).clear(((ViewHolderYTChartVideo) holder).getCover());
        ((ViewHolderYTChartVideo) holder).getCover().setImageBitmap(null);

        Log.d("sukablyat", "recycle");
        super.onViewRecycled(holder);
    }
//
//    @Override
//    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
//        GlideApp.with(context).clear(((ViewHolderYTChartVideo) holder).getCover());
//        ((ViewHolderYTChartVideo) holder).getCover().setImageBitmap(null);
//        super.onViewDetachedFromWindow(holder);
//    }
//
//    @Override
//    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
//        ((ViewHolderYTChartVideo) holder).reBindData();
//        super.onViewAttachedToWindow(holder);
//    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_yt_chart_video, parent, false);
        RecyclerView.ViewHolder vh = new ViewHolderYTChartVideo(view, ytChartVideoListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolderYTChartVideo) holder).bindData(objects.get(position));
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }
}
