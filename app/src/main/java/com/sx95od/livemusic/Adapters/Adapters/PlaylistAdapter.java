package com.sx95od.livemusic.Adapters.Adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.Adapters.AdapterModels.PlaylistModel;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.R;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 02.07.2017.
 */

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.ViewHolder> {
    List<PlaylistModel> objects;
    private OnItemMoreClickListener onItemMoreClickListener = null;

    public PlaylistAdapter(List<PlaylistModel> objects) {
        WeakReference<List<PlaylistModel>> arrayListWeakReference = new WeakReference<List<PlaylistModel>>(objects);
        this.objects = arrayListWeakReference.get();
    }

    public void setOnItemMoreClickListener(OnItemMoreClickListener onItemMoreClickListener){
        this.onItemMoreClickListener = onItemMoreClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title, im;
        private ImageView cover;
        private ImageButton more;
        private FlexboxLayout item;
        private int pos;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_PLAYLIST_SONGS_VIEW, objects.get(pos).getId()));
                }
            });

            title = (TextView) itemView.findViewById(R.id.title);
            im = (TextView) itemView.findViewById(R.id.im);
            more = (ImageButton) itemView.findViewById(R.id.more_liked);
            cover = (ImageView) itemView.findViewById(R.id.cover);
            item = (FlexboxLayout) itemView.findViewById(R.id.parent);

            getMore().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemMoreClickListener != null) {
                        Log.d("SA.getMoreListener", "size "+objects.size()+", pos "+getPos());
                        onItemMoreClickListener.onItemOptionClickListener(objects.get(getPos()));
                    }
                }
            });

        }

        public ImageView getCover(){
            return cover;
        }

        public TextView getTitle(){
            return title;
        }

        public void setPos(int pos){
            this.pos = pos;
        }

        public int getPos(){
            return pos;
        }

        public TextView getIm(){
            return im;
        }

        public ImageButton getMore(){
            return more;
        }

        public FlexboxLayout getItem(){
            return item;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_playlist, parent, false);
        PlaylistAdapter.ViewHolder vh = new PlaylistAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.setPos(position);
        holder.getTitle().setText(objects.get(position).getName());
        String im = "";
        for (String s : objects.get(position).getName().replaceAll("(\\w)\\w*","$1").split("\\W+")){
            im += s.toUpperCase();
            if (im.length()>=2) break;
        }
        holder.getIm().setText(im);
    }


    public interface OnItemMoreClickListener{
        public abstract void onItemOptionClickListener(PlaylistModel playlistField);
    }

    @Override
    public long getItemId(int position) {
        return objects.get(position).hashCode();
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }





}
