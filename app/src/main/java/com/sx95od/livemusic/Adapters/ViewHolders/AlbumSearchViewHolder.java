package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by stoly on 26.08.2017.
 */

public class AlbumSearchViewHolder extends RecyclerView.ViewHolder {
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.artist) TextView artist;
    @BindView(R2.id.count_textview) TextView count;

    public AlbumSearchViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindData(AlbumModel object){
            title.setText(object.getAlbum());
            artist.setText(object.getArtist());
            count.setText(String.valueOf(object.getCountSongs()));
    }
}
