package com.sx95od.livemusic.Adapters.AdapterModels;

import java.io.Serializable;

/**
 * Created by stoly on 02.07.2017.
 */

public class PlaylistModel implements Serializable {
    private int id;
    private String name;
    private byte[] cover;
    private long date;
    private int count;
    private String about;

    public PlaylistModel(int id, String name, byte[] cover, long date, int count, String about){
        this.id = id;
        this.name = name;
        this.cover = cover;
        this.date = date;
        this.count = count;
        this.about = about;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setCover(byte[] cover){
        this.cover = cover;
    }

    public void setDate(long date){
        this.date = date;
    }

    public void setAbout(String about){
        this.about = about;
    }

    public String getName(){
        return name;
    }

    public int getId(){
        return id;
    }

    public int getCount(){
        return count;
    }

    public byte[] getCover(){
        return cover;
    }

    public long getDate(){
        return date;
    }

    public String getAbout(){
        return about;
    }

}
