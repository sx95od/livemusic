package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sx95od.livemusic.API.FFMPEGConverter;
import com.sx95od.livemusic.Adapters.AdapterModels.ModelDownloads;
import com.sx95od.livemusic.R2;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by stoly on 03.09.2017.
 */

public class ViewHolderDownloads extends RecyclerView.ViewHolder {
    @BindView(R2.id.format_in_textview) TextView formatIn;
    @BindView(R2.id.format_out_textview) TextView formatOut;
    @BindView(R2.id.name_textview) TextView name;
    @BindView(R2.id.path_textview) TextView path;
    @BindView(R2.id.progress_converting) ProgressBar converting;
    Unbinder unbinder;
    WeakReference<ModelDownloads> weakObject;
    ModelDownloads object;
    int pos = -1;

    public ViewHolderDownloads(View itemView) {
        super(itemView);
        unbinder = ButterKnife.bind(this, itemView);

        formatOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FFMPEGConverter.opus2mp3(pos, object.getId(), object.getFileName(), object.getUri(),
                        object.getFormat(), object.getOutFormat(), object.getTitle(),
                        object.getArtist(), object.getAlbum(), "", "");
                formatOut.setVisibility(View.GONE);
                converting.setVisibility(View.VISIBLE);
            }
        });
    }

    public void bindData(ModelDownloads object, int pos){
        this.pos = pos;
        weakObject = new WeakReference<ModelDownloads>(object);
        this.object = weakObject.get();
        formatIn.setText(object.getFormat());
        formatOut.setText(object.getOutFormat());
        name.setText(object.getFileName());
        path.setText(object.getUri());
    }
}
