package com.sx95od.livemusic.Adapters.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.Adapters.ViewHolders.DividerViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.RecentSearchesViewHolder;
import com.sx95od.livemusic.Adapters.ViewHolders.SongViewHolder;
import com.sx95od.livemusic.Core.AppPreferences;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 27.08.2017.
 */

public class RecentSearchesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_DIVIDER = 1;
    private final String tag = RecentSearchesAdapter.class.getSimpleName();

    private OnItemClickListener onItemClickListener;

    List<String> objects;


    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof RecentSearchesViewHolder) {
            ((RecentSearchesViewHolder) holder).unregisterEventBus();
        }
    }

    public RecentSearchesAdapter(List<String> objcets, OnItemClickListener onItemClickListener){
        this.objects = objcets;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_recent_searches, parent, false);
            vh = new RecentSearchesViewHolder(v, onItemClickListener);
        } else if (viewType==TYPE_DIVIDER){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider_list_margined, parent, false);
            vh = new DividerViewHolder(v, (int) parent.getContext().getResources().getDimension(R.dimen.margin18),
                    (int) parent.getContext().getResources().getDimension(R.dimen.margin18));
        }
        return vh;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2 == 0? TYPE_ITEM : TYPE_DIVIDER;
    }

    @Override
    public long getItemId(int position) {
        return position % 2 == 0? objects.get(position/2).hashCode() : super.getItemId(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderGlobal, int position) {
        if (holderGlobal instanceof RecentSearchesViewHolder){
            RecentSearchesViewHolder holder = (RecentSearchesViewHolder) holderGlobal;
            holder.bindData(objects.get(position/2), position/2);
        }
    }

    @Override
    public int getItemCount() {
        if (Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS,
                AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS_DEFAULT) == 0){
            return objects.size()*2-1;
        } else {
            if (objects.size() < Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS,
                    AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS_DEFAULT)) {
                return objects.size() > 0 ? objects.size() * 2 - 1 : 0;
            } else {
                return Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS,
                        AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS_DEFAULT) * 2 - 1;
            }
        }
    }

    public interface OnItemClickListener{
        void OnItemClickListener(String query);
    }
}
