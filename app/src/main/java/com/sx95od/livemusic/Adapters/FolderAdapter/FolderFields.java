package com.sx95od.livemusic.Adapters.FolderAdapter;

import java.io.Serializable;

/**
 * Created by stoly on 04.03.2017.
 */

public class FolderFields implements Serializable{
    String path;
    String title;
    int count;

    public FolderFields(String path, String title, int count){
        this.path = path;
        this.title = title;
        this.count = count;
    }

    public int getCount(){
        return count;
    }

    public String getPath(){
        return path;
    }


    public String getTitle(){
    return title;
    }

    public void changeCount(int count){
        this.count = count;
    }
}
