package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by stoly on 25.08.2017.
 */

public class AlbumHeaderSearchViewHolder extends RecyclerView.ViewHolder {
    @BindView(R2.id.matches) TextView matches;
    Unbinder unbinder;

    public AlbumHeaderSearchViewHolder(View itemView) {
        super(itemView);
        unbinder = ButterKnife.bind(this, itemView);
    }

    public void setMatches(int count){
        matches.setText(Core.getCoreInstance().getResources().getQuantityString(R.plurals.matches, count, count));
    }
}
