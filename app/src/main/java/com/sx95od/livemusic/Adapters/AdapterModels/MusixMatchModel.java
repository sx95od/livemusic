package com.sx95od.livemusic.Adapters.AdapterModels;

import java.io.Serializable;

/**
 * Created by stoly on 23.08.2017.
 */

public class MusixMatchModel implements Serializable {
    long trackId;
    String trackName;
    int hasLyrics;
    long lyricsId;
    long albumId;
    String albumName;
    long artistId;
    String artistName;
    String albumCoverArt100;

    public MusixMatchModel(long trackId, String trackName, int hasLyrics, long lyricsId, long albumId,
                           String albumName, long artistId, String artistName, String albumCoverArt100){
        this.trackId = trackId;
        this.trackName = trackName;
        this.hasLyrics = hasLyrics;
        this.albumId = albumId;
        this.albumName = albumName;
        this.artistId = artistId;
        this.artistName = artistName;
        this.albumCoverArt100 = albumCoverArt100;
    }

    public void setTrackId(long trackId){
        this.trackId = trackId;
    }

    public void setTrackName(String trackName){
        this.trackName = trackName;
    }

    public void setHasLyrics(int hasLyrics){
        this.hasLyrics = hasLyrics;
    }

    public void setLyricsId(long lyricsId){
        this.lyricsId = lyricsId;
    }

    public void setAlbumId(long albumId){
        this.albumId = albumId;
    }

    public void setAlbumName(String albumName){
        this.albumName = albumName;
    }

    public void setArtistId(long artistId){
        this.artistId = artistId;
    }

    public void setArtistName(String artistName){
        this.artistName = artistName;
    }

    public void setAlbumCoverArt100(String albumCoverArt100){
        this.albumCoverArt100 = albumCoverArt100;
    }

    public long getTrackId(){
        return trackId;
    }

    public long getLyricsId(){
        return lyricsId;
    }

    public long getAlbumId(){
        return albumId;
    }

    public long getArtistId(){
        return artistId;
    }

    public int getHasLyrics(){
        return hasLyrics;
    }

    public String getTrackName(){
        return trackName;
    }

    public String getAlbumName(){
        return albumName;
    }

    public String getArtistName(){
        return artistName;
    }

    public String getAlbumCoverArt100(){
        return albumCoverArt100;
    }
}
