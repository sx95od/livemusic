package com.sx95od.livemusic.Adapters.AdapterModels;

import java.io.Serializable;

/**
 * Created by stoly on 10.08.2017.
 */

public class AlbumModel implements Serializable{
    long _id;
    String album;
    String albumArt;
    String artist;
    String firstYear, lastYear;
    long countSongs;

    public AlbumModel(String album, String albumArt, String artist, String firstYear, String lastYear,
                      long _id, long countSongs){
        this.countSongs = countSongs;
        this.artist = artist;
        this.album = album;
        this.albumArt = albumArt;
        this.firstYear = firstYear;
        this.lastYear = lastYear;
        this._id = _id;
    }

    public String getAlbum(){
        return album;
    }

    public long get_id() {
        return _id;
    }

    public String getAlbumArt() {
        return albumArt;
    }

    public String getArtist() {
        return artist;
    }

    public String getFirstYear() {
        return firstYear;
    }

    public String getLastYear() {
        return lastYear;
    }

    public long getCountSongs() {
        return countSongs;
    }


}
