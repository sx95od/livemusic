package com.sx95od.livemusic.Adapters.ViewHolders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.Adapters.AlbumAdapter;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stoly on 08.09.2017.
 */

public class InternalAlbumViewHolder extends RecyclerView.ViewHolder {
//    @BindView(R2.id.root_flex) FlexboxLayout rootFlex;
//    @BindView(R2.id.card_imageview) CardView cardImageview;
    @BindView(R2.id.cover_imageview) ImageView coverImageview;
    @BindView(R2.id.textview_album_name) TextView albumName;
    @BindView(R2.id.textview_artist_name) TextView artistName;
    @BindView(R2.id.textview_album_songs_count) TextView songsCount;
    AlbumAdapter.OnAlbumItemClickListener onAlbumItemClickListener;
    AlbumModel object;


    public InternalAlbumViewHolder(View itemView, AlbumAdapter.OnAlbumItemClickListener onAlbumItemClickListener) {
        super(itemView);
        this.onAlbumItemClickListener = onAlbumItemClickListener;
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(view -> {
            if (onAlbumItemClickListener != null){
                onAlbumItemClickListener.onClickListener(object);
            }
        });
//        coverImageview.getLayoutParams().height = (Core.getWidthScreen()/2) - (Core.getCoreInstance().getResources()
//                .getDimensionPixelSize(R.dimen.dp18) *2) + Core.getCoreInstance().getResources().getDimensionPixelSize(R.dimen.dp9);
    }

    public void bindData(AlbumModel object){
        this.object = object;
        albumName.setText(object.getAlbum());
        artistName.setText(object.getArtist());
        songsCount.setText(Core.getCoreInstance().getResources().getQuantityString(R.plurals.tracks_count, (int) object.getCountSongs(),(int) object.getCountSongs()));
        GlideApp.with(Core.getCoreInstance())
                .load(object.getAlbumArt())
                .placeholder(R.drawable.noimg)
                .error(R.drawable.noimg)
                .skipMemoryCache(true)
                .override(500)
                .into(coverImageview);
    }

    public ImageView getCover(){
        return coverImageview;
    }
}

