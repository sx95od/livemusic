package com.sx95od.livemusic.Adapters.AdapterModels;

import java.io.Serializable;

/**
 * Created by stoly on 03.09.2017.
 */

public class ModelDownloads implements Serializable {
    long id;
    String fileName;
    String uri;
    String title;
    String artist;
    String album;
    String format;
    String outFormat;

    public ModelDownloads(long id, String fileName, String uri, String title, String artist, String album, String format, String outFormat) {
        this.id = id;
        this.fileName = fileName;
        this.uri = uri;
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.format = format;
        this.outFormat = outFormat;
    }

    public long getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public String getUri() {
        return uri;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public String getFormat() {
        return format;
    }

    public String getOutFormat() {
        return outFormat;
    }
}
