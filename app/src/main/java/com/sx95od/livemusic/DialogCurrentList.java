package com.sx95od.livemusic;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.ArrayList;

/**
 * Created by stoly on 13.07.2017.
 */

public class DialogCurrentList extends DialogFragment{

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_current_list, null);

        RecyclerView list = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(layoutManager);
        SongAdapter songAdapter = new SongAdapter((ArrayList<SongModel>) getArguments().getSerializable("list"),  SongAdapter.FLAG_CURRENT_LIST, null);
        songAdapter.setHasStableIds(true);
        list.setDrawingCacheEnabled(true);
        list.setItemViewCacheSize(30);
        list.setAdapter(songAdapter);

        builder.setView(view);

        AlertDialog dialog = builder.create();

        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        return dialog;
    }


}
