package com.sx95od.livemusic.Activities.MainActivity.Presenter;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

/**
 * Created by stoly on 04.02.2018.
 */

public interface IPresenter {
    void onCreate();
    void onDestroy();
    void onStop();
    void onResume();
    void onStart();
    void onPause();
    void convertDurationToString(long secs, Presenter.SecConverterFlag secConverterFlag);
    void getCover(SongModel songModel);
    void setLikeSong(SongModel songModel);
    void getLikeSong(SongModel songModel);
}
