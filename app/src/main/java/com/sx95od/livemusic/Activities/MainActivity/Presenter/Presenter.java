package com.sx95od.livemusic.Activities.MainActivity.Presenter;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.facebook.stetho.common.LogUtil;
import com.sx95od.livemusic.Activities.MainActivity.Model.IModel;
import com.sx95od.livemusic.Activities.MainActivity.Model.Model;
import com.sx95od.livemusic.Activities.MainActivity.View.IView;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import javax.annotation.Nullable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by stoly on 10.09.2017.
 */

public class Presenter implements IPresenter{
    public enum SecConverterFlag {
        DURATION,
        CURRENT_TIME
    }

    private final IModel model = new Model();
    private final IView view;

    private final CompositeDisposable disposable = new CompositeDisposable();

    public Presenter(IView view) {
        this.view = view;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void convertDurationToString(long secs, SecConverterFlag secConverterFlag) {
        disposable.add(model.convertSecsToString(secs)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>() {
                    @Override
                    public void onNext(String s) {
                        if (secConverterFlag == SecConverterFlag.DURATION)
                            view.showDurationString(s);
                        else if (secConverterFlag == SecConverterFlag.CURRENT_TIME)
                            view.showCurrentTimeString(s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.e(e.toString());
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void getCover(SongModel songModel) {
        disposable.add(model.getCover(songModel)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<Bitmap>() {
            @Override
            public void onNext(Bitmap bitmap) {
                view.showCover(bitmap);
            }

            @Override
            public void onError(Throwable e) {
                view.showCover(null);
            }

            @Override
            public void onComplete() {

            }
        }));
    }

    @Override
    public void setLikeSong(SongModel songModel) {
        disposable.add(model.setLikeSong(songModel)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<Boolean>() {
            @Override
            public void onNext(Boolean aBoolean) {
                view.songIsLiked(aBoolean);
            }

            @Override
            public void onError(Throwable e) {
                view.songIsLiked(false);
            }

            @Override
            public void onComplete() {

            }
        }));
    }

    @Override
    public void getLikeSong(SongModel songModel) {
        disposable.add(model.getLikeSong(songModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean aBoolean) {
                        view.songIsLiked(aBoolean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.songIsLiked(false);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }
}
