package com.sx95od.livemusic.Activities.MainActivity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.bartoszlipinski.viewpropertyobjectanimator.ViewPropertyObjectAnimator;
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions;
import com.github.mmin18.widget.RealtimeBlurView;
import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.Activities.MainActivity.Presenter.Presenter;
import com.sx95od.livemusic.Activities.MainActivity.View.IView;
import com.sx95od.livemusic.Acts.GodActivity.View.IServiceView;
import com.sx95od.livemusic.Acts.MyActivity;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Adapters.Adapters.PlaylistNormalAdapter;
import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Dialogs.DialogSongMore.BottomSheetDialogSongFragment;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.GlobalEvent;
import com.sx95od.livemusic.Events.SongListEvent;
import com.sx95od.livemusic.Events.UIEvent;
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.FragmentAlbumsView;
import com.sx95od.livemusic.Fragments.FragmentDownload.FragmentDownload;
import com.sx95od.livemusic.Fragments.FragmentFav.FragmentFav;
import com.sx95od.livemusic.Fragments.FragmentHomeV2.FragmentHomeV2;
import com.sx95od.livemusic.Fragments.FragmentMore;
import com.sx95od.livemusic.Fragments.FragmentPlaylistSongsView;
import com.sx95od.livemusic.Fragments.FragmentPlaylistsView;
import com.sx95od.livemusic.Fragments.FragmentSearch.FragmentSearch;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.FragmentSearchMoreResults;
import com.sx95od.livemusic.Fragments.FragmentSongsView.FragmentSongsView;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.FragmentYouTubeShowVideo;
import com.sx95od.livemusic.IntentExtras;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.Services.AudioService.AudioService;
import com.sx95od.livemusic.ThemeViews.SBottomNavigationView;
import com.sx95od.livemusic.ThemeViews.SCardView;
import com.sx95od.livemusic.ThemeViews.SFrameLayout;
import com.sx95od.livemusic.ThemeViews.SImageButton;
import com.sx95od.livemusic.ThemeViews.STextView;
import com.sx95od.livemusic.Utils.SystemUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by stoly on 03.02.2018.
 */

public class MainActivity extends MyActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        View.OnTouchListener, IServiceView, IView, SeekBar.OnSeekBarChangeListener,
        Listeners.OnLocalSongItemClickListener {

    @BindView(R2.id.bottom_navigation_view)
    SBottomNavigationView _bottomNavigationView;

    @BindView(R2.id.player_layout)
    SFrameLayout _playerLayout;

    @BindView(R2.id.fbl_player_actions)
    FlexboxLayout _playerActionsLayout;

    @BindView(R2.id.fl_root)
    FrameLayout _root;

    @BindView(R2.id.status_bar)
    RealtimeBlurView _statusBar;

    @BindView(R2.id.cv_cover)
    CardView _cardCover;

    @BindView(R2.id.fragments_card)
    SCardView _fragmentsCard;

    @BindView(R2.id.cv_player_card)
    CardView _playerCard;

    @BindView(R2.id.sb_duration)
    AppCompatSeekBar _durationSeekBar;

    @BindView(R2.id.cv_player_widget)
    CardView _playerWidget;

    @BindView(R2.id.tv_title)
    STextView _title;

    @BindView(R2.id.tv_title_widget)
    STextView _titleWidget;

    @BindView(R2.id.tv_artist)
    STextView _artist;

    @BindView(R2.id.iv_cover)
    ImageView _cover;

    @BindView(R2.id.tv_current_time)
    STextView _currentTime;

    @BindView(R2.id.tv_duration)
    STextView _duration;

    @BindView(R2.id.ib_play)
    SImageButton _play;

    @BindView(R2.id.ib_play_widget)
    SImageButton _playWidget;

    @BindView(R2.id.ib_up)
    SImageButton _up;

    @BindView(R2.id.ib_like)
    SImageButton _like;

    @BindView(R2.id.fbl_current_list)
    FlexboxLayout _currentListLayout;

    @BindView(R2.id.rv_current_list)
    RecyclerView _currentRV;

    Drawable[] layers = new Drawable[2];

    TransitionDrawable transitionDrawable;

    boolean coverHelperDefault = true;


    float currentY = 0;
    float currentX = 0;
    int choosedMove = 0;
    float currentYTrans = 0;
    float swipeMultiplier = 1;
    float defaultPlayerTrans = 0;
    float transYCoverCard = 0;
    float transXCoverCard = 0;
    float calcFragmentsTopMargin = 0;
    float fragmentsCornerRadius = 0;
    float bottomBarHeight = 0;
    float playerWidgetHeight = 0;
    float widgetClickSensitivity = 0;
    float coverCardElevation = 0;

    int lastPos = -1;

    Unbinder unbinder;

    AudioService audioService;
    boolean bound = false;

    AnimatorSet animatorSetY;
    AnimatorSet animatorSetX;

    Drawable lastCover;

    Presenter presenter;

    SongAdapter songAdapter;

    LinearLayoutManager layoutManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        initView();

        Intent intent = new Intent(this, AudioService.class);
        bindService(intent, audioConnection, 0);


        presenter = new Presenter(this);

        EventBus.getDefault().register(this);

        if (getIntent().getBooleanExtra(IntentExtras.UI_RECREATE, false))
            _bottomNavigationView.setSelectedItemId(R.id.more);
        else addRootFragment(new FragmentHomeV2());
    }


    @Override
    protected void onDestroy() {
        unbindService(audioConnection);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    private ServiceConnection audioConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            AudioService.AudioBinder binder = (AudioService.AudioBinder) iBinder;
            audioService = binder.getService();
            audioService.bindViewListener(MainActivity.this);
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bound = false;
        }
    };


    @OnClick({R2.id.ib_next,
            R2.id.ib_play,
            R2.id.ib_prev,
            R.id.ib_play_widget,
            R.id.ib_next_widget,
            R.id.ib_up})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_next:
                audioService.nextTrack();
                break;
            case R.id.ib_play:
                audioService.setPlaying();
                break;
            case R.id.ib_prev:
                audioService.prevTrack();
                break;
            case R.id.ib_play_widget:
                audioService.setPlaying();
                break;
            case R.id.ib_next_widget:
                audioService.nextTrack();
                break;
            case R.id.ib_up:

                break;
        }
    }


    private void initView() {
        _statusBar.getLayoutParams().height = SystemUtils.getStatusBarHeight();

        widgetClickSensitivity = getPxInDp(6);

        coverCardElevation = getPxInDp(14);
        playerWidgetHeight = getPxInDp(60);
        bottomBarHeight = getPxInDp(46);
        transYCoverCard = -getPxInDp(36 - (60 - 44) / 2);
        transXCoverCard = -(Core.getWidthScreen() / 2 - getPxInDp(40));
        defaultPlayerTrans = Core.getHeightScreen() - (getPxInDp(16) + SystemUtils.getStatusBarHeight()) - bottomBarHeight - getPxInDp(60) + playerWidgetHeight;
        calcFragmentsTopMargin = SystemUtils.getStatusBarHeight() - (Core.getHeightScreen() * 0.025f);
        fragmentsCornerRadius = getPxInDp(12);

        ((ViewGroup.MarginLayoutParams) _playerLayout.getLayoutParams()).topMargin = getPxInDp(16) + SystemUtils.getStatusBarHeight();
        ((ViewGroup.MarginLayoutParams) _cardCover.getLayoutParams()).topMargin = getPxInDp(36);

        _fragmentsCard.setRadius(PrefManager.getFragmentContainerRadius());

        _bottomNavigationView.setOnNavigationItemSelectedListener(this);
        _bottomNavigationView.setSaveEnabled(false);

        _playerLayout.setTranslationY(defaultPlayerTrans);
        _playerWidget.setOnTouchListener(this);
        _playerLayout.setOnTouchListener(this);
        _playerLayout.setEnabled(false);

        _cardCover.setTranslationX(transXCoverCard);
        _cardCover.setTranslationY(transYCoverCard);
        _cardCover.getLayoutParams().width = getPxInDp(44);
        _cardCover.getLayoutParams().height = getPxInDp(44);

        _durationSeekBar.setOnSeekBarChangeListener(this);

        _currentListLayout.setTranslationX(Core.getWidthScreen());

        int heightActionsLayout = Core.getWidthScreen() - getPxInDp(27);
        ((ViewGroup.MarginLayoutParams) _playerActionsLayout.getLayoutParams()).topMargin = heightActionsLayout;

        layoutManager = new LinearLayoutManager(getApplicationContext());
        _currentRV.setLayoutManager(layoutManager);
        songAdapter = new SongAdapter(null, SongAdapter.FLAG_CURRENT_LIST, null);
        songAdapter.setOnItemClickListener(this);
        songAdapter.setHasStableIds(true);
        _currentRV.setAdapter(songAdapter);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.fragments_container)).commitAllowingStateLoss();
        switch (item.getItemId()) {
            case R.id.menu_home:
                addRootFragment(new FragmentHomeV2());
                break;
            case R.id.menu_search:
                addRootFragment(new FragmentSearch());
                break;
            case R.id.menu_fav:
                addRootFragment(new FragmentFav());
                break;
            case R.id.more:
                addRootFragment(new FragmentMore());
                break;
        }
        return true;
    }


    @Override
    public void requestBottomPadding() {
        if (songAdapter.getObjects() != null)
            EventBus.getDefault().post(new UIEvent.PlayerWidgetVisible());
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.player_layout:
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        currentY = motionEvent.getRawY();
                        currentX = motionEvent.getRawX();
                        currentYTrans = _playerLayout.getTranslationY();

                        _playerCard.setVisibility(View.VISIBLE);
                        _playerWidget.setVisibility(View.VISIBLE);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        if (choosedMove == 0) {
                            if (currentY == -1) currentY = motionEvent.getRawY();
                            if (currentX == -1) currentX = motionEvent.getRawX();

                            final float x = Math.abs(currentX - motionEvent.getRawX());
                            final float y = Math.abs(currentY - motionEvent.getRawY());
                            if (x > getPxInDp(8) || y > getPxInDp(8)) {
                                if (x > y) {
                                    choosedMove = 1; //x
                                } else {
                                    choosedMove = 2; //y
                                }
                            }
                        } else if (choosedMove == 2 && !CURRENT_LIST_LAYOUT_EXPANDED) {
                            if (!PLAYER_EXPANDED) {
                                if (currentY > motionEvent.getRawY()) {
                                    _playerLayout.setTranslationY(defaultPlayerTrans - (currentY - motionEvent.getRawY()) / swipeMultiplier);
                                }
                            } else {
                                if (currentY < motionEvent.getRawY() && (motionEvent.getRawY() - currentY) <= defaultPlayerTrans) {
                                    _playerLayout.setTranslationY((motionEvent.getRawY() - currentY) / swipeMultiplier);
                                    if (_playerLayout.getTranslationY() > defaultPlayerTrans / 2) {
                                        _playerCard.setAlpha(1f - ((_playerLayout.getTranslationY() - defaultPlayerTrans / 2) / (defaultPlayerTrans / 2 / 100)) / 100);
                                        _playerWidget.setAlpha(((_playerLayout.getTranslationY() - defaultPlayerTrans / 2) / (defaultPlayerTrans / 2 / 100)) / 100);
                                        _bottomNavigationView.setTranslationY(bottomBarHeight - ((_playerLayout.getTranslationY() - defaultPlayerTrans / 2) / (defaultPlayerTrans / 2 / 100)) / 100 * bottomBarHeight);
                                        setLightStatusBar(true);
                                    } else {
                                        _playerCard.setAlpha(1f);
                                        _playerWidget.setAlpha(0f);
                                        setLightStatusBar(false);
                                    }
                                    _playerCard.setCardElevation(getPxInDp(8) - ((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100 * getPxInDp(8)));

                                    _cardCover.setCardElevation(getPxInDp(14) - ((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100 * coverCardElevation) + getPxInDp(2));
                                    _cardCover.setTranslationX((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100 * transXCoverCard);
                                    _cardCover.setTranslationY((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100 * transYCoverCard);
                                    _cardCover.getLayoutParams().height = (int) (getPxInDp(44) + ((1 - (_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * (Core.getWidthScreen() - getPxInDp(116))));
                                    _cardCover.getLayoutParams().width = (int) (getPxInDp(44) + ((1 - (_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * (Core.getWidthScreen() - getPxInDp(116))));
                                    _statusBar.setAlpha((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100);

                                    float frScale = ((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * 0.05f + 0.95f;
                                    float frAlpha = ((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * 0.3f + 0.7f;
                                    float frTransY = (1 - (_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * calcFragmentsTopMargin;
                                    float frCorner = (1 - (_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * fragmentsCornerRadius;
                                    _fragmentsCard.setScaleX(frScale);
                                    _fragmentsCard.setScaleY(frScale);
                                    _fragmentsCard.setTranslationY(frTransY);
                                    _fragmentsCard.setAlpha(frAlpha);
                                    _fragmentsCard.setRadius(frCorner > 1f || frCorner == 0 ? frCorner : 0f);

                                    _cardCover.requestLayout();
                                }
                            }
                        } else if (choosedMove == 1) {
                            _cardCover.setVisibility(View.VISIBLE);
                            _playerActionsLayout.setVisibility(View.VISIBLE);
                            _currentListLayout.setVisibility(View.VISIBLE);

                            if (!CURRENT_LIST_LAYOUT_EXPANDED) {
                                if (currentX > motionEvent.getRawX()) {
                                    _cardCover.setTranslationX(-(currentX - motionEvent.getRawX()) / swipeMultiplier);
                                    _playerActionsLayout.setTranslationX(-(currentX - motionEvent.getRawX()) / swipeMultiplier);
                                    _currentListLayout.setTranslationX(Core.getWidthScreen() - (currentX - motionEvent.getRawX()) / swipeMultiplier);
                                }
                            } else {
                                if (currentX < motionEvent.getRawX()) {
                                    _cardCover.setTranslationX((motionEvent.getRawX() - currentX) / swipeMultiplier - Core.getWidthScreen());
                                    _playerActionsLayout.setTranslationX((motionEvent.getRawX() - currentX) / swipeMultiplier - Core.getWidthScreen());
                                    _currentListLayout.setTranslationX((motionEvent.getRawX() - currentX) / swipeMultiplier);
                                }
                            }
                        }
                        return true;
                    default:
                        if (choosedMove == 2 && !CURRENT_LIST_LAYOUT_EXPANDED) {

                            if (!PLAYER_EXPANDED) {
                                if (currentY - motionEvent.getRawY() > getPxInDp(60) * 2) {
                                    transitionPlayerLayoutY(true);
                                } else {
                                    transitionPlayerLayoutY(false);
                                }
                            } else {
                                if (motionEvent.getRawY() - currentY > getPxInDp(60) * 2) {
                                    transitionPlayerLayoutY(false);
                                } else {
                                    transitionPlayerLayoutY(true);
                                }
                            }
                        } else if (choosedMove == 1) {
                            if (!CURRENT_LIST_LAYOUT_EXPANDED) {
                                if (currentX - motionEvent.getRawX() > getPxInDp(60) * 1.75f)
                                    transitionPlayerLayoutX(false);
                                else transitionPlayerLayoutX(true);
                            } else {
                                if (motionEvent.getRawX() - currentX > getPxInDp(60)) {
                                    transitionPlayerLayoutX(true);
                                } else {
                                    transitionPlayerLayoutX(false);
                                }
                            }
                        }

                        currentX = -1;
                        currentY = -1;
                        choosedMove = 0;
                        return super.onTouchEvent(motionEvent);
                }
            case R.id.cv_player_widget:
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        currentY = motionEvent.getRawY();
                        currentX = motionEvent.getRawX();
                        currentYTrans = _playerLayout.getTranslationY();

                        _playerCard.setVisibility(View.VISIBLE);
                        _playerWidget.setVisibility(View.VISIBLE);

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        if (!PLAYER_EXPANDED) {
                            if (currentY > motionEvent.getRawY() && (currentY - motionEvent.getRawY()) <= defaultPlayerTrans) {
                                _playerLayout.setTranslationY(defaultPlayerTrans - (currentY - motionEvent.getRawY()) / swipeMultiplier);
                                if (_playerLayout.getTranslationY() > defaultPlayerTrans / 2) {
                                    _playerCard.setAlpha(1f - ((_playerLayout.getTranslationY() - defaultPlayerTrans / 2) / (defaultPlayerTrans / 2 / 100)) / 100);
                                    _playerWidget.setAlpha(((_playerLayout.getTranslationY() - defaultPlayerTrans / 2) / (defaultPlayerTrans / 2 / 100)) / 100);
                                    _bottomNavigationView.setTranslationY(bottomBarHeight - ((_playerLayout.getTranslationY() - defaultPlayerTrans / 2) / (defaultPlayerTrans / 2 / 100)) / 100 * bottomBarHeight);
                                    setLightStatusBar(true);
                                } else {
                                    _playerCard.setAlpha(1f);
                                    _playerWidget.setAlpha(0f);
                                    setLightStatusBar(false);
                                }
                                _playerCard.setCardElevation(getPxInDp(8) - ((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100 * getPxInDp(8)));

                                _cardCover.setCardElevation(getPxInDp(14) - ((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100 * coverCardElevation) + getPxInDp(2));
                                _cardCover.setTranslationX((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100 * transXCoverCard);
                                _cardCover.setTranslationY((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100 * transYCoverCard);
                                _cardCover.getLayoutParams().height = (int) (getPxInDp(44) + ((1 - (_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * (Core.getWidthScreen() - getPxInDp(116))));
                                _cardCover.getLayoutParams().width = (int) (getPxInDp(44) + ((1 - (_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * (Core.getWidthScreen() - getPxInDp(116))));
                                _statusBar.setAlpha((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100);

                                float frScale = ((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * 0.05f + 0.95f;
                                float frAlpha = ((_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * 0.3f + 0.7f;
                                float frTransY = (1 - (_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * calcFragmentsTopMargin;
                                float frCorner = (1 - (_playerLayout.getTranslationY() / (defaultPlayerTrans / 100)) / 100) * fragmentsCornerRadius;
                                _fragmentsCard.setScaleX(frScale);
                                _fragmentsCard.setScaleY(frScale);
                                _fragmentsCard.setTranslationY(frTransY);
                                _fragmentsCard.setAlpha(frAlpha);
                                _fragmentsCard.setRadius(frCorner > 1f ? frCorner : 0f);

                                _cardCover.requestLayout();
                            }
                        } else {
                            if (currentY < motionEvent.getRawY()) {
                                _playerLayout.setTranslationY((motionEvent.getRawY() - currentY) / swipeMultiplier);
                            }
                        }
                        return true;
                    default:
                        if (Math.abs(motionEvent.getRawX() - currentX) <= widgetClickSensitivity &&
                                Math.abs(motionEvent.getRawY() - currentY) <= widgetClickSensitivity) {
                            transitionPlayerLayoutY(true);
                        } else if (!PLAYER_EXPANDED) {
                            if (currentY - motionEvent.getRawY() > getPxInDp(60) * 2) {
                                transitionPlayerLayoutY(true);
                            } else {
                                transitionPlayerLayoutY(false);
                            }
                        } else {
                            if (motionEvent.getRawY() - currentY > getPxInDp(60) * 2) {
                                transitionPlayerLayoutY(false);
                            } else {
                                transitionPlayerLayoutY(true);
                            }
                        }
                        return false;
                }
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        if (DIALOG_SHOWED) {
            super.onBackPressed();
        } else if (CURRENT_LIST_LAYOUT_EXPANDED) {
            transitionPlayerLayoutX(true);
        } else if (PLAYER_EXPANDED) {
            transitionPlayerLayoutY(false);
        } else super.onBackPressed();
    }

    private void transitionPlayerLayoutX(boolean expand) {
        if (this.animatorSetX != null && (this.animatorSetX.isRunning() || this.animatorSetX.isStarted())) {
            this.animatorSetX.cancel();
            this.animatorSetX = null;
        }

        float playerActionsTransX = expand ? 0 : -Core.getWidthScreen();
        float currentListLayoutTransX = expand ? Core.getWidthScreen() : 0;
        float cardCoverTransX = expand ? 0 : -Core.getWidthScreen();

        ObjectAnimator objectAnimatorCardCoverTransX = ObjectAnimator.ofFloat(_cardCover, View.TRANSLATION_X, cardCoverTransX);
        ObjectAnimator objectAnimatorPlayerActionsTransX = ObjectAnimator.ofFloat(_playerActionsLayout, View.TRANSLATION_X, playerActionsTransX);
        ObjectAnimator objectAnimatorCurrentListTransX = ObjectAnimator.ofFloat(_currentListLayout, View.TRANSLATION_X, currentListLayoutTransX);

        animatorSetX = new AnimatorSet();
        animatorSetX.setDuration(350);

        animatorSetX.playTogether(objectAnimatorCardCoverTransX, objectAnimatorCurrentListTransX,
                objectAnimatorPlayerActionsTransX);

        animatorSetX.setInterpolator(new OvershootInterpolator());

        animatorSetX.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                if (expand) {
                    _playerActionsLayout.setVisibility(View.VISIBLE);
                    _cardCover.setVisibility(View.VISIBLE);
                } else _currentListLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!expand) {
                    _playerActionsLayout.setVisibility(View.GONE);
                    _cardCover.setVisibility(View.GONE);
                } else _currentListLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        CURRENT_LIST_LAYOUT_EXPANDED = !expand;

        _playerLayout.setInterceptTouchEvent(CURRENT_LIST_LAYOUT_EXPANDED);

        animatorSetX.start();
    }


    private void transitionPlayerLayoutY(boolean expand) {
        if (this.animatorSetY != null && (this.animatorSetY.isRunning() || this.animatorSetY.isStarted())) {
            this.animatorSetY.cancel();
            this.animatorSetY.removeAllListeners();
            this.animatorSetY = null;
        }

        float transY = expand ? 0 : defaultPlayerTrans;
        int sizeCoverCard = expand ? Core.getWidthScreen() - getPxInDp(72) : getPxInDp(44);
        float transYCoverCard = expand ? 0 : this.transYCoverCard;
        float transXCoverCard = expand ? 0 : this.transXCoverCard;
        float transYFragments = expand ? calcFragmentsTopMargin : 0;
        float scaleFragments = expand ? 0.95f : 1;
        float alphaFragments = expand ? 0.7f : 1;
        float cornerRadiusStart = _fragmentsCard.getRadius();
        float cornerRadiusEnd = expand ? fragmentsCornerRadius : 0;
        float widgetAlpha = expand ? 0f : 1f;
        float playerCardAlpha = expand ? 1f : 0f;
        float bottomBarTransY = expand ? bottomBarHeight : 0;
        float coverCardElevationEnd = expand ? getPxInDp(16) : getPxInDp(2);
        float playerCardElevationEnd = expand ? getPxInDp(8) : getPxInDp(0);
        float coverCardElevationStart = _cardCover.getCardElevation();
        float playerCardElevationStart = _playerCard.getCardElevation();


        PLAYER_EXPANDED = expand;

        AnimatorSet animatorSet = new AnimatorSet();

        ValueAnimator animatorFragmentsCorner = ValueAnimator.ofFloat(cornerRadiusStart, cornerRadiusEnd);
        ValueAnimator animatorCoverCardElevation = ValueAnimator.ofFloat(coverCardElevationStart, coverCardElevationEnd);
        ValueAnimator animatorPlayerCardElevation = ValueAnimator.ofFloat(playerCardElevationStart, playerCardElevationEnd);
        ObjectAnimator animatorFragmentsScaleX = ObjectAnimator.ofFloat(_fragmentsCard, View.SCALE_X, scaleFragments);
        ObjectAnimator animatorFragmentsScaleY = ObjectAnimator.ofFloat(_fragmentsCard, View.SCALE_Y, scaleFragments);
        ObjectAnimator animatorFragmentsTransY = ObjectAnimator.ofFloat(_fragmentsCard, View.TRANSLATION_Y, transYFragments);
        ObjectAnimator animatorFragmentsAlpha = ObjectAnimator.ofFloat(_fragmentsCard, View.ALPHA, alphaFragments);
        ObjectAnimator animatorPlayerTransY = ObjectAnimator.ofFloat(_playerLayout, View.TRANSLATION_Y, transY);
        ObjectAnimator animatorPlayerWidgetAlpha = ObjectAnimator.ofFloat(_playerWidget, View.ALPHA, widgetAlpha);
        ObjectAnimator animatorPlayerCardAlpha = ObjectAnimator.ofFloat(_playerCard, View.ALPHA, playerCardAlpha);
        ObjectAnimator animatorBottomBarTransY = ObjectAnimator.ofFloat(_bottomNavigationView, View.TRANSLATION_Y, bottomBarTransY);
        ObjectAnimator animatorCoverCardWidth = ViewPropertyObjectAnimator.animate(_cardCover)
                .width(sizeCoverCard)
                .height(sizeCoverCard)
                .translationX(transXCoverCard)
                .translationY(transYCoverCard)
                .get();


        animatorSet.playTogether(animatorPlayerTransY, animatorPlayerCardAlpha,
                animatorPlayerWidgetAlpha, animatorCoverCardWidth, animatorFragmentsScaleX,
                animatorFragmentsTransY, animatorFragmentsAlpha, animatorFragmentsScaleY,
                animatorFragmentsCorner, animatorBottomBarTransY, animatorPlayerCardElevation,
                animatorCoverCardElevation);
        animatorSet.setDuration(256);

        animatorSet.setInterpolator(new LinearInterpolator());


        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                setLightStatusBar(expand ? false : true);

                _playerLayout.setEnabled(false);
                _playerWidget.setEnabled(false);

                if (_playerWidget.getVisibility() == View.GONE)
                    _playerWidget.setVisibility(View.VISIBLE);
                if (_playerCard.getVisibility() == View.GONE)
                    _playerCard.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!expand) {
                    _playerCard.setVisibility(View.GONE);
                    _statusBar.setAlpha(1f);
                    _statusBar.requestLayout();
                } else {
                    _statusBar.setAlpha(0f);
                    _playerWidget.setVisibility(View.GONE);
                }

                _playerLayout.setEnabled(expand ? true : false);
                _playerWidget.setEnabled(!expand ? true : false);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });


        animatorFragmentsCorner.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                _fragmentsCard.setRadius(value >= 1f ? value : 0f);
            }
        });

        animatorPlayerCardElevation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                _playerCard.setCardElevation(value >= 1f ? value : 0f);
            }
        });

        animatorCoverCardElevation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                _cardCover.setCardElevation(value >= 1f ? value : 0f);
            }
        });

        this.animatorSetY = animatorSet;

        animatorSet.start();
    }


    private void transitionCover(boolean playing) {
        float scale = playing ? 1f : 0.85f;

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator objectAnimatorX = ObjectAnimator.ofFloat(_cardCover, View.SCALE_X, scale);
        ObjectAnimator objectAnimatorY = ObjectAnimator.ofFloat(_cardCover, View.SCALE_Y, scale);


        animatorSet.playTogether(objectAnimatorX, objectAnimatorY);
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.setDuration(150);
        animatorSet.start();
    }


    @Subscribe
    public void onEvent(GlobalEvent event) {
        switch (event.getEvent()) {
            case GlobalEvent.UI_RECREATE:
                Intent intent = new Intent();
                intent.putExtra(IntentExtras.UI_RECREATE, true);
                setIntent(intent);
                recreate();
                break;
        }
    }


    @OnClick(R2.id.ib_like)
    void onLikeClick() {
        presenter.setLikeSong(audioService.getTrackList().get(audioService.getPosition()));
    }

    @Override
    public void setTrackInfo(SongModel song, int pos) {
        presenter.getLikeSong(song);

        _title.setText(song.getTitle());
        _titleWidget.setText(song.getTitle());
        _artist.setText(song.getArtist());
        _durationSeekBar.setProgress(0);
        _durationSeekBar.setMax(song.getDuration());

        presenter.getCover(song);
        presenter.convertDurationToString(song.getDuration(), Presenter.SecConverterFlag.DURATION);

        songAdapter.setCurrPlaying(pos);
        if (lastPos != -1) {
            songAdapter.notifyItemChanged(lastPos);
        }
        songAdapter.notifyItemChanged(pos);
        lastPos = pos;

        if (!CURRENT_LIST_LAYOUT_EXPANDED)
            layoutManager.scrollToPositionWithOffset(pos - 1, 0);
    }


    @Override
    public void pauseMusic() {
        _play.setImageResource(R.drawable.ic_play32);
        _playWidget.setImageResource(R.drawable.ic_play20);
        transitionCover(false);
    }


    @Override
    public void playMusic() {
        _play.setImageResource(R.drawable.ic_pause32);
        _playWidget.setImageResource(R.drawable.ic_pause20);
        transitionCover(true);
    }


    @Override
    public void updateDurationPosition(int pos) {
        _durationSeekBar.setProgress(pos);
        presenter.convertDurationToString(pos, Presenter.SecConverterFlag.CURRENT_TIME);
    }


    @Override
    public void bindTrackList(List<SongModel> trackList) {
        if (songAdapter.getObjects() == null) {
            defaultPlayerTrans -= playerWidgetHeight;
            transitionPlayerLayoutY(false);
            EventBus.getDefault().post(new UIEvent.PlayerWidgetVisible());
        }

        songAdapter.changeTrackList(audioService.getTrackList());
    }


    @Override
    public void eventEmptyTrackList() {

    }


    @Override
    public void showDurationString(String duration) {
        _duration.setText(duration);
    }


    @Override
    public void showCurrentTimeString(String duration) {
        _currentTime.setText(duration);
    }


    @Override
    public void showCover(Bitmap cover) {
        if (cover == null)
            cover = BitmapFactory.decodeResource(getResources(), Core.getCoreInstance().getDarkTheme() == 0 ?
                    R.drawable.noimg2light : R.drawable.noimg2dark);

        Core.withGlide().clear(_cover);
        Core.withGlide().asBitmap()
                .placeholder(lastCover)
                .load(cover)
                .dontTransform()
                .transition(BitmapTransitionOptions.withCrossFade(300))
                .into(_cover);

        lastCover = cover != null ? new BitmapDrawable(getResources(), cover) : getDrawable(Core.getCoreInstance().getDarkTheme() == 0 ?
                R.drawable.noimg2light : R.drawable.noimg2dark);
    }

    @Override
    public void songIsLiked(boolean liked) {
        int icon = 0;
        if (liked) icon = R.drawable.like_dark;
        else icon = R.drawable.unlike_dark;
        _like.setImageResource(icon);
    }

    @OnClick(R2.id.ib_more)
    void onMoreClick() {
        BottomSheetDialogSongFragment bottomSheetFragment = new BottomSheetDialogSongFragment();
        bottomSheetFragment.bindSongModel(audioService.getTrackList().get(audioService.getPosition()));
        showDialog(bottomSheetFragment);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        presenter.convertDurationToString(i, Presenter.SecConverterFlag.CURRENT_TIME);
    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        audioService.pauseOnUpdateDuration();
    }


    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        audioService.seekTo(seekBar.getProgress());
        audioService.resetOnUpdateDuration();
    }


    @Subscribe
    public void onEvent(FragmentEvent fragmentEvent) {
        Log.i("LOG", "EVENT");
        switch (fragmentEvent.getMessage()) {
            case FragmentEvent.FRAGMENT_SEARCH_MORE_RESULTS:
                FragmentSearchMoreResults fragment = new FragmentSearchMoreResults();

                ArrayMap<String, Object> params = fragmentEvent.getParams();

                Bundle bundleFragmentMoreResults = new Bundle();
                bundleFragmentMoreResults.putInt(FragmentSearchMoreResults.TYPE_RESULTS, (int) params.get(FragmentSearchMoreResults.TYPE_RESULTS));
                bundleFragmentMoreResults.putString(FragmentSearchMoreResults.QUERY, (String) params.get(FragmentSearchMoreResults.QUERY));
                fragment.setArguments(bundleFragmentMoreResults);

                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, fragment)
                        .addToBackStack(null)
                        .commit();
                break;
            case FragmentEvent.FRAGMENT_LASTFM_ACC:
//                pagerAdapter.addFragment(new FragmentLastFMLoginForm());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_ALBUMS_VIEW:
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, new FragmentAlbumsView())
                        .addToBackStack(null)
                        .commit();
                break;
            case FragmentEvent.FRAGMENT_DOWNLOADS_SHOW:
//                pagerAdapter.addFragment(new FragmentDownloads());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_YOUTUBE_SHOW_VIDEO:
                FragmentYouTubeShowVideo youTubeShowVideo = new FragmentYouTubeShowVideo();
                youTubeShowVideo.setArguments(fragmentEvent.getBundle());
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, youTubeShowVideo)
                        .addToBackStack(null)
                        .commit();
                break;
            case FragmentEvent.PAGING:
//                viewPager.setPagingEnabled(fragmentEvent.getEnabled());
//                Log.d(tag, "<FragmentEvent> Paging is " + fragmentEvent.getEnabled());
                break;
            case FragmentEvent.SET_VIEWPAGER_INTERCEPT_TOUCH:
//                viewPager.setInterceptEnabled(fragmentEvent.getEnabled());
                break;
            case FragmentEvent.FRAGMENT_BACK:
                fragmentBackStack();
                break;
            case FragmentEvent.FRAGMENT_SONGSVIEW:
//                pagerAdapter.addFragment(new FragmentSongsView());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, new FragmentSongsView())
                        .addToBackStack(null)
                        .commit();
                break;
            case FragmentEvent.FRAGMENT_SHOW_ALL_FAV_SONGS_OPEN:
//                pagerAdapter.addFragment(new FragmentFavSongsView());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_SHOW_ALL_PLAYLISTS_OPEN:
                FragmentPlaylistsView fragmentPlaylistsView = new FragmentPlaylistsView();
                Bundle bundleFragmentPlaylistsView = new Bundle();
                bundleFragmentPlaylistsView.putInt("Code", 0);
                bundleFragmentPlaylistsView.putString("Title", getString(R.string.playlists));
                fragmentPlaylistsView.setArguments(bundleFragmentPlaylistsView);
//                pagerAdapter.addFragment(fragmentPlaylistsView);
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_NEW_PLAYLIST:
//                pagerAdapter.addFragment(new FragmentNewPlaylist());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_EXPAND_BOTTOM_PLAYER_CONTROL:
//                bottomPlayer.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case FragmentEvent.FRAGMENT_PLAYLIST_SONGS_VIEW:
                FragmentPlaylistSongsView fragmentPlaylistSongsView = new FragmentPlaylistSongsView();
                Bundle bundle = new Bundle();
                bundle.putInt("PlaylistID", fragmentEvent.getVarInt());
                fragmentPlaylistSongsView.setArguments(bundle);
//                pagerAdapter.addFragment(fragmentPlaylistSongsView);
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.TEST:
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.will_be_available_next_update), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                break;
            case FragmentEvent.FRAGMENT_SONG_ADD_TO_PLAYLIST:
                FragmentPlaylistsView fragmentPlaylistsViewAddSong = new FragmentPlaylistsView();
                Bundle bundleFragmentPlaylistsViewAddSong = new Bundle();
                bundleFragmentPlaylistsViewAddSong.putSerializable("Song", fragmentEvent.getSong());
                bundleFragmentPlaylistsViewAddSong.putString("Title", getString(R.string.add_to_a_playlist));
                bundleFragmentPlaylistsViewAddSong.putInt("Code", PlaylistNormalAdapter.ADD_TO_PLAYLIST);

                fragmentPlaylistsViewAddSong.setArguments(bundleFragmentPlaylistsViewAddSong);
//                pagerAdapter.addFragment(fragmentPlaylistsViewAddSong);
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_FORCE_LIGHT_STATUS_BAR:
//                lightStatusBar = fragmentEvent.getLightStatusBar();
//                setLightStatusBar(lightStatusBar);
                break;
            case FragmentEvent.FRAGMENT_DOWNLOAD:
                FragmentDownload fragmentDownload = new FragmentDownload();
                fragmentDownload.setArguments(fragmentEvent.getBundle());
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, fragmentDownload)
                        .addToBackStack(null)
                        .commit();
                break;
        }
    }

    @Subscribe
    public void onSongListEvent(SongListEvent event) {
        switch (event.event) {
            case SET_SONG:
                audioService.setTrack(event.pos);
                break;
            case BIND_LIST:
                audioService.bindTrackList(event.songList, event.pos);
                if (!audioService.getPlaying()) audioService.setPlaying();
                break;
            case ADD_SONG:
                audioService.addSong(event.songModel);
                break;
        }
    }

    @Override
    public void onSongItemClick(int pos) {
        audioService.setTrack(pos);
    }

    @Override
    public void onAlbumPlayClick() {

    }

    @Override
    public void onSongItemLongClick(int pos) {

    }
}
