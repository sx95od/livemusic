package com.sx95od.livemusic.Activities.MainActivity.Model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;

import com.bumptech.glide.load.DecodeFormat;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.FavSongs;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Utils.MD5;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 10.09.2017.
 */

public class Model implements IModel{

    @Override
    public Observable<String> convertSecsToString(long secs) {
        return Observable.fromCallable(() -> {
            long millSec = secs;
            long sec = millSec/1000;
            if (sec%60>9){
                return String.valueOf(Math.round(sec/60)+":"+sec%60);
            } else{
                return String.valueOf(Math.round(sec/60)+":"+"0"+sec%60);
            }
        });
    }

    @Override
    public Observable<Bitmap> getCover(SongModel song) {
        return Observable.fromCallable(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                Bitmap exportBitmap = null;
                Bitmap bitmap = null;
                try {
                    MediaMetadataRetriever mData = new MediaMetadataRetriever();
                    mData.setDataSource(song.getUri());
                    bitmap = Core.withGlide().asBitmap()
                            .centerCrop()
                            .load(mData.getEmbeddedPicture())
                            .submit(700, 700).get();

                    Bitmap newBitmap = Bitmap.createBitmap(700, 700, Bitmap.Config.RGB_565);
                    Canvas canvas = new Canvas(newBitmap);
                    canvas.drawColor(Color.WHITE);
                    canvas.drawBitmap(bitmap, 0, 0, null);
                    mData.release();
                    exportBitmap = newBitmap;
                }catch (Exception e){

                }
                return exportBitmap;
            }
        });
    }

    @Override
    public Observable<Boolean> setLikeSong(SongModel songModel) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Realm realm = Realm.getInstance(Core.getConfigRealm());

                RealmResults<FavSongs> results = realm.where(FavSongs.class)
                        .equalTo("_id", songModel.getId()).findAll();

                if (results.size() == 0) {
                    realm.beginTransaction();
                    FavSongs favSongs = realm.createObject(FavSongs.class, generateID());
                    favSongs.set_id(songModel.getId());
                    favSongs.setAlbum(songModel.getAlbum());
                    favSongs.setAlbum_id(songModel.getAlbum_id());
                    favSongs.setArtist(songModel.getArtist());
                    favSongs.setArtist_id(songModel.getArtist_id());
                    favSongs.setDate_modified(songModel.getDate());
                    favSongs.setDuration(songModel.getDuration());
                    favSongs.setUri(songModel.getUri());
                    favSongs.setYear(songModel.getYear());
                    favSongs.setTitle(songModel.getTitle());
                    favSongs.setMD5(MD5.checkMD5(songModel.getUri()));
                    realm.commitTransaction();
                    return true;
                } else {
                    realm.beginTransaction();
                    results.deleteAllFromRealm();
                    realm.commitTransaction();
                    return false;
                }
            }
        });
    }

    @Override
    public Observable<Boolean> getLikeSong(SongModel songModel) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Realm realm = Realm.getInstance(Core.getConfigRealm());
                RealmResults<FavSongs> results = realm.where(FavSongs.class)
                        .equalTo("_id", songModel.getId()).findAll();
                if (results.size()>0){
                    return true;
                } else return false;
            }
        });
    }

    private int generateID(){
        Realm realm = Realm.getInstance(Core.getConfigRealm());
        Number currentIdNum = realm.where(FavSongs.class).max("id");
        int nextId;
        if(currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }
        return nextId;
    }
}
