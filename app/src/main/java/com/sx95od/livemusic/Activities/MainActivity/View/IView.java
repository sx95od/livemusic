package com.sx95od.livemusic.Activities.MainActivity.View;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by stoly on 10.09.2017.
 */

public interface IView {
    void showDurationString(String duration);
    void showCurrentTimeString(String duration);
    void showCover(Bitmap cover);
    void songIsLiked(boolean liked);
}
