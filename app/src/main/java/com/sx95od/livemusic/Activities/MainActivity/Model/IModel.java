package com.sx95od.livemusic.Activities.MainActivity.Model;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import io.reactivex.Observable;

/**
 * Created by stoly on 04.02.2018.
 */

public interface IModel {
    Observable<String>  convertSecsToString(long secs);
    Observable<Bitmap>  getCover(SongModel song);
    Observable<Boolean> setLikeSong(SongModel songModel);
    Observable<Boolean> getLikeSong(SongModel songModel);
}
