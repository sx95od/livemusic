package com.sx95od.livemusic.Core;

/**
 * Created by stoly on 28.08.2017.
 */

public class AppPreferences {
    public static final String HIDDEN_PREFERENCE = "sx95od_livemusic_hidden_preference";
    public static final String HIDDEN_ACCESS_KEY = "sx95od_livemusic_hidden_access_key";

    public static final int FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS_DEFAULT = 5;
    public static final String FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS = "sx95od_livemusic_fragment_search_recent_visible_items";
    public static final int  FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS_DEFAULT = 5;
    public static final String  FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS = "sx95od_livemusic_fragment_search_result_list_visible_items";
}
