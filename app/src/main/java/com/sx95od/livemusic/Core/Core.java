package com.sx95od.livemusic.Core;

import android.Manifest;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.WorkerThread;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.sx95od.livemusic.API.GoogleAuthApi;
import com.sx95od.livemusic.API.GoogleTokenApi;
import com.sx95od.livemusic.API.LastFM.LastFMApi;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.API.YouTubeTokenManager;
import com.sx95od.livemusic.API.YoutubeHideApi.YoutubeHideAPI;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Core.Glide.GlideRequests;
import com.sx95od.livemusic.DataBase.DefaultRealmModule;
import com.sx95od.livemusic.DataBase.FavSongs;
import com.sx95od.livemusic.DataBase.HideRealmModule;
import com.sx95od.livemusic.DataBase.Playlists;
import com.sx95od.livemusic.DataBase.YouTubeCredentials;
import com.sx95od.livemusic.Events.CoreEvent;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Services.AudioService.AudioService;
import com.sx95od.livemusic.Utils.Preferences;
import com.sx95od.livemusic.Utils.Preload;
import com.sx95od.livemusic.Utils.SystemUtils;
import com.sx95od.livemusic.Utils.ThemeEngine;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.model.AudioFormat;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by stoly on 11.06.2017.
 */

public class Core extends Application {
    private final String tag = "Core";

    public static final int OBJECT_LASTFM_TOP_TRACKS_LIST = 1;
    public static final int OBJECT_LASTFM_TOP_TRACKS_PAGE = 2;
    public static final int OBJECT_LASTFM_GEO_TOP_TRACKS_LIST = 3;
    public static final int OBJECT_LASTFM_GEO_TOP_TRACKS_PAGE = 4;

    private List<SongModel> tracksArray;
    private List<AlbumModel> albumArray;
    static SharedPreferences pref;
    private int checkLoadCount = 0;
    static int statusBarHeight = 0;
    public static int widthScreen = 0;
    public static int heightScreen = 0;
    private static LastFMApi lastFMApi;
    private static YoutubeHideAPI youtubeHideAPI;
    private static YouTubeApi youtubeApi;
    public static GoogleAuthApi googleAuthApi;
    public static GoogleTokenApi googleTokenApi;
    private Retrofit retrofitLastFM;
    private Retrofit retrofitYouTubeHide;
    private Retrofit retrofitYouTube;
    private Retrofit retrofitGoogleAuth;
    private Retrofit retrofitGoogleToken;
    static private boolean h = false;

    public static int YT_LAST_CODE = 400;

    GoogleApiClient gApiClient;

    public static HashMap<Integer, Object> hashMap = new HashMap<>();

    private static Core coreInstance = null;

    private Tracker mTracker;

    boolean INTERNAL_SONGS_LOADED = false;

    Thread thread;



    public static void putObject(int key, Object object){
        hashMap.put(key, object);
    }

    public static Object getObject(int key){
        return hashMap.get(key);
    }

    public static final RealmConfiguration getConfigRealm(){
        RealmConfiguration config;
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder()
                .name("livemusic.realm")
                .schemaVersion(1);
        builder.modules(new DefaultRealmModule());
        config = builder.build();
        return config;
    }

    public static final RealmConfiguration getHideConfigRealm(){
        if (h) {
            RealmConfiguration config;
            RealmConfiguration.Builder builder = new RealmConfiguration.Builder()
                    .name("livemusichide.realm")
                    .schemaVersion(1);
            builder.modules(new HideRealmModule());
            config = builder.build();
            return config;
        } else{
            return null;
        }
    }


    public static int isGoogleConnected(){
        return YT_LAST_CODE;
    }


    private void initYouTubeTokenManager(){
        Realm realm = Realm.getInstance(Core.getConfigRealm());
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<YouTubeCredentials> resultsAT = realm.where(YouTubeCredentials.class)
                        .equalTo("data_name", "YT_AccessToken").findAll();
                RealmResults<YouTubeCredentials> resultsRT = realm.where(YouTubeCredentials.class)
                        .equalTo("data_name", "YT_RefreshToken").findAll();

                if (resultsAT.size() > 0){
                    byte[] byteAT = Base64.decode(resultsAT.get(0).getData(), Base64.DEFAULT);
                    String aT = new String(byteAT, StandardCharsets.UTF_8);
                    YouTubeTokenManager.ACCESS_TOKEN = aT;
                }

                if (resultsRT.size() > 0){
                    byte[] byteRT = Base64.decode(resultsRT.get(0).getData(), Base64.DEFAULT);
                    String rT = new String(byteRT, StandardCharsets.UTF_8);
                    YouTubeTokenManager.REFRESH_TOKEN = rT;
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.d(tag, YouTubeTokenManager.ACCESS_TOKEN + " - " + YouTubeTokenManager.REFRESH_TOKEN);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.d("YTTOKENMANAGER", "initYouTubeTokenManager error");
            }
        });
    }

    public void updateTokenManager(final String data){
        Log.d(tag, data+" is null?");

        Realm realm = Realm.getInstance(Core.getConfigRealm());
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
//                byte[] dataByte = data.getBytes(StandardCharsets.UTF_8);
//                String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);


                RealmResults<YouTubeCredentials> resultsAT = realm.where(YouTubeCredentials.class)
                        .equalTo("data_name", "YT_AccessToken").findAll();
                RealmResults<YouTubeCredentials> resultsRT = realm.where(YouTubeCredentials.class)
                        .equalTo("data_name", "YT_RefreshToken").findAll();

                if (resultsAT.size() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(data);
                        String access_token = jsonObject.getString("access_token");

                        byte[] dataByte = access_token.getBytes(StandardCharsets.UTF_8);
                        String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                        resultsAT.get(0).setData(base64);
                    }catch (Exception e){
                        Log.e(tag, e.toString());
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(data);
                        String access_token = jsonObject.getString("access_token");

                        byte[] dataByte = access_token.getBytes(StandardCharsets.UTF_8);
                        String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                        YouTubeCredentials youTubeCredentials = realm.createObject(YouTubeCredentials.class, 0);
                        youTubeCredentials.setData(base64);
                        youTubeCredentials.setData_name("YT_AccessToken");
                    }catch (Exception e){

                    }
                }

                if (resultsRT.size() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(data);
                        String refresh_token = jsonObject.getString("refresh_token");

                        byte[] dataByte = refresh_token.getBytes(StandardCharsets.UTF_8);
                        String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                        resultsRT.get(0).setData(base64);
                    }catch (Exception e){
                        Log.e(tag, e.toString());
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(data);
                        String refresh_token = jsonObject.getString("refresh_token");

                        byte[] dataByte = refresh_token.getBytes(StandardCharsets.UTF_8);
                        String base64 = Base64.encodeToString(dataByte, Base64.DEFAULT);

                        YouTubeCredentials youTubeCredentials = realm.createObject(YouTubeCredentials.class, 1);
                        youTubeCredentials.setData(base64);
                        youTubeCredentials.setData_name("YT_RefreshToken");
                    }catch (Exception e){

                    }
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                initYouTubeTokenManager();
                Log.d("YTTOKENMANAGER", "updateTokenManager success");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.d("YTTOKENMANAGER", error.toString());
            }
        });
    }

    public static final boolean getH(){
        return h;
    }


    public static Core getCoreInstance(){
        return coreInstance;
    }


    public boolean getShowingAd(){
       return pref.getBoolean("showing_ad", true);
    }


    synchronized public Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    public void scanLibrary(final Listeners.OnMediaScanCompleted onMediaScanCompleted){
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(Core.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    findSongs();
                    findAlbums();
                }
                onMediaScanCompleted.onScanFinished();
            }
        });
        thread.start();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        coreInstance = this;
        SystemUtils.init(this);

        pref = getSharedPreferences("sx95odlivemusic", Context.MODE_PRIVATE);
        if (!pref.getString(AppPreferences.HIDDEN_ACCESS_KEY, "null").equals(ProtectAPI.getEncryptAccessKey().trim())){
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(AppPreferences.HIDDEN_PREFERENCE, false);
            editor.apply();
            editor = null;
            h = false;
        } else{
            h = true;
        }

        Preload.loadPreloaders();

        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onStart() {
                    Log.d("FFMPEG", "library start loading...");
                }

                @Override
                public void onFailure() {
                    Log.e("FFMPEG", "Failed");
                }

                @Override
                public void onSuccess() {
                    Log.d("FFMPEG", "library loading successful!");
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            Log.e("FFMPEG", e.toString());
        }

        MobileAds.initialize(this, BuildConfig.ADMOB_ID);

        Realm.init(this);
        Realm.compactRealm(getConfigRealm());


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        widthScreen = metrics.widthPixels;
        heightScreen = metrics.heightPixels;

        try {
            Realm.deleteRealm(Realm.getDefaultInstance().getConfiguration());
        }catch (Exception e){
            Log.e(getClass().getSimpleName(), "<"+e.toString()+"> "+"old db already deleted");
        }


        retrofitLastFM = new Retrofit.Builder()
                .baseUrl("https://ws.audioscrobbler.com/2.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        lastFMApi = retrofitLastFM.create(LastFMApi.class);

        retrofitYouTubeHide = new Retrofit.Builder()
                .baseUrl("https://wt-b4a6581eaacd1ddf1e20ff9b891c2a4e-0.run.webtask.io/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        youtubeHideAPI = retrofitYouTubeHide.create(YoutubeHideAPI.class);

        retrofitYouTube = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/youtube/v3/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        youtubeApi = retrofitYouTube.create(YouTubeApi.class);

        retrofitGoogleAuth = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/oauth2/v4/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        googleAuthApi = retrofitGoogleAuth.create(GoogleAuthApi.class);

        retrofitGoogleToken = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/oauth2/v3/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        googleTokenApi = retrofitGoogleToken.create(GoogleTokenApi.class);

//        scalLibrary();

        initYouTubeTokenManager();

        Intent intent = new Intent(this, AudioService.class);
        startService(intent);
    }



    public static Glide getGlide(){
        return GlideApp.get(getCoreInstance());
    }


    public static GlideRequests withGlide(){
        return GlideApp.with(getCoreInstance());
    }


    public static int getWidthScreen(){
        return widthScreen;
    }


    public static int getHeightScreen(){
        return heightScreen;
    }


    public static LastFMApi getLastFMApi(){
        return lastFMApi;
    }


    public static YouTubeApi getYoutubeApi(){
        return youtubeApi;
    }


    public static YoutubeHideAPI getYoutubeHideAPI(){
        return youtubeHideAPI;
    }


    public static SharedPreferences getPref(){
        return pref;
    }


    public int getDarkTheme(){
        return pref.getInt(ThemeEngine.DARK_THEME, 0);
    }


    public static int getStatusBarHeight(){
        return statusBarHeight;
    }


    public List<SongModel> getSongs(){
        return tracksArray;
    }


    private void checkLoaded(){
        checkLoadCount++;
        if (checkLoadCount==2){
            INTERNAL_SONGS_LOADED = true;
            EventBus.getDefault().post(new CoreEvent(CoreEvent.LOCAL_SONGS_LOADED));
        }
    }



    public void scalLibrary(){
        thread = new Thread(() -> {
            if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(Core.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                findSongs();
                findAlbums();
            }
        });
        thread.start();
    }


    @WorkerThread
    public void findAlbums(){
        Log.d("Core", "<Content provider> start to search albums");
        ContentResolver contentResolver = getContentResolver();
        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(uri, null, null, null, "ALBUM ASC");
        albumArray = new ArrayList<AlbumModel>();
        while (cursor.moveToNext()){
            long _id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Albums._ID));
            String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM));
            String albumArt = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
            String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST));
            String firstYear = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.FIRST_YEAR));
            String lastYear = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.LAST_YEAR));
            long countSongs = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS));
            albumArray.add(new AlbumModel(album, albumArt, artist, firstYear, lastYear, _id, countSongs));
        }
        checkLoaded();
    }


    public List<AlbumModel> getAlbums(){
        return albumArray;
    }


    @WorkerThread
    public void findSongs(){
            try {
                tracksArray = new ArrayList<SongModel>();
                int dur = 0;
//                if (pref.getBoolean(SettingsConstants.PLAYING_DONT_SHOW_SHORT_AUDIO, true)) {
//                    dur = 60000;
//                } else {
//
//                }

                ContentResolver contentResolver = getContentResolver();
                Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                Cursor cursor = contentResolver.query(uri, null, "duration > ?", new String[]{String.valueOf(dur)}, "TITLE ASC");
                while (cursor.moveToNext()) {
                    tracksArray.add(new SongModel(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media._ID)),
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)), "",
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)),
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)),
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)),
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)),
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID)),
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)),
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.YEAR)),
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.DATE_MODIFIED)),
                            false));
                }
            } catch (Exception e) {
                Log.i("Error", e.toString());
            } finally {
                checkLoaded();
            }
    }

    public static void setStatusBarHeight(int height){
        statusBarHeight = height;
    }


    public boolean getInternalSongsLoaded(){
        return INTERNAL_SONGS_LOADED;
    }



}
