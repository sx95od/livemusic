package com.sx95od.livemusic.Core;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import com.sx95od.livemusic.BuildConfig;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by stoly on 29.08.2017.
 */

public class ProtectAPI {

    public static String getEncryptAccessKey(){
        byte[] key = null;
        try {
            key = BuildConfig.HiddenPrefsKey.getBytes("UTF-8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); // use only first 128 bit
        }catch (Exception e){
            Log.e("Encryption", "Generation of key fail");
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        byte[] encodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            encodedBytes = c.doFinal(Settings.Secure.getString(Core.getCoreInstance().getContentResolver(), Settings.Secure.ANDROID_ID).getBytes());
        } catch (Exception e) {
            Log.e("Encryption", "AES encryption error");
        }

        return Base64.encodeToString(encodedBytes, Base64.DEFAULT);
    }
}
