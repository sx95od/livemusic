package com.sx95od.livemusic.Core.Glide;


import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.DiskLruCacheFactory;
import com.bumptech.glide.load.engine.cache.ExternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by stoly on 01.09.2017.
 */

@com.bumptech.glide.annotation.GlideModule
public final class GlideModule extends AppGlideModule {
    private final String tag = GlideModule.class.getSimpleName();

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        Log.d(tag, "Glide Module is connected");
        int memoryCacheSizeBytes = 1024 * 1024 * 50; // 50mb
        builder.setDefaultRequestOptions(new RequestOptions()
                        .format(DecodeFormat.DEFAULT).disallowHardwareConfig()
                        .skipMemoryCache(true)
                        .fitCenter()
//                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        );
//                .setMemoryCache(new LruResourceCache(1024*1024*10))
//                .setBitmapPool(new LruBitmapPool(1024*1024*10))
//                .setDiskCache(new DiskLruCacheFactory(context.getExternalCacheDir().getAbsolutePath(), memoryCacheSizeBytes));

    }

    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {
        super.registerComponents(context, glide, registry);
    }


}
