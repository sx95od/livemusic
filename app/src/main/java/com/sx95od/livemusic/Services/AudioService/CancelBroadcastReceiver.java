package com.sx95od.livemusic.Services.AudioService;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import static com.sx95od.livemusic.Services.AudioService.AudioService.PLAYER_CHANNEL_ID;

/**
 * Created by stoly on 20.03.2018.
 */

public class CancelBroadcastReceiver extends BroadcastReceiver {
    private final String tag = "BR-Cancel";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                NotificationManager notificationManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.deleteNotificationChannel(PLAYER_CHANNEL_ID);
            }catch (Exception e){
                Log.e(tag, e.toString());
            }
        }
    }
}
