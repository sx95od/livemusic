package com.sx95od.livemusic.Services.AudioService.Listeners;

import android.content.Intent;

/**
 * Created by stoly on 16.09.2017.
 */

public interface BroadcastListener {
    void onReceive(Intent intent);
}
