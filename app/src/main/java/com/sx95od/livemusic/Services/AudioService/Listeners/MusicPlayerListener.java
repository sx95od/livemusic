package com.sx95od.livemusic.Services.AudioService.Listeners;

/**
 * Created by stoly on 12.09.2017.
 */

public interface MusicPlayerListener {
    void onUpdateDuration(int pos);
    void onComplete();
}
