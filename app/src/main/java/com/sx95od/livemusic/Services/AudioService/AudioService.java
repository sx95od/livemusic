package com.sx95od.livemusic.Services.AudioService;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.session.MediaSession;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.view.KeyEvent;

import com.sx95od.livemusic.API.MusicPlayer;
import com.sx95od.livemusic.Activities.MainActivity.MainActivity;
import com.sx95od.livemusic.Acts.GodActivity.View.IServiceView;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Services.AudioService.Listeners.BroadcastListener;
import com.sx95od.livemusic.Services.AudioService.Listeners.MusicPlayerListener;

import java.util.List;

/**
 * Created by stoly on 12.09.2017.
 */

public class AudioService extends Service implements MusicPlayerListener, BroadcastListener{
    public final static String PLAYER_CHANNEL_ID = "com.sx95od.livemusic.player";
    public final static String MEDIASESSION_LIVEMUSIC = "LiveMusicSession";
    private final String tag = AudioService.class.getSimpleName();
    private final IBinder binder = new AudioBinder();
    private IServiceView viewListener;
    private List<SongModel> trackList;

    private MusicPlayer musicPlayer;
    private MediaSessionCompat mediaSession;
    private NotificationManager managerCompat;
    private PlaybackStateCompat.Builder playbackStateCompat;
    private MediaBroadcast mediaBroadcast;

    private StartNotification startNotification;

    private int position = -1;
    private boolean playing = false;

    @Override
    public void onUpdateDuration(int pos) {
        if (viewListener == null){
            musicPlayer.forceStopTimer(); return;
        }
        viewListener.updateDurationPosition(pos);
    }

    @Override
    public void onComplete() {
        nextTrack();
    }

    public void seekTo(int pos){
        musicPlayer.seekTo(pos);
    }

    public boolean getPlaying(){
        return playing;
    }

    @Override
    public void onReceive(Intent intent) {
        KeyEvent event = (KeyEvent) intent
                .getParcelableExtra(Intent.EXTRA_KEY_EVENT);
        int keycode = event.getKeyCode();
        int action = event.getAction();
        Log.d("keycode", String.valueOf(keycode));
        Log.d("action", String.valueOf(action));
        //onKeyDown(keyCode, event)
        if (keycode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE || keycode == KeyEvent.KEYCODE_MEDIA_PAUSE)
            if (action == KeyEvent.ACTION_DOWN)
                setPlaying();
        if (keycode == KeyEvent.KEYCODE_MEDIA_NEXT)
            if (action == KeyEvent.ACTION_DOWN)
                nextTrack();
        if (keycode == KeyEvent.KEYCODE_MEDIA_PREVIOUS)
            if (action == KeyEvent.ACTION_DOWN)
                prevTrack();

    }

    public class AudioBinder extends Binder{
        public AudioService getService(){
            return AudioService.this;
        }
    }

    public void bindViewListener(IServiceView viewListener){
        Log.d(tag, "<BindViewListener>");
        this.viewListener = viewListener;
        if (getTrackList() != null) {
            if (getTrackList().size() > 0) {
                this.viewListener.bindTrackList(trackList);
                if (playing) {
                    viewListener.playMusic();
                    musicPlayer.forceStartTimer();
                } else {
                    viewListener.pauseMusic();
                }
                viewListener.setTrackInfo(getTrackList().get(position), position);
                viewListener.updateDurationPosition(musicPlayer.getCurrentPosition());
            } else {
                viewListener.eventEmptyTrackList();
            }
        }
    }

    public void addSong(SongModel songModel){
        getTrackList().add(getPosition() + 1, songModel);
    }

    public void unBindViewListener(){
        Log.d(tag, "<UnBindViewListener>");
        viewListener = null;
    }

    public void pauseOnUpdateDuration(){
        musicPlayer.forceStopTimer();
    }

    public void resetOnUpdateDuration(){
        musicPlayer.forceStartTimer();
    }

    public void bindTrackList(List<SongModel> trackList, int pos){
        try {
            this.trackList = trackList;
            position = pos;
            setTrack(position);
            if (this.viewListener != null)
            this.viewListener.bindTrackList(this.trackList);
        }catch (Exception e){
            position = -1;
            Log.e(tag, "<BindTrackList> "+e.toString());
        }
    }

    public List<SongModel> getTrackList(){
        return trackList;
    }

    public void setTrack(int position){
        try {
            this.position = position;
            musicPlayer.reset();
            musicPlayer.setUri(getTrackList().get(position).getUri());
        }catch (Exception e){
            Log.e(tag, "<setTrack> "+e.toString());
        } finally {
            if (viewListener != null) viewListener.setTrackInfo(getTrackList().get(position), position);
            callNotification(playing, 1);
        }
    }

    private void checkNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = PLAYER_CHANNEL_ID;

            assert notificationManager != null;
            NotificationChannel notificationChannel =
                        notificationManager.getNotificationChannel(channelId);

            if (notificationChannel == null) {
                // Create the NotificationChannel
                CharSequence name = "Live Music Player Channel";
                String description = "Widget for music listening";
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel mChannel = new NotificationChannel(PLAYER_CHANNEL_ID, name, importance);
                mChannel.setDescription(description);

                notificationManager.createNotificationChannel(mChannel);
            }
        }
    }

    public void setPlaying(){
        checkNotificationChannel();

        playing = !playing;
        musicPlayer.setCanPlay(playing);
        if (playing) {
            if (requestAudioFocus()) {
                musicPlayer.play();
                if (viewListener != null) viewListener.playMusic();
            }
        } else {
            musicPlayer.pause();
            if (viewListener != null) viewListener.pauseMusic();
            releaseAudioFocusForMyApp();
        }
        callNotification(playing, 0);
    }

    public void nextTrack(){
        position++;
        if (position >= getTrackList().size()) position = 0;
        setTrack(position);
    }

    public void prevTrack(){
        position--;
        if (position < 0) position = getTrackList().size()-1;
        setTrack(position);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(tag, "Service created");

        musicPlayer = new MusicPlayer(this);
        mediaSession = new MediaSessionCompat(this, MEDIASESSION_LIVEMUSIC);
        mediaBroadcast = new MediaBroadcast(this);
        registerReceiver(mediaBroadcast, new IntentFilter(Intent.ACTION_MEDIA_BUTTON));

        managerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        mediaSession.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS | MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setActive(true);

        playbackStateCompat = new PlaybackStateCompat.Builder()
                .setState(PlaybackStateCompat.STATE_PAUSED, 0, 0)
                .setActions(PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS);

        mediaSession.setPlaybackState(playbackStateCompat.build());

        mediaSession.setCallback(new MediaSessionCompat.Callback() {
            @Override
            public void onPlay() {
                setPlaying();
                super.onPlay();
            }

            @Override
            public boolean onMediaButtonEvent(Intent mediaButtonEvent) {
                return super.onMediaButtonEvent(mediaButtonEvent);
            }

            @Override
            public void onPause() {
                setPlaying();
                super.onPause();
            }

            @Override
            public void onSkipToNext() {
                nextTrack();
                super.onSkipToNext();
            }

            @Override
            public void onSkipToPrevious() {
                prevTrack();
                super.onSkipToPrevious();
            }

            @Override
            public void onStop() {
                super.onStop();
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(tag, "<OnStartCommand>");
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(tag, "<OnBind>");
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(tag, "<OnUnbind>");
//        return super.onUnbind(intent);
        return true;
    }

    public int getPosition(){
        return position;
    }

    @Override
    public void onDestroy() {
        Log.d(tag, "<OnDestroy>");
        super.onDestroy();
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d(tag, "<OnRebind>");
        super.onRebind(intent);
    }

    private void callNotification(boolean playing, int state){
        if (getTrackList().size() > 0) {
            if (startNotification != null) {
                startNotification.cancel(true);
                startNotification = null;
            }
            startNotification = (StartNotification) new StartNotification(playing, state).execute(getTrackList().get(position).getUri());
        }
    }

    private boolean requestAudioFocus(){
        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(mOnAudioFocusChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            Log.d("AudioFocus", "Audio focus received");
            return true;
        } else {
            Log.d("AudioFocus", "Audio focus NOT received");
            return false;
        }
    }

    void releaseAudioFocusForMyApp() {
        AudioManager am = (AudioManager)getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        am.abandonAudioFocus(mOnAudioFocusChangeListener);
    }

    private AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {

        @Override
        public void onAudioFocusChange(int focusChange) {
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_GAIN:
                    Log.i(tag, "AUDIOFOCUS_GAIN");
                    //restart/resume your sound
                    if (!getPlaying()) setPlaying();
                    musicPlayer.setVolume(1f);
                    break;
                case AudioManager.AUDIOFOCUS_LOSS:
                    Log.e(tag, "AUDIOFOCUS_LOSS");
                    //Loss of audio focus for a long time
                    //Stop playing the sound
                    if (getPlaying()) setPlaying();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    Log.e(tag, "AUDIOFOCUS_LOSS_TRANSIENT");
                    //Loss of audio focus for a short time
                    //Pause playing the sound
                    if (getPlaying()) setPlaying();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    Log.e(tag, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                    //Loss of audio focus for a short time.
                    //But one can duck. Lower the volume of playing the sound
                    musicPlayer.setVolume(0.05f);
                    break;

                default:
                    //
            }
        }
    };

    private class StartNotification extends AsyncTask<String, Void, NotificationCompat.Builder> {
        boolean playing = false;
        int     state   = 0;

        public StartNotification(boolean playing, int state) {
            this.playing = playing;
            this.state = state;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected NotificationCompat.Builder doInBackground(String... strings) {
            int     state        = 0;
            int     keyCode      = 0;
            boolean imgExist     = false;

            NotificationCompat.Builder builder = null;
            Bitmap img = null;
            try {
                MediaMetadataRetriever mData = new MediaMetadataRetriever();
                mData.setDataSource(strings[0]);
                img = decodeSampledBitmapFromByte(mData.getEmbeddedPicture(), 384, 384);
                mData.release();
                imgExist = true;
            }catch (Exception e){
                img = decodeSampledBitmapFromResource(Core.getCoreInstance().getDarkTheme() == 0 ?
                        R.drawable.noimg2light : R.drawable.noimg2dark, 256, 256);
                imgExist = false;
            }

            if (playing) playbackStateCompat.setState(PlaybackStateCompat.STATE_PLAYING, 0, 0);
                else playbackStateCompat.setState(PlaybackStateCompat.STATE_PAUSED, 0, 0);

            if (playing) keyCode = KeyEvent.KEYCODE_MEDIA_PAUSE;
                else keyCode = KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE;

            if (playing) state = R.drawable.ic_corner_pause22;
                else state = R.drawable.ic_corner_play22;

            MediaMetadataCompat.Builder builderMeta = new MediaMetadataCompat.Builder()
                    .putString(MediaMetadataCompat.METADATA_KEY_TITLE,  getTrackList().get(position).getTitle())
                    .putBitmap(MediaMetadataCompat.METADATA_KEY_ART,    img)
                    .putString(MediaMetadataCompat.METADATA_KEY_ALBUM,  getTrackList().get(position).getAlbum())
                    .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, getTrackList().get(position).getArtist());

            mediaSession.setMetadata(builderMeta.build());
            mediaSession.setPlaybackState(playbackStateCompat.build());

            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    0, new Intent(getApplicationContext(), MainActivity.class), 0);

            Intent intent = new Intent(getApplicationContext(), CancelBroadcastReceiver.class);
            intent.setAction("player_widget_cancelled");
            PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                    0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            Palette palette = Palette.from(img).generate();

            builder = MediaNotification.from(getApplicationContext(), mediaSession)
                    .setChannelId(PLAYER_CHANNEL_ID)
                    .setShowWhen(false)
                    .setSmallIcon(R.drawable.ic_eq6gfooux9)
                    .setColor(palette.getDominantSwatch().getRgb())
                    .setColorized(true)
                    .setDeleteIntent(cancelPendingIntent)
                    .setContentIntent(pendingIntent)
                    .addAction(new NotificationCompat.Action(R.drawable.ic_corner_prev22, "previous",
                            MediaNotification.getActionIntent(getApplicationContext(), KeyEvent.KEYCODE_MEDIA_PREVIOUS)))
                    .addAction(new NotificationCompat.Action(state, "play_pause",
                            MediaNotification.getActionIntent(getApplicationContext(), keyCode)))
                    .addAction(new NotificationCompat.Action(R.drawable.ic_corner_next22, "next",
                            MediaNotification.getActionIntent(getApplicationContext(), KeyEvent.KEYCODE_MEDIA_NEXT)))
                    .setStyle(new android.support.v4.media.app.NotificationCompat.DecoratedMediaCustomViewStyle()
                            .setMediaSession(mediaSession.getSessionToken())
                            .setShowActionsInCompactView(0, 1, 2));

            return builder;
        }

        @Override
        protected void onPostExecute(NotificationCompat.Builder builder) {
            super.onPostExecute(builder);
            if (!isCancelled()) {
                startForeground(1, builder.build());
                if (!playing) stopForeground(false);
            }
        }

        public Bitmap decodeSampledBitmapFromByte(byte[] array,
                                                      int reqWidth, int reqHeight) {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(array, 0, array.length, options);
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(array, 0, array.length, options);
        }

        public Bitmap decodeSampledBitmapFromResource(int res,
                                                      int reqWidth, int reqHeight) {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), res, options);
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeResource(getResources(), res, options);
        }

        public int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;
            if (height > reqHeight || width > reqWidth) {
                final int halfHeight = height / 2;
                final int halfWidth = width / 2;
                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }
            return inSampleSize;
        }
    }
}
