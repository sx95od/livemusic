package com.sx95od.livemusic.Services.AudioService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.sx95od.livemusic.Services.AudioService.Listeners.BroadcastListener;

/**
 * Created by stoly on 16.09.2017.
 */

public class MediaBroadcast extends BroadcastReceiver {
    private final String tag = "MediaBroadcast";
    BroadcastListener broadcastListener;

    public MediaBroadcast(BroadcastListener broadcastListener){
        this.broadcastListener = broadcastListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        broadcastListener.onReceive(intent);
    }
}
