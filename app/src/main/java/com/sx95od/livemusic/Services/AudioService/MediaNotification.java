package com.sx95od.livemusic.Services.AudioService;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.view.KeyEvent;

import java.security.Key;

/**
 * Created by stoly on 16.09.2017.
 */

public class MediaNotification {
    public static NotificationCompat.Builder from(Context context, MediaSessionCompat mediaSessionCompat){
        MediaControllerCompat mediaControllerCompat = mediaSessionCompat.getController();
        MediaMetadataCompat mediaMetadataCompat = mediaControllerCompat.getMetadata();
        MediaDescriptionCompat mediaDescriptionCompat = mediaMetadataCompat.getDescription();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "default");
        builder.setContentTitle(mediaDescriptionCompat.getTitle())
                .setContentText(mediaDescriptionCompat.getSubtitle())
                .setSubText(mediaDescriptionCompat.getDescription())
                .setLargeIcon(mediaDescriptionCompat.getIconBitmap())
                .setContentIntent(mediaControllerCompat.getSessionActivity())
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setDeleteIntent(getActionIntent(context, KeyEvent.KEYCODE_MEDIA_STOP));
        return builder;
    }

    public static PendingIntent getActionIntent(Context context, int mediaKeyEvent){
        Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        intent.setPackage(context.getPackageName());
        intent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, mediaKeyEvent));
        return PendingIntent.getBroadcast(context, mediaKeyEvent, intent, 0);
    }
}
