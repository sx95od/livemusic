package com.sx95od.livemusic.DataBase;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by stoly on 01.11.2017.
 */

public class YouTubeCredentials extends RealmObject {

    @PrimaryKey
    private int id;
    private String data_name;
    private String data;

    public void setId(int id) {
        this.id = id;
    }

    public void setData_name(String data_name) {
        this.data_name = data_name;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getId() {

        return id;
    }

    public String getData_name() {
        return data_name;
    }

    public String getData() {
        return data;
    }
}
