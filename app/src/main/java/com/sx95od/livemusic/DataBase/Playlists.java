package com.sx95od.livemusic.DataBase;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by stoly on 12.06.2017.
 */

public class Playlists extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private byte[] cover;
    private long date;
    private String about;

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setCover(byte[] cover){
        this.cover = cover;
    }

    public void setDate(long date){
        this.date = date;
    }

    public void setAbout(String about){
        this.about = about;
    }

    public String getName(){
        return name;
    }

    public int getId(){
        return id;
    }

    public byte[] getCover(){
        return cover;
    }

    public long getDate(){
        return date;
    }

    public String getAbout(){
        return about;
    }
}
