package com.sx95od.livemusic.DataBase;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by stoly on 11.06.2017.
 */

public class FavSongs extends RealmObject {
    @PrimaryKey
    private int id;
    private String title;
    private String artist;
    private String album;
    private int duration;
    private String uri;
    private int _id;
    private int artist_id;
    private int album_id;
    private int date_modified;
    private String year;
    private String md5;

    public String getTitle(){
        return title;
    }

    public String getArtist(){
        return artist;
    }

    public String getAlbum(){
        return album;
    }

    public String getUri(){
        return uri;
    }

    public int getId(){
        return id;
    }

    public int getDuration(){
        return  duration;
    }

    public int get_id(){
        return _id;
    }

    public int getArtist_id(){
        return artist_id;
    }

    public int getAlbum_id(){
        return album_id;
    }

    public int getDate_modified(){
        return date_modified;
    }

    public String getYear(){
        return year;
    }

    public String getMd5(){
        return md5;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setArtist(String artist){
        this.artist = artist;
    }

    public void setAlbum(String album){
        this.album = album;
    }

    public void setDuration(int duration){
        this.duration = duration;
    }

    public void setUri(String uri){
        this.uri = uri;
    }

    public void set_id(int _id){
        this._id = _id;
    }

    public void setArtist_id(int artist_id){
        this.artist_id = artist_id;
    }

    public void setAlbum_id(int album_id){
        this.album_id = album_id;
    }

    public void setDate_modified(int date_modified){
        this.date_modified = date_modified;
    }

    public void setYear(String year){
        this.year = year;
    }

    public void setMD5(String md5){
        this.md5 = md5;
    }
}
