package com.sx95od.livemusic.DataBase;

import io.realm.DynamicRealm;
import io.realm.RealmSchema;

/**
 * Created by stoly on 23.08.2017.
 */

public class RealmMigration implements io.realm.RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema realmSchema = realm.getSchema();

        if (oldVersion==0){
            realmSchema.get("PlaylistSongs")
                    .removeField("playlistId")
                    .addRealmObjectField("playlist", realmSchema.get("Playlists"));
        }
    }
}
