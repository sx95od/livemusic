package com.sx95od.livemusic.DataBase;



/**
 * Created by stoly on 03.09.2017.
 */

@io.realm.annotations.RealmModule(library = true, classes = { FavSongs.class, Playlists.class, PlaylistSongs.class, SearchHistory.class, YouTubeCredentials.class })
public class DefaultRealmModule {
}
