package com.sx95od.livemusic.DataBase;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by stoly on 26.08.2017.
 */

public class SearchHistory extends RealmObject {
    @PrimaryKey
    long id;
    String query;
    long countQueries;

    public void setQuery(String query){
        this.query = query;
    }

    public void setCountQueries(int countQueries){
        this.countQueries = countQueries;
    }

    public void incrementQuery(){
        this.countQueries++;
    }

    public String getQuery(){
        return query;
    }

    public long getCountQueries(){
        return countQueries;
    }

    public long getId(){
        return id;
    }
}
