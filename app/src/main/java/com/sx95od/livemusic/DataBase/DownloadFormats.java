package com.sx95od.livemusic.DataBase;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by stoly on 03.09.2017.
 */

public class DownloadFormats extends RealmObject{
    @Ignore
    public static final String OPUS = ".opus";
    @Ignore
    public static final String MP3 = ".mp3";

    @PrimaryKey
    long id;
    String fileName;
    String uri;
    String title;
    String artist;
    String album;
    String format;
    String outFormat;

    public long getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public String getUri() {
        return uri;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public String getFormat() {
        return format;
    }

    public String getOutFormat() {
        return outFormat;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setOutFormat(String outFormat) {
        this.outFormat = outFormat;
    }
}
