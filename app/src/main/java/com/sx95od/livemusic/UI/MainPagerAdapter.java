package com.sx95od.livemusic.UI;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stoly on 24.06.2017.
 */

public class MainPagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentArray = new ArrayList<Fragment>();
    boolean allRemoving = false;
    boolean stackBack = true;
    FragmentManager fragmentManager;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fragmentManager = fm;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArray.get(position);
    }


    @Override
    public int getCount() {
        return fragmentArray.size();
    }



    public void removeFragment(int pos){
        for (int i=fragmentArray.size()-1; i>pos; i--) {
            fragmentManager.beginTransaction().remove(fragmentArray.get(i)).commit();
            fragmentArray.remove(i);
            notifyDataSetChanged();
        }
    }



    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
//        super.restoreState(state, loader);
    }

    public void clear(){
        stackBack = false;
        fragmentArray.clear();
        notifyDataSetChanged();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (!stackBack) {
            super.destroyItem(container, position, object);
        } else {
            if (position > fragmentArray.size() - 1) {
                super.destroyItem(container, position, object);
            }
        }
    }

    public void addFragment(Fragment fragment){
//        Log.d(getClass().getSimpleName(), "addFragment");
        fragmentArray.add(fragment);
        notifyDataSetChanged();
        stackBack = true;
    }


    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }



}
