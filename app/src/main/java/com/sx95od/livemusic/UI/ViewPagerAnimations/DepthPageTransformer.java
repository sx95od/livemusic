package com.sx95od.livemusic.UI.ViewPagerAnimations;

import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

/**
 * Created by stoly on 25.06.2017.
 */

public class DepthPageTransformer implements ViewPager.PageTransformer {
    public float elevation = 0;

//    private final int viewToParallax;

    public DepthPageTransformer(float elevation) {
//        this.viewToParallax = viewToParallax;
        this.elevation = elevation;

    }





    @Override
    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();

        if (position<=-2){
            view.setTranslationX(0);
            view.setElevation(0);
        } else if (position <= -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
//            view.setAlpha(0);
            view.setTranslationX(0);
            view.setElevation(0);

        } else if (position <= 0) { // [-1,0]
            // Use the default slide transition when moving to the left page
            view.setTranslationX(-position * (pageWidth / 2)); //Half the normal speed
            view.setElevation(0);

        } else if (position <= 1) { // (0,1]
            view.setTranslationX(position);
            view.setElevation(elevation);

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
//            view.setAlpha(0);
        }


//--------------------------------------------------------------------------------------------------
//        if (position==-1f){
//            view.setTranslationX(1); //Half the normal speed
//        }
//        else
//        if (position<0) {
//            view.setTranslationX(-position * (pageWidth / 2)); //Half the normal speed
//            Log.d(getClass().getSimpleName(), view.getAlpha()+" alpha left screen");
//            Log.d(getClass().getSimpleName(), view.getVisibility()+" visibility left screen");
//            Log.d(getClass().getSimpleName(), view.getId()+" id left screen");
//        } else{
//            //second page slide to right
//            view.setTranslationX(position);
//            Log.d(getClass().getSimpleName(), view.getAlpha()+" alpha right screen");
//            Log.d(getClass().getSimpleName(), view.getVisibility()+" visibility right screen");
//            Log.d(getClass().getSimpleName(), view.getId()+" id right screen");
//        }

    }
}