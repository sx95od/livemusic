package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 05.01.2018.
 */

public class SFlexboxLayout extends FlexboxLayout {
    /**
     0 - Primary background
     1 - Secondary background
     **/

    public enum Type {
        PRIMARY,
        ACCENT,
        SECONDARY
    }

    private int bgtype = -1;
    private boolean customBg = false;

    public SFlexboxLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.SFlexboxLayout,
                    0, 0);
            try {
                bgtype = a.getInteger(R.styleable.SFlexboxLayout_flexbox_bgtype, -1);
                customBg = a.getBoolean(R.styleable.SFlexboxLayout_flexbox_custom_bg, false);
                init(context);
            } finally {
                a.recycle();
            }
        }
    }

    private void init(Context context){
        if (!customBg) {
            if (bgtype == 0) {
                setBackgroundColor(ContextCompat.getColor(context,
                        Core.getCoreInstance().getDarkTheme() == 0 ? R.color.light : R.color.dark));
            } else if (bgtype == 1) {
                setBackgroundColor(ContextCompat.getColor(context,
                        Core.getCoreInstance().getDarkTheme() == 0 ? R.color.white : R.color.dark2));
            } else if (bgtype == 2){
                setBackgroundColor(PrefManager.getColorAccent());
            }
        } else if (bgtype != -1) {
            if (bgtype == 0) {
                ((StateListDrawable) getBackground())
                        .setColorFilter(ContextCompat.getColor(getContext(),
                                Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark : R.color.light),
                                PorterDuff.Mode.SRC_IN);
            } else if (bgtype == 1) {
                ((StateListDrawable) getBackground())
                        .setColorFilter(ContextCompat.getColor(getContext(),
                                Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark2 : R.color.white),
                                PorterDuff.Mode.SRC_IN);
            } else if (bgtype == 2){
                ((StateListDrawable) getBackground())
                        .setColorFilter(PrefManager.getColorAccent(),
                                PorterDuff.Mode.SRC_IN);
            }
        }
    }


    public void setBGType(Type type){
        switch (type){
            case PRIMARY:
                bgtype = 0;
                break;
            case SECONDARY:
                bgtype = 1;
                break;
            case ACCENT:
                bgtype = 2;
                break;
        }
        init(getContext());
    }
}
