package com.sx95od.livemusic.ThemeViews.LiveMusicTab;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.Utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by stoly on 15.04.2018.
 */

public class ViewHolder extends RecyclerView.ViewHolder{

    int pos;

    OnClickListener onClickListener;

    @BindView(R2.id.type_1_tv)
    TextView _type1TV;


    public ViewHolder(View itemView, OnClickListener onClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.onClickListener = onClickListener;
    }

    @OnClick(R2.id.parent_item)
    public void onItemClick(){
        if (onClickListener!= null)
            onClickListener.onClick(pos);
    }

    public void setOnAlpha(){
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator objectAnimatorA  = ObjectAnimator.ofFloat(_type1TV, View.ALPHA,   Utils.getFloat(R.dimen.ap_tab_layout_text_inactive_alpha));
        ObjectAnimator objectAnimatorSX = ObjectAnimator.ofFloat(_type1TV, View.SCALE_Y, Utils.getFloat(R.dimen.ap_tab_layout_text_inactive_scale));
        ObjectAnimator objectAnimatorSY = ObjectAnimator.ofFloat(_type1TV, View.SCALE_X, Utils.getFloat(R.dimen.ap_tab_layout_text_inactive_scale));

        animatorSet.playTogether(objectAnimatorA, objectAnimatorSX, objectAnimatorSY);
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.setDuration(Core.getCoreInstance().getResources().getInteger(R.integer.ap_tab_layout_text_anim_duration));
        animatorSet.start();
    }

    public void setOffAlpha(){
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator objectAnimatorA = ObjectAnimator.ofFloat(_type1TV, View.ALPHA, 1f);
        ObjectAnimator objectAnimatorSX = ObjectAnimator.ofFloat(_type1TV, View.SCALE_Y, 1f);
        ObjectAnimator objectAnimatorSY = ObjectAnimator.ofFloat(_type1TV, View.SCALE_X, 1f);

        animatorSet.playTogether(objectAnimatorA, objectAnimatorSX, objectAnimatorSY);
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.setDuration(Core.getCoreInstance().getResources().getInteger(R.integer.ap_tab_layout_text_anim_duration));
        animatorSet.start();
    }

    public void bindString(String string, int pos){
        _type1TV.setText(string);
        this.pos = pos;
    }
}
