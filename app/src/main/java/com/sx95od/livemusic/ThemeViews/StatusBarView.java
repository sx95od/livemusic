package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.sx95od.livemusic.Utils.SystemUtils;

/**
 * Created by stoly on 27.03.2018.
 */

public class StatusBarView extends View {

    public StatusBarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        setMeasuredDimension(parentWidth, SystemUtils.getStatusBarHeight());
    }




}
