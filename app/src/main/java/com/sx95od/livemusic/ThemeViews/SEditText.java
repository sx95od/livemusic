package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 05.01.2018.
 */

public class SEditText extends AppCompatEditText {

    public SEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
       if (!isInEditMode()) init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        setTextColor(ContextCompat.getColor(context, Core.getCoreInstance().getDarkTheme() == 0 ?
                R.color.dark : R.color.light));
        setHintTextColor(ContextCompat.getColor(context, Core.getCoreInstance().getDarkTheme() == 0 ?
                R.color.grey : R.color.grey));
    }
}
