package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 05.01.2018.
 */

public class SImageView extends ImageView {

    public SImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SImageView,
                0, 0);

        try {
            boolean accent = a.getBoolean(R.styleable.SImageView_accent_image, false);
            if (accent) setColorFilter(PrefManager.getColorAccent());
            else setColorFilter(ContextCompat.getColor(getContext(), Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark : R.color.light));
        }finally {
            a.recycle();
        }
    }
}
