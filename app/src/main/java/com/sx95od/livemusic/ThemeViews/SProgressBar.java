package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.sx95od.livemusic.PrefManager;

/**
 * Created by stoly on 06.05.2018.
 */

public class SProgressBar extends ProgressBar {

    public SProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        getIndeterminateDrawable().setColorFilter(PrefManager.getColorAccent(), PorterDuff.Mode.SRC_IN);
    }
}
