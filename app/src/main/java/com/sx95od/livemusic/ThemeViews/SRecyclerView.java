package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 02.02.2018.
 */

public class SRecyclerView extends RecyclerView {
    public SRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SRecyclerView,
                0, 0);
        try {
            int bgtype = a.getInt(R.styleable.SRecyclerView_rv_bgtype, -1);

            if (bgtype == 0) {
                setBackgroundColor(ContextCompat.getColor(context,
                        Core.getCoreInstance().getDarkTheme() == 0 ? R.color.light : R.color.dark));
            } else if (bgtype == 1) {
                setBackgroundColor(ContextCompat.getColor(context,
                        Core.getCoreInstance().getDarkTheme() == 0 ? R.color.white : R.color.dark2));
            }
        }finally {
            a.recycle();
        }

    }
}
