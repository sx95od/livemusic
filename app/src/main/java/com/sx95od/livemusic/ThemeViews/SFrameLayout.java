package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Utils.Utils;

/**
 * Created by stoly on 05.01.2018.
 */

public class SFrameLayout extends FrameLayout {
    private boolean intercept = false;
    private boolean intercepted = false;
    private boolean blockIntercepted = false;

    float x = 0;
    float y = 0;

    public SFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
       if (!isInEditMode()) init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SFrameLayout,
                0, 0);

        try {
            int bgtype = a.getInteger(R.styleable.SFrameLayout_frame_bgtype, -1);
            boolean customBg = a.getBoolean(R.styleable.SFrameLayout_frame_custom_bg, false);

            if (!customBg) {
                if (bgtype == 0) {
                    setBackgroundColor(ContextCompat.getColor(context,
                            Core.getCoreInstance().getDarkTheme() == 0 ? R.color.light : R.color.dark));
                } else if (bgtype == 1) {
                    setBackgroundColor(ContextCompat.getColor(context,
                            Core.getCoreInstance().getDarkTheme() == 0 ? R.color.white : R.color.dark2));
                }
            } else {
                if (bgtype == 0) {
                    ((StateListDrawable) getBackground())
                            .setColorFilter(ContextCompat.getColor(getContext(),
                                    Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark : R.color.light),
                                    PorterDuff.Mode.SRC_IN);
                } else if (bgtype == 1) {
                    ((StateListDrawable) getBackground())
                            .setColorFilter(ContextCompat.getColor(getContext(),
                                    Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark2 : R.color.white),
                                    PorterDuff.Mode.SRC_IN);
                }
            }
        } finally {
            a.recycle();
        }
    }


    public void setInterceptTouchEvent(boolean intercept){
        this.intercept = intercept;
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (intercept) {
            switch (ev.getAction()){
                case MotionEvent.ACTION_DOWN:
                    blockIntercepted = false;
                    intercepted = false;

                    x = ev.getRawX();
                    y = ev.getRawY();
                    return false;
                case MotionEvent.ACTION_MOVE:
                    if (!intercepted && !blockIntercepted){
                        if (Math.abs(ev.getRawX() - x) > Utils.getPxInDp(24) ||
                                Math.abs(ev.getRawY() - y) > Utils.getPxInDp(24)){
                            if (Math.abs(ev.getRawX() - x) > Math.abs(ev.getRawY() - y)){
                                blockIntercepted = false;
                                intercepted = true;
                                return true;
                            } else {
                                blockIntercepted = true;
                                intercepted = false;
                                return super.onInterceptTouchEvent(ev);
                            }

                        } else return super.onInterceptTouchEvent(ev);
                    } else return blockIntercepted ? super.onInterceptTouchEvent(ev) : true;
                default:

                    return super.onInterceptTouchEvent(ev);
            }
        } else return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP ||
                event.getAction() == MotionEvent.ACTION_CANCEL){
            blockIntercepted = false;
            intercepted = false;
        }
        return super.onTouchEvent(event);
    }
}
