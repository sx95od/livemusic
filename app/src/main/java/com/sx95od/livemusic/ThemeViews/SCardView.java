package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 05.01.2018.
 */

public class SCardView extends CardView {



    public SCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SCardView,
                0, 0);
        try {
            boolean accent = a.getBoolean(R.styleable.SCardView_accent_card, false);
            if (accent) setCardBackgroundColor(PrefManager.getColorAccent());
            else setCardBackgroundColor(ContextCompat.getColor(context,
                    Core.getCoreInstance().getDarkTheme() == 0 ? R.color.white : R.color.dark2));
        }finally {
            a.recycle();
        }

    }
}
