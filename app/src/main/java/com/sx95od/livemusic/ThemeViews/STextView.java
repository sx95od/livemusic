package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 05.01.2018.
 */

public class STextView extends TextView {
    private final String SFF_M = "SanFranciscoFont/SanFranciscoDisplay-Medium.otf";
    private final String SFF_L = "SanFranciscoFont/SanFranciscoDisplay-Light.otf";
    private final String SFF_B = "SanFranciscoFont/SanFranciscoDisplay-Bold.otf";
    /**
     *
     0 = Not setting text color
     1 = Primary text color
     2 = Secondary text color
     3 = Accent text color
     *
     *
     */


    public STextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
       if (!isInEditMode()) init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.STextView,
                0, 0);

        int textStyle = getTypeface().getStyle();

        Typeface typeRegular = Typeface.createFromAsset(context.getAssets(), "fonts/ubuntu.ttf");
        Typeface typeMedium = Typeface.createFromAsset(context.getAssets(), "fonts/ubuntu_medium.ttf");

        try {
            int colorType = a.getInteger(R.styleable.STextView_color_type, -1);

            if (colorType == 1){
                setTextColor(ContextCompat.getColor(context,
                        Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark : R.color.light));
                setTypeface(typeRegular, textStyle);
            } else if (colorType == 2){
                setTextColor(ContextCompat.getColor(context,
                        Core.getCoreInstance().getDarkTheme() == 0 ? R.color.grey : R.color.grey));
                setTypeface(typeRegular, textStyle);
            } else if (colorType == 3){
                setTextColor(PrefManager.getColorAccent());
                setTypeface(typeRegular, textStyle);
            } else if (colorType == 4){
                setTextColor(ContextCompat.getColor(context,
                        Core.getCoreInstance().getDarkTheme() == 0 ? R.color.dark : R.color.light));
                setTypeface(typeMedium, textStyle);
            } else {
                setTypeface(typeMedium, textStyle);
            }
        } finally {
            a.recycle();
        }
    }
}
