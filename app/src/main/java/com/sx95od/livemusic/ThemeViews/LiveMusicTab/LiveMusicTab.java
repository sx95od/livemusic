package com.sx95od.livemusic.ThemeViews.LiveMusicTab;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.R;

import java.util.ArrayList;

/**
 * Created by stoly on 15.04.2018.
 */

public class LiveMusicTab extends RecyclerView.Adapter<ViewHolder> implements OnClickListener {

    ViewPager _viewPager;
    ArrayList<String> strings;
    int selectedPage = 0;

    public LiveMusicTab(ArrayList<String> strings, ViewPager viewPager) {
        this._viewPager = viewPager;
        this.strings = strings;
        listenViewPager(_viewPager);
    }

    private void listenViewPager(ViewPager viewPager) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedPage = position;
                notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.livemusic_tab_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindString(strings.get(position), position);
        if (selectedPage == position) holder.setOffAlpha();
        else holder.setOnAlpha();
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(int pos) {
        _viewPager.setCurrentItem(pos);
    }
}
