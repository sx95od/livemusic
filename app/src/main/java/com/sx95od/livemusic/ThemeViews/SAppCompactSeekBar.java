package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 09.02.2018.
 */

public class SAppCompactSeekBar extends AppCompatSeekBar {


    public SAppCompactSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SAppCompactSeekBar,
                0, 0);

        try {
            int colorFilter = a.getInteger(R.styleable.SAppCompactSeekBar_acsb_color_filter, -1);

           if (colorFilter == 0){

           } else if (colorFilter == 1){
               getProgressDrawable().setColorFilter(PrefManager.getColorAccent(), PorterDuff.Mode.SRC_IN);
               getThumb().setColorFilter(PrefManager.getColorAccent(), PorterDuff.Mode.SRC_IN);
           }
        } finally {
            a.recycle();
        }
    }
}
