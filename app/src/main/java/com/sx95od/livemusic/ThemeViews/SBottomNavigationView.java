package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Utils.ThemeEngine;

/**
 * Created by stoly on 05.01.2018.
 */

public class SBottomNavigationView extends BottomNavigationViewEx {

    public SBottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        setBackgroundColor(ContextCompat.getColor(context, Core.getCoreInstance().getDarkTheme() == 0 ?
                R.color.white : R.color.dark2));

        setItemIconTintList(getBottomNavViewColorStateList());
        setItemTextColor(getBottomNavViewColorStateList());

        enableShiftingMode(false);
        setTextVisibility(false);
    }

    private ColorStateList getBottomNavViewColorStateList(){
        int[][] states = new int[][] {
                new int[] {android.R.attr.state_checked}, // unchecked
                new int[] {-android.R.attr.state_checked}  // pressed
        };

        int[] colors = new int[] {
                PrefManager.getColorAccent(),
                ContextCompat.getColor(getContext(), R.color.grey)
        };

        ColorStateList csl = new ColorStateList(states, colors);
        return csl;
    }
}
