package com.sx95od.livemusic.ThemeViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.R;

/**
 * Created by stoly on 06.01.2018.
 */

public class SView extends View {

    public SView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SView,
                0, 0);

        try {
            int bgtype = a.getInteger(R.styleable.SView_view_bgtype, -1);
            boolean customBg = a.getBoolean(R.styleable.SView_view_custom_bg, false);

            if (!customBg) {
                if (bgtype == 0) {
                    setBackgroundColor(ContextCompat.getColor(context,
                            Core.getCoreInstance().getDarkTheme() == 0 ? R.color.light : R.color.dark));
                } else if (bgtype == 1) {
                    setBackgroundColor(ContextCompat.getColor(context,
                            Core.getCoreInstance().getDarkTheme() == 0 ? R.color.white : R.color.dark2));
                }
            } else {
                if (bgtype == 0) {
                    ((StateListDrawable) getBackground())
                            .setColorFilter(ContextCompat.getColor(getContext(),
                                    Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark : R.color.light),
                                    PorterDuff.Mode.SRC_IN);
                } else if (bgtype == 1) {
                    ((StateListDrawable) getBackground())
                            .setColorFilter(ContextCompat.getColor(getContext(),
                                    Core.getCoreInstance().getDarkTheme() != 0 ? R.color.dark2 : R.color.white),
                                    PorterDuff.Mode.SRC_IN);
                }
            }
        } finally {
            a.recycle();
        }
    }
}
