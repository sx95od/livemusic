package com.sx95od.livemusic.Listeners;

import android.util.ArrayMap;

import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.Track;
import com.sx95od.livemusic.API.LastFM.Models.TopTracks.Tracks;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

/**
 * Created by stoly on 25.03.2018.
 */

public class Listeners {

    public interface OnYouTubeVideoItemClickListener {
        void onVideoClickListener(Item item);
    }

    public interface OnLastFMGeoTopChartTracksItemClick {
        void onItemClick(Track item);
    }

    public interface OnMediaScanCompleted {
        void onScanFinished();
    }

    public interface OnLastFMTopChartTracksItemClick {
        void onItemClick(com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track track);
    }

    public interface OnLocalSongItemClickListener {
        void onSongItemClick(int pos);
        void onAlbumPlayClick();
        void onSongItemLongClick(int pos);
    }

    public interface OnYouTubeVideoTopContentClick {
        void likeClick();
        void dislikeClick();
        void viewsClick();
        void channelClick();
        void moreClick();
        void videoClick();
        void downloadClick(String id);
    }

    public interface OnSearchAdapterListener {
        void onYouTubeSeeAllClick(ArrayMap<String, Object> paramMap);
    }

    public interface OnYTChartVideoListener {
        void onVideoClickListener(com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.Item video);
    }
}
