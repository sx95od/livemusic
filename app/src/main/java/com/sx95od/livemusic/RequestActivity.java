package com.sx95od.livemusic;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.provider.DocumentFile;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.sx95od.livemusic.Utils.Preferences;

import java.io.OutputStream;

public class RequestActivity extends AppCompatActivity {

    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        for (int i=0; i<getExternalCacheDirs().length; i++){
            try {
                Log.i("EXTERNAL", getExternalCacheDirs()[i].toString());
            }catch (Exception e){

            }
        }



        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);





        if(result!=PackageManager.PERMISSION_GRANTED & result2!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        } else{
            if (pref.getString(Preferences.EXTERNAL_STORAGE, "null").equals("null")){
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                startActivityForResult(intent, 42);
            } else{
//                startActivity(new Intent(this, MainActivity.class));
                Intent intent = new Intent(this, BootActivity.class);
                intent.putExtra("rescan", true);
                startActivity(intent);
                finish();
            }

        }


    }

    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultCode == RESULT_OK) {
            Uri treeUri = resultData.getData();
            Log.i("URI1", treeUri.toString());
            DocumentFile pickedDir = DocumentFile.fromTreeUri(this, treeUri);
            Log.i("URI2", pickedDir.getUri().toString());
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Preferences.EXTERNAL_STORAGE, treeUri.toString());
            editor.apply();



//            startActivity(new Intent(this, MainActivity.class));
            Intent intent = new Intent(this, BootActivity.class);
            intent.putExtra("rescan", true);
            startActivity(intent);
            finish();

//            Log.i("REQUEST", pickedDir.getUri().getEncodedPath());
//
//            // List all existing files inside picked directory
//
//            for (DocumentFile file : pickedDir.listFiles()) {
//                Log.i("REQUEST", "Found file " + file.getName() + " with size " + file.length());
//            }
//
//            // Create a new file and write into it
//
//            DocumentFile newFile = pickedDir.findFile("Music");
//            DocumentFile newFile2 = newFile.findFile("296241941.mp3");
//            try {
//                newFile2.delete();
//            }catch (Exception e){
//
//            }


        }
    }




    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    startActivity(new Intent(this, MainActivity.class));
//                    finish();

                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    startActivityForResult(intent, 42);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
