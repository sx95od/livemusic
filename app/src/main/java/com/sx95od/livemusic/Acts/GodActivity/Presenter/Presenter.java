package com.sx95od.livemusic.Acts.GodActivity.Presenter;

import com.sx95od.livemusic.Acts.GodActivity.Model.IModel;
import com.sx95od.livemusic.Acts.GodActivity.Model.Model;
import com.sx95od.livemusic.Acts.GodActivity.View.IView;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by stoly on 14.09.2017.
 */

public class Presenter implements IPresenter {
    private IModel model = new Model();
    private IView view;
    private CompositeDisposable disposable = new CompositeDisposable();

    private boolean interceptLiked = false;


    public Presenter(IView view) {
        this.view = view;
    }

    @Override
    public void getDurationPosition() {
        disposable.add(model.getFormatDurationPosition(view.getDurationPosition())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>() {
                    @Override
                    public void onNext(String s) {
                        view.setDurationPosition(s);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    @Override
    public void getDuration() {
        disposable.add(model.getFormatDuration(view.getDuration())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>() {
                    @Override
                    public void onNext(String s) {
                        view.setDuration(s);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void likeSong(SongModel song) {
        interceptLiked = false;
        disposable.add(model.likeSong(song)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<Boolean>() {
            @Override
            public void onNext(Boolean aBoolean) {
                if (!interceptLiked) view.setLiked(aBoolean);
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onComplete() {

            }
        }));
    }

    @Override
    public void checkLike(SongModel song) {
        disposable.add(model.checkLike(song)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean aBoolean) {
                        view.setLiked(aBoolean);
                        interceptLiked = true;
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onDestroy() {
        if (!disposable.isDisposed()){
            disposable.dispose();
        }
        disposable = null;
        view = null;
        model = null;
    }
}
