package com.sx95od.livemusic.Acts.GodActivity.Model;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import io.reactivex.Observable;

/**
 * Created by stoly on 14.09.2017.
 */

public interface IModel {
    Observable<String> getFormatDuration(int pos);
    Observable<String> getFormatDurationPosition(int pos);
    Observable<Boolean> likeSong(SongModel songModel);
    Observable<Boolean> checkLike(SongModel songModel);
}
