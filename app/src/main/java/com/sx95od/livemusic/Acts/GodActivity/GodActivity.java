package com.sx95od.livemusic.Acts.GodActivity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bartoszlipinski.viewpropertyobjectanimator.ViewPropertyObjectAnimator;
import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.Acts.GodActivity.Presenter.Presenter;
import com.sx95od.livemusic.Acts.GodActivity.View.IServiceView;
import com.sx95od.livemusic.Acts.GodActivity.View.IView;
import com.sx95od.livemusic.Acts.MyActivity;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Adapters.Adapters.PlaylistNormalAdapter;
import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Dialogs.DialogSongMore.BottomSheetDialogSongFragment;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.ThemeEvent;
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.FragmentAlbumsView;
import com.sx95od.livemusic.Fragments.FragmentDownload.FragmentDownload;
import com.sx95od.livemusic.Fragments.FragmentFav.FragmentFav;
import com.sx95od.livemusic.Fragments.FragmentHome.FragmentHome;
import com.sx95od.livemusic.Fragments.FragmentMore;
import com.sx95od.livemusic.Fragments.FragmentPlaylistSongsView;
import com.sx95od.livemusic.Fragments.FragmentPlaylistsView;
import com.sx95od.livemusic.Fragments.FragmentSearch.FragmentSearch;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.FragmentSearchMoreResults;
import com.sx95od.livemusic.Fragments.FragmentSongsView.FragmentSongsView;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.FragmentYouTubeShowVideo;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.Services.AudioService.AudioService;
import com.sx95od.livemusic.ThemeViews.SCardView;
import com.sx95od.livemusic.UI.MainPagerAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by stoly on 10.09.2017.
 */

public class GodActivity extends MyActivity implements View.OnTouchListener, IServiceView,
        SeekBar.OnSeekBarChangeListener, Listeners.OnLocalSongItemClickListener, IView,
        ViewPager.OnPageChangeListener, BottomNavigationView.OnNavigationItemSelectedListener{
    private final String tag = GodActivity.class.getSimpleName();

    @BindView(R2.id.frame_player_layout_v2) SCardView playerFrameLayout;
    @BindView(R2.id.flex_control_music) FlexboxLayout controlMusicFlexLayout;
    @BindView(R2.id.flex_bottom_of_player_layout) FlexboxLayout bottomOfPlayerLayputFlex;
    @BindView(R2.id.flex_player_window) FlexboxLayout playerWindowFlex;
    @BindView(R2.id.bottom_navigation_view) BottomNavigationView bottomNavigationView;
    @BindView(R2.id.frame_cover) FrameLayout coverFrame;
    @BindView(R2.id.card_cover) CardView coverCard;
    @BindView(R2.id.textview_title) TextView title;
    @BindView(R2.id.textview_control_title) TextView controlTitle;
    @BindView(R2.id.textview_artist) TextView artist;
    @BindView(R2.id.textview_duration_position) TextView durationPosition;
    @BindView(R2.id.textview_duration) TextView duration;
    @BindView(R2.id.imageview_cover) ImageView cover;
    @BindView(R2.id.imagebutton_prev) ImageButton prevButton;
    @BindView(R2.id.imagebutton_play) ImageButton playButton;
    @BindView(R2.id.imagebutton_next) ImageButton nextButton;
    @BindView(R2.id.imagebutton_control_play) ImageButton playControlButton;
    @BindView(R2.id.imagebutton_control_next) ImageButton nextControlButton;
    @BindView(R2.id.imagebutton_show_currentlist) ImageButton _showCurrentlistButton;
    @BindView(R2.id.seekbar_duration) AppCompatSeekBar durationSeekbar;
    @BindView(R2.id.recyclerview_currentlist) RecyclerView currentlistRecyclerview;
    @BindView(R2.id.imagebutton_like) ImageButton _likeButton;
    @BindView(R2.id.imagebutton_more) ImageButton _moreButton;
    @BindView(R2.id.fragments_container) FrameLayout _fragmentContainer;
    Unbinder unbinder;
    AudioService audioService;

    MainPagerAdapter pagerAdapter;
    LinearLayoutManager layoutManager;
    SongAdapter songAdapter;

    float scaleUpCard = 1f;
    float scaleDownCard = 0.86f;
    float startPoint = 0;
    float startPointCurrentlist = 0;
    float defaultTranslation = 0;
    float margin = 0;
    float defaultTranslationBottomMenu = 0;
    boolean playerExpanded = false;
    boolean currentlistExpanded = false;
    boolean moving[] = new boolean[3];
    boolean bound = false;
    int lastPos = -1;
    int viewPagerCurrentPosition;

    Presenter presenter;

    LoadCover loadCover;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        unbinder = ButterKnife.bind(this);
        presenter = new Presenter(this);

        songAdapter = new SongAdapter(null, SongAdapter.FLAG_CURRENT_LIST, null);
        songAdapter.setOnItemClickListener(GodActivity.this);
        songAdapter.setHasStableIds(true);
        currentlistRecyclerview.setAdapter(songAdapter);

//        pagerAdapter.addFragment(new FragmentHome());


        durationSeekbar.setOnSeekBarChangeListener(this);
        playerFrameLayout.setOnTouchListener(this);
        _showCurrentlistButton.setOnTouchListener(this);
        controlMusicFlexLayout.setOnTouchListener(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        bottomNavigationView.setSaveEnabled(false);


        Intent intent = new Intent(this, AudioService.class);
        bindService(intent, audioConnection, 0);

        addRootFragment(new FragmentHome());

        tuneUI();
        if (getIntent().hasExtra("theme_changed")) bottomNavigationView.setSelectedItemId(R.id.more);
        Log.w(tag, "onCreate()");
    }

    @Subscribe
    public void themeEvent(ThemeEvent themeEvent){
//        tuneTheme();
//        finishAndRemoveTask();
//        startActivity(new Intent(getApplicationContext(), GodActivity.class));
        Intent intent = new Intent();
        intent.putExtra("theme_changed", 1);
        setIntent(intent);
        recreate();
    }


    private void tuneUI(){
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        defaultTranslation = Core.getHeightScreen() - getResources().getDimensionPixelSize(R.dimen.dp56)*2;
        defaultTranslationBottomMenu = getResources().getDimensionPixelSize(R.dimen.dp56);
        margin = getResources().getDimensionPixelSize(R.dimen.dp18)*3;

        playerFrameLayout.setTranslationY(defaultTranslation);

        coverFrame.getLayoutParams().height = Core.getWidthScreen()+Core.getStatusBarHeight();

        coverCard.getLayoutParams().height = (int) (Core.getWidthScreen() - margin);
        coverCard.getLayoutParams().width = (int) (Core.getWidthScreen() - margin);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) coverCard.getLayoutParams();
        layoutParams.setMargins(0, Core.getStatusBarHeight()/2, 0, 0);
        coverCard.requestLayout();

        ViewGroup.MarginLayoutParams layoutParamsCurrentlist = (ViewGroup.MarginLayoutParams) currentlistRecyclerview.getLayoutParams();
        layoutParamsCurrentlist.setMargins(0,0,0, Core.getStatusBarHeight()+getResources().getDimensionPixelSize(R.dimen.dp60));
        currentlistRecyclerview.requestLayout();

        currentlistRecyclerview.setTranslationY(Core.getHeightScreen());
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        currentlistRecyclerview.setLayoutManager(layoutManager);

        setLightStatusBar(true);
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        unbinder = null;
        if (bound) {
            audioService.unBindViewListener();
            unbindService(audioConnection);
        }
        presenter.onDestroy();
        presenter = null;
        pagerAdapter = null;
        audioConnection = null;
        songAdapter = null;
        layoutManager = null;
        loadCover = null;
        super.onDestroy();
        Log.w(tag, "onDestroy()");
    }


    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        EventBus.clearCaches();
        super.onStop();
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.imagebutton_show_currentlist:
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        startPointCurrentlist = motionEvent.getRawY();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        if (!moving[1]){
                            moving[1] = true;
                        }
                        if (!currentlistExpanded){
                            if (motionEvent.getRawY() < startPointCurrentlist){
                                currentlistRecyclerview.setTranslationY(Core.getHeightScreen() - (startPointCurrentlist - motionEvent.getRawY())/4);
                                playerWindowFlex.setTranslationY(-(startPointCurrentlist - motionEvent.getRawY())/4);
                            }
                        } else{
                            if (motionEvent.getRawY() > startPointCurrentlist){
                                playerWindowFlex.setTranslationY((-Core.getHeightScreen()+getResources().getDimensionPixelSize(R.dimen.dp60)
                                +Core.getStatusBarHeight()+(motionEvent.getRawY()-startPointCurrentlist)/4));
                                currentlistRecyclerview.setTranslationY(getResources().getDimensionPixelSize(R.dimen.dp60)
                                +Core.getStatusBarHeight()+(motionEvent.getRawY()-startPointCurrentlist)/4);
                            }
                        }
                        return true;
                    default:
                        if (moving[1]) {
                            moving[1] = false;
                            if (!currentlistExpanded) {
                                if (startPointCurrentlist - motionEvent.getRawY() > getResources().getDimensionPixelSize(R.dimen.slide_view_lite) * 3)
                                    animateExpandableCurrentlist(true);
                                else animateExpandableCurrentlist(false);
                            } else {
                                if (motionEvent.getRawY() - startPointCurrentlist > getResources().getDimensionPixelSize(R.dimen.slide_view_lite) * 3)
                                    animateExpandableCurrentlist(false);
                                else animateExpandableCurrentlist(true);
                            }
                        } else {
                            if (!currentlistExpanded) {
                                animateExpandableCurrentlist(true);
                            }
                            else {
                                animateExpandableCurrentlist(false);
                            }
                        }
                        return false;
                }
            case R.id.frame_player_layout_v2:
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        startPoint = motionEvent.getRawY();
                        showFragmentContainer(true);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        if (!currentlistExpanded) {
                            if (!moving[0]) {
                                animateControlMusicFlex(controlMusicFlexLayout, true);
                                moving[0] = true;
                            }
                            if (!playerExpanded) {
                                if (motionEvent.getRawY() < startPoint)
                                    playerFrameLayout.setTranslationY(defaultTranslation - (startPoint - motionEvent.getRawY()) / 4);
                            } else {
                                if (motionEvent.getRawY() > startPoint)
                                    playerFrameLayout.setTranslationY((motionEvent.getRawY() - startPoint) / 4);
                            }
                        }
                        return true;
                    default:
                        if (!currentlistExpanded) {
                            if (moving[0]) {
                                moving[0] = false;
                                if (!playerExpanded) {
                                    if (startPoint - motionEvent.getRawY() > getResources().getDimensionPixelSize(R.dimen.slide_view_lite) * 3) {
                                        animateExpandablePlayerLayout(true);
                                    } else {
                                        animateExpandablePlayerLayout(false);
                                    }
                                } else {
                                    if (motionEvent.getRawY() - startPoint > getResources().getDimensionPixelSize(R.dimen.slide_view_lite) * 3) {
                                        animateExpandablePlayerLayout(false);
                                    } else {
                                        animateExpandablePlayerLayout(true);
                                    }
                                }
                            }
                        }
                        return false;
                }
            case R.id.flex_control_music:
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        startPoint = motionEvent.getRawY();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        if (!currentlistExpanded) {
                            if (!moving[0]) {
                                animateControlMusicFlex(controlMusicFlexLayout, true);
                                moving[0] = true;
                            }
                            if (!playerExpanded) {
                                if (motionEvent.getRawY() < startPoint)
                                    playerFrameLayout.setTranslationY(defaultTranslation - (startPoint - motionEvent.getRawY()) / 4);
                            } else {
                                if (motionEvent.getRawY() > startPoint)
                                    playerFrameLayout.setTranslationY((motionEvent.getRawY() - startPoint) / 4);
                            }
                        }
                        return true;
                    default:
                        if (!currentlistExpanded) {
                            if (moving[0]) {
                                moving[0] = false;
                                if (!playerExpanded) {
                                    if (startPoint - motionEvent.getRawY() > getResources().getDimensionPixelSize(R.dimen.slide_view_lite) * 3) {
                                        animateExpandablePlayerLayout(true);
                                    } else {
                                        animateExpandablePlayerLayout(false);
                                    }
                                } else {
                                    if (motionEvent.getRawY() - startPoint > getResources().getDimensionPixelSize(R.dimen.slide_view_lite) * 3) {
                                        animateExpandablePlayerLayout(false);
                                    } else {
                                        animateExpandablePlayerLayout(true);
                                    }
                                }
                            } else {
                                animateControlMusicFlex(controlMusicFlexLayout, true);
                                animateExpandablePlayerLayout(true);
                            }
                        }
                        return false;
                }
                default: return true;
        }
    }


    private void animateControlMusicFlex(View view, boolean show){
        float alpha = 0;
        if (!show) {
            alpha = 1;
        }
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, alpha);
        objectAnimator.setDuration(256);
        if (view == null){
            return;
        }
        objectAnimator.start();
    }


    private void animateExpandablePlayerLayout(boolean show){
        float targetF = 0;
        float targetB = defaultTranslationBottomMenu;
        int targetC =  (int) (Core.getWidthScreen() - margin);
        int targetUX = 0;
        int targetUY = 0;
        if (!show) {
            targetF = defaultTranslation;
            targetB = 0;
            targetC = getResources().getDimensionPixelSize(R.dimen.dp46);
            targetUX = (-(Core.getWidthScreen()/2-getResources().getDimensionPixelSize(R.dimen.dp23)-getResources().getDimensionPixelSize(R.dimen.dp18)));
            targetUY = (-(Core.getWidthScreen()/2-getResources().getDimensionPixelSize(R.dimen.dp28)+Core.getStatusBarHeight()));
        }
        ObjectAnimator animator = ObjectAnimator.ofFloat(playerFrameLayout, "translationY", targetF);
        ObjectAnimator animatorBottomNavigationView = ObjectAnimator.ofFloat(bottomNavigationView, "translationY", targetB);
        ObjectAnimator animatorCoverCardX = ViewPropertyObjectAnimator.animate(coverCard).width(targetC).translationX(targetUX).get();
        ObjectAnimator animatorCoverCardY = ViewPropertyObjectAnimator.animate(coverCard).height(targetC).translationY(targetUY).get();
        ObjectAnimator animatorCoverScaleX = ObjectAnimator.ofFloat(coverCard,
                View.SCALE_X, show ? audioService.getPlaying() ? scaleUpCard : scaleDownCard : scaleUpCard);
        ObjectAnimator animatorCoverScaleY = ObjectAnimator.ofFloat(coverCard,
                View.SCALE_Y, show ? audioService.getPlaying() ? scaleUpCard : scaleDownCard : scaleUpCard);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                showFragmentContainer(true);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animateControlMusicFlex(controlMusicFlexLayout, show);
                if (show){
                    showFragmentContainer(false);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animatorSet.play(animator)
                .with(animatorBottomNavigationView)
                .with(animatorCoverCardX)
                .with(animatorCoverCardY)
                .with(animatorCoverScaleX)
                .with(animatorCoverScaleY);
        animatorSet.setDuration(550);
        animatorSet.start();
        if (show) playerExpanded = true;
        else playerExpanded = false;
    }


    private void hideExpandablePlayerLayout(boolean hide){
        float targetY = defaultTranslation;
        if (hide) targetY = Core.getHeightScreen();
        playerFrameLayout.setTranslationY(targetY);
    }


    private void animatePlayPause(boolean playing){
        if (playerExpanded) {
            int targetC = (int) (Core.getWidthScreen());
            if (playing) targetC = (int) (targetC - margin / 2);
            else targetC = (int) (targetC - margin);
//            ObjectAnimator animatorCoverCardX = ViewPropertyObjectAnimator.animate(coverCard).width(targetC).get();
//            ObjectAnimator animatorCoverCardY = ViewPropertyObjectAnimator.animate(coverCard).height(targetC).get();
            ObjectAnimator animatorCoverCardX = ObjectAnimator.ofFloat(coverCard, View.SCALE_X, playing ? scaleUpCard : scaleDownCard);
            ObjectAnimator animatorCoverCardY = ObjectAnimator.ofFloat(coverCard, View.SCALE_Y, playing ? scaleUpCard : scaleDownCard);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
            animatorSet.setDuration(250);
            animatorSet.play(animatorCoverCardX).with(animatorCoverCardY);
            animatorSet.start();
        }
    }


    private void animateExpandableCurrentlist(boolean show){
        int targetC = getResources().getDimensionPixelSize(R.dimen.dp60)+Core.getStatusBarHeight();
        int targetP = -(Core.getHeightScreen() - (getResources().getDimensionPixelSize(R.dimen.dp60)+Core.getStatusBarHeight()));
        int targetB = -180;
        if (!show){
            targetP = 0;
            targetC = Core.getHeightScreen();
            targetB = 0;
        }
        ObjectAnimator animatorC = ObjectAnimator.ofFloat(currentlistRecyclerview, View.TRANSLATION_Y, targetC);
        ObjectAnimator animatorP = ObjectAnimator.ofFloat(playerWindowFlex, View.TRANSLATION_Y, targetP);
        ObjectAnimator animatorB = ObjectAnimator.ofFloat(_showCurrentlistButton, View.ROTATION, targetB);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.play(animatorC).with(animatorP).with(animatorB);
        animatorSet.start();
        currentlistExpanded = show ? true : false;
    }


    private ServiceConnection audioConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            AudioService.AudioBinder binder = (AudioService.AudioBinder) iBinder;
            audioService = binder.getService();
            if (audioService.getTrackList() == null)
//                audioService.bindTrackList(Core.getCoreInstance().getSongs());
            audioService.bindViewListener(GodActivity.this);
            bound = true;
//            audioService.bindTrackList(Core.getCoreInstance().getSongs());
//            songAdapter = new SongAdapter(audioService.getTrackList(), SongAdapter.FLAG_CURRENT_LIST, null);
//            songAdapter.setOnItemClickListener(GodActivity.this);
//            songAdapter.setHasStableIds(true);
//            currentlistRecyclerview.setAdapter(songAdapter);
//            animateExpandablePlayerLayout(false);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bound = false;
        }
    };


    @Override
    public void setTrackInfo(SongModel song, int pos) {
        title.setText(song.getTitle());
        controlTitle.setText(song.getTitle());
        artist.setText(song.getArtist());
        durationSeekbar.setProgress(0);
        durationSeekbar.setMax(song.getDuration());
        presenter.getDuration();
        presenter.getDurationPosition();
        if (loadCover != null) {
            loadCover.cancel(true);
            loadCover = (LoadCover) new LoadCover(cover).execute(song.getUri());
        } else {
            loadCover = (LoadCover) new LoadCover(cover).execute(song.getUri());
        }
        if (!currentlistExpanded) {
            layoutManager.scrollToPositionWithOffset(pos - 2, 0);
        }
        songAdapter.setCurrPlaying(pos);
        if (lastPos != -1) {
            songAdapter.notifyItemChanged(lastPos);
        }
        songAdapter.notifyItemChanged(pos);
        lastPos = pos;

        presenter.checkLike(song);
    }


    @Override
    public void pauseMusic() {
        playButton.setImageResource(R.drawable.ic_play32);
        playControlButton.setImageResource(R.drawable.ic_play20);
        animatePlayPause(false);
    }


    @Override
    public void playMusic() {
        playButton.setImageResource(R.drawable.ic_pause32);
        playControlButton.setImageResource(R.drawable.ic_pause20);
        animatePlayPause(true);
    }


    @Override
    public void updateDurationPosition(int pos) {
        durationSeekbar.setProgress(pos);
        presenter.getDurationPosition();
    }


    @Override
    public void bindTrackList(List<SongModel> trackList) {
        songAdapter.changeTrackList(audioService.getTrackList());
        animateExpandablePlayerLayout(false);
    }


    @Override
    public void eventEmptyTrackList() {
        hideExpandablePlayerLayout(true);
        Log.i(tag, getString(R.string.no_music_files_on_this_device));
    }


    @OnClick({R2.id.imagebutton_prev,
            R2.id.imagebutton_play,
            R2.id.imagebutton_next,
            R2.id.imagebutton_control_play,
            R2.id.imagebutton_control_next,
            R2.id.imagebutton_like,
            R2.id.imagebutton_more}) void onClick(View view){
        switch (view.getId()){
            case R.id.imagebutton_prev:
                audioService.prevTrack();
                break;
            case R.id.imagebutton_play:
                audioService.setPlaying();
                break;
            case R.id.imagebutton_next:
                audioService.nextTrack();
                break;
            case R.id.imagebutton_control_play:
                audioService.setPlaying();
                break;
            case R.id.imagebutton_control_next:
                audioService.nextTrack();
                break;
            case R.id.imagebutton_like:
                presenter.likeSong(audioService.getTrackList().get(lastPos));
                break;
            case R.id.imagebutton_more:
                Bundle bundle = new Bundle();
                bundle.putSerializable("adapter_song", audioService.getTrackList().get(audioService.getPosition()));
                BottomSheetDialogSongFragment bottomSheetDialogSongFragment = new BottomSheetDialogSongFragment();
                bottomSheetDialogSongFragment.setArguments(bundle);
//                bottomSheetDialogSongFragment.show(getSupportFragmentManager(), null);
//                PopupMenu p  = new PopupMenu(this, null);
//                Menu menu = p.getMenu();
//                getMenuInflater().inflate(R.menu.bottom_menu, menu);
//                showMenu(bottomSheetDialogSongFragment);
                break;
        }
    }



    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        switch (seekBar.getId()){
            case R.id.seekbar_duration:
                presenter.getDurationPosition();
                break;
        }
    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        switch (seekBar.getId()){
            case R.id.seekbar_duration:
                audioService.pauseOnUpdateDuration();
                break;
        }
    }


    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        switch (seekBar.getId()){
            case R.id.seekbar_duration:
                audioService.seekTo(seekBar.getProgress());
                if (audioService.getPlaying()) audioService.resetOnUpdateDuration();
                break;
        }
    }

    @Override
    public void hidePlayerWindow() {
        super.hidePlayerWindow();
        if (currentlistExpanded){
            animateExpandableCurrentlist(false);
        }
        if (playerExpanded){
            animateExpandablePlayerLayout(false);
        }
    }

    @Override
    public void onBackPressed() {
        if (currentlistExpanded){
            animateExpandableCurrentlist(false);
        } else if (playerExpanded){
            animateExpandablePlayerLayout(false);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public int getDurationPosition() {
        return durationSeekbar.getProgress();
    }


    @Override
    public int getDuration() {
        return durationSeekbar.getMax();
    }


    @Override
    public void setDurationPosition(String pos) {
        durationPosition.setText(pos);
    }


    @Override
    public void setDuration(String duration) {
        this.duration.setText(duration);
    }

    @Override
    public void setLiked(boolean liked) {
        if (liked) _likeButton.setImageResource(R.drawable.like_dark);
        else _likeButton.setImageResource(R.drawable.unlike_dark);
    }


    @Subscribe
    public void onEvent(FragmentEvent fragmentEvent) {
        Log.i("LOG", "EVENT");
        switch (fragmentEvent.getMessage()) {
            case FragmentEvent.FRAGMENT_SEARCH_MORE_RESULTS:
                FragmentSearchMoreResults fragment = new FragmentSearchMoreResults();

                ArrayMap<String, Object> params = fragmentEvent.getParams();

                Bundle bundleFragmentMoreResults = new Bundle();
                bundleFragmentMoreResults.putInt(FragmentSearchMoreResults.TYPE_RESULTS, (int) params.get(FragmentSearchMoreResults.TYPE_RESULTS));
                bundleFragmentMoreResults.putString(FragmentSearchMoreResults.QUERY, (String) params.get(FragmentSearchMoreResults.QUERY));
                fragment.setArguments(bundleFragmentMoreResults);

                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, fragment)
                        .addToBackStack(null)
                        .commit();
                break;
            case FragmentEvent.FRAGMENT_LASTFM_ACC:
//                pagerAdapter.addFragment(new FragmentLastFMLoginForm());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_ALBUMS_VIEW:
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, new FragmentAlbumsView())
                        .addToBackStack(null)
                        .commit();
                break;
            case FragmentEvent.FRAGMENT_DOWNLOADS_SHOW:
//                pagerAdapter.addFragment(new FragmentDownloads());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_YOUTUBE_SHOW_VIDEO:
                FragmentYouTubeShowVideo youTubeShowVideo = new FragmentYouTubeShowVideo();
                youTubeShowVideo.setArguments(fragmentEvent.getBundle());
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, youTubeShowVideo)
                        .addToBackStack(null)
                        .commit();
                break;
            case FragmentEvent.PAGING:
//                viewPager.setPagingEnabled(fragmentEvent.getEnabled());
//                Log.d(tag, "<FragmentEvent> Paging is " + fragmentEvent.getEnabled());
                break;
            case FragmentEvent.SET_VIEWPAGER_INTERCEPT_TOUCH:
//                viewPager.setInterceptEnabled(fragmentEvent.getEnabled());
                break;
            case FragmentEvent.FRAGMENT_BACK:
                fragmentBackStack();
                break;
            case FragmentEvent.FRAGMENT_SONGSVIEW:
//                pagerAdapter.addFragment(new FragmentSongsView());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, new FragmentSongsView())
                        .addToBackStack(null)
                        .commit();
                break;
            case FragmentEvent.FRAGMENT_SHOW_ALL_FAV_SONGS_OPEN:
//                pagerAdapter.addFragment(new FragmentFavSongsView());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_SHOW_ALL_PLAYLISTS_OPEN:
                FragmentPlaylistsView fragmentPlaylistsView = new FragmentPlaylistsView();
                Bundle bundleFragmentPlaylistsView = new Bundle();
                bundleFragmentPlaylistsView.putInt("Code", 0);
                bundleFragmentPlaylistsView.putString("Title", getString(R.string.playlists));
                fragmentPlaylistsView.setArguments(bundleFragmentPlaylistsView);
//                pagerAdapter.addFragment(fragmentPlaylistsView);
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_NEW_PLAYLIST:
//                pagerAdapter.addFragment(new FragmentNewPlaylist());
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_EXPAND_BOTTOM_PLAYER_CONTROL:
//                bottomPlayer.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case FragmentEvent.FRAGMENT_PLAYLIST_SONGS_VIEW:
                FragmentPlaylistSongsView fragmentPlaylistSongsView = new FragmentPlaylistSongsView();
                Bundle bundle = new Bundle();
                bundle.putInt("PlaylistID", fragmentEvent.getVarInt());
                fragmentPlaylistSongsView.setArguments(bundle);
//                pagerAdapter.addFragment(fragmentPlaylistSongsView);
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.TEST:
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.will_be_available_next_update), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                break;
            case FragmentEvent.FRAGMENT_SONG_ADD_TO_PLAYLIST:
                FragmentPlaylistsView fragmentPlaylistsViewAddSong = new FragmentPlaylistsView();
                Bundle bundleFragmentPlaylistsViewAddSong = new Bundle();
                bundleFragmentPlaylistsViewAddSong.putSerializable("Song", fragmentEvent.getSong());
                bundleFragmentPlaylistsViewAddSong.putString("Title", getString(R.string.add_to_a_playlist));
                bundleFragmentPlaylistsViewAddSong.putInt("Code", PlaylistNormalAdapter.ADD_TO_PLAYLIST);

                fragmentPlaylistsViewAddSong.setArguments(bundleFragmentPlaylistsViewAddSong);
//                pagerAdapter.addFragment(fragmentPlaylistsViewAddSong);
//                viewPager.setCurrentItem(pagerAdapter.getCount() - 1, true);
                break;
            case FragmentEvent.FRAGMENT_FORCE_LIGHT_STATUS_BAR:
//                lightStatusBar = fragmentEvent.getLightStatusBar();
//                setLightStatusBar(lightStatusBar);
                break;
            case FragmentEvent.FRAGMENT_DOWNLOAD:
                FragmentDownload fragmentDownload = new FragmentDownload();
                fragmentDownload.setArguments(fragmentEvent.getBundle());
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragments_container, fragmentDownload)
                        .addToBackStack(null)
                        .commit();
                break;
        }
    }

    @Override
    public void onLowMemory() {
        GlideApp.get(getApplicationContext()).onLowMemory();
        GlideApp.get(getApplicationContext()).clearMemory();
        Log.d("sukablyat", "onLowMemory");
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        GlideApp.get(getApplicationContext()).trimMemory(level);
        Log.d("sukablyat", "onLowMemory");
        super.onTrimMemory(level);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }


    @Override
    public void onPageSelected(int position) {
        viewPagerCurrentPosition = position;
    }


    @Override
    public void onPageScrollStateChanged(int state) {
        switch (state){
            case ViewPager.SCROLL_STATE_IDLE:
                Log.d(getClass().getSimpleName(), "viewpager state idle");
//                pagerAdapter.removeFragment(viewPagerCurrentPosition);
//                viewPager.setScrollDurationFactor(3);
//                viewPager.setPagingEnabled(true);
                break;
            case ViewPager.SCROLL_STATE_DRAGGING:

                Log.d(getClass().getSimpleName(), "viewpager state dragging");
//                viewPager.setScrollDurationFactor(2);
                break;
            case ViewPager.SCROLL_STATE_SETTLING:
//                viewPager.setPagingEnabled(false);
                Log.d(getClass().getSimpleName(), "viewpager state settling");

                break;
            default:

                break;
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.fragments_container)).commit();
        switch (item.getItemId()) {
            case R.id.menu_home:
                addRootFragment(new FragmentHome());
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .add(R.id.fragments_container, new FragmentHome())
//                        .commit();
                break;
            case R.id.menu_search:
                addRootFragment(new FragmentSearch());
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .add(R.id.fragments_container, new FragmentSearch())
//                        .commit();
                break;
            case R.id.menu_fav:
                addRootFragment(new FragmentFav());
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .add(R.id.fragments_container, new FragmentFav())
//                        .commit();
                break;
            case R.id.more:
                addRootFragment(new FragmentMore());
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .add(R.id.fragments_container, new FragmentMore())
//                        .commit();
                break;
        }
        return true;
    }


    @Override
    protected void hideBottomBar(boolean hide) {
        super.hideBottomBar(hide);
        ObjectAnimator bottomBarAnimator = ObjectAnimator.ofFloat(bottomNavigationView,
                View.TRANSLATION_Y, hide ? getResources().getDimension(R.dimen.dp112) : 0f);
        ObjectAnimator playerLayoutAnimator = ObjectAnimator.ofFloat(playerFrameLayout,
                View.TRANSLATION_Y, hide ?
                        Core.getHeightScreen() :
                        Core.getHeightScreen()-getResources().getDimension(R.dimen.dp112));

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(500);
        animatorSet.playTogether(bottomBarAnimator, playerLayoutAnimator);
        animatorSet.start();
    }

    @Override
    public void onSongItemClick(int pos) {
        audioService.setTrack(pos);
    }

    @Override
    public void onAlbumPlayClick() {

    }

    @Override
    public void onSongItemLongClick(int pos) {

    }

    class LoadCover extends AsyncTask<String, Void, Bitmap>{
        WeakReference<ImageView> imageViewWeakReference;

        public LoadCover(ImageView imageView) {
            imageViewWeakReference = new WeakReference<ImageView>(imageView);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ImageView imageView = imageViewWeakReference.get();
            imageView.setImageResource(Core.getCoreInstance().getDarkTheme() == 0 ?
                    R.drawable.noimg2light : R.drawable.noimg2dark);
//            GlideApp.with(getApplicationContext()).load(Core.getCoreInstance().getDarkTheme() == 0 ?
//                    R.drawable.noimg2light : R.drawable.noimg2dark)
//                    .into(imageView);
//            imageView.setImageResource(R.drawable.noimg2light);
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap img = null;
            try {
                MediaMetadataRetriever mData = new MediaMetadataRetriever();
                mData.setDataSource(strings[0]);
                img = decodeSampledBitmapFromResource(mData.getEmbeddedPicture(), 720, 720);
                mData.release();
            }catch (Exception e){

            }
            return img;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (!isCancelled()) {
                if (bitmap != null) {
                    ImageView imageView = imageViewWeakReference.get();
                    imageView.setImageBitmap(bitmap);
                }
            }
        }

        public Bitmap decodeSampledBitmapFromResource(byte[] array,
                                                      int reqWidth, int reqHeight) {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(array, 0, array.length, options);
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(array, 0, array.length, options);
        }

        public int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;
            if (height > reqHeight || width > reqWidth) {
                final int halfHeight = height / 2;
                final int halfWidth = width / 2;
                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }
            return inSampleSize;
        }
    }
}