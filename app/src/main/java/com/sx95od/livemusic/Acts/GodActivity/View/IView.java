package com.sx95od.livemusic.Acts.GodActivity.View;

/**
 * Created by stoly on 12.09.2017.
 */

public interface IView {
    int getDurationPosition();
    int getDuration();
    void setDurationPosition(String pos);
    void setDuration(String duration);
    void setLiked(boolean liked);
}
