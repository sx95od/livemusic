package com.sx95od.livemusic.Acts.GodActivity.View;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.List;

/**
 * Created by stoly on 12.09.2017.
 */

public interface IServiceView {
    void setTrackInfo(SongModel song, int pos);
    void pauseMusic();
    void playMusic();
    void updateDurationPosition(int pos);
    void bindTrackList(List<SongModel> trackList);
    void eventEmptyTrackList();
}
