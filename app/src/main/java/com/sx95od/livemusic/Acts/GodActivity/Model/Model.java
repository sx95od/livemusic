package com.sx95od.livemusic.Acts.GodActivity.Model;


import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.FavSongs;
import com.sx95od.livemusic.Utils.MD5;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 14.09.2017.
 */

public class Model implements IModel {

    @Override
    public Observable<String> getFormatDuration(int pos) {
        int millSec = pos;
        int sec = millSec/1000;
        if (sec%60>9){
            return Observable.just(String.valueOf(Math.round(sec/60)+":"+sec%60));
        } else{
            return Observable.just(String.valueOf(Math.round(sec/60)+":"+"0"+sec%60));
        }
    }

    @Override
    public Observable<String> getFormatDurationPosition(int pos) {
        int millSec = pos;
        int sec = millSec/1000;
        if (sec%60>9){
            return Observable.just(String.valueOf(Math.round(sec/60)+":"+sec%60));
        } else{
            return Observable.just(String.valueOf(Math.round(sec/60)+":"+"0"+sec%60));
        }
    }

    @Override
    public Observable<Boolean> likeSong(SongModel songModel) {
        return Observable.defer(() -> {
            Realm realm = Realm.getInstance(Core.getConfigRealm());

            RealmResults<FavSongs> results = realm.where(FavSongs.class)
                    .equalTo("_id", songModel.getId()).findAll();

            if (results.size() == 0) {
                realm.beginTransaction();
                FavSongs favSongs = realm.createObject(FavSongs.class, generateID());
                favSongs.set_id(songModel.getId());
                favSongs.setAlbum(songModel.getAlbum());
                favSongs.setAlbum_id(songModel.getAlbum_id());
                favSongs.setArtist(songModel.getArtist());
                favSongs.setArtist_id(songModel.getArtist_id());
                favSongs.setDate_modified(songModel.getDate());
                favSongs.setDuration(songModel.getDuration());
                favSongs.setUri(songModel.getUri());
                favSongs.setYear(songModel.getYear());
                favSongs.setTitle(songModel.getTitle());
                favSongs.setMD5(MD5.checkMD5(songModel.getUri()));
                realm.commitTransaction();
                return Observable.just(true);
            } else {
                realm.beginTransaction();
                results.deleteAllFromRealm();
                realm.commitTransaction();
                return Observable.just(false);
            }
        });
    }

    @Override
    public Observable<Boolean> checkLike(SongModel songModel) {
        return Observable.defer(new Callable<ObservableSource<? extends Boolean>>() {
            @Override
            public ObservableSource<? extends Boolean> call() throws Exception {
                Realm realm = Realm.getInstance(Core.getConfigRealm());
                RealmResults<FavSongs> results = realm.where(FavSongs.class)
                        .equalTo("_id", songModel.getId()).findAll();
                if (results.size()>0){
                    return Observable.just(true);
                } else return Observable.just(false);
            }
        });
    }

    private int generateID(){
        Realm realm = Realm.getInstance(Core.getConfigRealm());
        Number currentIdNum = realm.where(FavSongs.class).max("id");
        int nextId;
        if(currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }
        return nextId;
    }
}
