package com.sx95od.livemusic.Acts;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.github.mmin18.widget.RealtimeBlurView;
import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.Adapters.Adapters.AdapterMenu;
import com.sx95od.livemusic.AnimationConstants;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Dialogs.MyDialog;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.FragmentYouTubeShowVideo;
import com.sx95od.livemusic.Fragments.MyFragment;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.Utils.ThemeEngine;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.blurry.Blurry;

import static android.view.View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION;

/**
 * Created by stoly on 15.10.2017.
 */

public class MyActivity extends AppCompatActivity {
    protected boolean PLAYER_EXPANDED              = false;
    protected boolean DIALOG_SHOWED                = false;
    protected boolean CURRENT_LIST_LAYOUT_EXPANDED = false;

    public static final int TYPE_SONG_MORE = 1;
    private ObjectAnimator dialogAnimator;

    @BindView(R2.id.fragments_container)
    FrameLayout _fContainer;

    @BindView(R2.id.dialog_blur)
    RealtimeBlurView _dialogBlur;

    @BindView(R2.id.fl_dialog)
    FrameLayout _fDialog;

    public boolean canBackPress = true;
    boolean canUpdateFragments = true;

    List<Fragment> fragments = new ArrayList<Fragment>();
    List<Fragment> dialogs   = new ArrayList<Fragment>();

    public void setView(){
        LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflator.inflate(R.layout.activity_main, null);
        setContentView(view);
    }


    public void requestBottomPadding(){

    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Core.getCoreInstance().getDarkTheme() == 0 ? R.style.AppTheme : R.style.AppThemeDark);
        setView();
        ButterKnife.bind(this);
    }


    private void animateDialogs(boolean show){
        if (dialogAnimator != null) dialogAnimator.removeAllListeners();
        dialogAnimator = null;
        float alpha = show ? 1f : 0f;
        dialogAnimator = ObjectAnimator.ofFloat(_dialogBlur, View.ALPHA, alpha);
        dialogAnimator.setDuration(AnimationConstants.Companion.xFragTransDur());
        dialogAnimator.setInterpolator(new DecelerateInterpolator());
        dialogAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                if (show) {
                    _dialogBlur.setVisibility(View.VISIBLE);
                    if (_fDialog.getVisibility() == View.GONE) _fDialog.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!show) {
                    _dialogBlur.setVisibility(View.GONE);
                    _fDialog.setLayerType(View.LAYER_TYPE_NONE, null);
                    if (_fDialog.getVisibility() == View.VISIBLE &&
                            dialogs.size() == 0) _fDialog.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        dialogAnimator.start();
    }

    public void showDialog(Fragment fragment){
        getSupportFragmentManager().beginTransaction().add(R.id.fl_dialog, fragment, "DIALOG").addToBackStack("DIALOG").commit();
        animateDialogs(true);
        dialogs.add(fragment);
        DIALOG_SHOWED = true;

    }


    @Override
    public void onBackPressed() {
        if (DIALOG_SHOWED){
            if (dialogs.size() == 1) {
                dialogs.remove(dialogs.size()-1);
                animateDialogs(false);
            }
            ((MyDialog) getSupportFragmentManager().findFragmentById(R.id.fl_dialog)).backStack();
        } else if (getSupportFragmentManager().getBackStackEntryCount()>0){
            try {
                ((MyFragment) getSupportFragmentManager().findFragmentById(R.id.fragments_container)).backStack();
            }catch (Exception e){
                super.onBackPressed();
            }
        } else {
            if (canBackPress) super.onBackPressed();
        }
    }

    public void showFragmentContainer(boolean show){
        _fContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void hidePlayerWindow(){

    }

    public void onRemoveFragment(){
        if (fragments.get(fragments.size()-2) instanceof FragmentYouTubeShowVideo) {
            setLightStatusBar(false);
            hideBottomBar(true);
        } else {
            setLightStatusBar(true);
            hideBottomBar(false);
        }

        getSupportFragmentManager()
                .beginTransaction()
                .show(fragments.get(fragments.size()-2))
                .commit();
        fragments.remove(fragments.size()-1);
    }


    public void addFragment(Fragment fragment){
        if (canUpdateFragments) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragments_container, fragment)
                    .addToBackStack(null)
                    .commit();
            fragments.add(fragment);
            canUpdateFragments = false;
            if (fragment instanceof FragmentYouTubeShowVideo) {
                setLightStatusBar(false);
                hideBottomBar(true);
            } else {
                setLightStatusBar(true);
                hideBottomBar(false);
            }
        }
    }


    protected void hideBottomBar(boolean hide){

    }

    public void setLightStatusBar(boolean lightStatusBar){
        if (((Core) getApplication()).getDarkTheme() != 1){
            if (!lightStatusBar) {
                getWindow().getDecorView().setSystemUiVisibility(0
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            } else {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }
        } else{
            getWindow().getDecorView().setSystemUiVisibility(0
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }


    public void addRootFragment(Fragment fragment){
        setLightStatusBar(true);
        fragments.clear();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragments_container, fragment, "rootFragment")
                .commit();
        fragments.add(fragment);
    }


    public void onNewFragment(){
        if (fragments.size() == 2){
            getSupportFragmentManager().beginTransaction().hide(fragments.get(0)).commit();
        } else if (fragments.size() > 2)
            getSupportFragmentManager()
                    .beginTransaction()
                    .hide(fragments.get(fragments.size()-2))
                    .commit();
            canUpdateFragments = true;
    }


    public int getPxInDp(int dp){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    public void fragmentBackStack(){
        getSupportFragmentManager().popBackStack();
    }

    public void dialogBackStack(){
        getSupportFragmentManager().popBackStack("DIALOG", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (dialogs.size() == 0) {
            DIALOG_SHOWED = false;
        }
    }
}
