package com.sx95od.livemusic.Acts.GodActivity.Presenter;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

/**
 * Created by stoly on 14.09.2017.
 */

public interface IPresenter {
    void getDurationPosition();
    void getDuration();
    void likeSong(SongModel song);
    void checkLike(SongModel song);
    void onDestroy();
}
