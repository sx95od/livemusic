package com.sx95od.livemusic.Fragments.FragmentFav;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.sx95od.livemusic.API.LastFM.Models.ChartTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.GeoTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.Track;
import com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.ChartVideosModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel.MyChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.Acts.MyActivity;
import com.sx95od.livemusic.Adapters.Adapters.LastFMChartTopTracksAdapter;
import com.sx95od.livemusic.Adapters.Adapters.LastFMGeoChartTopTracksAdapter;
import com.sx95od.livemusic.Adapters.Adapters.PlaylistAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.PlaylistModel;
import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Adapters.Adapters.YTChartAdapter;
import com.sx95od.livemusic.AnimationConstants;
import com.sx95od.livemusic.BottomSheetDialogPlaylist;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.Glide.GlideApp;
import com.sx95od.livemusic.DataBase.FavSongs;
import com.sx95od.livemusic.DataBase.Playlists;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.ThemeEvent;
import com.sx95od.livemusic.Events.UIEvent;
import com.sx95od.livemusic.Fragments.BaseFragment;
import com.sx95od.livemusic.Fragments.FragmentFav.Presenter.Presenter;
import com.sx95od.livemusic.Fragments.FragmentFav.View.IView;
import com.sx95od.livemusic.Fragments.FragmentLastFMLoginForm.FragmentLastFMLoginForm;
import com.sx95od.livemusic.Fragments.FragmentYTMyChannel.FragmentYTMyChannel;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.FragmentYouTubeShowVideo;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.Utils.SystemUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.sx95od.livemusic.Utils.Utils.getPxInDp;

/**
 * Created by stoly on 11.06.2017.
 */

public class FragmentFav extends BaseFragment implements View.OnClickListener,
        PlaylistAdapter.OnItemMoreClickListener, IView, Listeners.OnLastFMGeoTopChartTracksItemClick,
        Listeners.OnLastFMTopChartTracksItemClick, Listeners.OnYTChartVideoListener{

    private final String tag = FragmentFav.class.getSimpleName();

    @BindView(R2.id.recyclerview_lastfm_chart_top_tracks) RecyclerView _lastFMChartTopTracksRV;
    @BindView(R2.id.recyclerview_lastfm_geo_chart_top_tracks) RecyclerView _lastFMGeoChartTopTracksRV;
    @BindView(R2.id.recyclerview_yt_chart_videos) RecyclerView _ytChartVideosRV;
    @BindView(R2.id.imageview_yt_acc) ImageView _ivYTAcc;
    @BindView(R2.id.pb_lastfm_geo_top_tracks) ProgressBar _pbLastFMGeoTopTracks;
    @BindView(R2.id.pb_lastfm_top_tracks) ProgressBar _pbLastFMTopTracks;
    @BindView(R2.id.pb_yt_chart_videos) ProgressBar _pbYTChartVideos;
    @BindView(R2.id.l_lfm_geotracks_load_data) ViewGroup _lfmGeoTracksLoadData;
    @BindView(R2.id.l_lfm_load_data) ViewGroup _lfmLoadData;
    @BindView(R2.id.l_yt_load_data) ViewGroup _ytLoadData;
    @BindView(R2.id.tv_root_fragment_name) TextView _tvRootName;
    @BindView(R2.id.fl_acc) FrameLayout _flAcc;

    @BindView(R2.id.scroll_view)
    ScrollView _scrollView;

    private boolean ytChartVideosDownloaded = false;

    private static final int pass = 2;
    RecyclerView likedRV, playlistsRV;

    RecyclerView.LayoutManager
            lmLiked,
            lmPlaylists,
            lmLFMChartTopTracks,
            lmLFMGeoChartTopTracks,
            lmYTChartVideos;

    List<SongModel> songsArray;
    List<PlaylistModel> playlistsArray;
    List<ChartTopTracksModel> lastFMChartTopTrackObjects;
    GeoTopTracksModel lastFMGeoTopTracksModel;

    SongAdapter songsAdapter;
    PlaylistAdapter playlistAdapter;

    LastFMChartTopTracksAdapter lastFMChartTopTracksAdapter;
    LastFMGeoChartTopTracksAdapter lastFMGeoChartTopTracksAdapter;

    YTChartAdapter ytChartAdapter;

    TextView showAllFavSongs, showAllPlaylists, likedCount, playlistsCount;
    LinearLayout newPlaylist, playlistParent;
    FrameLayout nonPlaylistParent;
    ImageButton morePlaylists;
    CardView cardLiked;
    int count = 0;
    Unbinder unbinder;

    int pageGeoTopTracks = 1;

    ProgressDialog progressDialog;

    Presenter presenter;

    MyChannelModel myChannelModel;


    @Subscribe
    public void fragmentEvent(FragmentEvent fragmentEvent){
        switch (fragmentEvent.getMessage()){
            case FragmentEvent.FRAGMENT_CREATED_PLAYLIST:

                break;
            case FragmentEvent.FRAGMENT_DELETED_PLAYLIST:

                break;
        }
    }


    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);

        presenter.onDestroy();
        presenter = null;

        likedRV.setAdapter(null);
        _lastFMChartTopTracksRV.setAdapter(null);
        _lastFMGeoChartTopTracksRV.setAdapter(null);
        playlistsRV.setAdapter(null);

        GlideApp.get(getContext()).clearMemory();

        playlistsArray.clear();
        songsArray.clear();
        playlistsArray = null;
        songsArray = null;

        unbinder.unbind();
        Log.d(tag, "OnDestroyView");
        super.onDestroyView();
    }


    private void tuneUI(){
        _scrollView.setPadding(0, 0, 0, PrefManager.getBottomNavigationBarHeight());
        _scrollView.requestLayout();
    }


    @Subscribe
    private void themeEvent(ThemeEvent themeEvent){
        showAllFavSongs.setTextColor(PrefManager.getColorAccent());
        _pbLastFMGeoTopTracks.getIndeterminateDrawable().setColorFilter(PrefManager.getColorAccent(), PorterDuff.Mode.SRC_IN);
        _pbLastFMTopTracks.getIndeterminateDrawable().setColorFilter(PrefManager.getColorAccent(), PorterDuff.Mode.SRC_IN);
    }


    private void onViewCreatedTransition(){
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator objectAnimatorA   = ObjectAnimator.ofFloat(_flAcc, View.ALPHA, -0.5f, 1f);
        ObjectAnimator objectAnimatorTX  = ObjectAnimator.ofFloat(_flAcc, View.TRANSLATION_X,
                -getPxInDp(getResources().getInteger(R.integer.top_base_flex_translation_x)), 0f);
        ObjectAnimator objectAnimatorA1  = ObjectAnimator.ofFloat(_tvRootName, View.ALPHA, -0.5f, 1f);
        ObjectAnimator objectAnimatorTX1 = ObjectAnimator.ofFloat(_tvRootName, View.TRANSLATION_X,
                -getPxInDp(getResources().getInteger(R.integer.top_base_flex_translation_x)), 0f);

        animatorSet.playTogether(objectAnimatorA, objectAnimatorTX, objectAnimatorA1, objectAnimatorTX1);
        animatorSet.setDuration(getResources().getInteger(R.integer.top_base_flex_animation_duration));
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animatorSet.start();
    }

    private void preCreateViewTransition(){
        _tvRootName.setTranslationX(-getResources().getDimensionPixelSize(R.dimen.dp56));
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fav, container, false);
        EventBus.getDefault().register(this);
        unbinder = ButterKnife.bind(this, view);

        initAdapters();
        initLastFMGeoTopTracks();
        initLastFMTopTracks();
        initYTChartAdapter();

        preCreateViewTransition();

        showAllFavSongs = (TextView) view.findViewById(R.id.show_all_fav_songs);
        showAllPlaylists = (TextView) view.findViewById(R.id.show_all_playlists);
        cardLiked = (CardView) view.findViewById(R.id.card_liked);
        likedCount = (TextView) view.findViewById(R.id.liked_count);
        playlistsCount = (TextView) view.findViewById(R.id.playlists_count);
        showAllFavSongs.setOnClickListener(this);
        showAllPlaylists.setOnClickListener(this);
        newPlaylist = (LinearLayout) view.findViewById(R.id.new_playlist);
        playlistParent = (LinearLayout) view.findViewById(R.id.playlist_parent);
        nonPlaylistParent = (FrameLayout) view.findViewById(R.id.nonplaylist_parent);
        newPlaylist.setOnClickListener(this);
        likedRV = (RecyclerView) view.findViewById(R.id.recyclerview_liked);
        playlistsRV = (RecyclerView) view.findViewById(R.id.recyclerview_playlists);
        lmLiked = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lmPlaylists = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        likedRV.setLayoutManager(lmLiked);
        playlistsRV.setLayoutManager(lmPlaylists);
        morePlaylists = (ImageButton) view.findViewById(R.id.more_playlists);
        morePlaylists.setOnClickListener(this);
        _ivYTAcc.setOnClickListener(this);
//        SnapHelper snapHelperStart = new GravitySnapHelper(Gravity.START);
//        snapHelperStart.attachToRecyclerView(likedRV);
        songsArray = new ArrayList<SongModel>();
        playlistsArray = new ArrayList<PlaylistModel>();
        loadLiked();

        themeEvent(null);

        tuneUI();
        Log.d(tag, "OnCreateView");
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        onViewCreatedTransition();

        presenter = new Presenter(this);
        presenter.onCreate();
        presenter.loadMyYTData();
        presenter.loadYTChartVideos();
        Log.d(tag, "OnViewCreated");
        ((MyActivity) getActivity()).requestBottomPadding();
        super.onViewCreated(view, savedInstanceState);
    }

    private void initAdapters(){
        lastFMGeoChartTopTracksAdapter = new LastFMGeoChartTopTracksAdapter(this, getActivity());
        lastFMChartTopTracksAdapter = new LastFMChartTopTracksAdapter(this, getActivity());
        ytChartAdapter = new YTChartAdapter(this, getActivity());
        Log.d(tag, "InitAdapters");
    }


    @Subscribe
    public void onPlayerWidgetVisibleEvent(UIEvent.PlayerWidgetVisible event){
        _scrollView.setPadding(0, 0, 0, PrefManager.getBottomListPadding());
        _scrollView.requestLayout();
    }

    private void initLastFMGeoTopTracks(){
        lmLFMGeoChartTopTracks = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.HORIZONTAL, false);

        _lastFMGeoChartTopTracksRV.setItemViewCacheSize(1);
        _lastFMGeoChartTopTracksRV.setLayoutManager(lmLFMGeoChartTopTracks);
        _lastFMGeoChartTopTracksRV.setAdapter(lastFMGeoChartTopTracksAdapter);
        SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(_lastFMGeoChartTopTracksRV);
        _lastFMGeoChartTopTracksRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (((LinearLayoutManager) lmLFMGeoChartTopTracks).findLastVisibleItemPosition() > lastFMGeoChartTopTracksAdapter.getItemCount()-20)
                    presenter.getLastFMGeoTopTracks();
            }
        });
        Log.d(tag, "InitLastFMGeoTopTracks");
    }

    private void initLastFMTopTracks(){
        lmLFMChartTopTracks = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        _lastFMChartTopTracksRV.setItemViewCacheSize(1);
        _lastFMChartTopTracksRV.setLayoutManager(lmLFMChartTopTracks);
        _lastFMChartTopTracksRV.setAdapter(lastFMChartTopTracksAdapter);
        SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(_lastFMChartTopTracksRV);
        _lastFMChartTopTracksRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (((LinearLayoutManager) lmLFMChartTopTracks).findLastVisibleItemPosition() > lastFMChartTopTracksAdapter.getItemCount()-20)
                    presenter.getLastFMTopTracks();
            }
        });
        Log.d(tag, "InitLastFMTopTracks");
    }

    private void initYTChartAdapter(){
        lmYTChartVideos = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        _ytChartVideosRV.setItemViewCacheSize(1);
        _ytChartVideosRV.setLayoutManager(lmYTChartVideos);
        _ytChartVideosRV.setAdapter(ytChartAdapter);
        SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(_ytChartVideosRV);
        _ytChartVideosRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (ytChartVideosDownloaded && ((LinearLayoutManager) lmYTChartVideos).findLastVisibleItemPosition() > ytChartAdapter.getItemCount()-10) {
                    ytChartVideosDownloaded = false;
                    presenter.loadYTChartVideos();
                }
            }
        });
        Log.d(tag, "InitLastFMTopTracks");
    }





    private void loadLastFMChartTopTracks(){
//        lastFMChartTopTrackObjects = new ArrayList<ChartTopTracksModel>();
//        Core.getLastFMApi().getTopTracks(BuildConfig.LASTFM_API_KEY, 64, 1, Constants.JSON).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    String resp = response.body().string();
//                    Log.d("FragmentFav", resp);
//                    lastFMChartTopTrackObjects = LastFMResponseParser.getTopTracks(resp);
//                    lastFMChartTopTracksAdapter = new LastFMChartTopTracksAdapter(lastFMChartTopTrackObjects);
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            _lastFMChartTopTracksRV.setAdapter(lastFMChartTopTracksAdapter);
//                        }
//                    });
//                }catch (Exception e){
//                    Log.e("RESPONSECHART", e.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });

    }

    private void showMenu(View view){
        PopupMenu popupMenu = new PopupMenu(getActivity(), view, Gravity.RIGHT);
        popupMenu.inflate(R.menu.card_playlists_menu); // Для Android 4.0
        // для версии Android 3.0 нужно использовать длинный вариант
        // popupMenu.getMenuInflater().inflate(R.menu.popupmenu,
        // popupMenu.getMenu());

        popupMenu
                .setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Toast.makeText(PopupMenuDemoActivity.this,
                        // item.toString(), Toast.LENGTH_LONG).show();
                        // return true;
                        switch (item.getItemId()) {
                            case R.id.add_playlist:
                                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_NEW_PLAYLIST));
                                return true;
//                            case R.id.tune_playlist:
//                                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.TUNE_CARD_PLAYLIST));
//                                return true;
                            default:
                                return false;
                        }
                    }
                });

        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu menu) {

            }
        });
        popupMenu.show();
    }

    private void loadPlaylists(){
        SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(playlistsRV);
        playlistsArray.clear();
            try{
                final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<Playlists> result = realm.where(Playlists.class)
                                .findAll();
                        final int count = result.size();

                        Log.d(FragmentFav.this.getClass().getSimpleName(), "<Results playlists count> "+count);

                        getActivity().runOnUiThread(() -> {
                            try {
                                playlistsCount.setText(getResources().getQuantityString(R.plurals.playlists_count, count, count));
                            }catch (Exception e){
                            }
                        });

                        for (int i=0; i<result.size(); i++){
                            playlistsArray.add(new PlaylistModel(result.get(i).getId(),
                                    result.get(i).getName(), result.get(i).getCover(), result.get(i).getDate(),
                                    0, result.get(i).getAbout()));
                        }
                        playlistAdapter = new PlaylistAdapter(playlistsArray);
                        playlistAdapter.setOnItemMoreClickListener(FragmentFav.this);
                        playlistAdapter.setHasStableIds(true);
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        playlistsRV.setAdapter(playlistAdapter);
                        realm.close();
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        realm.close();
                    }
                });
            }catch (Exception e){
                Log.i(getClass().getSimpleName(), e.toString());
            }
    }


    private void loadLiked(){
        try {
            final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<FavSongs> result = realm
                            .where(FavSongs.class)
                            .findAllSorted("id", Sort.DESCENDING);
                    final int count = result.size();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                likedCount.setText(getResources().getQuantityString(R.plurals.tracks_count, count, count));
                                ViewGroup.LayoutParams layoutParams = likedRV.getLayoutParams();
                                if (count > 4) {
                                    layoutParams.height = (int) getResources().getDimension(R.dimen.song_adapter_normal_height) * 4;
                                } else if (count!=0){
                                    layoutParams.height = (int) getResources().getDimension(R.dimen.song_adapter_normal_height) * count;
                                } else{
                                    cardLiked.setVisibility(View.GONE);
                                }
                                likedRV.setLayoutParams(layoutParams);
                            }catch (Exception e){

                            }
                        }
                    });


                    for (int i = 0; i < result.size(); i++) {
                        Log.i("Realm", result.get(i).getTitle());
                        songsArray.add(new SongModel(result.get(i).get_id(), result.get(i).getUri(), "",
                                result.get(i).getTitle(), result.get(i).getArtist(), result.get(i).getAlbum(),
                                result.get(i).getAlbum_id(), result.get(i).getArtist_id(), result.get(i).getDuration(),
                                result.get(i).getYear(), result.get(i).getDate_modified(), false));
                    }
                    songsAdapter = new SongAdapter(songsArray, SongAdapter.FLAG_SET_OFF_MORE, songsArray.size()>4 ? 4 : songsArray.size(), null);
                    songsAdapter.setHasStableIds(true);
                }
            }, () -> {

                likedRV.setAdapter(songsAdapter);
                realm.close();

            }, error -> {
                realm.close();
            });
        }catch (Exception e){
            Log.i(getClass().getSimpleName(), e.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.new_playlist:
                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_NEW_PLAYLIST));
                break;
            case R.id.show_all_fav_songs:
                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_SHOW_ALL_FAV_SONGS_OPEN));
                break;
            case R.id.show_all_playlists:
                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_SHOW_ALL_PLAYLISTS_OPEN));
                break;
            case R.id.more_playlists:
                showMenu(v);
                break;
            case R.id.imageview_yt_acc:
                FragmentYTMyChannel fragmentYTMyChannel = new FragmentYTMyChannel();
                Bundle bundle = new Bundle();
                bundle.putSerializable("yt_my_channel", myChannelModel);
                fragmentYTMyChannel.setArguments(bundle);
                if (myChannelModel != null)
                    ((MyActivity) getActivity()).addFragment(fragmentYTMyChannel);
                else ((MyActivity) getActivity()).addFragment(new FragmentLastFMLoginForm());
                break;
        }
    }

    @Override
    public void onItemOptionClickListener(PlaylistModel playlistField) {
        BottomSheetDialogPlaylist bottomSheetDialogFragment = new BottomSheetDialogPlaylist();
        Bundle args = new Bundle();
        args.putSerializable("playlist", (Serializable) playlistField);
        bottomSheetDialogFragment.setArguments(args);
        bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void openFirstYouTubeVideo(Item item) {
        progressDialog.dismiss();
        progressDialog = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable("youtube_item", item);
        FragmentYouTubeShowVideo fragmentYouTubeShowVideo = new FragmentYouTubeShowVideo();
        fragmentYouTubeShowVideo.setArguments(bundle);
        ((MyActivity) getActivity()).addFragment(fragmentYouTubeShowVideo);
//        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_YOUTUBE_SHOW_VIDEO, bundle));

    }

    @Override
    public void youTubeShowError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showGeoTopTracksLastFM(List<Track> tracks) {
        if (_lfmGeoTracksLoadData.getVisibility() == View.VISIBLE) _lfmGeoTracksLoadData.setVisibility(View.GONE);
        final int countItems = lastFMGeoChartTopTracksAdapter.getItemCount();
        lastFMGeoChartTopTracksAdapter.addItems(tracks);
        lastFMGeoChartTopTracksAdapter.notifyItemInserted(countItems);
        presenter.cacheLastFMGeoTopTracks(lastFMGeoChartTopTracksAdapter.getObjects());
    }

    @Override
    public void showTopTracksLastFM(List<com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track> tracks) {
        if (_lfmLoadData.getVisibility() == View.VISIBLE) _lfmLoadData.setVisibility(View.GONE);
        final int countItems = lastFMChartTopTracksAdapter.getItemCount();
        lastFMChartTopTracksAdapter.addItems(tracks);
        lastFMChartTopTracksAdapter.notifyItemInserted(countItems);
        presenter.cacheLastFMTopTracks(lastFMChartTopTracksAdapter.getObjects());
    }

    @Override
    public void showMyYT(MyChannelModel myChannelModel) {
        this.myChannelModel = myChannelModel;
        Core.withGlide()
                .load(myChannelModel.getItems().get(0).getSnippet().getThumbnails().getMedium().getUrl())
                .override(290)
                .fitCenter()
                .into(_ivYTAcc);
        _ivYTAcc.setScaleType(ImageView.ScaleType.CENTER_CROP);
        _ivYTAcc.setColorFilter(0);
    }

    @Override
    public void showVideoChartYT(ChartVideosModel chartVideosModel) {
        if (_ytLoadData.getVisibility() == View.VISIBLE) _ytLoadData.setVisibility(View.GONE);
        ytChartAdapter.addItems(chartVideosModel.getItems());
        ytChartAdapter.notifyItemInserted(ytChartAdapter.getItemCount());
        ytChartVideosDownloaded = true;
        Log.d(tag, chartVideosModel.getPageInfo().getTotalResults()+" results");
    }

    @Override
    public void onItemClick(Track item) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(getString(R.string.search));
        progressDialog.setMessage(getString(R.string.searching_track_on_youtube));
        progressDialog.show();
        presenter.onSearchVideosOnYouTube(item.getName()+" "+item.getArtist().getName());
    }

    @Override
    public void onItemClick(com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track track) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(getString(R.string.search));
        progressDialog.setMessage(getString(R.string.searching_track_on_youtube));
        progressDialog.show();
        presenter.onSearchVideosOnYouTube(track.getName()+" "+track.getArtist().getName());
    }


    @Override
    public void onVideoClickListener(com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.Item video) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("youtube_item", video);
        FragmentYouTubeShowVideo fragmentYouTubeShowVideo = new FragmentYouTubeShowVideo();
        fragmentYouTubeShowVideo.setArguments(bundle);
        ((MyActivity) getActivity()).addFragment(fragmentYouTubeShowVideo);
    }
}
