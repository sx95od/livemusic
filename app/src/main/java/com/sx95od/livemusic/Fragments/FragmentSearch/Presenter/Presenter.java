package com.sx95od.livemusic.Fragments.FragmentSearch.Presenter;

import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.sx95od.livemusic.API.LastFM.Constants;
import com.sx95od.livemusic.API.LastFM.LastFMApi;
import com.sx95od.livemusic.API.LastFM.Models.AlbumSearch.AlbumSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.ArtistSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.Track;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.TrackSearchModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Analytics.MyGoogleAnalytics;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Fragments.FragmentSearch.Model.IModel;
import com.sx95od.livemusic.Fragments.FragmentSearch.Model.Model;
import com.sx95od.livemusic.Fragments.FragmentSearch.View.IView;
import com.sx95od.livemusic.R;


import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by stoly on 04.09.2017.
 */

public class Presenter implements IPresenter {
    private final String tag = Presenter.class.getSimpleName();
    private int countLoaded = 0;
    private int countFailed = 0;
    private final int ALLCOUNT = 6;

    private IModel model = new Model();
    private IView iView;
    CompositeDisposable disposables = new CompositeDisposable();


    public Presenter(IView iView) {
        this.iView = iView;
    }

    private void loadCounter(){
        countLoaded++;
        if (countLoaded == 1){
            iView.animateShowResults();
        }
        final int gCount = countFailed + countLoaded;
        if (gCount == ALLCOUNT){
            iView.showLoading(false);
        }
    }

    private void failCounter(){
        countFailed++;
        final int gCount = countFailed + countLoaded;
        if (gCount == ALLCOUNT){
            iView.showLoading(false);
        }
    }

    private void updateSearchHistory(){
        disposables.add(model.getSearchHistory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<String>>() {
                    @Override
                    public void onNext(List<String> strings) {
                        iView.showSearchHistory(strings);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onSearchEnter() {
        countLoaded = 0;
        countFailed = 0;
        iView.showLoading(true);
        disposables.add(model.addQueryToDB(iView.getQuery())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<String>() {
            @Override
            public void onNext(String s) {
                updateSearchHistory();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(tag, e.toString());
            }

            @Override
            public void onComplete() {

            }
        }));
        disposables.add(model.getSearchByKeyWordYoutube(iView.getQuery(), 15, YouTubeApi.TYPE_VIDEO, YouTubeApi.CATEGORY_MUSIC,
                YouTubeApi.SNIPPET, BuildConfig.YOUTUBE_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<SearchByKeywordModel>() {
                    @Override
                    public void onNext(SearchByKeywordModel searchByKeywordModel) {
                        if (searchByKeywordModel.getItems().size()>0) {
                            iView.showYouTubeVideoModel(searchByKeywordModel);
                            loadCounter();
                        } else failCounter();
                    }

                    @Override
                    public void onError(Throwable e) {
                        iView.showError(e.toString());
                        failCounter();
                        Log.e(tag, "<FragmentSearch> -> <DisposableObserver.Next> "+e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
        //Searching lastfm tracks
            disposables.add(model.getLastFMTrackList(iView.getQuery(), "", BuildConfig.LASTFM_API_KEY,
                    25, 1, Constants.JSON)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<TrackSearchModel>() {
                        @Override
                        public void onNext(TrackSearchModel trackSearchModel) {
                            Log.d(tag, "<FragmentSearch> -> <DisposableObserver.Next> "+Thread.currentThread().getName());
                            if (trackSearchModel.getResults().getTrackmatches().getTrack().size()>0) {
                                iView.showLastFMTrackList(trackSearchModel);
                                loadCounter();
                            } else failCounter();
                        }

                        @Override
                        public void onError(Throwable e) {
                            iView.showError(e.toString());
                            failCounter();
                            Log.e(tag, "<FragmentSearch> -> <DisposableObserver.Next> "+e.toString());
                        }

                        @Override
                        public void onComplete() {

                        }
                    }));

        //Searching internal songs
        disposables.add(model.getInternalSongList(iView.getQuery())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<SongModel>>() {
                    @Override
                    public void onNext(List<SongModel> songObjects) {
                        Log.d(tag, "<FragmentSearch> -> <DisposableObserver.Next> "+Thread.currentThread().getName());
                        if (songObjects.size()>0){
                            iView.showInternalSongList(songObjects);
                            loadCounter();
                        } else failCounter();
                    }

                    @Override
                    public void onError(Throwable e) {
                        iView.showError(e.toString());
                        loadCounter();
                        Log.e(tag, "<FragmentSearch> -> <DisposableObserver.Error> "+e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
       disposables.add(model.getLastFMArtistList(iView.getQuery(), BuildConfig.LASTFM_API_KEY,
               25, 1, Constants.JSON)
       .subscribeOn(Schedulers.io())
       .observeOn(AndroidSchedulers.mainThread())
       .subscribeWith(new DisposableObserver<ArtistSearchModel>() {
           @Override
           public void onNext(ArtistSearchModel artistSearchModel) {
               Log.d(tag, "<FragmentSearch> -> <DisposableObserver.Next> "+artistSearchModel.getResults().getArtistmatches().getArtist().size());
               if (artistSearchModel.getResults().getArtistmatches().getArtist().size() > 0){
                   iView.showLastFMArtistList(artistSearchModel);
                   loadCounter();
               } else{
                   failCounter();
               }
           }

           @Override
           public void onError(Throwable e) {
                failCounter();
                Log.e(tag, "<FragmentSearch> -> <DisposableObserver.Error> "+e.toString());
           }

           @Override
           public void onComplete() {

           }
       }));
       disposables.add(model.getLastFMAlbumList(iView.getQuery(), BuildConfig.LASTFM_API_KEY,
               16, 1, Constants.JSON)
       .subscribeOn(Schedulers.io())
       .observeOn(AndroidSchedulers.mainThread())
       .subscribeWith(new DisposableObserver<AlbumSearchModel>() {
           @Override
           public void onNext(AlbumSearchModel albumSearchModel) {
               if (albumSearchModel.getResults().getAlbummatches().getAlbum().size()>0) {
                   iView.showLastFMAlbumList(albumSearchModel);
                   loadCounter();
               } else{
                   failCounter();
               }

           }

           @Override
           public void onError(Throwable e) {
                failCounter();
           }

           @Override
           public void onComplete() {

           }
       }));
       disposables.add(model.getInternalAlbumList(iView.getQuery())
       .subscribeOn(Schedulers.io())
       .observeOn(AndroidSchedulers.mainThread())
       .subscribeWith(new DisposableObserver<List<AlbumModel>>() {
           @Override
           public void onNext(List<AlbumModel> albumModels) {
                if (albumModels.size()>0){
                    iView.showInternalAlbumList(albumModels);
                    loadCounter();
                } else{
                    failCounter();
                }
           }

           @Override
           public void onError(Throwable e) {
                failCounter();
           }

           @Override
           public void onComplete() {

           }
       }));
    }

    @Override
    public void onSearchVideosOnYouTube(Track track) {
        disposables.add(model.getSearchByKeyWordYoutube(track.getName()+" "+track.getArtist(), 1, YouTubeApi.TYPE_VIDEO,
                YouTubeApi.CATEGORY_MUSIC, YouTubeApi.SNIPPET, BuildConfig.YOUTUBE_API_KEY)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<SearchByKeywordModel>() {
            @Override
            public void onNext(SearchByKeywordModel searchByKeywordModel) {
                if (searchByKeywordModel.getPageInfo().getTotalResults()>0) {
                    iView.openFirstYouTubeVideo(searchByKeywordModel.getItems().get(0), track);
                } else{
                    iView.youTubeShowError(Core.getCoreInstance()
                            .getString(R.string.nothing_found));
                }
            }

            @Override
            public void onError(Throwable e) {
                iView.youTubeShowError(Core.getCoreInstance()
                .getString(R.string.connection_failed));
            }

            @Override
            public void onComplete() {

            }
        }));
    }

    @Override
    public void onCreate() {
        updateSearchHistory();
        Core.getCoreInstance().getTracker().setScreenName(MyGoogleAnalytics.FRAGMENT_SEARCH);
        Core.getCoreInstance().getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }


    @Override
    public void onStop() {
//            if (!disposables.isDisposed()){
//                disposables.dispose();
//            }
    }

    @Override
    public void onDestroy() {
            if (!disposables.isDisposed()){
                disposables.dispose();
            }
            model = null;
            iView = null;
            disposables = null;
    }

}
