package com.sx95od.livemusic.Fragments.FragmentSongsView.Presenter;

/**
 * Created by stoly on 22.10.2017.
 */

public interface IPresenter {
    void getSongsByAlbumID(long id);
    void getSongs();
    void getFavSongs();
    void onDestroy();
}
