package com.sx95od.livemusic.Fragments.FragmentAlbumsView.Model;

import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by stoly on 08.09.2017.
 */

public interface IModel {
    Observable<List<AlbumModel>> getAlbumList();
}
