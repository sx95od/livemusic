package com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.Model;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.Core.Core;

import io.reactivex.Observable;

/**
 * Created by stoly on 23.09.2017.
 */

public class Model implements IModel {
    YouTubeApi youTubeApi = Core.getYoutubeApi();

    @Override
    public Observable<SearchByKeywordModel> loadVideos(String q, int maxResults, String type, String categoryID, String pageToken, String part, String apiKey) {
        if (pageToken == null){
             return youTubeApi.searchVideo(q, maxResults, type, categoryID, part, apiKey);
        } else{
            return youTubeApi.searchVideo(q, maxResults, type, categoryID, pageToken, part, apiKey);
        }
    }
}
