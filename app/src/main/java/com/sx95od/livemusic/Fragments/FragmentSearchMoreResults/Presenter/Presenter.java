package com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.Presenter;

import android.util.Log;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.Model.IModel;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.Model.Model;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.View.IView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by stoly on 23.09.2017.
 */

public class Presenter implements IPresenter {
    private final String tag = "FSearchMoreResults";
    private IModel model = new Model();
    private IView iView;
    CompositeDisposable disposables = new CompositeDisposable();

    public Presenter(IView iView) {
        this.iView = iView;
    }


    @Override
    public void loadYouTubeVideo(String query, String pageToken) {
        disposables.add(model.loadVideos(query, 25, YouTubeApi.TYPE_VIDEO,
                YouTubeApi.CATEGORY_MUSIC, pageToken, YouTubeApi.SNIPPET, BuildConfig.YOUTUBE_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<SearchByKeywordModel>() {
                    @Override
                    public void onNext(SearchByKeywordModel searchByKeywordModel) {
                        iView.loadVideos(searchByKeywordModel);
                        Log.d(tag, "OnNext");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(tag, e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void loadYouTubeVideo(String query) {
        disposables.add(model.loadVideos(query, 25, YouTubeApi.TYPE_VIDEO,
                YouTubeApi.CATEGORY_MUSIC, null, YouTubeApi.SNIPPET, BuildConfig.YOUTUBE_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<SearchByKeywordModel>() {
                    @Override
                    public void onNext(SearchByKeywordModel searchByKeywordModel) {
                        iView.loadVideos(searchByKeywordModel);
                        Log.d(tag, "OnNext");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(tag, e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }
}
