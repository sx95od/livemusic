package com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.Presenter;

/**
 * Created by stoly on 24.09.2017.
 */

public interface IPresenter {
    void onCreate();
    void onDestroy();

    void getChannelID(String id);
}
