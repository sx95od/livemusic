package com.sx95od.livemusic.Fragments.FragmentSearch.View;

import com.sx95od.livemusic.API.LastFM.Models.AlbumSearch.AlbumSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.ArtistSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.Track;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.TrackSearchModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.AdapterModels.ArtistModel;
import com.sx95od.livemusic.Adapters.AdapterModels.GenreModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.List;

/**
 * Created by stoly on 04.09.2017.
 */

public interface IView {
    void showLastFMTrackList(TrackSearchModel lastFMTrackModel);
    void showLastFMArtistList(ArtistSearchModel lastFMArtistModel);
    void showLastFMAlbumList(AlbumSearchModel lastFMAlbumModel);
    void showInternalSongList(List<SongModel> songList);
    void showInternalAlbumList(List<AlbumModel> albumList);
    void showInternalArtistList(List<ArtistModel> artistList);
    void showInternalGenreList(List<GenreModel> genreList);
    void showYouTubeVideoModel(SearchByKeywordModel youTubeVideoModel);

    void showSearchHistory(List<String> searchList);

    void animateShowResults();

    void openFirstYouTubeVideo(Item item, Track object);
    void youTubeShowError(String errorMessage);

    void showLoading(boolean show);
    String getQuery();
    void showError(String errorMessage);
}
