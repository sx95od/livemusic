package com.sx95od.livemusic.Fragments.FragmentSongsView.Presenter;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Fragments.FragmentSongsView.Model.IModel;
import com.sx95od.livemusic.Fragments.FragmentSongsView.Model.Model;
import com.sx95od.livemusic.Fragments.FragmentSongsView.View.IView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by stoly on 22.10.2017.
 */

public class Presenter implements IPresenter {
    private IModel model = new Model();
    private IView view;

    private CompositeDisposable disposable = new CompositeDisposable();


    public Presenter(IView view) {
        this.view = view;
    }

    @Override
    public void getSongsByAlbumID(long id) {
        disposable.add(model.getSongsByAlbumID(id)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<List<SongModel>>() {
            @Override
            public void onNext(List<SongModel> songs) {
                view.showSongs(songs);
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onComplete() {

            }
        }));
    }

    @Override
    public void getSongs() {
        disposable.add(model.getSongs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<SongModel>>() {
                    @Override
                    public void onNext(List<SongModel> songs) {
                        if (songs.size() > 0) view.showSongs(songs);
                        else view.showNothingFound();
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void getFavSongs() {
        disposable.add(model.getFavSongs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<SongModel>>() {
                    @Override
                    public void onNext(List<SongModel> songs) {
                        if (songs.size() > 0) view.showSongs(songs);
                        else view.showNothingFound();
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onDestroy() {
        if (!disposable.isDisposed())
            disposable.dispose();
    }
}
