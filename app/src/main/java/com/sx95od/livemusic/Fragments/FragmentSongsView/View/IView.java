package com.sx95od.livemusic.Fragments.FragmentSongsView.View;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.List;

/**
 * Created by stoly on 22.10.2017.
 */

public interface IView {
    void showSongs(List<SongModel> songs);
    void showNothingFound();
}
