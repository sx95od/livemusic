package com.sx95od.livemusic.Fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sx95od.livemusic.Adapters.AdapterModels.PlaylistModel;
import com.sx95od.livemusic.Adapters.Adapters.PlaylistNormalAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.BottomSheetDialogPlaylist;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.PlaylistSongs;
import com.sx95od.livemusic.DataBase.Playlists;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Utils.MD5;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 04.07.2017.
 */

public class FragmentPlaylistsView extends Fragment implements PlaylistNormalAdapter.OnItemMoreClickListener, View.OnClickListener{
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<PlaylistModel> objects;
    PlaylistNormalAdapter playlistNormalAdapter;
    Handler handler;
    ProgressBar loading;
    TextView actionBarTitle;
    LinearLayout back;


    @Subscribe
    public void fragmentEvent(FragmentEvent fragmentEvent){
        switch (fragmentEvent.getMessage()){
            case FragmentEvent.FRAGMENT_CREATED_PLAYLIST:

                break;
            case FragmentEvent.FRAGMENT_DELETED_PLAYLIST:

                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlists_view, container, false);
        EventBus.getDefault().register(this);

        handler = new Handler();
        actionBarTitle = (TextView) view.findViewById(R.id.action_bar_title);
        loading = (ProgressBar) view.findViewById(R.id.loading);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        objects = new ArrayList<PlaylistModel>();
        back = (LinearLayout) view.findViewById(R.id.back);
        back.setOnClickListener(this);

        actionBarTitle.setText(getArguments().getString("Title"));


        loadPlaylists();
        return view;
    }

    private void loadPlaylists(){
        objects.clear();
        Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Playlists> results = realm.where(Playlists.class)
                        .findAll();

                for (int i=0; i<results.size(); i++){
                    int id = results.get(i).getId();
                    String name = results.get(i).getName();
                    String about = results.get(i).getAbout();
                    byte[] cover = results.get(i).getCover();
                    long date = results.get(i).getDate();

                    RealmResults<PlaylistSongs> results2 = realm.where(PlaylistSongs.class)
                            .equalTo("playlist.id", results.get(i).getId())
                            .findAll();


                    objects.add(new PlaylistModel(id, name, cover,
                            date, results2.size(), about));

                    results2 = null;
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (getArguments().getInt("Code")==0){
                            playlistNormalAdapter = new PlaylistNormalAdapter(objects, getContext());
                            playlistNormalAdapter.setOnItemMoreClickListener(FragmentPlaylistsView.this);
                            recyclerView.setAdapter(playlistNormalAdapter);
                            doneLoading();
                        } else if (getArguments().getInt("Code")==PlaylistNormalAdapter.ADD_TO_PLAYLIST){
                            playlistNormalAdapter = new PlaylistNormalAdapter(objects, getContext(), PlaylistNormalAdapter.ADD_TO_PLAYLIST);
                            playlistNormalAdapter.setOnItemMoreClickListener(FragmentPlaylistsView.this);
                            recyclerView.setAdapter(playlistNormalAdapter);
                            doneLoading();
                        }

                    }
                });
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(FragmentPlaylistsView.class.getSimpleName(), error.toString());
            }
        });
    }

    private void doneLoading(){
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(loading, "alpha",
                0f);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(recyclerView, "alpha",
                1f);
        fadeOut.setDuration(350);
        fadeIn.setDuration(350);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (getView()!=null)
                    ViewCompat.setLayerType(getView(), ViewCompat.LAYER_TYPE_HARDWARE, null);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                loading.setVisibility(View.GONE);
                if (getView()!=null)
                    ViewCompat.setLayerType(getView(), ViewCompat.LAYER_TYPE_NONE, null);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.play(fadeOut).with(fadeIn);
        animatorSet.start();
    }

    @Override
    public void onItemOptionClickListener(final PlaylistModel playlistField) {
        if (getArguments().getInt("Code")==0) {
            BottomSheetDialogPlaylist bottomSheetDialogFragment = new BottomSheetDialogPlaylist();
            Bundle args = new Bundle();
            args.putSerializable("playlist", (Serializable) playlistField);
            bottomSheetDialogFragment.setArguments(args);
            bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        }else if (getArguments().getInt("Code")==PlaylistNormalAdapter.ADD_TO_PLAYLIST){
            final SongModel song = (SongModel) getArguments().getSerializable("Song");
            Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<Playlists> results = realm.where(Playlists.class)
                                .equalTo("id", playlistField.getId()).findAll();

                        final int idsong = getNextKeySongs();
                        PlaylistSongs playlistSongs = realm.createObject(PlaylistSongs.class, idsong);
                        playlistSongs.set_id(song.getId());
                        playlistSongs.setAlbum(song.getAlbum());
                        playlistSongs.setAlbum_id(song.getAlbum_id());
                        playlistSongs.setArtist(song.getArtist());
                        playlistSongs.setArtist_id(song.getArtist_id());
                        playlistSongs.setDate_modified(song.getDate());
                        playlistSongs.setDuration(song.getDuration());
                        playlistSongs.setTitle(song.getTitle());
                        playlistSongs.setUri(song.getUri());
                        playlistSongs.setYear(song.getYear());
                        playlistSongs.setMD5(MD5.checkMD5(song.getUri()));
                        playlistSongs.setPlaylist(results.get(0));
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(getContext(), getString(R.string.song_was_added_to_the_playlist), Toast.LENGTH_LONG).show();
                        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_BACK));
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {

                    }
                });
        }
    }

    public int getNextKeySongs()
    {
        Realm realm = null;
        int nextId = 0;
        try{
            realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
            Number maxId = realm.where(PlaylistSongs.class).max("id");
            // If there are no rows, currentId is null, so the next id must be 1
            // If currentId is not null, increment it by 1
            nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
        }catch (Exception e){
            realm.close();
            return nextId;
        } finally {
            realm.close();
            return nextId;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_BACK));
                break;
        }
    }
}
