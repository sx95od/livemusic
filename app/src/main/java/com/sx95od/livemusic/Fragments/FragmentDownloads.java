package com.sx95od.livemusic.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.Adapters.AdapterModels.ModelDownloads;
import com.sx95od.livemusic.Adapters.Adapters.AdapterDownloads;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.DownloadFormats;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 03.09.2017.
 */

public class FragmentDownloads extends Fragment {
    private final String tag = FragmentDownloads.class.getSimpleName();
    @BindView(R2.id.recyclerview_downloads) RecyclerView recyclerViewDownloads;
    @BindView(R2.id.status_bar) View statusBar;

    AdapterDownloads adapterDownloads;
    List<ModelDownloads> downloadObjects;
    RecyclerView.LayoutManager layoutManager;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        View view = inflater.inflate(R.layout.fragment_downloads_show, container, false);
        unbinder = ButterKnife.bind(this, view);
        statusBar.getLayoutParams().height = Core.getStatusBarHeight();
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewDownloads.setLayoutManager(layoutManager);

        loadDownloadList();
        return view;
    }

    @Subscribe
    public void fragmentEvent(FragmentEvent event){
        switch(event.getMessage()){
            case FragmentEvent.FRAGMENT_DOWNLOADS_REMOVE_ITEM:
                downloadObjects.remove(event.getVarInt());
                adapterDownloads.notifyItemRemoved(event.getVarInt());
                try {
                    adapterDownloads.notifyItemRangeChanged(event.getVarInt(), downloadObjects.size());
                }catch (Exception e){

                }
                break;
        }
    }

    private void loadDownloadList(){
        final Realm realm = Realm.getInstance(Core.getHideConfigRealm());
        downloadObjects = new ArrayList<ModelDownloads>();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<DownloadFormats> results = realm.where(DownloadFormats.class)
                        .findAll();
                for (int i=0; i<results.size(); i++){
                    downloadObjects.add(new ModelDownloads(results.get(i).getId(),
                            results.get(i).getFileName(), results.get(i).getUri(),
                            results.get(i).getTitle(), results.get(i).getArtist(),
                            results.get(i).getAlbum(), results.get(i).getFormat(),
                            results.get(i).getOutFormat()));
                }
                results = null;
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                adapterDownloads = new AdapterDownloads(downloadObjects);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerViewDownloads.setAdapter(adapterDownloads);
                    }
                });
                realm.close();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(tag, error.toString());
                realm.close();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        unbinder = null;
        EventBus.getDefault().unregister(this);
    }
}
