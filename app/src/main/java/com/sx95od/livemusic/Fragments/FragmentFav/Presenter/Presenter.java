package com.sx95od.livemusic.Fragments.FragmentFav.Presenter;

import android.util.ArrayMap;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.sx95od.livemusic.API.Google.Models.TokenErrorModel.TokenErrorModel;
import com.sx95od.livemusic.API.GoogleAuthApi;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.GeoTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.Track;
import com.sx95od.livemusic.API.LastFM.Models.TopTracks.TopTracksModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.ChartVideosModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel.MyChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.API.YouTubeRefreshTokenFunction;
import com.sx95od.livemusic.API.YouTubeTokenManager;
import com.sx95od.livemusic.Analytics.MyGoogleAnalytics;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Fragments.FragmentFav.Model.IModel;
import com.sx95od.livemusic.Fragments.FragmentFav.Model.Model;
import com.sx95od.livemusic.Fragments.FragmentFav.View.IView;
import com.sx95od.livemusic.R;

import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Nullable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.operators.flowable.FlowableFromCallable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.http.HTTP;

/**
 * Created by stoly on 01.10.2017.
 */

public class Presenter implements IPresenter {
    private final String tag = "FavPresenter";

    private IView view;
    private IModel model = new Model();

    private int geoTopTracksPage = 1;
    private boolean geoTopTracksDownloaded = true;

    private int topTracksPage = 1;
    private boolean topTracksDownloaded = true;

    private CompositeDisposable disposable = new CompositeDisposable();

    private String ytChartVideoToken;
    private boolean ytChartVideoLastPage;


    public Presenter(IView view){
        this.view = view;
    }


    @Override
    public void getLastFMGeoTopTracks() {
        if (geoTopTracksDownloaded) {
            geoTopTracksDownloaded = false;
            disposable.add(model.getLastFMGeoTopTracks(geoTopTracksPage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<GeoTopTracksModel>() {
                        @Override
                        public void onNext(GeoTopTracksModel geoTopTracksModel) {
                            Core.putObject(Core.OBJECT_LASTFM_GEO_TOP_TRACKS_PAGE, Integer.valueOf(geoTopTracksModel.getTracks().getAttr().getPage())+1);
                            geoTopTracksPage = Integer.valueOf(geoTopTracksModel.getTracks().getAttr().getPage())+1;
                            view.showGeoTopTracksLastFM(geoTopTracksModel.getTracks().getTrack());
                            geoTopTracksDownloaded = true;
                            Log.d(tag, "<GetGeoTopTracks> loading success" + "\n-----------\n" +
                                    " Page "+geoTopTracksModel.getTracks().getAttr().getPage()+
                                    " Total pages "+geoTopTracksModel.getTracks().getAttr().getTotalPages()+
                                    " Per page "+geoTopTracksModel.getTracks().getAttr().getPerPage()+
                                    " Total "+geoTopTracksModel.getTracks().getAttr().getTotal());
                        }

                        @Override
                        public void onError(Throwable e) {
                            geoTopTracksDownloaded = true;
                            Log.d(tag, "<GetGeoTopTracks> fail " + e.toString());
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    }));
        }
    }

    @Override
    public void getLastFMTopTracks() {
        if (topTracksDownloaded) {
            topTracksDownloaded = false;
            if (Core.getObject(Core.OBJECT_LASTFM_TOP_TRACKS_PAGE) != null)
                topTracksPage = (int) Core.getObject(Core.OBJECT_LASTFM_TOP_TRACKS_PAGE);
            disposable.add(model.getLastFMTopTracks(topTracksPage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<TopTracksModel>() {
                        @Override
                        public void onNext(TopTracksModel topTracksModel) {
                            Core.putObject(Core.OBJECT_LASTFM_TOP_TRACKS_PAGE, Integer.valueOf(topTracksModel.getTracks().getAttr().getPage())+1);
                            view.showTopTracksLastFM(topTracksModel.getTracks().getTrack());
                            topTracksDownloaded = true;
                            Log.d(tag, "<GetTopTracks> loading success" + "\n-----------\n" +
                                    " Page "+topTracksModel.getTracks().getAttr().getPage()+
                                    " Total pages "+topTracksModel.getTracks().getAttr().getTotalPages()+
                                    " Per page "+topTracksModel.getTracks().getAttr().getPerPage()+
                                    " Total "+topTracksModel.getTracks().getAttr().getTotal());
                        }

                        @Override
                        public void onError(Throwable e) {
                            topTracksDownloaded = true;
                            Log.d(tag, "<GetTopTracks> fail " + e.toString());
                        }

                        @Override
                        public void onComplete() {

                        }
                    }));
        }
    }

    @Override
    public void onSearchVideosOnYouTube(String query) {
        disposable.add(model.getSearchByKeyWordYoutube(query, 1, YouTubeApi.TYPE_VIDEO,
                YouTubeApi.CATEGORY_MUSIC, YouTubeApi.SNIPPET, BuildConfig.YOUTUBE_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<SearchByKeywordModel>() {
                    @Override
                    public void onNext(SearchByKeywordModel searchByKeywordModel) {
                        if (searchByKeywordModel.getPageInfo().getTotalResults()>0) {
                            view.openFirstYouTubeVideo(searchByKeywordModel.getItems().get(0));
                        } else{
                            view.youTubeShowError(Core.getCoreInstance()
                                    .getString(R.string.nothing_found));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.youTubeShowError(Core.getCoreInstance()
                                .getString(R.string.connection_failed));
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onDestroy() {
        if (!disposable.isDisposed()) {
            disposable.dispose();
            disposable.clear();
        }
        disposable = null;
        model = null;
        view = null;
    }

    @Override
    public void onCreate() {
        Core.getCoreInstance().getTracker().setScreenName(MyGoogleAnalytics.FRAGMENT_FAVORITE);
        Core.getCoreInstance().getTracker().send(new HitBuilders.ScreenViewBuilder().build());

        if (Core.getObject(Core.OBJECT_LASTFM_TOP_TRACKS_PAGE) != null)
            topTracksPage = (int) Core.getObject(Core.OBJECT_LASTFM_TOP_TRACKS_PAGE);
        else topTracksPage = 1;

        if (Core.getObject(Core.OBJECT_LASTFM_TOP_TRACKS_LIST) != null)
            view.showTopTracksLastFM((List<com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track>) Core.getObject(Core.OBJECT_LASTFM_TOP_TRACKS_LIST));
        else getLastFMTopTracks();


        if (Core.getObject(Core.OBJECT_LASTFM_GEO_TOP_TRACKS_PAGE) != null)
            geoTopTracksPage = (int) Core.getObject(Core.OBJECT_LASTFM_GEO_TOP_TRACKS_PAGE);
        else geoTopTracksPage = 1;

        if (Core.getObject(Core.OBJECT_LASTFM_GEO_TOP_TRACKS_LIST) != null)
            view.showGeoTopTracksLastFM((List<Track>) Core.getObject(Core.OBJECT_LASTFM_GEO_TOP_TRACKS_LIST));
        else getLastFMGeoTopTracks();
    }

    @Override
    public void loadMyYTData() {
        disposable.add(model.getMyChannel()
        .subscribeOn(Schedulers.newThread())
        .onErrorResumeNext(new Function<Throwable, ObservableSource<MyChannelModel>>() {
            @Override
            public ObservableSource<MyChannelModel> apply(Throwable throwable) throws Exception {
                Log.d(tag, "OnErrorResumeNext");
                YouTubeTokenManager.checkToken();
                return model.getMyChannel();
            }
        })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<MyChannelModel>() {
            @Override
            public void onNext(MyChannelModel myChannelModel) {
                Log.d(tag, "pobedka");
                if (myChannelModel != null) view.showMyYT(myChannelModel);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(tag, e.toString());
                try {
                    Log.e(tag, ((HttpException) e).response().errorBody().string());
                }catch (Exception e1){

                }

            }

            @Override
            public void onComplete() {

            }
        }));

//        disposable.add(model.getMyChannel()
//        .subscribeOn(Schedulers.io())
//        .observeOn(AndroidSchedulers.mainThread())
//        .subscribeWith(new DisposableObserver<MyChannelModel>() {
//            @Override
//            public void onNext(MyChannelModel myChannelModel) {
//                Core.YT_LAST_CODE = 200;
//                if (myChannelModel != null) view.showMyYT(myChannelModel);
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                Log.e("trash", "hey...");
//                Log.e(tag, e.toString());
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        }));
    }

    @Override
    public void loadYTChartVideos() {
        if (!ytChartVideoLastPage) disposable.add(model.getChartVideos(ytChartVideoToken)
                .retryWhen(new YouTubeRefreshTokenFunction())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ChartVideosModel>() {
                    @Override
                    public void onNext(ChartVideosModel chartVideosModel) {
                        Core.YT_LAST_CODE = 200;
                        view.showVideoChartYT(chartVideosModel);
                        ytChartVideoToken = chartVideosModel.getNextPageToken();
                        if (ytChartVideoToken == null) ytChartVideoLastPage = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(tag, e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void cacheLastFMTopTracks(List<com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track> tracks) {
        Core.putObject(Core.OBJECT_LASTFM_TOP_TRACKS_LIST, tracks);
    }

    @Override
    public void cacheLastFMGeoTopTracks(List<Track> tracks) {
        Core.putObject(Core.OBJECT_LASTFM_GEO_TOP_TRACKS_LIST, tracks);
    }
}
