package com.sx95od.livemusic.Fragments.FragmentHomeV2;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.sx95od.livemusic.Fragments.BaseFragment;
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.FragmentAlbumsView;
import com.sx95od.livemusic.Fragments.FragmentSongsView.FragmentSongsView;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.ThemeViews.LiveMusicTab.LiveMusicTab;
import com.sx95od.livemusic.Utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.sx95od.livemusic.Utils.Utils.getPxInDp;

/**
 * Created by stoly on 04.04.2018.
 */

public class FragmentHomeV2 extends BaseFragment {

    ArrayList<Fragment> fragments = new ArrayList<Fragment>();

    PagerAdapter pagerAdapter;

    @BindView(R2.id.view_pager)
    ViewPager _viewPager;

    @BindView(R2.id.tab_layout)
    RecyclerView _tabLayout;

    @BindView(R2.id.top_base_flex)
    FlexboxLayout _topBaseFlex;

//    @BindView(R2.id.ib_search)
//    ImageButton _searchBtn;

//    @BindView(R2.id.et_search)
//    EditText _searchInput;

    @BindView(R2.id.tv_top)
    TextView _topText;

    LiveMusicTab liveMusicTab;

    LinearLayoutManager layoutManager;

    AnimatorSet animatorSet;

    Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_v2, container, false);
        unbinder  = ButterKnife.bind(this, view);

        setUpTabLayout();

        return view;
    }

    private void setUpViewPager() {
        Bundle bundleSongs = new Bundle();
        Bundle bundleFavs = new Bundle();

        bundleSongs.putInt(FragmentSongsView.TYPES_FRAGMENT_SONGS_VIEW, FragmentSongsView.TYPE_ALL_SONGS);
        bundleFavs.putInt(FragmentSongsView.TYPES_FRAGMENT_SONGS_VIEW, FragmentSongsView.TYPE_FAV_SONGS);

        FragmentSongsView fragmentSongsView    = new FragmentSongsView();
        FragmentSongsView fragmentFavSongsView = new FragmentSongsView();

        fragmentSongsView.setArguments(bundleSongs);
        fragmentFavSongsView.setArguments(bundleFavs);

        fragments.add(fragmentSongsView);
        fragments.add(new FragmentAlbumsView());
        fragments.add(fragmentFavSongsView);

        pagerAdapter = new PagerAdapter(getChildFragmentManager());

        pagerAdapter.bindFragments(fragments);

        _viewPager.setAdapter(pagerAdapter);

        _viewPager.setOffscreenPageLimit(5);
    }


    private void setUpTabLayout(){
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        _tabLayout.setLayoutManager(layoutManager);

        ArrayList<String> tabs = new ArrayList<String>();

        tabs.add(getString(R.string.songs));
        tabs.add(getString(R.string.albums));
        tabs.add(getString(R.string.favourite_songs));

        liveMusicTab = new LiveMusicTab(tabs, _viewPager);
        liveMusicTab.setHasStableIds(true);

        _tabLayout.setHasFixedSize(true);
        _tabLayout.setItemAnimator(null);
        _tabLayout.setAdapter(liveMusicTab);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator objectAnimatorA  = ObjectAnimator.ofFloat(_topBaseFlex, View.ALPHA, -0.5f, 1f);
        ObjectAnimator objectAnimatorTX = ObjectAnimator.ofFloat(_topBaseFlex, View.TRANSLATION_X,
                -getPxInDp(getResources().getInteger(R.integer.top_base_flex_translation_x)), 0f);

        animatorSet.playTogether(objectAnimatorA, objectAnimatorTX);
        animatorSet.setDuration(getResources().getInteger(R.integer.top_base_flex_animation_duration));
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (getView() != null)
                    setUpViewPager();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animatorSet.start();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }


    private class PagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Fragment> fragments;

        public void bindFragments(ArrayList<Fragment> fragments) {
            this.fragments = fragments;
        }

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }
}
