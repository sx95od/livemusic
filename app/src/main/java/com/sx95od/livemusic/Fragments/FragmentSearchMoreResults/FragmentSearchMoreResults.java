package com.sx95od.livemusic.Fragments.FragmentSearchMoreResults;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.Acts.MyActivity;
import com.sx95od.livemusic.Adapters.Adapters.YouTubeVideoAdapter;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.Presenter.Presenter;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.View.IView;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.FragmentYouTubeShowVideo;
import com.sx95od.livemusic.Fragments.MyFragment;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by stoly on 23.09.2017.
 */

public class FragmentSearchMoreResults extends MyFragment implements IView,
        Listeners.OnYouTubeVideoItemClickListener{
    private final String tag = "FSearchMoreResults";

    public final static String TYPE_RESULTS = "type_results";
    public final static String QUERY = "type_query";

    public final static int YOUTUBE_VIDEO = 1;
    public final static int YOUTUBE_CHANNEL = 2;
    public final static int YOUTUBE_PLAYLIST = 3;

    Unbinder unbinder;
    @BindView(R2.id.recyclerview) RecyclerView recyclerView;

    List<Item> youTubeVideoList;

    YouTubeVideoAdapter youTubeVideoAdapter;
    LinearLayoutManager layoutManager;

    String pageToken;

    boolean loadedNewPage = false;

    Presenter presenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_more_results, container, false);
        unbinder = ButterKnife.bind(this, view);

        presenter = new Presenter(this);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (layoutManager.findLastVisibleItemPosition()>youTubeVideoAdapter.getItemCount()-10 && loadedNewPage) {
                    presenter.loadYouTubeVideo(getArguments().getString(QUERY), pageToken);
                    loadedNewPage = false;
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        switch (getArguments().getInt(TYPE_RESULTS)){
            case YOUTUBE_VIDEO:
                youTubeVideoAdapter = new YouTubeVideoAdapter(this);
                presenter.loadYouTubeVideo(getArguments().getString(QUERY));
                break;
            case YOUTUBE_CHANNEL:

                break;
            case YOUTUBE_PLAYLIST:

                break;
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        unbinder = null;
        super.onDestroyView();
    }




    @Override
    public void loadVideos(SearchByKeywordModel response) {
        Log.d("testReceive", "ok");
        pageToken = response.getNextPageToken();
        this.youTubeVideoList = response.getItems();
        if (recyclerView.getAdapter() == null){
            youTubeVideoAdapter.addObjects(this.youTubeVideoList);
            recyclerView.setAdapter(youTubeVideoAdapter);
        } else {
            int pos = youTubeVideoAdapter.getItemCount()-1;
            youTubeVideoAdapter.addObjects(this.youTubeVideoList);
            youTubeVideoAdapter.notifyItemRangeChanged(pos, youTubeVideoAdapter.getItemCount());
        }
        loadedNewPage = true;
    }

    @Override
    public void onVideoClickListener(Item item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("youtube_item", item);
        bundle.putString("filename", item.getSnippet().getTitle());
        FragmentYouTubeShowVideo youTubeShowVideo = new FragmentYouTubeShowVideo();
        youTubeShowVideo.setArguments(bundle);

        ((MyActivity) getActivity()).addFragment(youTubeShowVideo);

//        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_YOUTUBE_SHOW_VIDEO, bundle));
//        addFragment(youTubeShowVideo);
    }
}
