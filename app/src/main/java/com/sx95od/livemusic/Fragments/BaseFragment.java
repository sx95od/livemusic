package com.sx95od.livemusic.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx95od.livemusic.Acts.MyActivity;

/**
 * Created by stoly on 02.04.2018.
 */

public class BaseFragment extends Fragment{

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MyActivity) getActivity()).requestBottomPadding();
    }


}
