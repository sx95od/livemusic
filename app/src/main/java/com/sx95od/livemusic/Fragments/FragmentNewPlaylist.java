package com.sx95od.livemusic.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.PlaylistSongs;
import com.sx95od.livemusic.DataBase.Playlists;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Utils.MD5;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 12.06.2017.
 */

public class FragmentNewPlaylist extends android.support.v4.app.Fragment implements View.OnClickListener{
    SongAdapter tracksAdapter;
    RecyclerView rvTracks;
    LinearLayoutManager layoutManager;
    AppCompatEditText name, about;
    int found = 0;
    int id = 0;
    ArrayList<SongModel> tracksArray, selectedArray;
    ProgressDialog progressdialog;

    LinearLayout back, save;


    @Override
    public void onDestroyView() {
        selectedArray = null;
        tracksArray = null;
        tracksAdapter = null;
        rvTracks = null;
        back = null;
        save = null;
        layoutManager = null;
        name = null;
        about = null;
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Subscribe
    public void fragmentEvent(FragmentEvent fragmentEvent){
        switch (fragmentEvent.getMessage()){
            case FragmentEvent.PLAYLIST_ADD_ITEM:
                selectedArray.add(fragmentEvent.getSong());
                break;
            case FragmentEvent.PLAYLIST_REMOVE_ITEM:
                selectedArray.remove(fragmentEvent.getSong());
                break;

        }
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        View view = inflater.inflate(R.layout.fragment_new_playlist, container, false);
        selectedArray = new ArrayList<SongModel>();
        Log.i(getClass().getSimpleName(), getClass().getSimpleName().toString()+" created");
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvTracks = (RecyclerView) view.findViewById(R.id.songs);
        rvTracks.setSaveEnabled(false);
        rvTracks.setLayoutManager(layoutManager);
        back = (LinearLayout) view.findViewById(R.id.back);
        save = (LinearLayout) view.findViewById(R.id.save);
        name = (AppCompatEditText) view.findViewById(R.id.name);
        about = (AppCompatEditText) view.findViewById(R.id.about);
        back.setOnClickListener(this);
        save.setOnClickListener(this);


//        rvTracks.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rvTracks, new RecyclerItemClickListener.RecyclerViewClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                            if (tracksAdapter.select(position)){
//                                selectedArray.add(tracksArray.get(position));
//                            } else{
//                                selectedArray.remove(tracksArray.get(position));
//                            }
//                tracksAdapter.notifyItemChanged(position);
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));


        loadTracks();

        return view;
    }



    private void loadTracks() {
        Log.i("FragmentSongsView", "loadTracks");
        try {
            tracksArray = new ArrayList<SongModel>();
            final ArrayList<SongModel> arrayList = new ArrayList<SongModel>(((Core) getActivity().getApplicationContext()).getSongs());
            for (int i=0; i<arrayList.size(); i++){
                tracksArray.add(new SongModel(arrayList.get(i).getId(), arrayList.get(i).getUri(),
                        arrayList.get(i).getArt(), arrayList.get(i).getTitle(), arrayList.get(i).getArtist(),
                        arrayList.get(i).getAlbum(), arrayList.get(i).getAlbum_id(), arrayList.get(i).getArtist_id(),
                        arrayList.get(i).getDuration(), arrayList.get(i).getYear(), arrayList.get(i).getDate(),
                        arrayList.get(i).getSelected()));
            }
            tracksAdapter = new SongAdapter(tracksArray,  SongAdapter.FLAG_ADDING_TO_PLAYLIST, null);
            tracksAdapter.setHasStableIds(true);
//            rvTracks.setDrawingCacheEnabled(true);
//            rvTracks.setItemViewCacheSize(30);
            rvTracks.setAdapter(tracksAdapter);
            if (tracksArray.size()>0){
//                nothingFound.setVisibility(View.GONE);
            }
        }catch (Exception e){
            Log.i("Error", e.toString());
        } finally {
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.save:
                if(name.getText().toString().trim().length()>0) {
                    checkName(name.getText().toString(), selectedArray);
                }
                break;
            case R.id.back:
                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_BACK));
                break;
        }
    }

    public void addPlaylist(final Playlists playlist, final ArrayList<SongModel> arrayList){
        try{
            progressdialog = new ProgressDialog(getActivity());
            progressdialog.setIndeterminate(false);
            progressdialog.setCancelable(false);
            progressdialog.setTitle(R.string.creating_playlist);
            progressdialog.show();
            final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                        for (int i=0; i<arrayList.size(); i++){
                            final int progress = i;
                            final int idsong = getNextKeySongs();
                            PlaylistSongs playlistSongs = realm.createObject(PlaylistSongs.class, idsong);
                            playlistSongs.set_id(arrayList.get(i).getId());
                            playlistSongs.setAlbum(arrayList.get(i).getAlbum());
                            playlistSongs.setAlbum_id(arrayList.get(i).getAlbum_id());
                            playlistSongs.setArtist(arrayList.get(i).getArtist());
                            playlistSongs.setArtist_id(arrayList.get(i).getArtist_id());
                            playlistSongs.setDate_modified(arrayList.get(i).getDate());
                            playlistSongs.setDuration(arrayList.get(i).getDuration());
                            playlistSongs.setTitle(arrayList.get(i).getTitle());
                            playlistSongs.setUri(arrayList.get(i).getUri());
                            playlistSongs.setYear(arrayList.get(i).getYear());
                            playlistSongs.setMD5(MD5.checkMD5(arrayList.get(i).getUri()));
                            playlistSongs.setPlaylist(playlist);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressdialog.setMessage(progress+"/"+arrayList.size());
                                }
                            });

                        }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    realm.close();
                    name.post(new Runnable() {
                        @Override
                        public void run() {
                            name.setText(null);
                        }
                    });
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressdialog.dismiss();
                        }
                    });
                    EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_BACK));
                    EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_CREATED_PLAYLIST));
                    Toast.makeText(getActivity(), getString(R.string.playlist_created), Toast.LENGTH_LONG).show();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    Log.i("RealmAddPlaylist", error.toString());
                    realm.close();
                }
            });
        }catch (Exception e){
            Log.i("Realm", e.toString());
        }
    }

    public void checkName(final String name, final ArrayList<SongModel> arrayList){
        try{
            progressdialog = new ProgressDialog(getActivity());
            progressdialog.setIndeterminate(false);
            progressdialog.setCancelable(false);
            progressdialog.setTitle(R.string.creating_playlist);
            progressdialog.show();
            final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Playlists> result = realm.where(Playlists.class)
                            .equalTo("name", name)
                            .findAll();
                    if (result.size()>0) found = 1;
                    else {
                        found = 0;
                        id = getNextKey();
                        Playlists playlist = realm.createObject(Playlists.class, id);
                        playlist.setCover(null);
                        playlist.setAbout(about.getText().toString());
                        playlist.setDate(System.currentTimeMillis());
                        playlist.setName(name);

                        for (int i=0; i<arrayList.size(); i++) {
                            final int progress = i;
                            final int idsong = getNextKeySongs();
                            PlaylistSongs playlistSongs = realm.createObject(PlaylistSongs.class, idsong);
                            playlistSongs.set_id(arrayList.get(i).getId());
                            playlistSongs.setAlbum(arrayList.get(i).getAlbum());
                            playlistSongs.setAlbum_id(arrayList.get(i).getAlbum_id());
                            playlistSongs.setArtist(arrayList.get(i).getArtist());
                            playlistSongs.setArtist_id(arrayList.get(i).getArtist_id());
                            playlistSongs.setDate_modified(arrayList.get(i).getDate());
                            playlistSongs.setDuration(arrayList.get(i).getDuration());
                            playlistSongs.setTitle(arrayList.get(i).getTitle());
                            playlistSongs.setUri(arrayList.get(i).getUri());
                            playlistSongs.setYear(arrayList.get(i).getYear());
                            playlistSongs.setMD5(MD5.checkMD5(arrayList.get(i).getUri()));
                            playlistSongs.setPlaylist(playlist);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressdialog.setMessage(progress + "/" + arrayList.size());
                                }
                            });
                        }
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    realm.close();
                    FragmentNewPlaylist.this.name.post(new Runnable() {
                        @Override
                        public void run() {
                            FragmentNewPlaylist.this.name.setText(null);
                        }
                    });
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressdialog.dismiss();
                        }
                    });
                    EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_BACK));
                    EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_CREATED_PLAYLIST));
                    Toast.makeText(getActivity(), getString(R.string.playlist_created), Toast.LENGTH_LONG).show();
//                    if(found==0) {
//                        addPlaylist(playlist, arrayList);
//                    } else{
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Toast.makeText(getActivity(), getString(R.string.playlist_already_exists), Toast.LENGTH_LONG).show();
//                            }
//                        });
//                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    Log.i("RealmCheckName", error.toString());
                    realm.close();
                }
            });
        }catch (Exception e){
            Log.i("Realm", e.toString());
        }
    }

    public int getNextKey()
    {
        Realm realm = null;
        int nextId = 0;
        try{
            realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
            Number maxId = realm.where(Playlists.class).max("id");
            // If there are no rows, currentId is null, so the next id must be 1
            // If currentId is not null, increment it by 1
            nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
        }catch (Exception e){
            realm.close();
            return nextId;
        } finally {
            realm.close();
            return nextId;
        }
    }

    public int getNextKeySongs()
    {
        Realm realm = null;
        int nextId = 0;
        try{
            realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
            Number maxId = realm.where(PlaylistSongs.class).max("id");
            // If there are no rows, currentId is null, so the next id must be 1
            // If currentId is not null, increment it by 1
            nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
        }catch (Exception e){
            realm.close();
            return nextId;
        } finally {
            realm.close();
            return nextId;
        }
    }
}
