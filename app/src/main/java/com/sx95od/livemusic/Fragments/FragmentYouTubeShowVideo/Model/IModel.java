package com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.Model;

import com.sx95od.livemusic.API.YouTubeApi.Models.Channel.ChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.Video.YouTubeVideoModel;

import io.reactivex.Observable;

/**
 * Created by stoly on 24.09.2017.
 */

public interface IModel {
    Observable<YouTubeVideoModel> getVideoInfo(String id, String part, String apiKey);
    Observable<ChannelModel> getChannelInfo(String apiKey, String id, String part);
}
