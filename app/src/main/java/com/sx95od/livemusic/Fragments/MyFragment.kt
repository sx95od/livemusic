package com.sx95od.livemusic.Fragments

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator

import com.sx95od.livemusic.Acts.MyActivity
import com.sx95od.livemusic.AnimationConstants
import com.sx95od.livemusic.Core.Core
import com.sx95od.livemusic.R
import com.sx95od.livemusic.ThemeViews.STextView
import kotlinx.android.synthetic.main.action_bar_regular.*

/**
 * Created by stoly on 15.10.2017.
 */

open class MyFragment : Fragment() {
    private val animatorSet = AnimatorSet()
    private var alreadyDestroy = false

    fun onFragmentResume() {
        view!!.visibility = View.VISIBLE
    }


    fun setActionBarTitle(title : String?){
        view?.findViewById<STextView>(R.id.tv_title)?.text = title
    }

    fun setTransparentActionBar(){
        action_bar!!.setBackgroundResource(R.color.transparent)
//        action_bar!!.elevation = 0f
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MyActivity).requestBottomPadding()

        val animatorSet = AnimatorSet()
        animatorSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {

            }

            override fun onAnimationEnd(animator: Animator) {
                onFragmentCreated()
            }

            override fun onAnimationCancel(animator: Animator) {

            }

            override fun onAnimationRepeat(animator: Animator) {

            }
        })
        val objectAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f)
        val objectAnimatorX = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, Core.getWidthScreen() / 3f, 0f)
        animatorSet.duration = AnimationConstants.xFragTransDur()
        animatorSet.interpolator = DecelerateInterpolator()
        animatorSet.playTogether(objectAnimator, objectAnimatorX)
        animatorSet.start()

    }

    open fun onFragmentCreated() {
        if (activity != null) (activity as MyActivity).onNewFragment()
    }

    open fun backStack() {
        if (animatorSet.isStarted) {
            animatorSet.cancel()
            (activity as MyActivity).fragmentBackStack()
        } else {
            alreadyDestroy = true
            val objectAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 1f, 0f)
            val objectAnimatorX = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, 0f, Core.getWidthScreen() / 3f)
            animatorSet.duration = AnimationConstants.xFragTransDur()
            animatorSet.interpolator = LinearInterpolator()
            animatorSet.playTogether(objectAnimator, objectAnimatorX)
            animatorSet.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {
                    (activity as MyActivity).onRemoveFragment()
                }

                override fun onAnimationEnd(animator: Animator) {
                    try {
                        (activity as MyActivity).fragmentBackStack()
                    } catch (e: Exception) {

                    }

                }

                override fun onAnimationCancel(animator: Animator) {
                    (activity as MyActivity).onRemoveFragment()
                }

                override fun onAnimationRepeat(animator: Animator) {

                }
            })
            animatorSet.start()
        }
    }

    fun resetFragment() {

    }

    fun addFragment(myFragment: MyFragment) {
        //        getFragmentManager().beginTransaction().add(R.id.fragments_container, myFragment)
        //                .addToBackStack(null).commit();
        (activity as MyActivity).addFragment(myFragment)
    }

    fun getMyActivity(): MyActivity {
        return activity as MyActivity
    }


}
