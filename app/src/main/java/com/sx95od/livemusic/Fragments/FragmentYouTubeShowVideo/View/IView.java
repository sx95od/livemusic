package com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.View;

import com.sx95od.livemusic.API.YouTubeApi.Models.Channel.ChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.Video.YouTubeVideoModel;

/**
 * Created by stoly on 24.09.2017.
 */

public interface IView {
    void showVideoInfo(YouTubeVideoModel video);
    void showChannelInfo(ChannelModel channel);
    String getVideoID();
}
