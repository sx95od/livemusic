package com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.Model;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;

import io.reactivex.Observable;

/**
 * Created by stoly on 23.09.2017.
 */

public interface IModel {
    Observable<SearchByKeywordModel> loadVideos(String q, int maxResults, String type, String categoryID,
                                                String pageToken, String part, String apiKey);
}
