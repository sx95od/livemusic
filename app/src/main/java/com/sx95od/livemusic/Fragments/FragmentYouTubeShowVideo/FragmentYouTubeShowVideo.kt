package com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.sx95od.livemusic.API.YouTubeApi.Models.Channel.ChannelModel
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item
import com.sx95od.livemusic.API.YouTubeApi.Models.Video.YouTubeVideoModel
import com.sx95od.livemusic.Adapters.Adapters.YouTubeVideoViewContentAdapter
import com.sx95od.livemusic.BuildConfig
import com.sx95od.livemusic.Core.Core
import com.sx95od.livemusic.Fragments.FragmentDownload.FragmentDownload
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.Presenter.Presenter
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.View.IView
import com.sx95od.livemusic.Fragments.MyFragment
import com.sx95od.livemusic.Listeners.Listeners
import com.sx95od.livemusic.R
import java.lang.ref.WeakReference
import kotlinx.android.synthetic.main.fragment_youtube_show_video.*

/**
 * Created by stoly on 31.08.2017.
 */

class FragmentYouTubeShowVideo : MyFragment(), IView, Listeners.OnYouTubeVideoTopContentClick {
    internal val tag = "<F-YouTubeShowVideo>"

    private var youTubeVideoModel: YouTubeVideoModel? = null
    private var youTubeChannelModel: ChannelModel? = null
    private var youTubeVideoViewContentAdapter: YouTubeVideoViewContentAdapter? = null

    internal var layoutManager: LinearLayoutManager? = null

    private var youtubeFragment: YouTubePlayerSupportFragment? = null
    internal var url: String? = null
    internal var mainYouTubePlayer: YouTubePlayer? = null
    internal var youTubePlayerWeakReference: WeakReference<YouTubePlayer>? = null

    internal var presenter: Presenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_youtube_show_video, container, false)
        return view
    }


    override fun onFragmentCreated() {
        super.onFragmentCreated()
        presenter = Presenter(this)

        flexbox_loading_container.visibility = View.VISIBLE

        youTubeVideoViewContentAdapter = YouTubeVideoViewContentAdapter(this)

        layoutManager = LinearLayoutManager(Core.getCoreInstance(), LinearLayoutManager.VERTICAL, false)
        recyclerview_content!!.layoutManager = layoutManager
        recyclerview_content!!.adapter = youTubeVideoViewContentAdapter

        root_youtube_fragment!!.layoutParams.height = Core.getWidthScreen() / 16 * 9
        image_overlay!!.layoutParams.height = Core.getWidthScreen() / 16 * 9
        var frameLayout: FrameLayout? = view!!.findViewById<View>(R.id.YoutubeFragment) as FrameLayout
        frameLayout!!.layoutParams.height = Core.getWidthScreen() / 16 * 9
        frameLayout = null

        youtubeFragment = YouTubePlayerSupportFragment()

        fragmentManager!!.beginTransaction().replace(R.id.YoutubeFragment, youtubeFragment).commit()


        youtubeFragment!!.initialize(BuildConfig.YOUTUBE_API_KEY, object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(provider: YouTubePlayer.Provider, youTubePlayer: YouTubePlayer, b: Boolean) {
                youTubePlayerWeakReference = WeakReference(youTubePlayer)
                mainYouTubePlayer = youTubePlayerWeakReference?.get()
                if (arguments!!.getSerializable("youtube_item") is Item) {
                    mainYouTubePlayer!!.loadVideo((arguments!!.getSerializable("youtube_item") as Item).id.videoId)
                } else
                    mainYouTubePlayer!!.loadVideo((arguments!!.getSerializable("youtube_item") as com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.Item).id)
                mainYouTubePlayer!!.setShowFullscreenButton(false)
//                mainYouTubePlayer!!.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS)
            }

            override fun onInitializationFailure(provider: YouTubePlayer.Provider, youTubeInitializationResult: YouTubeInitializationResult) {

            }
        })

        presenter?.onCreate()
    }

    override fun onDestroyView() {
        presenter?.onDestroy()
        url = null
        mainYouTubePlayer!!.release()
        youtubeFragment = null
        mainYouTubePlayer = null
        super.onDestroyView()
    }


    override fun showVideoInfo(video: YouTubeVideoModel) {
        youTubeVideoModel = video
        presenter?.getChannelID(video.items[0].snippet.channelId)
    }


    override fun showChannelInfo(channel: ChannelModel) {
        flexbox_loading_container.visibility = View.GONE
        youTubeChannelModel = channel
        youTubeVideoViewContentAdapter!!.addTopContent(youTubeVideoModel, youTubeChannelModel)

        Core.withGlide().load(channel.items.get(0).snippet.thumbnails.high.url).override(128)
                .circleCrop()
                .into(iv_channel_cover)

        tv_channel_name.text = channel.items.get(0).snippet.title
    }


    override fun getVideoID(): String {
        return if (arguments!!.getSerializable("youtube_item") is Item) {
            (arguments!!.getSerializable("youtube_item") as Item).id.videoId
        } else
            (arguments!!.getSerializable("youtube_item") as com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.Item).id
    }


    override fun likeClick() {

    }


    override fun dislikeClick() {

    }


    override fun viewsClick() {

    }


    override fun channelClick() {

    }


    override fun moreClick() {

    }


    override fun videoClick() {

    }

    override fun downloadClick(id: String) {
        val bundle = Bundle()
        bundle.putString("youtube_video_id", id)
        val fragmentDownload = FragmentDownload()
        fragmentDownload.arguments = bundle
        addFragment(fragmentDownload)
    }


}
