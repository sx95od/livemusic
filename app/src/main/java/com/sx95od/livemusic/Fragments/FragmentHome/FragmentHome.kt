package com.sx95od.livemusic.Fragments.FragmentHome

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sx95od.livemusic.Acts.MyActivity
import com.sx95od.livemusic.Core.Core
import com.sx95od.livemusic.Events.FragmentEvent
import com.sx95od.livemusic.Fragments.BaseFragment
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.FragmentAlbumsView
import com.sx95od.livemusic.Fragments.FragmentSongsView.FragmentSongsView
import com.sx95od.livemusic.R
import com.sx95od.livemusic.R2
import org.greenrobot.eventbus.EventBus
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.sx95od.livemusic.AnimationConstants
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * Created by stoly on 25.05.2017.
 */

class FragmentHome : BaseFragment() {
    internal var unbinder : Unbinder? = null
    internal val tag = "F-Home"


    override fun onDestroyView() {
        adView!!.destroy()
        unbinder!!.unbind()
        super.onDestroyView()
    }


    private fun applyTheme() {
        if ((activity!!.application as Core).darkTheme == R.style.AppThemeDark) {
            bottom_view!!.setBackgroundResource(R.color.dark)
        } else {

        }
    }


    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        Log.d(javaClass.simpleName, menuVisible.toString() + " visibility")
        EventBus.getDefault().post(FragmentEvent(FragmentEvent.FRAGMENT_FORCE_LIGHT_STATUS_BAR, !menuVisible))
    }


    private fun tuneUI() {
        if (Core.getCoreInstance().songs.size > 0) {
            bottom_view!!.layoutParams.height = resources.getDimensionPixelSize(R.dimen.dp56) * 2
        } else {
            bottom_view!!.layoutParams.height = resources.getDimensionPixelSize(R.dimen.dp56)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    @OnClick(R2.id.textview_songs,
            R2.id.textview_albums,
            R2.id.textview_favs) internal fun onClick(view: View?) {
        var fragment : Fragment? = null
        val bundle = Bundle()
        when (view!!.id) {
            R.id.textview_songs -> {
                fragment = FragmentSongsView()
                bundle.putInt(FragmentSongsView.TYPES_FRAGMENT_SONGS_VIEW, FragmentSongsView.TYPE_ALL_SONGS)
                fragment.arguments = bundle
                (activity as MyActivity).addFragment(fragment) }

            R.id.textview_albums ->
                (activity as MyActivity).addFragment(FragmentAlbumsView())

            R.id.textview_favs -> {
                fragment = FragmentSongsView()
                bundle.putInt(FragmentSongsView.TYPES_FRAGMENT_SONGS_VIEW, FragmentSongsView.TYPE_FAV_SONGS)
                fragment.arguments = bundle
                (activity as MyActivity).addFragment(fragment)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        animate()
        unbinder = ButterKnife.bind(this, view)
        tuneUI()
        //        AdView mAdView = new AdView(getActivity());
        //        AdRequest adRequest = new AdRequest.Builder().build();
        //        mAdView.setAdSize(AdSize.BANNER);
        //        if (BuildConfig.DEBUG) mAdView.setAdUnitId("ca-app-pub-3940256099942544/6300978111"); else
        //            mAdView.setAdUnitId(BuildConfig.ADMOB_TEST_BANNER_1);
        //        _adFrame.addView(mAdView);
        //
        //        mAdView.loadAd(adRequest);

        //        if (Core.getCoreInstance().getShowingAd()) {
        //            MobileAds.initialize(getContext(), BuildConfig.ADMOB_ID);
        //            AdRequest adRequest = new AdRequest.Builder().build();
        //            mAdView.loadAd(adRequest);
        //        }

        applyTheme()

        super.onViewCreated(view, savedInstanceState)
    }

    private fun animate(){
        textview_songs.alpha = 0f
        textview_albums.alpha = 0f
        textview_favs.alpha = 0f

        val objectAnimatorTVTopTrans = ObjectAnimator.ofFloat(tv_top, View.TRANSLATION_X, -AnimationConstants.xTrans(), 0f)
        val objectAnimatorTVTopAlpha = ObjectAnimator.ofFloat(tv_top, View.ALPHA, 0f, 1f)

        val objectAnimatorTVSongsTrans = ObjectAnimator.ofFloat(textview_songs, View.TRANSLATION_X, -AnimationConstants.xTrans(), 0f)
        val objectAnimatorTVSongsAlpha = ObjectAnimator.ofFloat(textview_songs, View.ALPHA, 0f, 1f)
        objectAnimatorTVSongsTrans.startDelay = 128
        objectAnimatorTVSongsAlpha.startDelay = 128

        val objectAnimatorTVAlbumsTrans = ObjectAnimator.ofFloat(textview_albums, View.TRANSLATION_X, -AnimationConstants.xTrans(), 0f)
        val objectAnimatorTVAlbumsAlpha = ObjectAnimator.ofFloat(textview_albums, View.ALPHA, 0f, 1f)
        objectAnimatorTVAlbumsTrans.startDelay = 256
        objectAnimatorTVAlbumsAlpha.startDelay = 256

        val objectAnimatorTVFavsTrans = ObjectAnimator.ofFloat(textview_favs, View.TRANSLATION_X, -AnimationConstants.xTrans(), 0f)
        val objectAnimatorTVFavsAlpha = ObjectAnimator.ofFloat(textview_favs, View.ALPHA, 0f, 1f)
        objectAnimatorTVFavsTrans.startDelay = 384
        objectAnimatorTVFavsAlpha.startDelay = 384


        val animatorSet = AnimatorSet()

        animatorSet.playTogether(objectAnimatorTVTopAlpha, objectAnimatorTVTopTrans,
                objectAnimatorTVSongsTrans, objectAnimatorTVSongsAlpha, objectAnimatorTVAlbumsAlpha,
                objectAnimatorTVAlbumsTrans, objectAnimatorTVFavsTrans, objectAnimatorTVFavsAlpha)

        animatorSet.duration = AnimationConstants.xTransDur()
        animatorSet.start()
    }

}
