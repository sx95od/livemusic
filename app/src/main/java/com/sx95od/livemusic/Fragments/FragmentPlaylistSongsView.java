package com.sx95od.livemusic.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Dialogs.DialogSongMore.BottomSheetDialogSongFragment;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.PlaylistSongs;
import com.sx95od.livemusic.DataBase.Playlists;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by stoly on 14.07.2017.
 */

public class FragmentPlaylistSongsView extends Fragment implements SongAdapter.OnItemMoreClickListener, View.OnClickListener{
    @BindView(R2.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R2.id.app_bar_layout) AppBarLayout appBarLayout;
    @BindView(R2.id.cover_card) CardView coverCard;
    @BindView(R2.id.back_cover) ImageView backCover;
    @BindView(R2.id.cover) ImageView cover;
    @BindView(R2.id.title) TextView title;
    @BindView(R2.id.im) TextView im;
    Unbinder unbinder;



    int appBarOffset = 0;
//    TextView logo, title;
    SongAdapter songAdapter;
    ArrayList<SongModel> songsArray;
    RecyclerView rvSongs;
    RecyclerView.LayoutManager layoutManager;
    Thread thread;

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlist_songs_view, null);
        unbinder = ButterKnife.bind(this, view);
        songsArray = new ArrayList<SongModel>();




//        coverCard.setCardBackgroundColor(ContextCompat.getColor(getActivity(), ((Core) getActivity().getApplication()).getColor()));

        rvSongs = (RecyclerView) view.findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvSongs.setLayoutManager(layoutManager);
        rvSongs.setSaveEnabled(false);

//        title = (TextView) view.findViewById(R.id.title);

        getPlaylist(getArguments().getInt("PlaylistID"));
        getSongs(getArguments().getInt("PlaylistID"));






        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(final AppBarLayout appBarLayout, final int verticalOffset) {
                thread = null;
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final long c = System.currentTimeMillis();
                        double offsetMax = appBarLayout.getTotalScrollRange();
                        double percentOffset = offsetMax/100; //get offset global in 1 percent
                        final double factorPercent =  Math.abs(verticalOffset / percentOffset); //get percents(100)
                        final int intPercent = (int) factorPercent;
                        final float floatPercent = (float) intPercent/100;

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                               coverCard.setAlpha(1f-floatPercent);
                                backCover.setAlpha(1f-floatPercent);
                            }
                        });
                        Log.d("Cover", System.currentTimeMillis() - c+"");

                    }
                });
                thread.start();




            }
        });
        return view;
    }

    private void getPlaylist(final int id){
        final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                final RealmResults<Playlists> results = realm.where(Playlists.class)
                                                    .equalTo("id", id).findAll();
                String im1 = "";
                for (String s : results.get(0).getName().replaceAll("(\\w)\\w*","$1").split("\\W+")){
                    im1 += s.toUpperCase();
                    if (im1.length()>=2) break;
                }
                final String im = im1;
                final String name = results.get(0).getName();


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        FragmentPlaylistSongsView.this.im.setText(im);
                        title.setText(name);

                    }
                });

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                realm.close();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e("FPSV.getPlaylist", error.toString());
                realm.close();
            }
        });
    }

    private void getSongs(final int id){
        final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<PlaylistSongs> result = realm.where(PlaylistSongs.class)
                                                        .equalTo("playlist.id", id).findAll();
                Log.d("FragmentPSV", "<DB Query> count of songs in that playlist "+result.size());
                for (int i = 0; i < result.size(); i++) {
                    songsArray.add(new SongModel(result.get(i).get_id(), result.get(i).getUri(), "",
                            result.get(i).getTitle(), result.get(i).getArtist(), result.get(i).getAlbum(),
                            result.get(i).getAlbum_id(), result.get(i).getArtist_id(), result.get(i).getDuration(),
                            result.get(i).getYear(), result.get(i).getDate_modified(), false));
                }
                songAdapter = new SongAdapter(songsArray,  0, FragmentPlaylistSongsView.this);
//                songAdapter.setOnItemMoreClickListener(FragmentPlaylistSongsView.this);
                songAdapter.setHasStableIds(true);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                rvSongs.setAdapter(songAdapter);
                realm.close();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e("FPSV.getSongs", error.toString());
                realm.close();
            }
        });
    }

    @Override
    public void onItemOptionClickListener(SongModel songField) {
        BottomSheetDialogSongFragment bottomSheetDialogFragment = new BottomSheetDialogSongFragment();
        Bundle args = new Bundle();
        args.putSerializable("adapter_song", (Serializable) songField);
        bottomSheetDialogFragment.setArguments(args);
//        bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
//        ((MyActivity) getActivity()).showMenu(bottomSheetDialogFragment);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_BACK));
                break;
        }
    }
}
