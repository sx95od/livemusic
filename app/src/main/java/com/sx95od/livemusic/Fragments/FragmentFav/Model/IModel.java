package com.sx95od.livemusic.Fragments.FragmentFav.Model;

import com.sx95od.livemusic.API.Google.Models.TokenErrorModel.TokenErrorModel;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.GeoTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.TopTracks.TopTracksModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.ChartVideosModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel.MyChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;

import io.reactivex.Observable;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by stoly on 01.10.2017.
 */

public interface IModel {
    Observable<SearchByKeywordModel> getSearchByKeyWordYoutube(String q, int maxResults, String type, String categoryID,
                                                               String part, String apiKey);

    Observable<GeoTopTracksModel> getLastFMGeoTopTracks(int page);
    Observable<TopTracksModel> getLastFMTopTracks(int page);

    Observable<MyChannelModel> getMyChannel();
    Observable<ChartVideosModel> getChartVideos(String pageToken);
//    Observable<ResponseBody> getTokenInfo();

//    Observable<ResponseBody> getNewToken();
}
