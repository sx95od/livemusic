package com.sx95od.livemusic.Fragments.FragmentAlbumsView.Presenter;

/**
 * Created by stoly on 08.09.2017.
 */

public interface IPresenter {
    void onCreate();
    void onDestroy();
}
