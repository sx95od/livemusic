package com.sx95od.livemusic.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Dialogs.DialogSongMore.BottomSheetDialogSongFragment;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.FavSongs;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by stoly on 28.06.2017.
 */

public class FragmentFavSongsView extends MyFragment implements View.OnClickListener, SongAdapter.OnItemMoreClickListener, Listeners.OnLocalSongItemClickListener {
    @BindView(R2.id.status_bar) View _statusBar;
    @BindView(R2.id.imagebutton_back) ImageButton _backButton;
    @BindView(R2.id.imagebutton_more) ImageButton _moreButton;
    RecyclerView rvTracks;
    LinearLayoutManager layoutManager;
    ArrayList<SongModel> tracksArray;
    CoordinatorLayout screen;
    SongAdapter tracksAdapter;

    Unbinder unbinder;

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        unbinder = null;
        super.onDestroyView();
    }


    @Override
    public void onFragmentCreated() {
        super.onFragmentCreated();
        loadLiked();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fav_songs_view, container, false);
        unbinder = ButterKnife.bind(this, view);

        _statusBar.getLayoutParams().height = Core.getStatusBarHeight();

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvTracks = (RecyclerView) view.findViewById(R.id.songs);
        screen = (CoordinatorLayout) view.findViewById(R.id.screen);
        rvTracks.setLayoutManager(layoutManager);
        rvTracks.setSaveEnabled(false);
        tracksArray = new ArrayList<SongModel>();

        return view;
    }


    @OnClick(R2.id.imagebutton_back) void onBackClick(){
        backStack();
    }

    private void loadLiked(){
        Realm realm = null;
        try {
            realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<FavSongs> result = realm
                            .where(FavSongs.class)
                            .findAllSorted("id", Sort.DESCENDING);


                    for (int i = 0; i < result.size(); i++) {
                        Log.i("Realm",  result.get(i).getTitle());
                        tracksArray.add(new SongModel(result.get(i).get_id(), result.get(i).getUri(), "",
                                result.get(i).getTitle(), result.get(i).getArtist(), result.get(i).getAlbum(),
                                result.get(i).getAlbum_id(), result.get(i).getArtist_id(), result.get(i).getDuration(),
                                result.get(i).getYear(), result.get(i).getDate_modified(), false));
                    }
                    tracksAdapter = new SongAdapter(tracksArray,  0, FragmentFavSongsView.this, FragmentFavSongsView.this);
//                    tracksAdapter.setOnItemMoreClickListener(FragmentFavSongsView.this);
                    tracksAdapter.setHasStableIds(true);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {


                    rvTracks.setAdapter(tracksAdapter);
                }
            });
        }catch (Exception e){
            if (realm!=null)
                realm.close();
        } finally {
            if (realm!=null)
                realm.close();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_BACK));
                break;
        }
    }

    @Override
    public void onItemOptionClickListener(SongModel songField) {
        BottomSheetDialogSongFragment bottomSheetDialogFragment = new BottomSheetDialogSongFragment();
        Bundle args = new Bundle();
        args.putSerializable("adapter_song", (Serializable) songField);
        bottomSheetDialogFragment.setArguments(args);
//        bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
//        ((MyActivity) getActivity()).showMenu(bottomSheetDialogFragment);
    }

    @Override
    public void onSongItemClick(int pos) {
        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_SET_SONG, pos, tracksArray));
    }

    @Override
    public void onAlbumPlayClick() {

    }

    @Override
    public void onSongItemLongClick(int pos) {

    }
}
