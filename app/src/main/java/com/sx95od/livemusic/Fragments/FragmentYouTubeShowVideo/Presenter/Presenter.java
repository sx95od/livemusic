package com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.Presenter;

import com.sx95od.livemusic.API.YouTubeApi.Models.Channel.ChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.Video.YouTubeVideoModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.Model.IModel;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.Model.Model;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.View.IView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by stoly on 24.09.2017.
 */

public class Presenter implements IPresenter{
    private IModel model = new Model();
    private IView iView;
    private CompositeDisposable disposables = new CompositeDisposable();

    public Presenter(IView iView) {
        this.iView = iView;
    }


    @Override
    public void onCreate() {
        disposables.add(model.getVideoInfo(iView.getVideoID(), YouTubeApi.SNIPPET+","+
        YouTubeApi.TOPIC_DETAILS+","+YouTubeApi.CONTENT_DETAILS+","+YouTubeApi.STATISTICS, BuildConfig.YOUTUBE_API_KEY)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<YouTubeVideoModel>() {
            @Override
            public void onNext(YouTubeVideoModel youTubeVideoModel) {
                iView.showVideoInfo(youTubeVideoModel);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        }));
    }

    @Override
    public void onDestroy() {
        if (!disposables.isDisposed()){
            disposables.dispose();
        }
    }

    @Override
    public void getChannelID(String id) {
        disposables.add(model.getChannelInfo(null, id, null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ChannelModel>() {
                    @Override
                    public void onNext(ChannelModel channelModel) {
                        iView.showChannelInfo(channelModel);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }
}
