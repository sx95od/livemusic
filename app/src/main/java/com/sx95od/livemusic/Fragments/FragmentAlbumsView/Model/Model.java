package com.sx95od.livemusic.Fragments.FragmentAlbumsView.Model;

import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Core.Core;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by stoly on 08.09.2017.
 */

public class Model implements IModel {
    @Override
    public Observable<List<AlbumModel>> getAlbumList() {
        return Observable.fromArray(Core.getCoreInstance().getAlbums());
    }
}
