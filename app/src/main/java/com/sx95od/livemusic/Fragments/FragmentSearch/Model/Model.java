package com.sx95od.livemusic.Fragments.FragmentSearch.Model;


import com.sx95od.livemusic.API.LastFM.LastFMApi;
import com.sx95od.livemusic.API.LastFM.Models.AlbumSearch.AlbumSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.ArtistSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.TrackSearchModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.SearchHistory;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by stoly on 04.09.2017.
 */

public class Model implements IModel {
    LastFMApi lastFMApi = Core.getLastFMApi();
    YouTubeApi youTubeApi = Core.getYoutubeApi();

    @Override
    public Observable<TrackSearchModel> getLastFMTrackList(String trackName, String artist, String apiKey, int limit, int page, String format) {
        return lastFMApi.searchTrack(trackName, artist, apiKey, limit, page, format);
    }

    @Override
    public Observable<AlbumSearchModel> getLastFMAlbumList(String artist, String apiKey, int limit, int page, String format) {
        return lastFMApi.searchAlbum(artist, apiKey, limit, page, format);
    }

    @Override
    public Observable<ArtistSearchModel> getLastFMArtistList(String artist, String apiKey, int limit, int page, String format) {
        return lastFMApi.searchArtist(artist, apiKey, limit, page, format);
    }

    @Override
    public Observable<SearchByKeywordModel> getSearchByKeyWordYoutube(String q, int maxResults, String type, String categoryID, String part, String apiKey) {
        return youTubeApi.searchVideo(q, maxResults, YouTubeApi.TYPE_VIDEO, YouTubeApi.CATEGORY_MUSIC, part, apiKey);
//        return youTubeApi.searchVideo(q, maxResults, YouTubeApi.TYPE_VIDEO, part, apiKey);
    }

    @Override
    public Observable<List<SongModel>> getInternalSongList(String song) {
        List<SongModel> songObjects = new ArrayList<SongModel>();

        for (int i = 0; i < ((Core) Core.getCoreInstance()).getSongs().size(); i++) {
            if (((Core) Core.getCoreInstance()).getSongs().get(i).getTitle().toLowerCase().contains(song.toLowerCase())
                    | ((Core) Core.getCoreInstance()).getSongs().get(i).getArtist().toLowerCase().contains(song.toLowerCase())
                    ) {
                songObjects.add(((Core) Core.getCoreInstance()).getSongs().get(i));
            } else {
            }
        }
        return Observable.fromArray(songObjects);
    }

    @Override
    public Observable<List<AlbumModel>> getInternalAlbumList(String album) {
        List<AlbumModel> albumObjects = new ArrayList<AlbumModel>();

        for (int i = 0; i < ((Core) Core.getCoreInstance()).getAlbums().size(); i++) {
            if (((Core) Core.getCoreInstance()).getAlbums().get(i).getAlbum().toLowerCase().contains(album.toLowerCase())) {
                albumObjects.add(((Core) Core.getCoreInstance()).getAlbums().get(i));
            } else {
            }
        }
        return Observable.fromArray(albumObjects);
    }

    @Override
    public Observable<String> addQueryToDB(String query){
        Realm realm = Realm.getInstance(Core.getConfigRealm());
        realm.beginTransaction();
        RealmResults<SearchHistory> results = realm.where(SearchHistory.class)
                .equalTo("query", query).findAll();
        if (results.size() == 0) {
            SearchHistory searchHistory = realm.createObject(SearchHistory.class, getNextKey());
            searchHistory.setCountQueries(1);
            searchHistory.setQuery(query);
        } else {
            results.get(0).incrementQuery();
        }
        realm.commitTransaction();
        realm.close();
        realm = null;
       return Observable.just(query);
    }

    public int getNextKey() {
        Realm realm = null;
        int nextId = 0;
        try{
            realm = Realm.getInstance(Core.getConfigRealm());
            Number maxId = realm.where(SearchHistory.class).max("id");
            // If there are no rows, currentId is null, so the next id must be 1
            // If currentId is not null, increment it by 1
            nextId = (maxId == null) ? 0 : maxId.intValue() + 1;
        }catch (Exception e){
            realm.close();
            return nextId;
        } finally {
            realm.close();
            return nextId;
        }
    }

    @Override
    public Observable<List<String>> getSearchHistory() {
            List<String> recentSearchesObjects = new ArrayList<String>();
        Realm realm = Realm.getInstance(Core.getConfigRealm());
        realm.executeTransaction(realm1 -> {
            String []fieldNames={"countQueries", "id"};
            Sort sort[]={Sort.DESCENDING,Sort.DESCENDING};
            RealmResults<SearchHistory> results = realm1.where(SearchHistory.class).findAllSorted(fieldNames, sort);
            for (int i=0; i<results.size(); i++){
                recentSearchesObjects.add(results.get(i).getQuery());
            }
        });
        realm.close();
        realm = null;
        return Observable.fromArray(recentSearchesObjects);
    }
}
