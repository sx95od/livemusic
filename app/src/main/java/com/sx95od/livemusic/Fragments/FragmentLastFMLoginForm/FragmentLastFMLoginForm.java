package com.sx95od.livemusic.Fragments.FragmentLastFMLoginForm;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sx95od.livemusic.API.AuthRequestBody;
import com.sx95od.livemusic.API.GoogleAuthApi;
import com.sx95od.livemusic.API.YouTubeTokenManager;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Fragments.FragmentLastFMLoginForm.View.IView;
import com.sx95od.livemusic.Fragments.MyFragment;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by stoly on 09.09.2017.
 */

public class FragmentLastFMLoginForm extends MyFragment implements IView, GoogleApiClient.OnConnectionFailedListener{
    private final String tag = "LoginLastFM";

    GoogleApiClient gApiClient;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lastfm_login_form, container, false);
        unbinder = ButterKnife.bind(this, view);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(BuildConfig.YOUTUBE_WEB_CLIENT_ID, true)
                .requestEmail()
                .requestScopes(new Scope("https://www.googleapis.com/auth/youtube"))
                .build();



        gApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();

        Log.d("YTTOKENMANAGER", YouTubeTokenManager.ACCESS_TOKEN+"");
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1488) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                String authCode = acct.getServerAuthCode();
                Log.d("AUTHME", authCode);
                try {
                    AuthRequestBody authRequestBody = new AuthRequestBody();
                    authRequestBody.setClientId(BuildConfig.YOUTUBE_WEB_CLIENT_ID);
                    authRequestBody.setClientSecret(BuildConfig.YOUTUBE_WEB_CLIENT_SECRET);
                    authRequestBody.setCode(authCode);
                    authRequestBody.setGrantType(GoogleAuthApi.CODE_AUTHORIZATION);



                    Core.googleAuthApi.getToken(authRequestBody.getClientId(), authRequestBody.getClientSecret(),
                            authRequestBody.getCode(), authRequestBody.getGrantType())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new DisposableObserver<ResponseBody>() {
                                @Override
                                public void onNext(ResponseBody responseBody) {
                                    try {
                                        YouTubeTokenManager.writeTokenToDB(responseBody.string());
                                        backStack();
                                    }catch (Exception e){
                                        Log.e("AUTHME", e.toString());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.e("AUTHME", e.toString());
                                }

                                @Override
                                public void onComplete() {

                                }
                            });
                }catch (Exception e){
                    Log.e("AUTHME", e.toString());
                }
            } else {
                Log.e("AUTHME", "NODATA");
            }
        }
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        gApiClient.stopAutoManage(getActivity());
        gApiClient.disconnect();
    }

    @OnClick(R2.id.button_login) void onClick(View view){
//        String login = loginEditText.getText().toString();
//        String password = passwordEditText.getText().toString();
//        String signature = MD5.md5String("api_key"+BuildConfig.LASTFM_API_KEY+
//        "methodauth.getMobileSessionpassword"+password+"username"+login+"a2365288af3bd6d2587cbfffcb8e3ba1");
//        Log.d(tag, signature);

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(gApiClient);
        startActivityForResult(signInIntent, 1488);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
