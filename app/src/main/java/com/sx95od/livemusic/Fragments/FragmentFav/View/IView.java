package com.sx95od.livemusic.Fragments.FragmentFav.View;

import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.GeoTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.Tracks;
import com.sx95od.livemusic.API.LastFM.Models.TopTracks.TopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.Track;
import com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.ChartVideosModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel.MyChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;

import java.util.List;

/**
 * Created by stoly on 01.10.2017.
 */

public interface IView {
    void openFirstYouTubeVideo(Item item);
    void youTubeShowError(String msg);

    void showGeoTopTracksLastFM(List<com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.Track> tracks);
    void showTopTracksLastFM(List<com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track> tracks);

    void showMyYT(MyChannelModel myChannelModel);
    void showVideoChartYT(ChartVideosModel chartVideosModel);
}
