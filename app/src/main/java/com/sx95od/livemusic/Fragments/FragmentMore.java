package com.sx95od.livemusic.Fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.flexbox.*;
import com.google.android.gms.ads.InterstitialAd;
import com.sx95od.livemusic.*;
import com.sx95od.livemusic.Adapters.Adapters.ColorAdapter;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.AppPreferences;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Core.ProtectAPI;
import com.sx95od.livemusic.Events.AppEvent;
import com.sx95od.livemusic.Events.GlobalEvent;
import com.sx95od.livemusic.Events.ThemeEvent;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.Utils.ThemeEngine;
import org.greenrobot.eventbus.EventBus;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Unbinder;

/**
 * Created by stoly on 24.07.2017.
 */

public class FragmentMore extends BaseFragment implements View.OnClickListener, ColorAdapter.OnItemClickListener{
    @BindView(R2.id.recycler_view_colors) RecyclerView recyclerViewColors;
    @BindView(R2.id.color_accent_back) ImageButton colorAccentBack;
    @BindView(R2.id.color_accent_choose) FlexboxLayout colorAccentChoose;
    @BindView(R2.id.color_accent_item) FlexboxLayout colorAccentItem;
    @BindView(R2.id.color_accent_current_color) ImageView colorAccentCurrentColor;
    @BindView(R2.id.switch_dark_theme) SwitchCompat switchDarkTheme;
    @BindView(R2.id.switch_show_ad) SwitchCompat _switchShowAd;
    @BindView(R2.id.dark_theme_item) TextView darkThemeItem;
    @BindView(R2.id.appname_top) TextView _appNameTop;
    @BindView(R2.id.ask_to_developer) TextView askDeveloper;
    @BindView(R2.id.tv_version) TextView _version;
    Unbinder unbinder;
    int clickCount = 0;
    Handler handlerClick;

    private InterstitialAd mInterstitialAd;

    ArrayList<Integer> colors;
    ColorAdapter colorAdapter;

    LinearLayoutManager layoutManager;

    private void initPrefs(){
        colorAccentCurrentColor.setColorFilter(PrefManager.getColorAccent());
        EventBus.getDefault().post(new AppEvent(AppEvent.THEME_CHANGED));
        if (Core.getCoreInstance().getDarkTheme() == 0){
            switchDarkTheme.setChecked(false);
        } else{
            switchDarkTheme.setChecked(true);
        }
        if (!Core.getCoreInstance().getShowingAd()){
            _switchShowAd.setChecked(false);
        } else{
            _switchShowAd.setChecked(true);
        }
    }



    private Runnable incrementClickToShowExtendsPrefs = () -> clickCount = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        unbinder = ButterKnife.bind(this, view);
        handlerClick = new Handler();

        initPrefs();

        colors = new ArrayList<Integer>();
        colors.add(R.color.red);
        colors.add(R.color.pink);
        colors.add(R.color.purple);
        colors.add(R.color.deep_purple);
        colors.add(R.color.indigo);
        colors.add(R.color.blue);
        colors.add(R.color.light_blue);
        colors.add(R.color.cyan);
        colors.add(R.color.teal);
        colors.add(R.color.green);
        colors.add(R.color.light_green);
        colors.add(R.color.lime);
        colors.add(R.color.yellow);
        colors.add(R.color.amber);
        colors.add(R.color.orange);
        colors.add(R.color.deep_orange);
        colors.add(R.color.brown);

        _version.setText(getString(R.string.version)+" "+BuildConfig.VERSION_NAME);

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewColors.setLayoutManager(layoutManager);
        colorAdapter = new ColorAdapter(getActivity(), colors);
        colorAdapter.setOnItemClickListener(this);
        recyclerViewColors.setAdapter(colorAdapter);
        askDeveloper.setOnClickListener(this);

//        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "pacifico_regular.ttf");
//        _appNameTop.setTypeface(face);

//        MobileAds.initialize(getContext(),
//                BuildConfig.ADMOB_ID);
//
//        mInterstitialAd = new InterstitialAd(getContext());
//        mInterstitialAd.setAdUnitId(BuildConfig.ADMOB_TEST_BANNER_INTERSTITIAL_1);
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        return view;
    }

    @OnClick({R2.id.color_accent_back,
            R2.id.color_accent_item,
            R2.id.dark_theme_item,
            R2.id.logo,
            R2.id.showing_ad_item}) void click(View view){
        switch (view.getId()){
            case R.id.color_accent_back:
                animateFlexbox(colorAccentChoose, colorAccentItem);
                break;
            case R.id.color_accent_item:
                animateFlexbox(colorAccentItem, colorAccentChoose);
                break;
            case R.id.dark_theme_item:
                SharedPreferences pref = ((Core) getActivity().getApplication()).getPref();
                SharedPreferences.Editor editor = pref.edit();
                if (switchDarkTheme.isChecked()){
                    switchDarkTheme.setChecked(false);
                    editor.putInt(ThemeEngine.DARK_THEME, 0);
                } else {
                    switchDarkTheme.setChecked(true);
                    editor.putInt(ThemeEngine.DARK_THEME, 1);
                }
                editor.apply();
                initPrefs();
                pref = null;
                EventBus.getDefault().post(new GlobalEvent(GlobalEvent.UI_RECREATE));
                break;
            case R.id.logo:
                handlerClick.removeCallbacks(incrementClickToShowExtendsPrefs);
                clickCount++;
                if (clickCount!=16) {
                    handlerClick.postDelayed(incrementClickToShowExtendsPrefs, 1000);
                }
                break;
            case R.id.showing_ad_item:
                SharedPreferences pref1 = ((Core) getActivity().getApplication()).getPref();
                SharedPreferences.Editor editor1 = pref1.edit();
                if (_switchShowAd.isChecked()){
                    _switchShowAd.setChecked(false);
                    editor1.putBoolean("showing_ad", false);
                } else {
                    _switchShowAd.setChecked(true);
                    editor1.putBoolean("showing_ad", true);
                }
                editor1.apply();
                break;
        }
    }

    @OnLongClick(R2.id.logo) boolean onLongClick(View view){
        switch (view.getId()){
            case R.id.appname_top:
                if (Core.getCoreInstance().getShowingAd()) {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    } else {
                        Log.d("TAG", "The interstitial wasn't loaded yet.");
                    }
                }

                if (clickCount == com.sx95od.livemusic.BuildConfig.HiddenPrefsKeyCountClick){
                    SharedPreferences.Editor editor = Core.getPref().edit();
                    editor.putString(AppPreferences.HIDDEN_ACCESS_KEY, ProtectAPI.getEncryptAccessKey().trim());
                    editor.putBoolean(AppPreferences.HIDDEN_PREFERENCE, true);
                    editor.apply();
                    Toast.makeText(getContext(), getString(R.string.hidden_prefs_available), Toast.LENGTH_LONG).show();
                    editor = null;
                }
                break;
        }
        return true;
    }

    private void animateFlexbox(final FlexboxLayout first, final FlexboxLayout second){
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(first, "alpha",
                0f);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(second, "alpha",
                1f);
        fadeOut.setDuration(350);
        fadeIn.setDuration(350);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                first.setVisibility(View.VISIBLE);
                second.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                first.setVisibility(View.GONE);
                if (getView()!=null)
                    ViewCompat.setLayerType(getView(), ViewCompat.LAYER_TYPE_NONE, null);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.play(fadeOut).with(fadeIn);
        animatorSet.start();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recyclerViewColors.setAdapter(null);
        handlerClick.removeCallbacks(incrementClickToShowExtendsPrefs);
        handlerClick = null;
        incrementClickToShowExtendsPrefs = null;
        colorAdapter = null;
        colors.clear();
        colors = null;
        layoutManager = null;
        unbinder.unbind();
        unbinder = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ask_to_developer:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto: sx95od@gmail.com"));
                startActivity(Intent.createChooser(emailIntent, "Send question"));
                break;
        }
    }

    @Override
    public void onItemClickListener(int color) {
        PrefManager.setColorAccent(ContextCompat.getColor(getContext(), color));
        animateFlexbox(colorAccentChoose, colorAccentItem);
        EventBus.getDefault().post(new GlobalEvent(GlobalEvent.UI_RECREATE));
    }
}
