package com.sx95od.livemusic.Fragments.FragmentSongsView.Model;

import android.util.Log;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.FavSongs;
import com.sx95od.livemusic.Fragments.FragmentFavSongsView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by stoly on 22.10.2017.
 */

public class Model implements IModel{
    @Override
    public Observable<List<SongModel>> getSongsByAlbumID(long albumID) {
        return Observable.defer(new Callable<ObservableSource<? extends List<SongModel>>>() {
            @Override
            public ObservableSource<? extends List<SongModel>> call() throws Exception {
                List<SongModel> songModel = new ArrayList<SongModel>();
                if (albumID != -1) {
                    for (int i = 0; i < (Core.getCoreInstance().getSongs().size()); i++) {
                        if (Core.getCoreInstance().getSongs().get(i).getAlbum_id() == albumID)
                            songModel.add(Core.getCoreInstance().getSongs().get(i));
                    }
                } else {
                    songModel.addAll(Core.getCoreInstance().getSongs());
                }
                return Observable.fromArray(songModel);
            }
        });
    }

    @Override
    public Observable<List<SongModel>> getSongs() {
        return Observable.defer(new Callable<ObservableSource<? extends List<SongModel>>>() {
            @Override
            public ObservableSource<? extends List<SongModel>> call() throws Exception {
                return Observable.fromArray(Core.getCoreInstance().getSongs());
            }
        });
    }

    @Override
    public Observable<List<SongModel>> getFavSongs() {
        return Observable.fromCallable(new Callable<List<SongModel>>() {
            @Override
            public List<SongModel> call() throws Exception {
                ArrayList<SongModel> tracksArray = new ArrayList<SongModel>();
                Realm realm = null;
                try {
                    realm = Realm.getInstance(Core.getConfigRealm());
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<FavSongs> result = realm
                                    .where(FavSongs.class)
                                    .findAllSorted("id", Sort.DESCENDING);


                            for (int i = 0; i < result.size(); i++) {
                                Log.i("Realm",  result.get(i).getTitle());
                                tracksArray.add(new SongModel(result.get(i).get_id(), result.get(i).getUri(), "",
                                        result.get(i).getTitle(), result.get(i).getArtist(), result.get(i).getAlbum(),
                                        result.get(i).getAlbum_id(), result.get(i).getArtist_id(), result.get(i).getDuration(),
                                        result.get(i).getYear(), result.get(i).getDate_modified(), false));
                            }
                        }
                    });
                }catch (Exception e){
                    if (realm!=null)
                        realm.close();
                } finally {
                    if (realm!=null)
                        realm.close();
                }
                return tracksArray;
            }
        });
    }
}
