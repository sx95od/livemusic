package com.sx95od.livemusic.Fragments.FragmentFav.Model;

import com.sx95od.livemusic.API.Google.Models.TokenErrorModel.TokenErrorModel;
import com.sx95od.livemusic.API.GoogleAuthApi;
import com.sx95od.livemusic.API.GoogleTokenApi;
import com.sx95od.livemusic.API.LastFM.Constants;
import com.sx95od.livemusic.API.LastFM.LastFMApi;
import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.GeoTopTracksModel;
import com.sx95od.livemusic.API.LastFM.Models.TopTracks.TopTracksModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.ChartVideos.ChartVideosModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel.MyChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.API.YouTubeTokenManager;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Core;

import io.reactivex.Observable;
import okhttp3.ResponseBody;

/**
 * Created by stoly on 01.10.2017.
 */

public class Model implements IModel{
    private LastFMApi lastFMApi = Core.getLastFMApi();
    private YouTubeApi youTubeApi = Core.getYoutubeApi();
    private GoogleTokenApi googleTokenApi = Core.googleTokenApi;
    private GoogleAuthApi googleAuthApi = Core.googleAuthApi;

    @Override
    public Observable<SearchByKeywordModel> getSearchByKeyWordYoutube(String q, int maxResults, String type, String categoryID, String part, String apiKey) {
        return youTubeApi.searchVideo(q, maxResults, type, categoryID, part, apiKey);
    }

    @Override
    public Observable<GeoTopTracksModel> getLastFMGeoTopTracks(int page) {
        return lastFMApi.getGeoTopTracks(BuildConfig.LASTFM_API_KEY, Constants.JSON, Constants.USA, Constants.LIMIT, page);
    }

    @Override
    public Observable<TopTracksModel> getLastFMTopTracks(int page) {
        return lastFMApi.getTopTracks(BuildConfig.LASTFM_API_KEY, Constants.LIMIT, page, Constants.JSON);
    }

    @Override
    public Observable<MyChannelModel> getMyChannel() {
        return youTubeApi.getMyChannel("Bearer "+YouTubeTokenManager.ACCESS_TOKEN, true, YouTubeApi.TOPIC_DETAILS+","
        +YouTubeApi.STATISTICS+","+YouTubeApi.SNIPPET+","+YouTubeApi.CONTENT_DETAILS+","+YouTubeApi.BRANDING_SETTINGS, BuildConfig.YOUTUBE_API_KEY);
    }

    @Override
    public Observable<ChartVideosModel> getChartVideos(String pageToken) {
        if (pageToken == null)
            return youTubeApi.chartVideos(YouTubeApi.REGION_UA, 25, YouTubeApi.CATEGORY_MUSIC, YouTubeApi.CHART,
                    YouTubeApi.TOPIC_DETAILS+","+YouTubeApi.STATISTICS+","+YouTubeApi.SNIPPET+","+YouTubeApi.CONTENT_DETAILS,
                    BuildConfig.YOUTUBE_API_KEY);
        else
        return youTubeApi.chartVideos(YouTubeApi.REGION_UA, 25, YouTubeApi.CATEGORY_MUSIC, pageToken, YouTubeApi.CHART,
                YouTubeApi.TOPIC_DETAILS+","+YouTubeApi.STATISTICS+","+YouTubeApi.SNIPPET+","+YouTubeApi.CONTENT_DETAILS, BuildConfig.YOUTUBE_API_KEY);
    }

//    @Override
//    public Observable<ResponseBody> getTokenInfo() {
//        return googleTokenApi.getTokenInfo(YouTubeTokenManager.ACCESS_TOKEN);
//    }

//    @Override
//    public Observable<ResponseBody> getNewToken() {
//        return googleAuthApi.refreshToken(BuildConfig.YOUTUBE_WEB_CLIENT_ID, BuildConfig.YOUTUBE_WEB_CLIENT_SECRET,
//                YouTubeTokenManager.REFRESH_TOKEN, GoogleAuthApi.CODE_REFRESH_TOKEN);
//    }
}
