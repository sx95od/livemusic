package com.sx95od.livemusic.Fragments.FragmentSearch.Presenter;

import android.view.View;

import com.sx95od.livemusic.API.LastFM.Models.SearchTrackModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.Track;
import com.sx95od.livemusic.Fragments.FragmentSearch.View.IView;

/**
 * Created by stoly on 04.09.2017.
 */

public interface IPresenter {
    void onSearchEnter();
    void onSearchVideosOnYouTube(Track track);
    void onCreate();
    void onStop();
    void onDestroy();
}
