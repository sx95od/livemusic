package com.sx95od.livemusic.Fragments.FragmentSongsView;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Analytics.MyGoogleAnalytics;
import com.sx95od.livemusic.Dialogs.DialogSongMore.BottomSheetDialogSongFragment;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Events.AppEvent;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.SongListEvent;
import com.sx95od.livemusic.Events.UIEvent;
import com.sx95od.livemusic.Fragments.FragmentSongsView.Presenter.Presenter;
import com.sx95od.livemusic.Fragments.FragmentSongsView.View.IView;
import com.sx95od.livemusic.Fragments.MyFragment;
import com.sx95od.livemusic.Fragments.MyFragmentWithoutAnim;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.ThemeViews.RecyclerViewLoaderListener;
import com.sx95od.livemusic.ThemeViews.SFlexboxLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by stoly on 07.06.2017.
 * Migrate to ButterKnife
 */

public class FragmentSongsView extends MyFragment implements SongAdapter.OnItemMoreClickListener,
        Listeners.OnLocalSongItemClickListener,
        Listeners.OnMediaScanCompleted, IView, SongAdapter.OnShuffleClickListener, RecyclerViewLoaderListener{
    private final String tag = "F-SongsView";
    public static final String TYPES_FRAGMENT_SONGS_VIEW = "f_sv_type";
    public static final int TYPE_ALL_SONGS = 0;
    public static final int TYPE_ALBUM_SONGS = 1;
    public static final int TYPE_FAV_SONGS = 2;

    @BindView(R2.id.back)
    ImageButton back;

    @BindView(R2.id.imagebutton_more)
    ImageButton _more;

    @BindView(R2.id.action_bar)
    SFlexboxLayout actionBar;

    @BindView(R2.id.root)
    FrameLayout root;

    @BindView(R2.id.songs)
    RecyclerView _rvTracks;

    @BindView(R2.id.tv_title)
    TextView _title;

    @BindView(R2.id.cv_letter)
    CardView _letter;

    @BindView(R2.id.tv_letter)
    TextView _tvLetter;

    @BindView(R2.id.tv_nothing_found)
    TextView _nothingFound;

    @BindView(R2.id.pb_loader)
    ProgressBar _loader;

    boolean RVReady = false;

    private Unbinder unbinder;

    LinearLayoutManager layoutManager;
    ArrayList<SongModel> tracksArray;
    SongAdapter tracksAdapter;

    int adapterFlag = 0;

    float halfTopAlbumLayoutHeight = 0;
    float oneF = 0;

    Presenter presenter;

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_FORCE_LIGHT_STATUS_BAR, menuVisible));
    }

    @Override
    public void onFragmentCreated() {
        super.onFragmentCreated();

        if (presenter != null)
            switch (getArguments().getInt(TYPES_FRAGMENT_SONGS_VIEW)) {
                case TYPE_ALL_SONGS:
                    presenter.getSongs();
                    break;
                case TYPE_FAV_SONGS:
                    presenter.getFavSongs();
                    break;
                case TYPE_ALBUM_SONGS:
                    presenter.getSongsByAlbumID(getArguments().getLong("album_id"));
                    break;
            }
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        switch (getArguments().getInt(TYPES_FRAGMENT_SONGS_VIEW)){
            case TYPE_ALL_SONGS:
                setActionBarTitle(getString(R.string.songs));
                break;
            case TYPE_FAV_SONGS:
                setActionBarTitle(getString(R.string.favourite_songs));
                break;
            case TYPE_ALBUM_SONGS:
                setTransparentActionBar();
                adapterFlag = SongAdapter.FLAG_ALBUM_SONGS;
                _rvTracks.setPadding(0, 0, 0, _rvTracks.getPaddingBottom());
                halfTopAlbumLayoutHeight = getResources().getDimensionPixelSize(R.dimen.action_bar_adapter_album_top_height) / 2;
                oneF = halfTopAlbumLayoutHeight / 100;
                _title.setAlpha(0f);
                _title.setText(getArguments().getString("album_title"));
                break;
        }
        EventBus.getDefault().register(this);
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_songs_view, container, false);
        unbinder = ButterKnife.bind(this, view);

        presenter = new Presenter(this);

        Core.getCoreInstance().getTracker().setScreenName(MyGoogleAnalytics.FRAGMENT_SONGS_VIEW);
        Core.getCoreInstance().getTracker().send(new HitBuilders.ScreenViewBuilder().build());


        Log.i(getClass().getSimpleName(), getClass().getSimpleName().toString()+" created");
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false){
            @Override
            public void onLayoutCompleted(RecyclerView.State state) {
                super.onLayoutCompleted(state);
                if (!RVReady) {
                    onRVLoaded();
                    RVReady = true;
                }
            }
        };
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        layoutManager.setInitialPrefetchItemCount(16);
//        layoutManager.setItemPrefetchEnabled(true);
        _rvTracks.setSaveEnabled(false);
        _rvTracks.setLayoutManager(layoutManager);
        _rvTracks.setPadding(0, 0, 0, PrefManager.getBottomNavigationBarHeight());
        _rvTracks.requestLayout();
//        _rvTracks.setItemViewCacheSize(32);

        _rvTracks.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (_letter != null && getView() != null) {
                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING && layoutManager.findFirstVisibleItemPosition() > 0) {
                        if (_letter.getVisibility() == View.GONE) {
                            showLetter(true);
                        }
                    } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        if (_letter.getVisibility() == View.VISIBLE) {
                            showLetter(false);
                        }
                    }
                    super.onScrollStateChanged(recyclerView, newState);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (adapterFlag == SongAdapter.FLAG_ALBUM_SONGS) {
                    if (recyclerView.computeVerticalScrollOffset() < halfTopAlbumLayoutHeight &&
                            layoutManager.findFirstVisibleItemPosition() == 0) {
                        float res = recyclerView.computeVerticalScrollOffset() / oneF / 100;
                        actionBar.setBackgroundColor(adjustAlpha(ContextCompat.getColor(getContext(), R.color.white), res));
                        _title.setAlpha(res);
                    } else {
                        actionBar.setBackgroundColor(adjustAlpha(ContextCompat.getColor(getContext(), R.color.white), 1f));
                        _title.setAlpha(1f);
                    }
                }
                if (layoutManager.findFirstVisibleItemPosition() > 0 && _letter.getVisibility() == View.GONE)
                    showLetter(true);
                if (adapterFlag != SongAdapter.FLAG_ALBUM_SONGS)
                    _tvLetter.setText(tracksAdapter.getFirstLetter(layoutManager.findFirstVisibleItemPosition()));
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        return view;
    }


    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }


    @OnClick(R2.id.back) void clickBack(View view){
        backStack();
    }


    private void showLetter(boolean show){
        if (show) _letter.setVisibility(View.VISIBLE);
        float scale = show ? 1f : 0.5f;
        float swipe = !show ? -(getResources().getDimensionPixelSize(R.dimen.dp56))/3 : 0;
        float alpha = show ? 1f : 0f;
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator objectAnimatorAlpha  = ObjectAnimator.ofFloat(_letter, View.ALPHA, alpha);
        ObjectAnimator objectAnimatorScaleX = ObjectAnimator.ofFloat(_letter, View.SCALE_X, scale);
        ObjectAnimator objectAnimatorScaleY = ObjectAnimator.ofFloat(_letter, View.SCALE_Y, scale);
        ObjectAnimator objectAnimatorSwipe  = ObjectAnimator.ofFloat(_letter, View.TRANSLATION_Y, swipe);
        animatorSet.setDuration(256);
        animatorSet.playTogether(objectAnimatorAlpha, objectAnimatorScaleX, objectAnimatorScaleY, objectAnimatorSwipe);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!show && getView() != null) _letter.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animatorSet.start();
    }

    @Subscribe
    public void onAppEvent(AppEvent appEvent){
        switch (appEvent.getCode()){
            case AppEvent.FILE_DELETED:
                for (int i=0; i<tracksArray.size(); i++){
                    if (tracksArray.get(i).getId()==appEvent.getId()){
                        tracksArray.remove(i);
                        tracksAdapter.notifyItemRemoved(i);
                        tracksAdapter.notifyItemRangeChanged(i, tracksArray.size());
                        break;
                    }
                }
                break;
        }
    }


    @Subscribe
    public void onPlayerWidgetVisibleEvent(UIEvent.PlayerWidgetVisible event){
        _rvTracks.setPadding(0, _rvTracks.getPaddingTop(), 0, PrefManager.getBottomListPadding());
        _rvTracks.requestLayout();
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        EventBus.getDefault().unregister(this);
        presenter = null;
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onItemOptionClickListener(SongModel songField) {
        BottomSheetDialogSongFragment bottomSheetDialogFragment = new BottomSheetDialogSongFragment();
        Bundle args = new Bundle();
        args.putSerializable("adapter_song", (Serializable) songField);
        bottomSheetDialogFragment.setArguments(args);

//        ((MyActivity) getActivity()).showMenu(bottomSheetDialogFragment);
//        bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void onSongItemClick(int pos) {
        EventBus.getDefault().post(new SongListEvent(SongListEvent.SongListEvents.BIND_LIST, pos, tracksAdapter.getObjects()));
    }


    @Override
    public void onScanFinished() {
        switch (getArguments().getInt(TYPES_FRAGMENT_SONGS_VIEW)){
            case TYPE_ALL_SONGS:
                presenter.getSongs();
                break;
            case TYPE_FAV_SONGS:
                presenter.getFavSongs();
                break;
            case TYPE_ALBUM_SONGS:
                presenter.getSongsByAlbumID(getArguments().getLong("album_id"));
                break;
        }
    }

    @Override
    public void showSongs(List<SongModel> songs) {
        tracksAdapter = new SongAdapter(adapterFlag);
        tracksAdapter.setAlbumTop((AlbumModel) getArguments().getSerializable("album_model"));
        tracksAdapter.setOnItemClickListener(this);
        tracksAdapter.changeTrackList(songs);
        tracksAdapter.setHasStableIds(true);
        tracksAdapter.setOnShuffleClickListener(this);
        _rvTracks.setAdapter(tracksAdapter);
    }

    @Override
    public void showNothingFound() {
        _loader.setVisibility(View.GONE);
        _rvTracks.setVisibility(View.GONE);
        _nothingFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void onShuffleClickListener() {
        ArrayList<SongModel> shuffleList = new ArrayList<SongModel>();
        shuffleList.addAll(tracksAdapter.getObjects());
        long seed = System.nanoTime();
        Collections.shuffle(shuffleList, new Random(seed));
        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_SET_SONG, 0, shuffleList));
    }

    @Override
    public void onAlbumPlayClick() {
        EventBus.getDefault().post(new SongListEvent(SongListEvent.SongListEvents.BIND_LIST, 0, tracksAdapter.getObjects()));
    }

    @Override
    public void onSongItemLongClick(int pos) {
        BottomSheetDialogSongFragment bottomSheetFragment = new BottomSheetDialogSongFragment();
        bottomSheetFragment.bindSongModel(tracksAdapter.getObjects().get(pos));
        getMyActivity().showDialog(bottomSheetFragment);
    }

    @Override
    public void onRVLoaded() {
        _loader.setVisibility(View.GONE);
    }
}
