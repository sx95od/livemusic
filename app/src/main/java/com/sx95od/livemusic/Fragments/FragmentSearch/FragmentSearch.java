package com.sx95od.livemusic.Fragments.FragmentSearch;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.sx95od.livemusic.API.LastFM.Models.AlbumSearch.AlbumSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.ArtistSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.SearchAlbumModel;
import com.sx95od.livemusic.API.LastFM.Models.SearchArtistModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.Track;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.TrackSearchModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.Acts.MyActivity;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.AdapterModels.ArtistModel;
import com.sx95od.livemusic.Adapters.AdapterModels.GenreModel;
import com.sx95od.livemusic.Adapters.Adapters.RecentSearchesAdapter;
import com.sx95od.livemusic.Adapters.Adapters.SearchAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Analytics.MyGoogleAnalytics;
import com.sx95od.livemusic.Core.AppPreferences;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.RecentListEvent;
import com.sx95od.livemusic.Fragments.BaseFragment;
import com.sx95od.livemusic.Fragments.FragmentSearch.Presenter.Presenter;
import com.sx95od.livemusic.Fragments.FragmentSearch.View.IView;
import com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.FragmentSearchMoreResults;
import com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.FragmentYouTubeShowVideo;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.Utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by stoly on 25.05.2017.
 */

public class FragmentSearch extends BaseFragment implements View.OnFocusChangeListener,
        RecentSearchesAdapter.OnItemClickListener, View.OnTouchListener, SearchAdapter.OnItemClickListener,
        IView, Listeners.OnYouTubeVideoItemClickListener, Listeners.OnSearchAdapterListener {
    @BindView(R2.id.input_search) EditText inputSearch;
    @BindView(R2.id.results) RecyclerView resultsRecyclerView;
    @BindView(R2.id.recent_recyclerview) RecyclerView recentRecyclerView;
    @BindView(R2.id.loading) ProgressBar loading;
    @BindView(R2.id.clear) ImageButton clear;
    @BindView(R2.id.action_bar_back) FrameLayout actionBarBack;
    @BindView(R2.id.action_bar_fore) FrameLayout actionBarFore;
    @BindView(R2.id.under_menu) FlexboxLayout underMenu;
    @BindView(R2.id.hidden_status_bar) View hiddenStatusBar;
    @BindView(R2.id.root) FrameLayout root;
    @BindView(R2.id.results_frame) FrameLayout resultsFrame;
    @BindView(R2.id.action_bar_frame) FlexboxLayout actionbarFrame;
    @BindView(R2.id.top_flex) FlexboxLayout topFlex;
    @BindView(R2.id.root_flex) FlexboxLayout rootFlex;
    @BindView(R2.id.recent_flex) FlexboxLayout recentFlex;
    /////////////////////////////////////////////////////////////////////
    @BindView(R2.id.under_menu_recent_visible_add) ImageButton underMenuRecentVisibleAdd;
    @BindView(R2.id.under_menu_recent_visible_remove) ImageButton underMenuRecentVisibleRemove;
    @BindView(R2.id.under_menu_recent_visible_textview) TextView underMenuRecentVisibleTextView;
    @BindView(R2.id.under_menu_results_visible_add) ImageButton underMenuResultsVisibleAdd;
    @BindView(R2.id.under_menu_results_visible_remove) ImageButton underMenuResultsVisibleRemove;
    @BindView(R2.id.under_menu_results_visible_textview) TextView underMenuResultsVisibleTextView;

    private int[] resultCount;

    ProgressDialog progressDialog;

    private final String tag = "FragmentSearch";
    int height = 0;
    int topFlexHeight = 0;
    boolean showedHiddenMenu = false;

    SearchAdapter searchAdapter;
    RecentSearchesAdapter recentSearchesAdapter;

    List<SongModel> songObjects;
    List<AlbumModel> albumObjects;
    TrackSearchModel lastFmTrackModel;
    ArtistSearchModel lastFmArtistSearchModel;
    AlbumSearchModel lastFmAlbumSearchModel;
    SearchByKeywordModel youTubeVideoModel;

    List<String> recentSearchesObjects = new ArrayList<String>();


    int statusBarHeight = 0;
    float startPointTouch = 0;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManagerRecent;
    Unbinder unbinder;

    Presenter presenter;

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        Log.d(tag, "<FragmentVisible> "+!menuVisible);
        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.SET_VIEWPAGER_INTERCEPT_TOUCH, menuVisible));
//        if (presenter != null) {
//            if (!menuVisible) {
//                presenter.unnindView();
//            }
//        } else{
////            if (menuVisible) {
////                presenter.bindView(this);
////            }
//        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        unbinder = ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        presenter = new Presenter(this);
        presenter.onCreate();

        Core.getCoreInstance().getTracker().setScreenName("Fragment <Search>");
        Core.getCoreInstance().getTracker().send(new HitBuilders.ScreenViewBuilder().build());

        resultCount = new int[4];
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        layoutManagerRecent = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        resultsRecyclerView.setLayoutManager(layoutManager);
        recentRecyclerView.setLayoutManager(layoutManagerRecent);

        searchAdapter = new SearchAdapter(this,this, this,
                getQuery(), resultCount);

        resultsRecyclerView.setAdapter(searchAdapter);

        inputSearch.setOnKeyListener((View view2, int i, KeyEvent e) -> {
            Log.d(tag, e.getAction()+"/"+i+"/"+view2.getId());
            if (view2.getId() == R.id.input_search && e.getAction() == KeyEvent.ACTION_DOWN
                    && i == KeyEvent.KEYCODE_ENTER && inputSearch.getText().toString().length()>0) {
                animateHideResults(true);
                Log.d(tag, "Enter");
                return true;
            } else return false;
        });


        underMenuResultsVisibleTextView.setText(String.valueOf(Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS,
                AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS_DEFAULT)));
        underMenuRecentVisibleTextView.setText(String.valueOf(Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS,
                AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS_DEFAULT)));

        if (Core.getPref().getBoolean(AppPreferences.HIDDEN_PREFERENCE, false)) {
            root.setOnTouchListener(this);
        }

        hiddenStatusBar.getLayoutParams().height = statusBarHeight;
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) actionBarFore.getLayoutParams();
        layoutParams.setMargins(0, -statusBarHeight, 0, 0);
        actionBarFore.requestLayout();

        root.setOnFocusChangeListener(this);

        resultsFrame.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getView() != null) {
                    resultsFrame.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    height = resultsFrame.getHeight(); //height is ready
                    Log.d("FragmentSearch", "<getViewTreeObserver> height of recyclerview is " + height);
//                    resultsRecyclerView.setTranslationY(height);
                }
            }
        });

        topFlex.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getView() != null) {
                    topFlex.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    topFlexHeight = topFlex.getHeight();
                }
            }
        });

        resultsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d("FragmentSearch", "<RecyclerView ScrollListener> state"+newState);
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING){
                    Utils.hideKeyboard(getActivity(), FragmentSearch.this.getView());
                }
            }
        });
        inputSearch.setOnFocusChangeListener(this);

        return view;
    }


    public void animateHideResults(final boolean afterSearch){
        slideView(recentFlex, true, true);
        slideView(resultsRecyclerView, true, false).addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (resultsRecyclerView != null)
                    resultsRecyclerView.setAdapter(null);
                    searchAdapter = new SearchAdapter(FragmentSearch.this,
                            FragmentSearch.this, FragmentSearch.this, getQuery(), resultCount);
                    resultsRecyclerView.setAdapter(searchAdapter);
                    Log.d(tag, resultsRecyclerView.getAdapter().getItemCount()+" SUKA");
                    if (afterSearch){
                        presenter.onSearchEnter();
                    }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    public void animateShowResults(){
        slideView(recentFlex, false, false);
        slideView(resultsRecyclerView, false, true);
    }

    private void failed(final String message){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean clear(){
        if (recentFlex.getVisibility() == View.GONE) {
            animateHideResults(false);
        }
        if (inputSearch.getText().length()>0){
            inputSearch.setText("");
            return true;
        } else {
            Utils.hideKeyboard(getActivity(), getView());
            inputSearch.clearFocus();
            root.requestFocus();
            animateExpandActionBar(true);
            return false;
        }
    }

    @OnClick({R2.id.clear, R2.id.root, R2.id.under_menu_recent_visible_add,
            R2.id.under_menu_recent_visible_remove, R2.id.under_menu_results_visible_add,
            R2.id.under_menu_results_visible_remove}) void onClick(View view){
        switch (view.getId()){
            case R.id.root:
                clear();
                Log.d("FragmentSearch", "<OnClick> root clicked");
                break;
            case R.id.clear:
                clear();
                Log.d("FragmentSearch", "<OnClick> clear button clicked");
                break;
            case R.id.under_menu_recent_visible_add:
                int visibleItems = Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS,
                        AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS_DEFAULT)+1;
                SharedPreferences.Editor editor = Core.getPref().edit();
                editor.putInt(AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS, visibleItems);
                editor.apply();
                underMenuRecentVisibleTextView.setText(String.valueOf(visibleItems));
                recentSearchesAdapter.notifyDataSetChanged();
                editor = null;
                break;
            case R.id.under_menu_recent_visible_remove:
                int visibleItems2 = Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS,
                        AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS_DEFAULT)-1;
                if (visibleItems2>=0) {
                    SharedPreferences.Editor editor2 = Core.getPref().edit();
                    editor2.putInt(AppPreferences.FRAGMENT_SEARCH_RECENT_VISIBLE_ITEMS, visibleItems2);
                    editor2.apply();
                    underMenuRecentVisibleTextView.setText(String.valueOf(visibleItems2));
                    recentSearchesAdapter.notifyDataSetChanged();
                    editor2 = null;
                }
                break;
            case R.id.under_menu_results_visible_add:
                int visibleItems3 = Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS,
                        AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS_DEFAULT)+1;
                SharedPreferences.Editor editor3 = Core.getPref().edit();
                editor3.putInt(AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS, visibleItems3);
                editor3.apply();
                underMenuResultsVisibleTextView.setText(String.valueOf(visibleItems3));
                if (searchAdapter != null) searchAdapter.notifyDataSetChanged();
                editor3 = null;
                break;
            case R.id.under_menu_results_visible_remove:
                int visibleItems4 = Core.getPref().getInt(AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS,
                        AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS_DEFAULT)-1;
                if (visibleItems4>=0) {
                    SharedPreferences.Editor editor4 = Core.getPref().edit();
                    editor4.putInt(AppPreferences.FRAGMENT_SEARCH_RESULT_LIST_VISIBLE_ITEMS, visibleItems4);
                    editor4.apply();
                    underMenuResultsVisibleTextView.setText(String.valueOf(visibleItems4));
                    if (searchAdapter != null) searchAdapter.notifyDataSetChanged();
                    editor4 = null;
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        EventBus.clearCaches();
        presenter.onDestroy();
        inputSearch.setText(null);
        resultsRecyclerView.setAdapter(null);
        recentRecyclerView.setAdapter(null);
        resultCount = null;
        unbinder.unbind();
        layoutManager = null;
        layoutManagerRecent = null;
        lastFmArtistSearchModel = null;
        lastFmTrackModel = null;
        lastFmAlbumSearchModel = null;
        searchAdapter = null;
        recentSearchesAdapter = null;
        presenter = null;
        unbinder = null;
        recentSearchesObjects = null;
        songObjects = null;
        super.onDestroyView();
    }

    @Subscribe
    public void recentEvent(final RecentListEvent event){
        switch (event.getCode()){
            case RecentListEvent.REMOVE_ITEM:
                getActivity().runOnUiThread(() -> {
                    Log.d("SUKA", ""+event.getPos()*2+"/"+recentSearchesAdapter.getItemCount());
                    if (event.getPos()*2 != recentSearchesAdapter.getItemCount()-1) {
                        recentSearchesObjects.remove(event.getPos());
                        recentSearchesAdapter.notifyItemRemoved(event.getPos() * 2);
                        recentSearchesAdapter.notifyItemRemoved(event.getPos() * 2);
                        recentSearchesAdapter.notifyItemRangeChanged(event.getPos() * 2, recentSearchesAdapter.getItemCount());
                    } else{
                        recentSearchesObjects.remove(event.getPos());
                        recentSearchesAdapter.notifyItemRemoved(event.getPos()*2-1);
                        recentSearchesAdapter.notifyItemRemoved(event.getPos()*2-1);
                    }
                });

                break;
        }
    }



    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()){
            case R.id.root:
                Log.d("FragmentSearch", "<OnFocusChange> root change focus "+hasFocus);
                if (hasFocus && inputSearch.getText().length()==0)
                    clear();
                break;
            case R.id.results:
                Log.d("FragmentSearch", "<OnFocusChange> resultsRecyclerview change focus "+hasFocus);
                break;
            case R.id.input_search:
                Log.d("FragmentSearch", "<OnFocusChange> inputSearch change focus "+hasFocus);
                if (hasFocus){
                    animateExpandActionBar(false);
                    slideHiddenMenu(rootFlex, false);
                } else{
                    Utils.hideKeyboard(getActivity(), FragmentSearch.this.getView());
                }
                break;

        }
    }

    private void animateExpandActionBar(boolean reverse){
        if (reverse){
            resultsFrame.getLayoutParams().height = height;
            clear.animate().scaleX(0).scaleY(0).setDuration(150).withEndAction(() -> clear.setVisibility(View.GONE)).start();
            topFlex.animate().translationY(0).alpha(1)
                    .setDuration(500)
                    .start();
            actionBarFore.animate().translationY(0)
                    .setDuration(500)
                    .start();
            actionBarBack.animate().alpha(0)
                    .setDuration(500)
                    .withStartAction(() -> {
                        actionBarFore.setElevation(0);
                        actionBarFore.setBackground(null);
                    })
                    .start();
            resultsFrame.animate().translationY(0)
                    .setDuration(500)
                    .start();
        } else{
//            resultsFrame.getLayoutParams().height = height+topFlexHeight-statusBarHeight;
            resultsFrame.getLayoutParams().height = height+topFlexHeight-statusBarHeight;

            clear.animate().scaleX(1).scaleY(1).setDuration(150).withStartAction(() -> clear.setVisibility(View.VISIBLE)).start();
            topFlex.animate().translationY(-topFlexHeight*2).alpha(0)
                    .setDuration(500)
                    .start();
            actionBarFore.animate().translationY(-topFlexHeight+statusBarHeight)
                    .setDuration(500)
                    .start();
            actionBarBack.animate().alpha(1)
                    .setDuration(500)
                    .withEndAction(() -> {
                        if (getView() != null) {
                            actionBarFore.setElevation((int) getResources().getDimension(R.dimen.action_bar_elevation));
                            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rootFlex.getLayoutParams();
                            layoutParams.setMargins(0, 0, 0, -topFlexHeight);
                            actionBarFore.setBackgroundResource(R.color.white);
                            rootFlex.requestLayout();
                        }
                    })
                    .start();
            resultsFrame.animate().translationY(-topFlexHeight+statusBarHeight)
                    .setDuration(500)
                    .start();
        }
    }

    private AnimatorSet slideView(final View view, boolean up, final boolean show){
        int slideLite = (int) getResources().getDimension(R.dimen.slide_view_lite);
        int alpha = 0;
        if (show) alpha = 1;
        if (up) slideLite = -slideLite;

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "translationY", show ? 0 : slideLite);
        ObjectAnimator objectAnimatorAlpha = ObjectAnimator.ofFloat(view, "alpha", alpha);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setAlpha(show ? 0 : 1);
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!show) {
                    view.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.play(objectAnimator).with(objectAnimatorAlpha);
        animatorSet.setDuration(320);
        animatorSet.start();
        return animatorSet;
    }

    @Override
    public void OnItemClickListener(String query) {
        Core.getCoreInstance().getTracker().send(new HitBuilders.EventBuilder()
                .setCategory(MyGoogleAnalytics.CATEGORY_SEARCH)
                .setAction(query)
                .build());
        slideHiddenMenu(rootFlex, false);
        inputSearch.requestFocus();
        inputSearch.setText(query);
        animateHideResults(true);
//        presenter.onSearchEnter();
    }

    private void slideHiddenMenu(View view, boolean show){
        if (show){
            view.animate().translationY((float) getResources().getDimension(R.dimen.search_fragment_hidden_menu_height))
                    .setDuration(350)
                    .withEndAction(() -> {

                    }).start();
        } else{
            view.animate().translationY(0)
                    .setDuration(350)
                    .withEndAction(() -> {
                        if (getView() != null) underMenu.setVisibility(View.GONE);
                    }).start();
        }


    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
               startPointTouch = event.getY();
                underMenu.setVisibility(View.VISIBLE);
                if (rootFlex.getTranslationY() == 0){
                    showedHiddenMenu = false;
                } else{
                    showedHiddenMenu = true;
                }
                return false;
            case MotionEvent.ACTION_MOVE:
                if (!showedHiddenMenu) {
                    if (event.getY()>startPointTouch) rootFlex.setTranslationY((event.getY() - startPointTouch) / 4);
                } else{
                    if (event.getY()<startPointTouch) rootFlex.setTranslationY( (float) getResources()
                            .getDimension(R.dimen.search_fragment_hidden_menu_height) - (startPointTouch - event.getY()) / 4);
                }
                return true;
            default:
                if (!showedHiddenMenu){
                    if ((event.getY() - startPointTouch) / 2.75f > (float) getResources().getDimension(R.dimen.slide_view_lite)) {
                        slideHiddenMenu(rootFlex, true);
                    } else {
                        slideHiddenMenu(rootFlex, false);
                    }
                } else {
                    if ((startPointTouch - event.getY()) / 1.25f > (float) getResources().getDimension(R.dimen.slide_view_lite)) {
                        slideHiddenMenu(rootFlex, false);
                    } else {
                        slideHiddenMenu(rootFlex, true);
                    }
                }
              return false;
        }
    }

    @Override
    public void onItemClickLister(final Track object) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(getString(R.string.search));
        progressDialog.setMessage(getString(R.string.searching_track_on_youtube));
        progressDialog.show();
        presenter.onSearchVideosOnYouTube(object);
    }

    @Override
    public void onItemClickLister(SearchArtistModel object) {

    }

    @Override
    public void onItemClickLister(SearchAlbumModel object) {

    }



    @Override
    public void showLastFMTrackList(TrackSearchModel lastFMTrackList) {
                this.lastFmTrackModel = lastFMTrackList;
                    searchAdapter.addTrackLastFM(lastFmTrackModel.getResults().getTrackmatches().getTrack(),
                        Integer.valueOf(lastFmTrackModel.getResults().getOpensearchTotalResults()));
    }

    @Override
    public void showLastFMArtistList(ArtistSearchModel lastFMArtistModel) {
            this.lastFmArtistSearchModel = lastFMArtistModel;
            searchAdapter.addArtistLastFM(lastFmArtistSearchModel.getResults()
                    .getArtistmatches().getArtist(),
                    Integer.valueOf(lastFmArtistSearchModel.getResults().getOpensearchTotalResults()));
    }

    @Override
    public void showLastFMAlbumList(AlbumSearchModel lastFMAlbumModel) {
        this.lastFmAlbumSearchModel = lastFMAlbumModel;
        searchAdapter.addAlbumLastFM(this.lastFmAlbumSearchModel.getResults()
        .getAlbummatches().getAlbum(), Integer.valueOf(this.lastFmAlbumSearchModel.getResults()
        .getOpensearchTotalResults()));
    }

    @Override
    public void showInternalSongList(List<SongModel> songList) {
        this.songObjects = songList;
        searchAdapter.addInternalSongs(this.songObjects);
    }

    @Override
    public void showInternalAlbumList(List<AlbumModel> albumList) {
            this.albumObjects = albumList;
            searchAdapter.addInternalAlbums(this.albumObjects);
    }

    @Override
    public void showInternalArtistList(List<ArtistModel> artistList) {

    }

    @Override
    public void showInternalGenreList(List<GenreModel> genreList) {

    }

    @Override
    public void showYouTubeVideoModel(SearchByKeywordModel youTubeVideoModel) {
        this.youTubeVideoModel = youTubeVideoModel;
        searchAdapter.addYouTubeVideo(this.youTubeVideoModel.getItems(), youTubeVideoModel.getPageInfo().getTotalResults());
    }

    @Override
    public void showSearchHistory(List<String> searchList) {
        if (getView() != null) {
            recentSearchesObjects.clear();
            recentSearchesObjects.addAll(searchList);
            recentRecyclerView.setAdapter(null);
            recentSearchesAdapter = null;
            recentSearchesAdapter = new RecentSearchesAdapter(recentSearchesObjects, FragmentSearch.this);
            recentRecyclerView.setAdapter(recentSearchesAdapter);
        }
    }

    @Override
    public void openFirstYouTubeVideo(Item item, Track object) {
        progressDialog.dismiss();
        progressDialog = null;
        FragmentYouTubeShowVideo youTubeShowVideo = new FragmentYouTubeShowVideo();
        Bundle bundle = new Bundle();
        bundle.putSerializable("youtube_item", item);
        bundle.putString("filename", object.getName()+" - "+object.getArtist());
        bundle.putSerializable("object", object);
        youTubeShowVideo.setArguments(bundle);
        ((MyActivity) getActivity()).addFragment(youTubeShowVideo);
    }

    @Override
    public void youTubeShowError(String errorMessage) {
        progressDialog.dismiss();
        progressDialog = null;
        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean show) {
            animateLoadClear(show);
    }

    private void animateLoadClear(boolean show){
                Object object1 = null;
                Object object2 = null;
                if (!show){
                    object1 = loading;
                    object2 = clear;
                } else{
                    object1 = clear;
                    object2 = loading;
                }

                final Object finalObject = object1;

                ObjectAnimator animX1 = ObjectAnimator.ofFloat(object1, "scaleX", 0f);
                ObjectAnimator animY1 = ObjectAnimator.ofFloat(object1, "scaleY", 0f);
                ObjectAnimator animX2 = ObjectAnimator.ofFloat(object2, "scaleX", 1f);
                ObjectAnimator animY2 = ObjectAnimator.ofFloat(object2, "scaleY", 1f);

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.setDuration(150);
                animatorSet.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        loading.setVisibility(View.VISIBLE);
                        clear.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (getView() != null) {
                            ((View) finalObject).setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                animatorSet.play(animX1).with(animX2).with(animY1).with(animY2);
                animatorSet.start();
                if (getView() != null) {
                    if (inputSearch.getText().toString().length() == 0) {
//                        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rootFlex.getLayoutParams();
//                        layoutParams.setMargins(0, 0, 0, 0);
//                        rootFlex.requestLayout();

                        slideView(recentFlex, true, true);
                        AnimatorSet animatorSet1 = slideView(resultsRecyclerView, true, false);
                        animatorSet1.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                if (resultsRecyclerView != null)
                                    resultsRecyclerView.setAdapter(null);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });

                    }
                }
    }

    @Override
    public String getQuery() {
        return inputSearch.getText().toString();
    }

    @Override
    public void showError(String errorMessage) {
            Toast.makeText(getContext(), getString(R.string.connection_failed), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVideoClickListener(Item item) {
        FragmentYouTubeShowVideo youTubeShowVideo = new FragmentYouTubeShowVideo();
        Bundle bundle = new Bundle();
        bundle.putSerializable("youtube_item", item);
        bundle.putString("filename", item.getSnippet().getTitle());
//        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_YOUTUBE_SHOW_VIDEO, bundle));

        youTubeShowVideo.setArguments(bundle);
        ((MyActivity) getActivity()).addFragment(youTubeShowVideo);
    }

    @Override
    public void onYouTubeSeeAllClick(ArrayMap<String, Object> paramsMap) {
        FragmentSearchMoreResults fragment = new FragmentSearchMoreResults();

        ArrayMap<String, Object> params = paramsMap;

        Bundle bundleFragmentMoreResults = new Bundle();
        bundleFragmentMoreResults.putInt(FragmentSearchMoreResults.TYPE_RESULTS, (int) params.get(FragmentSearchMoreResults.TYPE_RESULTS));
        bundleFragmentMoreResults.putString(FragmentSearchMoreResults.QUERY, (String) params.get(FragmentSearchMoreResults.QUERY));
        fragment.setArguments(bundleFragmentMoreResults);

        ((MyActivity) getActivity()).addFragment(fragment);

//        getSupportFragmentManager()
//                .beginTransaction()
//                .add(R.id.fragments_container, fragment)
//                .addToBackStack(null)
//                .commit();
    }


}
