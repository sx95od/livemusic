package com.sx95od.livemusic.Fragments.FragmentSearch.Model;


import com.sx95od.livemusic.API.LastFM.Models.AlbumSearch.AlbumSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.ArtistSearch.ArtistSearchModel;
import com.sx95od.livemusic.API.LastFM.Models.TrackSearch.TrackSearchModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;
import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by stoly on 04.09.2017.
 */

public interface IModel {
    Observable<TrackSearchModel> getLastFMTrackList(String trackName, String artist,
                                                    String apiKey, int limit,
                                                    int page, String format);

    Observable<AlbumSearchModel> getLastFMAlbumList(String artist, String apiKey,
                                                    int limit, int page,
                                                    String format);

    Observable<ArtistSearchModel> getLastFMArtistList(String artist, String apiKey,
                                                     int limit, int page,
                                                     String format);

    Observable<SearchByKeywordModel> getSearchByKeyWordYoutube(String q, int maxResults, String type, String categoryID,
                                                               String part, String apiKey);

    Observable<List<SongModel>> getInternalSongList(String song);

    Observable<List<AlbumModel>> getInternalAlbumList(String album);

    Observable<List<String>> getSearchHistory();

    Observable<String> addQueryToDB(String query);

}
