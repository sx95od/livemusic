package com.sx95od.livemusic.Fragments.FragmentAlbumsView.View;

import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;

import java.util.List;

/**
 * Created by stoly on 08.09.2017.
 */

public interface IView {
    void showAlbumList(List<AlbumModel> albumList);
    void showNothingFound();
    void showError(String message);
}
