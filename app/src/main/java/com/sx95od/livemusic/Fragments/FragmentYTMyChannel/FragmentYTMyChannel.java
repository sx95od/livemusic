package com.sx95od.livemusic.Fragments.FragmentYTMyChannel;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sx95od.livemusic.API.YouTubeApi.Models.MyChannel.MyChannelModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Fragments.FragmentYTMyChannel.Presenter.Presenter;
import com.sx95od.livemusic.Fragments.FragmentYTMyChannel.View.IView;
import com.sx95od.livemusic.Fragments.MyFragment;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by stoly on 02.11.2017.
 */

public class FragmentYTMyChannel extends MyFragment implements IView{
    private final String tag = "YTMyChannel";

    Unbinder unbinder;

    @BindView(R2.id.iv_ava)
    ImageView _ava;

    @BindView(R2.id.iv_back)
    ImageView _back;

    @BindView(R2.id.tv_channel_name)
    TextView _channelName;

    Presenter presenter;

    MyChannelModel myChannelModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_yt_my_channel, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new Presenter(this);

        myChannelModel = (MyChannelModel) getArguments().getSerializable("yt_my_channel");

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onCreate();

        _channelName.setText(myChannelModel.getItems().get(0).getSnippet().getTitle());
        Core.withGlide().load(myChannelModel.getItems().get(0).getBrandingSettings().getImage().getBannerMobileHdImageUrl())
                .placeholder(R.drawable.ic_user).into(_back);
        Core.withGlide().load(myChannelModel.getItems().get(0).getSnippet().getThumbnails().getHigh().getUrl())
                .placeholder(R.color.red)
                .override(256)
                .into(_ava);
    }
}
