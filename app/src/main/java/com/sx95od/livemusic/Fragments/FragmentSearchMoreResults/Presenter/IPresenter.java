package com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.Presenter;

/**
 * Created by stoly on 23.09.2017.
 */

public interface IPresenter {
    void loadYouTubeVideo(String query, String pageToken);
    void loadYouTubeVideo(String query);
}
