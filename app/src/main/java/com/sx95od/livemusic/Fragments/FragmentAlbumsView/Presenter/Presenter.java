package com.sx95od.livemusic.Fragments.FragmentAlbumsView.Presenter;

import android.transition.Scene;

import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.Model.IModel;
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.Model.Model;
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.View.IView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by stoly on 08.09.2017.
 */

public class Presenter implements IPresenter {
    IModel model = new Model();
    IView iView;
    CompositeDisposable disposables = new CompositeDisposable();

    public Presenter(IView iView) {
        this.iView = iView;
    }

    @Override
    public void onCreate() {
        disposables.add(model.getAlbumList()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableObserver<List<AlbumModel>>() {
            @Override
            public void onNext(List<AlbumModel> albumModels) {
                if (albumModels.size() > 0){
                    iView.showAlbumList(albumModels);
                } else{
                    iView.showNothingFound();
                }
            }

            @Override
            public void onError(Throwable e) {
                iView.showError(e.toString());
            }

            @Override
            public void onComplete() {

            }
        }));
    }

    @Override
    public void onDestroy() {
        if (!disposables.isDisposed()){
            disposables.dispose();
        }
    }
}
