package com.sx95od.livemusic.Fragments.FragmentAlbumsView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sx95od.livemusic.Adapters.AdapterModels.AlbumModel;
import com.sx95od.livemusic.Adapters.Adapters.AlbumAdapter;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Events.UIEvent;
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.Presenter.Presenter;
import com.sx95od.livemusic.Fragments.FragmentAlbumsView.View.IView;
import com.sx95od.livemusic.Fragments.FragmentSongsView.FragmentSongsView;
import com.sx95od.livemusic.Fragments.MyFragment;
import com.sx95od.livemusic.Fragments.MyFragmentWithoutAnim;
import com.sx95od.livemusic.PrefManager;
import com.sx95od.livemusic.R;
import com.sx95od.livemusic.R2;
import com.sx95od.livemusic.ThemeViews.RecyclerViewLoaderListener;
import com.sx95od.livemusic.Utils.SystemUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by stoly on 08.09.2017.
 */

public class FragmentAlbumsView extends MyFragment implements IView, AlbumAdapter.OnAlbumItemClickListener,
        RecyclerViewLoaderListener{
    private final String tag = FragmentAlbumsView.class.getSimpleName();

    @BindView(R2.id.recyclerview_albums)
    RecyclerView albumsRV;

    @BindView(R2.id.tv_nothing_found)
    TextView _nothingFound;

    @BindView(R2.id.pb_loader)
    ProgressBar _loader;

    RecyclerView.LayoutManager layoutManager;
    AlbumAdapter albumAdapter;
    Unbinder unbinder;
    Presenter presenter;

    boolean RVReady = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albums_view, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new Presenter(this);

        tuneUI();
        return view;
    }

    private void tuneUI(){
        layoutManager = new LinearLayoutManager(getActivity()){
            @Override
            public void onLayoutCompleted(RecyclerView.State state) {
                super.onLayoutCompleted(state);
                if (!RVReady){
                    onRVLoaded();
                    RVReady = true;
                }
            }
        };
        albumsRV.setLayoutManager(layoutManager);
        albumsRV.setSaveEnabled(false);
        albumsRV.setPadding(0, 0, 0, PrefManager.getBottomNavigationBarHeight());
    }

    @Override
    public void onViewCreated(@NotNull View view, @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        setActionBarTitle(getString(R.string.albums));
        EventBus.getDefault().register(this);
        super.onViewCreated(view, savedInstanceState);
    }


    @Subscribe
    public void onPlayerWidgetVisibleEvent(UIEvent.PlayerWidgetVisible event){
        albumsRV.setPadding(0, albumsRV.getPaddingTop(), 0, PrefManager.getBottomListPadding());
        albumsRV.requestLayout();
    }


    @Override
    public void onFragmentCreated() {
        super.onFragmentCreated();
        presenter.onCreate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R2.id.back) void onBackClick(){
        backStack();
    }

    @Override
    public void showAlbumList(List<AlbumModel> albumList) {
        albumAdapter = new AlbumAdapter(albumList, this);
        albumsRV.setAdapter(albumAdapter);
    }

    @Override
    public void showNothingFound() {
        _loader.setVisibility(View.GONE);
        albumsRV.setVisibility(View.GONE);
        _nothingFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClickListener(AlbumModel object) {
        FragmentSongsView songsViewFragment = new FragmentSongsView();
        Bundle bundle = new Bundle();
        bundle.putLong("album_id", object.get_id());
        bundle.putString("album_title", object.getAlbum());
        bundle.putSerializable("album_model", object);
        bundle.putInt(FragmentSongsView.TYPES_FRAGMENT_SONGS_VIEW, FragmentSongsView.TYPE_ALBUM_SONGS);
        songsViewFragment.setArguments(bundle);
        addFragment(songsViewFragment);
    }

    @Override
    public void onRVLoaded() {
        _loader.setVisibility(View.GONE);
    }
}
