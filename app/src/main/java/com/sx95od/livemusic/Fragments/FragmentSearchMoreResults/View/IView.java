package com.sx95od.livemusic.Fragments.FragmentSearchMoreResults.View;

import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.Item;
import com.sx95od.livemusic.API.YouTubeApi.Models.SearchByKeyword.SearchByKeywordModel;

import java.util.List;

/**
 * Created by stoly on 23.09.2017.
 */

public interface IView {
    void loadVideos(SearchByKeywordModel response);
}
