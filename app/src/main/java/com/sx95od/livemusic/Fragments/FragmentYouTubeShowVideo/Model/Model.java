package com.sx95od.livemusic.Fragments.FragmentYouTubeShowVideo.Model;

import com.sx95od.livemusic.API.YouTubeApi.Models.Channel.ChannelModel;
import com.sx95od.livemusic.API.YouTubeApi.Models.Video.YouTubeVideoModel;
import com.sx95od.livemusic.API.YouTubeApi.YouTubeApi;
import com.sx95od.livemusic.BuildConfig;
import com.sx95od.livemusic.Core.Core;

import io.reactivex.Observable;

/**
 * Created by stoly on 24.09.2017.
 */

public class Model implements IModel{
    YouTubeApi youTubeApi = Core.getYoutubeApi();

    @Override
    public Observable<YouTubeVideoModel> getVideoInfo(String id, String part, String apiKey) {
        return youTubeApi.getVideoByID(id, part, apiKey);
    }

    @Override
    public Observable<ChannelModel> getChannelInfo(String apiKey, String id, String part) {
        return youTubeApi.getChannelByID(BuildConfig.YOUTUBE_API_KEY, id, YouTubeApi.STATISTICS+"," +
                YouTubeApi.CONTENT_DETAILS+","+YouTubeApi.TOPIC_DETAILS+","+YouTubeApi.SNIPPET+
                ","+YouTubeApi.BRANDING_SETTINGS);
    }
}
