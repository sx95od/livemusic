package com.sx95od.livemusic.Fragments.FragmentFav.Presenter;

import com.sx95od.livemusic.API.LastFM.Models.GeoTopTracks.Track;

import java.util.List;

/**
 * Created by stoly on 01.10.2017.
 */

public interface IPresenter {
    void getLastFMGeoTopTracks();
    void getLastFMTopTracks();

    void onSearchVideosOnYouTube(String track);

    void onDestroy();
    void onCreate();

    void loadMyYTData();
    void loadYTChartVideos();

    void cacheLastFMTopTracks(List<com.sx95od.livemusic.API.LastFM.Models.TopTracks.Track> tracks);
    void cacheLastFMGeoTopTracks(List<Track> tracks);
}
