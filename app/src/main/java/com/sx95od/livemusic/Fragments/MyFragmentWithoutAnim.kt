package com.sx95od.livemusic.Fragments

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import com.sx95od.livemusic.Acts.MyActivity
import com.sx95od.livemusic.AnimationConstants
import com.sx95od.livemusic.Core.Core
import com.sx95od.livemusic.R
import com.sx95od.livemusic.ThemeViews.STextView
import kotlinx.android.synthetic.main.action_bar_regular.*

/**
 * Created by stoly on 06.05.2018.
 */
open class MyFragmentWithoutAnim : Fragment() {
    private var alreadyDestroy = false

    fun onFragmentResume() {
        view!!.visibility = View.VISIBLE
    }


    fun setActionBarTitle(title: String?) {
        view?.findViewById<STextView>(R.id.tv_title)?.text = title
    }

    fun setTransparentActionBar() {
        action_bar!!.setBackgroundResource(R.color.transparent)
//        action_bar!!.elevation = 0f
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MyActivity).requestBottomPadding()
        onFragmentCreated()
    }

    open fun onFragmentCreated() {
        (activity as MyActivity).onNewFragment()
    }

    open fun backStack() {
            alreadyDestroy = true
        (activity as MyActivity).fragmentBackStack()
    }

    fun resetFragment() {

    }

    fun addFragment(myFragment: Fragment) {
        //        getFragmentManager().beginTransaction().add(R.id.fragments_container, myFragment)
        //                .addToBackStack(null).commit();
        (activity as MyActivity).addFragment(myFragment)
    }

    fun getMyActivity(): MyActivity {
        return activity as MyActivity
    }


}