package com.sx95od.livemusic.Fragments.FragmentSongsView.Model;

import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by stoly on 22.10.2017.
 */

public interface IModel {
    Observable<List<SongModel>> getSongsByAlbumID(long albumID);
    Observable<List<SongModel>> getSongs();
    Observable<List<SongModel>> getFavSongs();
}
