package com.sx95od.livemusic;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.sx95od.livemusic.Activities.MainActivity.MainActivity;
import com.sx95od.livemusic.Acts.GodActivity.GodActivity;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.Events.CoreEvent;
import com.sx95od.livemusic.Listeners.Listeners;
import com.sx95od.livemusic.Utils.SystemUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.fabric.sdk.android.Fabric;

public class BootActivity extends Activity {
    Handler handler;
    boolean pass;

    @BindView(R2.id.logo)
    TextView logo;

    @BindView(R2.id.fl_root)
    FrameLayout _root;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_boot);
        unbinder = ButterKnife.bind(this);

        _root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                SystemUtils.setStatusBarHeight(Core.getHeightScreen() - _root.getMeasuredHeight());

                _root.getViewTreeObserver()
                        .removeOnGlobalLayoutListener(this);
            }
        });

        pass = false;

        Typeface face = Typeface.createFromAsset(getAssets(), "pacifico_regular.ttf");
        logo.setTypeface(face);

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if(result!=PackageManager.PERMISSION_GRANTED & result2!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Core.getCoreInstance().scanLibrary(() -> {
                        Intent intent = new Intent(BootActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    });
                }
            }, 1500);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == 1) if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Core.getCoreInstance().scanLibrary(new Listeners.OnMediaScanCompleted() {
                        @Override
                        public void onScanFinished() {
                            Intent intent = new Intent(BootActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
            }, 1500);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler = null;
        unbinder.unbind();
        unbinder = null;
    }
}
