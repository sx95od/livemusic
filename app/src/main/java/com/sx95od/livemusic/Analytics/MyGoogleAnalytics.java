package com.sx95od.livemusic.Analytics;

/**
 * Created by stoly on 03.10.2017.
 */

public final class MyGoogleAnalytics {
    public static final String CATEGORY_SEARCH = "Search";
    public static final String CATEGORY_CLICK = "Click";

    public static final String FRAGMENT_FAVORITE = "F-Favorite";
    public static final String FRAGMENT_SEARCH = "F-Search";
    public static final String FRAGMENT_SONGS_VIEW = "F-Songs View";
}
