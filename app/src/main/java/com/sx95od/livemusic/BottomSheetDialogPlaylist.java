package com.sx95od.livemusic;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.sx95od.livemusic.Adapters.AdapterModels.PlaylistModel;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Core.Core;
import com.sx95od.livemusic.DataBase.PlaylistSongs;
import com.sx95od.livemusic.DataBase.Playlists;
import com.sx95od.livemusic.Events.FragmentEvent;
import com.sx95od.livemusic.Events.PlayerEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by stoly on 20.07.2017.
 */

public class BottomSheetDialogPlaylist extends BottomSheetDialogFragment implements View.OnClickListener {
    TextView title, songsCount, im, about;
    ImageView cover;
    FlexboxLayout parentPlay, parentDelete;
    LinearLayout openPlaylist;
    TextView favText;
    boolean liked = false;
    CoordinatorLayout.Behavior behavior;
    Thread thread;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.dialog_playlist_more, null);
        dialog.setContentView(contentView);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        dialog.getWindow().setStatusBarColor(getResources().getColor(R.color.transparent));

        parentPlay = (FlexboxLayout) contentView.findViewById(R.id.play_parent);
        parentDelete = (FlexboxLayout) contentView.findViewById(R.id.delete_parent);

        openPlaylist = (LinearLayout) contentView.findViewById(R.id.open_playlist);

        parentDelete.setOnClickListener(this);
        parentPlay.setOnClickListener(this);

        title = (TextView) contentView.findViewById(R.id.title);
        im = (TextView) contentView.findViewById(R.id.im);
        about = (TextView) contentView.findViewById(R.id.about);
        cover = (ImageView) contentView.findViewById(R.id.cover);
        songsCount = (TextView) contentView.findViewById(R.id.songs_count);


            openPlaylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_PLAYLIST_SONGS_VIEW,
                            ((PlaylistModel) getArguments().getSerializable("playlist")).getId()));
                }
            });

        readInfo((PlaylistModel) getArguments().getSerializable("playlist"));

    }


    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                ((BottomSheetBehavior) behavior).setState(BottomSheetBehavior.STATE_EXPANDED);
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };


    private void askDelete(final SongModel song){
        final ContentResolver cr = getActivity().getContentResolver();
        final String where = "_id=?";
        final String[] args = new String[] { String.valueOf(song.getId()) };

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle(getString(R.string.delete_song));
        adb.setMessage(getString(R.string.delete_song_from_device, song.getTitle()+" - "+song.getArtist()));
        adb.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                int result = cr.delete( android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, where, args );
//                File file = new File(adapter_song.getUri());
//                boolean resBool = file.delete();
//                if (result!=-1 && resBool){
//                    Toast.makeText(getActivity(), getString(R.string.song_was_deleted, adapter_song.getTitle()+" - "+adapter_song.getArtist()), Toast.LENGTH_LONG).show();
//                    EventBus.getDefault().post(new AppEvent(AppEvent.FILE_DELETED, adapter_song.getId()));
//                    ((Core) getActivity().getApplicationContext()).deleteSong(adapter_song.getId());
//                    dialog.dismiss();
//                    BottomSheetDialogSongFragment.this.dismiss();
//                } else{
//
//                }

                getActivity().getContentResolver().takePersistableUriPermission(Uri.parse(song.getUri()), (Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION));

//                Log.i("FFF", ""+DocumentFile.fromSingleUri(getContext(), Uri.parse(adapter_song.getUri())).canWrite());
//                if (getActivity().getContentResolver().delete(Uri.parse("content:/"+adapter_song.getUri()), null, null)>0){
//                    Toast.makeText(getActivity(), getString(R.string.song_was_deleted, adapter_song.getTitle()+" - "+adapter_song.getArtist()), Toast.LENGTH_LONG).show();
//                    EventBus.getDefault().post(new AppEvent(AppEvent.FILE_DELETED, adapter_song.getId()));
//                    ((Core) getActivity().getApplicationContext()).deleteSong(adapter_song.getId());
//                    dialog.dismiss();
//                    BottomSheetDialogSongFragment.this.dismiss();
//                }
            }
        }).setNeutralButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = adb.create();
        dialog.show();


    }

    @UiThread
    public void readInfo(final PlaylistModel playlist){
        final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                final RealmResults<PlaylistSongs> result = realm.where(PlaylistSongs.class)
                        .equalTo("playlist.id", playlist.getId())
                        .findAll();
                final int count = result.size();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        songsCount.setText(getResources().getQuantityString(R.plurals.tracks_count, count,
                                count));
                    }
                });
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                realm.close();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                realm.close();
                Log.i(getClass().getSimpleName(), error.toString());
            }
        });

        String im = "";
        for (String s : playlist.getName().replaceAll("(\\w)\\w*","$1").split("\\W+")){
            im += s.toUpperCase();
            if (im.length()>=2) break;
        }
        BottomSheetDialogPlaylist.this.im.setText(im);
        BottomSheetDialogPlaylist.this.title.setText(playlist.getName());
        BottomSheetDialogPlaylist.this.about.setText(playlist.getAbout());
    }


    @WorkerThread
    private Bitmap getArt(final String uri){
        Bitmap img = null;
        MediaMetadataRetriever mData = new MediaMetadataRetriever();
        try {
            mData.setDataSource(uri);
            img = decodeSampledBitmapFromResource(mData.getEmbeddedPicture(), 128, 128);
        } catch (Exception e) {
//            img = BitmapFactory.decodeResource(getResources(), R.drawable.note_mini);
            mData.release();
        } finally {
            mData.release();
        }
        return img;
    }

    @WorkerThread
    private Bitmap decodeSampledBitmapFromResource(byte[] array, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(array, 0, array.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(array, 0, array.length, options);
    }

    @WorkerThread
    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private void deletePlaylist(){
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle(getString(R.string.delete_playlist));
        adb.setMessage(getString(R.string.delete_playlist_from_device));
        adb.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<PlaylistSongs> results = realm.where(PlaylistSongs.class)
                                .equalTo("playlist.id", ((PlaylistModel) getArguments().getSerializable("playlist")).getId())
                                .findAll();

                        results.deleteAllFromRealm();

                        RealmResults<Playlists> results2 = realm.where(Playlists.class)
                                .equalTo("id", ((PlaylistModel) getArguments().getSerializable("playlist")).getId())
                                .findAll();

                        results2.deleteAllFromRealm();
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(getActivity(), getString(R.string.playlist_was_deleted), Toast.LENGTH_LONG).show();
                        BottomSheetDialogPlaylist.this.dismiss();
                        EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_DELETED_PLAYLIST));
                        realm.close();
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        realm.close();
                    }
                });

            }
        }).setNeutralButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = adb.create();
        dialog.show();
    }

    private void playPlaylist(){
        final Realm realm = Realm.getInstance(((Core) getActivity().getApplication()).getConfigRealm());
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            final RealmResults<PlaylistSongs> result = realm.where(PlaylistSongs.class)
                                    .equalTo("playlistId", ((PlaylistModel) getArguments().getSerializable("playlist")).getId())
                                    .findAllSorted("title", Sort.ASCENDING);
                            final ArrayList<SongModel> arrayList = new ArrayList<SongModel>();
                            for (int i = 0; i < result.size(); i++) {
                                arrayList.add(new SongModel(result.get(i).get_id(), result.get(i).getUri(),
                                        null, result.get(i).getTitle(), result.get(i).getArtist(),
                                        result.get(i).getAlbum(), result.get(i).getAlbum_id(), result.get(i).getArtist_id(),
                                        result.get(i).getDuration(), result.get(i).getYear(), result.get(i).getDate_modified(), false));
                            }
                            EventBus.getDefault().post(new PlayerEvent(PlayerEvent.SET_SONG_FROM_LIST, 0, arrayList));
                            BottomSheetDialogPlaylist.this.dismiss();
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            realm.close();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            realm.close();
                            Log.i(getClass().getSimpleName(), error.toString());
                        }
                    });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.delete_parent:
                    deletePlaylist();
                break;
            case R.id.play_parent:
                    playPlaylist();
                EventBus.getDefault().post(new FragmentEvent(FragmentEvent.FRAGMENT_EXPAND_BOTTOM_PLAYER_CONTROL));
                break;
        }
    }
}
