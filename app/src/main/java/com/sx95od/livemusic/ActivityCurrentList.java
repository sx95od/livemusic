package com.sx95od.livemusic;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.sx95od.livemusic.Adapters.Adapters.SongAdapter;
import com.sx95od.livemusic.Adapters.AdapterModels.SongModel;
import com.sx95od.livemusic.Events.PlayerEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

public class ActivityCurrentList extends Activity implements View.OnClickListener{
    SongAdapter songAdapter;
    int lastPos = -1;
    int pos = -1;
    ImageButton close;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
//                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        getWindow().getDecorView().setSystemUiVisibility(0);

        setContentView(R.layout.activity_current_list);

        close = (ImageButton) findViewById(R.id.close);
        close.setOnClickListener(this);



        ImageView back = (ImageView) findViewById(R.id.back);


        RecyclerView list = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setInitialPrefetchItemCount(10);
        list.setLayoutManager(layoutManager);
        songAdapter = new SongAdapter((ArrayList<SongModel>) getIntent().getSerializableExtra("list"),  SongAdapter.FLAG_CURRENT_LIST, null);
        songAdapter.setHasStableIds(true);
        list.setItemViewCacheSize(16);
        list.setAdapter(songAdapter);
        layoutManager.scrollToPositionWithOffset(getIntent().getIntExtra("pos", 0)-2, 0);
        EventBus.getDefault().post(new PlayerEvent(PlayerEvent.REQUEST_POSITION));
    }

    @Subscribe
    public void onPlayerEvent(PlayerEvent playerEvent){
        switch (playerEvent.getCode()){
            case PlayerEvent.LIST_POSITION:
                Log.d(getClass().getSimpleName()+" onPlayerEvent", "pos = "+
                        pos);
                pos = playerEvent.getPosition();
                songAdapter.setCurrPlaying(pos);
                if (lastPos!=-1){
                    songAdapter.notifyItemChanged(lastPos);
                }
                songAdapter.notifyItemChanged(pos);
                lastPos = playerEvent.getPosition();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close:
                finish();
                break;
        }
    }
}
